<?
/*
* Copyright (c) 2013, Andrey_B
* http://ab-log.ru
* Подробнее см. LICENSE.txt или http://www.gnu.org/licenses/
*/

require_once("ab-cms/class/main_class.php");
$ab = new cms_lib();
include("key.php");

$loop = 1;
$pio_state = 0;
//$dir = opendir("/mnt/1wire/bus.0/alarm/");
$dir = opendir("/mnt/1wire/alarm/");
$log = fopen("/var/www/server.log", "a");


$keys_id = $ab->get_id("keys");
$keys_journal = $ab->get_id("keys_journal");

$keys = array ( 
		0 => array (
			'key_address' => '12.7AD769000000',
			'key_label' => 'water_pump',
			'alarm' => 0, 'key_pio' => 0, 'key_id' => 0
		),
		1 => array (
			'key_address' => '12.E2FB69000000',
			'key_label' => 'gsm_arm',
			'alarm' => 0, 'key_pio' => 0, 'key_id' => 0
		),
		2 => array (
			'key_address' => '12.A52D7D000000',
			'key_label' => 'leak',
			'alarm' => 0, 'key_pio' => 0, 'key_id' => 0
		)
	);

# Получаем id ключа в БД
for ( $i = 0; $i < count($keys); $i++ )
$keys[$i]['key_id'] = $ab->mod_read($keys_id, "#tmpID#", "", 1, "key_label='".$keys[$i]['key_label']."'");

$down_time = time();

while ( $loop == 1 )
{
	//usleep(500000);
	//sleep(3);
	sleep(5);
	$sleep_count++;
	# Каждые 5 минут проверяем соединение с БД
	if ( $sleep_count == 600 )
	{
		$sleep_count=0;
		if (!$ab->db_conn->ping())
		echo "MySQL connectio is lost\n";
	}

	for ( $i = 0; $i < count($keys); $i++ )
	$keys[$i]['alarm'] = 0;

	rewinddir($dir);
	while (false !== ($file = readdir($dir)))
	{
		for ( $i = 0; $i < count($keys); $i++ )
		{
			if ( $file == $keys[$i]['key_address'] )
			$keys[$i]['alarm'] = 1;
		}
	}

	// Временно
	if ( file_exists("/var/www/virt_alarm/water_pump.act") )
	$keys[0]['alarm'] = 1;

	for ( $i = 0; $i < count($keys); $i++ )
	{
		# Обработка событий для сигнализации и датчика протечки
		if ( $keys[$i]['key_label'] == "gsm_arm" || $keys[$i]['key_label'] == "leak" )
		{
			if ( $keys[$i]['alarm'] == 1 && $keys[$i]['key_pio'] == 0 )
			{
				$keys[$i]['key_pio'] = 1;
				$log_txt = date('H:i:s')." ".$keys[$i]['key_label']." ON\n";
				fwrite($log, $log_txt);
				$ab->mod_write($keys_id, "key_pio='1'", "key_label='".$keys[$i]['key_label']."'");
				$ab->mod_write($keys_journal, "SYSDATE(), ".$keys[$i]['key_id'].", '1'");

				if ( $keys[$i]['key_label'] == "leak" )
				{
					key_sw("water_valve_op", 0);
					key_sw("water_valve_cl", 1);
					file_get_contents("http://192.168.0.251/alarm.php");
					$ab->mod_write($ab->get_id("alarm", 1), "SYSDATE(), 'Зафиксирована утечка воды!'");
					sleep(10);
					key_sw("water_valve_cl", 0);
				}
			}
			elseif ( $keys[$i]['alarm'] == 0 && $keys[$i]['key_pio'] == 1 )
			{
				$keys[$i]['key_pio'] = 0;
				$temp_time = 0;
				$log_txt = date('H:i:s')." ".$keys[$i]['key_label']." OFF\n";
				fwrite($log, $log_txt);
				// echo $work_time."\n";
				$ab->mod_write($keys_id, "key_pio='0'", "key_label='".$keys[$i]['key_label']."'");
				$ab->mod_write($keys_journal, "SYSDATE(), ".$keys[$i]['key_id'].", '0'");
			}

		}
		# Обработка событий для насоса водоснабжения
		if ( $keys[$i]['key_label'] == "water_pump" )
		{
			# Насос включен
			if ( $keys[$i]['alarm'] == 1 && $keys[$i]['key_pio'] == 0 )
			{
				$keys[$i]['key_pio'] = 1;
				$log_txt = date('H:i:s')." ".$keys[$i]['key_label']." ON\n";
				fwrite($log, $log_txt);
				$ab->mod_write($keys_id, "key_pio='1'", "key_label='".$keys[$i]['key_label']."'");
				$ab->mod_write($keys_journal, "SYSDATE(), ".$keys[$i]['key_id'].", '1'");
			}
			# Насос выключен
			elseif ( $keys[$i]['alarm'] == 0 && $keys[$i]['key_pio'] == 1 )
			{
				$keys[$i]['key_pio'] = 0;
				$temp_time = 0;
				$log_txt = date('H:i:s')." ".$keys[$i]['key_label']." OFF\n";
				fwrite($log, $log_txt);
				// echo $work_time."\n";
				$ab->mod_write($keys_id, "key_pio='0'", "key_label='".$keys[$i]['key_label']."'");
				$ab->mod_write($keys_journal, "SYSDATE(), ".$keys[$i]['key_id'].", '0'");
			}
			# Отслеживаем время работы насоса
			if ( $keys[$i]['alarm'] == 1 && $keys[$i]['key_pio'] == 1 )
			{
				if ( empty($temp_time) )
				$temp_time = time();
				else
				{
					$work_time = $work_time + (time() - $temp_time);
					$temp_time = time();
				}

			}
			# Если совокупная наработка насоса больше 2 минут, включаем аэрацию
			if ( $work_time >= 120 && $wc_flag == 0 )
			{
				$log_txt = date('H:i:s')." water clean ON\n";
				fwrite($log, $log_txt);
				key_sw("water_clean2", 1, 'A');

				$wc_flag = 1;
				$wc_time = time();
				$uptime = time();
			}
			# Выключаем аэрацию
			elseif ( $work_time == 0 && $wc_flag == 1 )
			{
				$log_txt = date('H:i:s')." water clean OFF\n";
				fwrite($log, $log_txt);
				key_sw("water_clean2", 0, 'A');

				$wc_flag = 0;
				$wc_time = 0;
				$down_time = time();
				$uptime = time() - $uptime;
				//$ab->mod_write($ab->get_id("lamps", 1), "lamp_uptime=lamp_uptime+$uptime", "lamp_place='Водоочистка'");
			}
			# Ведем учет времени работы аэрации
			if ( $wc_time > 0 && $work_time > 0 )
			{
				$work_time = $work_time - (time() - $wc_time);
				$wc_time = time();
				if ( $work_time < 0 )
				$work_time = 0;
			}

			# Если аэрация не включалась ... минут, включаем на ... минут.
			# Каждый час
			#if ( time() - $down_time >= 3600 && $wc_flag == 0 && date('H') > 9 && date('H') < 23 )
			if ( time() - $down_time >= 10800 && $wc_flag == 0 && date('H') > 9 && date('H') < 23 )
			{
				$log_txt = date('H:i:s')." down time!\n";
				fwrite($log, $log_txt);
				$work_time = 300;
				$down_time = time();
			}
		}
	}
}

fclose($log);

?>