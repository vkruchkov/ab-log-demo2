<?
/*
* Copyright (c) 2016, Andrey_B
* http://ab-log.ru
* Подробнее см. LICENSE.txt или http://www.gnu.org/licenses/
*/

/*
Скрипт для отображение данных на дисплее OLED с контроллером SSD1306
Использует драйвер SSD1306 и библиотеку I2C-PHP
*/

define("SCL", "10");
define("SDA", "11");
define("MD", "http://192.168.0.14/sec/?");

// Вариант реализации I2C: 1 - полностью программный; 2 - частично аппаратный (прошивка 3.43beta1 и выше)
define("V", "2");

require_once("mod_i2c_ssd1306.php");

$time1 = microtime(TRUE);

ssd1306_init();
ssd1306_clear_display();

//ssd1306_write_text("Температура: 22.60", "verdana_10", 0, 0);
//ssd1306_write_text("Влажность: 70 %", "verdana_10", 0, 0);
//ssd1306_write_text("Температура: 24.62");
//ssd1306_write_text("22.82 ", "verdana_10", 84, 0);

ssd1306_write_text("Влажность: 47%", "verdana_10", 0, 2);
ssd1306_write_text("   ХОРОШЕГО ДНЯ!", "mistral_10", 0, 5);

//ssd1306_draw_pic("ab_log_logo");

$time2 = microtime(TRUE);
$time3 = $time2 - $time1;
echo $time3."\n";

?>