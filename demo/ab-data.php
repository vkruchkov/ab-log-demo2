<?
/*
* Copyright (c) 2013-2017, Andrey_B
* http://ab-log.ru
* Подробнее см. LICENSE.txt или http://www.gnu.org/licenses/
*/

//setlocale(LC_ALL, "ru_RU.cp1251");

require_once("ab-cms/class/main_class.php");
$ab = new cms_lib();

if ( $_REQUEST['p'] == "dev" )
echo $ab->mod_read($ab->get_id($_GET['id']), "#dev_value#", "tmpID DESC", 1);

elseif ( $_REQUEST['p'] == "el_p" )
echo $ab->mod_read($ab->get_id("el_p"), "#el_val0#", "tmpID DESC", 1);

elseif ( $_REQUEST['p'] == "rtop" )
echo strftime('%A, %e %B, %H:%M');

elseif ( $_REQUEST['p'] == "wind_home" )
{
	$wind_dir_current = round($ab->mod_read($ab->get_id("wh_wind_dir"), "#dev_value#", "tmpID DESC", 1));
	if ( $wind_dir_current > 15 )
	$wind_dir_current = 16;

	$wind_dir_text = array("Северный", "Северо-Северо-Восточный", "Северо-Восточный", "Северо-Восточный-Восточный",
				"Восточный", "Юго-Восточный-Восточный", "Юго-Восточный", "Юго-Юго-Восточный",
				"Южный", "Юго-Юго-Западный", "Юго-Западный", "Юго-Западный-Западный",
				"Западный", "Северо-Западный-Западный", "Северо-Западный", "Северо-Северо-Западный", "Нет данных");
	echo $wind_dir_text[$wind_dir_current];
	echo "<br>";
	$wind_ave = round($ab->mod_read($ab->get_id("wh_wind_ave"), "#dev_value#", "tmpID DESC", 1), 1);
	if ( $wind_ave > 0 )
	echo $wind_ave." м/с";
	else
	echo "Штиль";
}

// Максимальные порывы ветра за час
elseif ( $_REQUEST['p'] == "wind_max" )
{
	$wind_gust = $ab->doc_read($ab->get_id("wh_wind_gust"));
	$result = $ab->select_line("SELECT MAX(dev_value) wind_gust FROM tmp_".$wind_gust['ListID']." WHERE ContID=".$wind_gust['ContID']." AND dev_date > NOW() - INTERVAL 1 HOUR");
	$wind_gust_max = round($result['wind_gust'], 1);

	echo "<span style=\"font-size: 0.9em\">Порывы: $wind_gust_max м/с</span>";
}

elseif ( $_REQUEST['p'] == "forecast_home" )
{
	$wind_dir_text_uk = array("N", "NNE", "NE", "ENE", "E", "ESE", "SE", "SSE", "S", "SSW", "SW", "WSW", "W", "WNW", "NW", "NNW", "FALSE");

	// Направление ветра среднее за час
	$wind_dir_p = $ab->doc_read($ab->get_id("wh_wind_dir"));
	$A = 0;
	$B = 0;
	
	$result = $ab->select("SELECT dev_value	FROM tmp_".$wind_dir_p['ListID']." WHERE ContID=".$wind_dir_p['ContID']." AND dev_value<16 AND dev_date > NOW() - INTERVAL 1 HOUR");
	for ( $i = 0; $i < count($result); $i++ )
	{
		// Приводим нормализованное значение к градусам
		$result[$i]['dev_value'] = $result[$i]['dev_value'] * 22.5;
		$A = $A + sin(deg2rad($result[$i]['dev_value']));
		$B = $B + cos(deg2rad($result[$i]['dev_value']));
	}

	$wind_dir_avg1 = rad2deg(atan2($A, $B));
	if ( $wind_dir_avg1 < 0 )
	$wind_dir_avg1 = 360 + $wind_dir_avg1;

	$wind_dir_avg = round($wind_dir_avg1 / 22.5);

	$abs_pressure = round($ab->mod_read($ab->get_id("wh_abs_pressure"), "#dev_value#", "tmpID DESC", 1) / 0.750062, 2);
	$abs_pressure_1h = round($ab->mod_read($ab->get_id("wh_abs_pressure"), "#dev_value#", "tmpID DESC", 1, "dev_date < NOW() - INTERVAL 1 HOUR") / 0.750062, 2);
	if ( $abs_pressure > $abs_pressure_1h + 0.35)
	{
		$pressure_trend = 1;
		$pressure_trend_text = "Рост";
	}
	elseif ( $abs_pressure_1h > $abs_pressure + 0.35)
	{
		$pressure_trend = 2;
		$pressure_trend_text = "Падение";
	}
	else
	{
		$pressure_trend = 0;
		$pressure_trend_text = "Не меняется";
	}

	include("libs/zambretti.php");
	$wh_temp_out = $ab->mod_read($ab->get_id("wh_temp_out"), "#dev_value#", "tmpID DESC", 1);
	$forecast = betel_cast($abs_pressure, date('n'), $wind_dir_text_uk[$wind_dir_avg], $pressure_trend, 1, 1050, 950, $wh_temp_out, 1);

	if ( $wh_temp_out < 2 )
	{
		$z_forecast = Array("Отличная, ясно", "Хорошая, ясно", "Становление хорошей, ясной", "Хорошая, но ухудшается", "Хорошая, возможен снегопад", "Достаточно хорошая, улучшается", "Достаточно хорошая, возможен снегопад", "Достаточно хорошая, но ожидается снегопад", "Снегопад, но улучшается", "Переменчивая, но улучшается", "Достаточно хорошая, вероятен снегопад", "Пасмурно, но проясняется", "Пасмурно, возможно, улучшение", "Снегопады, возможны временные прояснения", "Снегопады, становится менее устойчивой", "Переменчивая, небольшой снег", "Пасмурная, короткие прояснения", "Пасмурная, ожидается снег", "Пасмурная, временами снег", "Преимущественно, очень пасмурная", "Временами снег, ухудшение", "Временами снег, очень плохая, пасмурно", "Снег очень часто", "Снег, очень плохая, пасмурно", "Штормовая, но улучшается", "Штормовая!, снегопад"); 
		$z_forecast_img = Array(1, 2, 6, 4, 21, 4, 20, 23, 23, 6, 21, 23, 23, 23, 24, 21, 21, 24, 24, 22, 22, 22, 22, 22, 23, 22); 
	}
	else
	{
		$z_forecast = Array("Отличная, ясно", "Хорошая, ясно", "Становление хорошей, ясной", "Хорошая, но ухудшается", "Хорошая, возможен ливень", "Достаточно хорошая, улучшается", "Достаточно хорошая, возможен ливень", "Достаточно хорошая, но ожидается ливень", "Ливень, но улучшается", "Переменчивая, но улучшается", "Достаточно хорошая, вероятны ливни", "Пасмурно, но проясняется", "Пасмурно, возможно, улучшение", "Ливни, возможны временные прояснения", "Ливни, становится менее устойчивой", "Переменчивая, небольшие дожди", "Пасмурная, короткие прояснения", "Пасмурная, ожидаются дожди", "Пасмурная, временами дожди", "Преимущественно, очень пасмурная", "Временами дожди, ухудшение", "Временами дожди, очень плохая, пасмурно", "Дожди очень часто", "Дожди, очень плохая, пасмурно", "Штормовая, но улучшается", "Штормовая!, дожди"); 
		$z_forecast_img = Array(1, 2, 6, 4, 17, 4, 14, 17, 16, 6, 14, 13, 14, 16, 15, 14, 13, 12, 12, 18, 18, 18, 18, 18, 16, 15); 
	}

	echo "<span style=\"font-size: 0.9em;float:right\">".$z_forecast[$forecast]."</span>";
	echo "<div style=\"position:relative;top:-20px\"><img src=\"img/forecast/".$z_forecast_img[$forecast].".png\" width=\"320\" height=\"206\"></div>";
}

// События и праздники
elseif ( $_REQUEST['p'] == "events" )
{
	$hols = $ab->mod_read($ab->get_id("hols"), "#event_title#; ", "", 0, "DATE_FORMAT(event_date, \"%m%d\")=".date("md")); 
	if ( !empty($hols) )
	{
		$hols_a = preg_split("/Именины:/", $hols);
		$hols_a[0] = preg_replace("/\s$/", "", $hols_a[0]);
		$hols_a[1] = preg_replace("/\s$/", "", $hols_a[1]);
		$hols_a[0] = preg_replace("/;$/", "", $hols_a[0]);
		$hols_a[1] = preg_replace("/;$/", "", $hols_a[1]);
		if ( !empty($hols_a[0]) )
		echo "Праздники: <b>$hols_a[0]</b><br>";
		if ( !empty($hols_a[1]) )
		echo "Именины: <b>$hols_a[1]</b><br>";
	}

	$birth_p = $ab->doc_read($ab->get_id("birth"));
	$my_cnt = 0;

	echo "Ближайший семейные праздники:<br>";
	$result = $ab->select("SELECT DATE_FORMAT(event_date, '%d.%m') event_date, event_title,
		DATEDIFF(CONCAT(DATE_FORMAT(NOW(), '%Y'), '-', DATE_FORMAT(event_date, '%m-%d')), NOW()) event_days,
		DATE_FORMAT(NOW(), '%Y') - DATE_FORMAT(event_date, '%Y') event_years, DATE_FORMAT(event_date, '%m')
		FROM tmp_".$birth_p['ListID']."
		WHERE ContID=".$birth_p['ContID']."
		AND DATE_FORMAT(event_date, '%m.%d') >= DATE_FORMAT(NOW(), '%m.%d')
		ORDER BY DATE_FORMAT(event_date, '%m%d')
		LIMIT 3");
	
	for ( $i = 0; $i < count($result); $i++ )
	{
		$my_cnt++;

		if ( $result[$i]['event_date'] == date('d.m') )
		echo "<font color=\"red\">Ура! Ура! ".$result[$i]['event_title']."! Исполняется лет: ".$result[$i]['event_years']." Поздравляем!</font><br>";
		else
		echo $result[$i]['event_date']." - ".$result[$i]['event_title']." (".$result[$i]['event_days']." дн., исполняется лет: ".$result[$i]['event_years'].")<br>";
	}

	if ( $my_cnt < 3 )
	{
		$my_limit = 3 - $my_cnt;
		$result = $ab->select("SELECT DATE_FORMAT(event_date, '%d.%m') event_date, event_title, 
			DATEDIFF(CONCAT(DATE_FORMAT(NOW(), '%Y')+1, '-', DATE_FORMAT(event_date, '%m-%d')), NOW()) event_days,
			DATE_FORMAT(NOW(), '%Y') - DATE_FORMAT(event_date, '%Y') + 1 event_years
			FROM tmp_".$birth_p['ListID']."
			WHERE ContID=".$birth_p['ContID']."
			ORDER BY DATE_FORMAT(event_date, '%m%d')
			LIMIT $my_limit");

		for ( $i = 0; $i < count($result); $i++ )
		//echo "$data[0] - $data[1] ($data[2] дн., исполняется лет: $data[3])<br>";
		echo $result[$i]['event_date']." - ".$result[$i]['event_title']." (".$result[$i]['event_days']." дн., исполняется лет: ".$result[$i]['event_years'].")<br>";
	}
}

//<div class="lamp off" label="light_mainstairs" style="top:210px;left:550px"></div>
elseif ( $_REQUEST['p'] == "keys2" )
{

	$keys_p = $ab->doc_read($ab->get_id("keys"));

	$result = $ab->select("SELECT key_title, tmpID, key_pio, key_label, key_addr, key_type, key_level, key_place FROM tmp_".$keys_p['ListID']." WHERE ( key_place LIKE '".intval($_GET['floor']).";%' OR key_place LIKE '12;%' ) AND ContID=".$keys_p['ContID']);
	for ( $i = 0; $i < count($result); $i++ )
	{
		if ( $result[$i]['key_pio'] == 0 )
		$key_class = "off";
		else
		$key_class = "on";

		$key_place = explode(";", $result[$i]['key_place']);

		echo "<div class=\"lamp $key_class\" label=\"".$result[$i]['key_label']."\" style=\"top:".$key_place[1]."px;left:".$key_place[2]."px\"></div>";
	}

}

elseif ( $_REQUEST['p'] == "keys" )
{

	?>
	<!-- ИЗМЕНИТЬ! -->
	<!--link rel="stylesheet" type="text/css" href="css2/trackbar.css" />
	<scripT type="text/javascript" src="js2/jquery.trackbar.js"></script-->

	<scripT type="text/javascript">

	$(document).ready(
	function()
	{
		$('.key').click(function () {
			var label = $(this).attr("label");
			var action = $(this).attr("action");
			var key = $(this);

			key.text("подождите");

			$.get("key.php?key_label=" +label + "&key_pio=" + action, function(data){ 
				$.get("ab-data.php?p=keys", function(data){$("#keys").html(data);});
			});
		 });

		$('.dim').click(function () {
			var label = $(this).attr("label");
			var level = $('#dim_' + label).attr("value");
			$.get("key.php?key_label=" +label + "&key_level=" + level, function(data){ 
			});
		 });

	})
	</script>                                                               

	<?

	$keys_p = $ab->doc_read($ab->get_id("keys"));
	$keysj_p = $ab->doc_read($ab->get_id("keys_journal"));

	echo "<table>";

		echo "<tr><th>Устройство</th><th>Сост.</th><th>Посл. включ.</th><th>Посл. выключ.</th></tr>";
		$result = $ab->select("SELECT key_title, tmpID, key_pio, key_label, key_addr, key_type, key_level FROM tmp_".$keys_p['ListID']." WHERE ContID=".$keys_p['ContID']." ORDER BY tmpID");
		for ( $i = 0; $i < count($result); $i++ )
		{
			echo "<tr>";
				echo "<td>".$result[$i]['key_title']."</td>";

				echo "<td nowrap>";
					if ( $result[$i]['key_pio'] == 1 )
					echo "<b>Вкл</b>";
					else
					echo "Выкл";

						if ( $result[$i]['key_type'] != "auto" )
						{
							if ( $result[$i]['key_pio'] == 0 )
							echo " / <a href=\"javascript:void(0)\" class=\"key\" label=\"".$result[$i]['key_label']."\" action=\"1\">Вкл</a>";
							else
							echo " / <a href=\"javascript:void(0)\" class=\"key\" label=\"".$result[$i]['key_label']."\" action=\"0\">Выкл</a>";
						}

						/*
						if ( preg_match("/^x10\./", $data[4]) )
						{
							if ( $data[5] == "dimmer" && $data[2] == 1 )
							{
								echo "<input type=\"hidden\" name=\"level\" id=\"dim_$data[3]\" value=\"$data[6]\" size=\"2\"> ";
								?>

								<scripT>
								$(document).ready(function()
								{
									$('#dim_<? echo $data[1]; ?>').trackbar(
									{
										onMove : function() { $('#dim_<? echo $data[3]; ?>').attr("value", this.leftValue); },
										dual : false,
										width : 50,
										leftLimit : 1,
										leftValue : <? echo $data[6]; ?>,
										rightLimit : 20,
										rightValue : 1
									});
								});
								</script>

								<?
								echo "<div id=\"dim_$data[1]\"></div>";
								echo "<a href=\"javascript:void(0)\" class=\"dim\" label=\"$data[3]\" action=\"0\">изменить</a>";
							}
						}
						*/
					
				echo "</td>";

				$my_res = "<td>&nbsp</td>";
				$last_on = $ab->select_line("SELECT DATE_FORMAT(key_j_date, '%y-%m-%d %H:%i:%s') key_j_date FROM tmp_".$keysj_p['ListID']." WHERE ContID=".$keysj_p['ContID']." AND key_j_label=".$result[$i]['tmpID']." AND key_i_pio='1' ORDER BY key_j_date DESC LIMIT 1");
				if ( count($last_on) > 0 )
				echo "<td nowrap>".$last_on['key_j_date']."</td>";
				else
				echo "<td>&nbsp;</td>";

				$my_res = "<td>&nbsp</td>";;
				$last_off = $ab->select_line("SELECT DATE_FORMAT(key_j_date, '%y-%m-%d %H:%i:%s') key_j_date FROM tmp_".$keysj_p['ListID']." WHERE ContID=".$keysj_p['ContID']." AND key_j_label=".$result[$i]['tmpID']." AND key_i_pio='0' ORDER BY key_j_date DESC LIMIT 1");
				if ( count($last_off) > 0 )
				echo "<td nowrap>".$last_off['key_j_date']."</td>";
				else
				echo "<td>&nbsp;</td>";


		    echo "</tr>";
		}

	echo "</table>";
}

elseif ( $_REQUEST['p'] == "keys_hist" )
{
	$keys_p = $ab->doc_read($ab->get_id("keys"));
	$keysj_p = $ab->doc_read($ab->get_id("keys_journal"));

	echo "<h3>Журнал</h3>";

	echo "<table>";

		$result = $ab->select("SELECT DATE_FORMAT(a.key_j_date, '%H:%i:%s') key_j_date, b.key_title, a.key_i_pio
			FROM tmp_".$keysj_p['ListID']." a, tmp_".$keys_p['ListID']." b
			WHERE a.ContID=".$keysj_p['ContID']." AND b.ContID=".$keys_p['ContID']."
			AND a.key_j_label=b.tmpID
			ORDER BY a.key_j_date DESC
			LIMIT 17");
		for ( $i = 0; $i < count($result); $i++ )
		{
			echo "<tr>";
				echo "<td>".$result[$i]['key_j_date']."</td>";
				echo "<td>".$result[$i]['key_title']."</td>";
				if ( $result[$i]['key_i_pio'] == "1" )
				echo "<td>Вкл</td>";
				else
				echo "<td>Выкл</td>";
			echo "</tr>";
		}
	echo "</table>";
}
// Погода
elseif ( $_REQUEST['p'] == "weather" )
{

	$wind_dir_text = array("Северный", "Северо-Северо-Восточный", "Северо-Восточный", "Северо-Восточный-Восточный",
	"Восточный", "Юго-Восточный-Восточный", "Юго-Восточный", "Юго-Юго-Восточный",
	"Южный", "Юго-Юго-Западный", "Юго-Западный", "Юго-Западный-Западный",
	"Западный", "Северо-Западный-Западный", "Северо-Западный", "Северо-Северо-Западный", "Нет данных");

	$wind_dir_text_uk = array("N", "NNE", "NE", "ENE", "E", "ESE", "SE", "SSE", "S", "SSW", "SW", "WSW", "W", "WNW", "NW", "NNW", "FALSE");

	$wind_dir = $ab->doc_read($ab->get_id("wh_wind_dir"));

	// Направление ветра текущее
	$result = $ab->select("SELECT dev_value FROM tmp_".$wind_dir['ListID']." WHERE ContID=".$wind_dir['ContID']." ORDER BY tmpID DESC LIMIT 1");
	for ( $i = 0; $i < count($result); $i++ )
	$wind_dir_current = round($result[$i]['dev_value']);

	if ( $wind_dir_current > 15 )
	$wind_dir_current = 16;

	// Направление ветра среднее за час

	$A = 0;
	$B = 0;
	$result = $ab->select("SELECT dev_value	FROM tmp_".$wind_dir['ListID']." WHERE ContID=".$wind_dir['ContID']." AND dev_value<16 AND dev_date > NOW() - INTERVAL 1 HOUR");
	for ( $i = 0; $i < count($result); $i++ )
	{
		// Приводим нормализованное значение к градусам
		$result[$i]['dev_value'] = $result[$i]['dev_value'] * 22.5;
		$A = $A + sin(deg2rad($result[$i]['dev_value']));
		$B = $B + cos(deg2rad($result[$i]['dev_value']));
	}

	$wind_dir_avg1 = rad2deg(atan2($A, $B));
	if ( $wind_dir_avg1 < 0 )
	$wind_dir_avg1 = 360 + $wind_dir_avg1;

	$wind_dir_avg = round($wind_dir_avg1 / 22.5);

	$wind_ave = $ab->doc_read($ab->get_id("wh_wind_ave"));

	// Скорость ветра текущая

	$result = $ab->select_line("SELECT dev_value FROM tmp_".$wind_ave['ListID']." WHERE ContID=".$wind_ave['ContID']." ORDER BY tmpID DESC LIMIT 1");
	$wind_ave_current = round($result['dev_value'], 1);

	// Скорость ветра средняя за час

	$result = $ab->select_line("SELECT AVG(dev_value) wind_avg FROM tmp_".$wind_ave['ListID']." WHERE ContID=".$wind_ave['ContID']." AND dev_date > NOW() - INTERVAL 1 HOUR");
	$wind_ave_avg = round($result['wind_avg'], 2);

	// Максимальные порывы ветра за час
	$wind_gust = $ab->doc_read($ab->get_id("wh_wind_gust"));
	$result = $ab->select_line("SELECT MAX(dev_value) wind_gust FROM tmp_".$wind_gust['ListID']." WHERE ContID=".$wind_gust['ContID']." AND dev_date > NOW() - INTERVAL 1 HOUR");
	$wind_gust_max = round($result['wind_gust'], 1);

	$rain = $ab->doc_read($ab->get_id("wh_rain"));
	
	// Осадки за последние 30 дней
	$result = $ab->select_line("SELECT dev_value FROM tmp_".$rain['ListID']." WHERE ContID=".$rain['ContID']." AND dev_date > NOW() - INTERVAL 30 DAY ORDER BY dev_date LIMIT 1");
	$rain_30d = round($result['dev_value'], 1);

	// Осадки сутки назад
	$result = $ab->select_line("SELECT dev_value FROM tmp_".$rain['ListID']." WHERE ContID=".$rain['ContID']." AND dev_date > NOW() - INTERVAL 1 DAY ORDER BY dev_date LIMIT 1");
	$rain_1d = round($result['dev_value'], 1);

	// Осадки час назад
	$result = $ab->select_line("SELECT dev_value FROM tmp_".$rain['ListID']." WHERE ContID=".$rain['ContID']." AND dev_date > NOW() - INTERVAL 1 HOUR ORDER BY dev_date LIMIT 1");
	$rain_1h = round($result['dev_value'], 1);

	// Осадки текущие
	$result = $ab->select_line("SELECT dev_value FROM tmp_".$rain['ListID']." WHERE ContID=".$rain['ContID']." ORDER BY tmpID DESC LIMIT 1");
	$rain_current = round($result['dev_value'], 1);

	$rain_hour = round($rain_current - $rain_1h, 1);
	$rain_day = round($rain_current - $rain_1d, 1);
	$rain_month = round($rain_current - $rain_30d, 1);
	if ( $rain_day == 0 )
	$rain_str = "(Нет)";
	elseif ( $rain_day < 5 )
	$rain_str = "(Мало)";
	elseif ( $rain_day >= 5 && $rain_day < 15 )
	$rain_str = "(Умеренные)";
	elseif ( $rain_day >= 15 && $rain_day < 49 )
	$rain_str = "(Сильные)";
	elseif ( $rain_day >= 50 )
	$rain_str = "(Очень сильные)";

	echo "Направление ветра: ".$wind_dir_text[$wind_dir_current]."<br>";
	echo "Направление ветра (среднее за час): ".$wind_dir_text[$wind_dir_avg]." [$wind_dir_avg]<br>";
	echo "Скорость ветра: ".$wind_ave_current." м/с<br>";
	echo "Скорость ветра (средняя за час): ".$wind_ave_avg." м/с<br>";
	echo "Максимальные порывы ветра за час: ".$wind_gust_max." м/с<br>";

	$wh_hum_out = round($ab->mod_read($ab->get_id("wh_hum_out"), "#dev_value#", "tmpID DESC", 1));
	$temp_out = round($ab->mod_read($ab->get_id("temp_out2"), "#dev_value#", "tmpID DESC", 1));
	echo "Влажность: ".$wh_hum_out."%<br>";
	echo "Осадки за час: ".$rain_hour." мм<br>";
	echo "Осадки за сутки: ".$rain_day." мм $rain_str<br>";
	echo "Осадки за последние 30 дней: ".$rain_month." мм <br>";
	
	// Расчет точки росы
	$a = 17.27;
	$b = 237.7;
	$gamma = (($a * $temp_out) / ($b + $temp_out)) + log($wh_hum_out / 100.0);
	$dew_point = round(($b * $gamma) / ($a - $gamma), 2);
	echo "Точка росы: ".$dew_point."C<br>";

	// ПРОГНОЗ
	include("libs/zambretti.php");
	$abs_pressure = round($ab->mod_read($ab->get_id("wh_abs_pressure"), "#dev_value#", "tmpID DESC", 1) / 0.750062, 2);
	$abs_pressure_1h = round($ab->mod_read($ab->get_id("wh_abs_pressure"), "#dev_value#", "tmpID DESC", 1, "dev_date < NOW() - INTERVAL 1 HOUR") / 0.750062, 2);
	if ( $abs_pressure > $abs_pressure_1h + 0.35)
	{
		$pressure_trend = 1;
		$pressure_trend_text = "Рост";
	}
	elseif ( $abs_pressure_1h > $abs_pressure + 0.35)
	{
		$pressure_trend = 2;
		$pressure_trend_text = "Падение";
	}
	else
	{
		$pressure_trend = 0;
		$pressure_trend_text = "Не меняется";
	}
	$wh_temp_out = $ab->mod_read($ab->get_id("wh_temp_out"), "#dev_value#", "tmpID DESC", 1);
	$forecast = betel_cast($abs_pressure, date('n'), $wind_dir_text_uk[$wind_dir_avg], $pressure_trend, 1, 1050, 950, $wh_temp_out);
	echo "Тенденция давления: $pressure_trend_text<br>";
	echo "Прогноз: $forecast<br>";

	// Расчет заморозков
	if ( date('G') < 20 )
	$z = "";
	else
	{
		$date1 = date('Y-m-d 13:00:00');
		$date2 = date('Y-m-d 21:00:00');
		$temp_id = $ab->get_id("wh_temp_out");

		$t13 = $ab->mod_read($temp_id, "#dev_value#", "dev_date", 1, "dev_date>'$date1'");
		$t21 = $ab->mod_read($temp_id, "#dev_value#", "dev_date", 1, "dev_date>'$date2'");

		$z = "0%";
		$x = $t13 - $t21;

		if ( $t21 >= $t13 )
		$z = "расчет невозможен. Температура растет";
		elseif ( $t21 < 0 )
		$z = "расчет невозможен. Температура ниже нуля";
		elseif ( $t21 < 11 && $x < 11 )
		{
			$t_graph = array(
				0 => array(0.375, 11, 0),
				1 => array(0.391, 8.7, 10),
				2 => array(0.382, 6.7, 20),
				3 => array(0.382, 4.7, 40),
				4 => array(0.391, 2.7, 60),
				5 => array(0.4, 1.6, 80));
	
			$z = "100%";
			for ( $i = 0; $i < count($t_graph); $i++ )
			{
				$y1 = $t_graph[$i][0] * $x + $t_graph[$i][1];
	
				if ( $t21 > $y1)
				{
					$z = $t_graph[$i][2]."%";
					break;
				}
			}
		}
	}
		
	if ( !empty($z) )
	echo "Вероятность заморозков: $z";

}
// Состояние системы отопления и котла
elseif ( $_REQUEST['p'] == "heat_stat_main" )
{
	$heat_conf = $ab->doc_read($ab->get_id("heat_conf"));
	$boiler = $ab->doc_read($ab->get_id("boiler"));

	$heat_conf_data = $ab->select_line("SELECT * from tmp_".$heat_conf['ListID']." WHERE ContID=".$heat_conf['ContID']." LIMIT 1");
	$boiler_data = $ab->select_line("SELECT * from tmp_".$boiler['ListID']." WHERE ContID=".$boiler['ContID']." LIMIT 1");

	if ( $heat_conf_data['heat_boiler_status'] == 1 )
	$heat_conf_data['heat_boiler_status'] = "<b>Вкл</b>";
	else
	$heat_conf_data['heat_boiler_status'] = "Выкл";

	if ( $boiler_data['boiler_fire'] == 1 )
	$boiler_data['boiler_fire'] = "<b>Вкл</b>";
	else
	$boiler_data['boiler_fire'] = "Выкл";

	$boiler_data['boiler_date'] = preg_replace("/(.*)\s(.*)/", "$2", $boiler_data['boiler_date']);
	
	echo "<table border=\"1\">";

		echo "<tr><td>Режим работы</td><td>".$heat_conf_data['heat_mode']."</td>";

		if ( $boiler_data['boiler_status'] == 0 || $boiler_data['boiler_status'] == 30 )
		$boiler_data['boiler_status'] = "требование тепла отсутствует";
		elseif ( $boiler_data['boiler_status'] == 2 )
		$boiler_data['boiler_status'] = "работа насоса перед включением горелок";
		elseif ( $boiler_data['boiler_status'] == 3 )
		$boiler_data['boiler_status'] = "розжиг";
		elseif ( $boiler_data['boiler_status'] == 4 )
		$boiler_data['boiler_status'] = "горелки включены";
		elseif ( $boiler_data['boiler_status'] == 7 )
		$boiler_data['boiler_status'] = "работа насоса после выключения горелок";
		elseif ( $boiler_data['boiler_status'] == 8 )
		$boiler_data['boiler_status'] = "блокировка горелок после отключения";
		else
		$boiler_data['boiler_status'] = "<b>Неизвестно</b>";
		echo "<tr><td>Котел / горелки </td><td>".$heat_conf_data['heat_boiler_status']." / ".$boiler_data['boiler_fire']."</td>";

		echo "<tr><td>Котел: состояние [".$boiler_data['boiler_date']."]</td><td>".$boiler_data['boiler_status']."</td>";
		echo "<tr><td>Котел, розжиг: макс / сред / сбой</td><td>".$boiler_data['boiler_burn_max']." с / ".$boiler_data['boiler_burn_avg']." с / ".$boiler_data['boiler_burn_fault']."</td>";

		if ( $boiler_data['boiler_write'] == 1 )
		$heat_conf_data['heat_boiler_temp_calc'] = "<font color=\"red\">".$heat_conf_data['heat_boiler_temp_calc']."&deg;</font>";
		else
		$heat_conf_data['heat_boiler_temp_calc'] .= "&deg;";

		$heat_boiler_temp_percent = round(($heat_conf_data['heat_boiler_temp'] - 35) / 48 * 100);
		echo "<tr><td>Макс. темп. котла: факт / расчет</td><td>".$heat_conf_data['heat_boiler_temp']."&deg; / ".$heat_conf_data['heat_boiler_temp_calc']." ($heat_boiler_temp_percent%)</td>";

		//echo $ab->mod_read($ab->get_id("temp_heat_in_befor"), "<tr><td>Факт. тем.: ГР / котел / выхлоп</td><td>#dev_value#&deg; / ".$boiler_data['boiler_temp_out']."</td></tr>", "dev_date DESC");
		echo "<tr><td>Факт. тем.: котел / выхлоп</td><td>".$boiler_data['boiler_temp_out']."&deg; / ".$boiler_data['boiler_temp_gaz']."</td></tr>";

		$wh_temp_in = $ab->mod_read($ab->get_id("wh_temp_in"), "#dev_value#", "tmpID DESC", 1);
		$wh_hum_in = round($ab->mod_read($ab->get_id("wh_hum_in"), "#dev_value#", "tmpID DESC", 1));
		echo "<tr><td>Темп. / влажность в котельной</td><td>$wh_temp_in&deg; / $wh_hum_in%</td></tr>";

	echo "</table>";

}
elseif ( $_REQUEST['p'] == "heat_stat_circ" )
{
	echo "<h3>Контуры отопления</h3>";

	$heat_circ = $ab->doc_read($ab->get_id("heat_circ"));

	echo "<table border=\"1\">";

		$result = $ab->select("SELECT * FROM tmp_".$heat_circ['ListID']." WHERE ContID=".$heat_circ['ContID']);
		
		for ( $i = 0; $i < count($result); $i++ )
		{
			if ( $i > 0 )
			echo "<tr><td colspan=\"2\">&nbsp;</td></tr>";

			echo "<tr><td>Наименование / Тип</td><td>".$result[$i]['circ_title']." / ".$result[$i]['circ_type']."</td></tr>";
			echo "<tr><td>Температура комфорта</td><td>".$result[$i]['circ_temp_calc']."&deg;</td></tr>";
			if ( $result[$i]['circ_pump_status'] == 1 )
			$result[$i]['circ_pump_status'] = "<b>Включен</b>";
			else
			$result[$i]['circ_pump_status'] = "Выключен";
			echo "<tr><td>Насос контура</td><td>".$result[$i]['circ_pump_status']."</td></tr>";

			if ( $result[$i]['circ_type'] == "Смесительный" || $result[$i]['circ_type'] == "Теплый пол" )
			{
				$circ_flap_percent = round($result[$i]['circ_flap'] / 120 * 100);
				echo "<tr><td>Положение привода</td><td>".$result[$i]['circ_flap']." c ($circ_flap_percent%)</td></tr>";
				echo "<tr><td>Расчетная t подачи</td><td>".$result[$i]['circ_temp_water']."</td></tr>";
			}
			if ( !empty($result['circ_t_in']) )
			echo $ab->mod_read($ab->get_id($circ_t_in), "<tr><td>Температура подачи</td><td>#dev_value#&deg;</td></tr>", "dev_date DESC");
		}
	echo "</table>";

}
elseif ( $_REQUEST['p'] == "heat_stat_log" )
{
	echo "<h3>Журнал системы управления отоплением</h3>";

	ob_start();
	passthru("tail -n 18 log_heat.log");
	$log = ob_get_contents();
	ob_end_clean();
	$log = preg_replace("/\n/", "<br>", $log);
	//echo iconv("CP1251", "UTF8", $log);
	echo $log;
}

elseif ( $_REQUEST['p'] == "el_cur" )
{
	echo "<table border=\"1\">";
	echo "<tr><th>&nbsp;</th><th>Сумма</th><th>Ф1</th><th>Ф2</th><th>Ф3</th></tr>";

	$el_p = $ab->doc_read($ab->get_id("el_p"));
	echo $ab->mod_read($el_p['ContID'], "<tr><td>P</td><td>#el_val0#</td><td>#el_val1#</td><td>#el_val2#</td><td>#el_val3#</td></tr>", "tmpID DESC", 1);
	
	$el_u = $ab->doc_read($ab->get_id("el_u"));
	$result = $ab->select_line("SELECT el_val0, el_val1, el_val2, el_val3 FROM tmp_".$el_u['ListID']." WHERE ContID=".$el_u['ContID']." ORDER BY tmpID DESC LIMIT 1");
	foreach($result as $key => $value)
	{
		if ( $key != "el_val0" && ($value < 207 || $value > 253) )
		$result[$key] = "<font color=\"red\">$value</font>";
	}
	echo "<tr><td>U</td><td>&nbsp;</td><td>".$result['el_val1']."</td><td>".$result['el_val2']."</td><td>".$result['el_val3']."</td></tr>";

	echo $ab->mod_read($ab->get_id("el_i"), "<tr><td>I</td><td>#el_val0#</td><td>#el_val1#</td><td>#el_val2#</td><td>#el_val3#</td></tr>", "tmpID DESC", 1);
	echo $ab->mod_read($ab->get_id("el_cosf"), "<tr><td>cosФ</td><td>#el_val0#</td><td>#el_val1#</td><td>#el_val2#</td><td>#el_val3#</td></tr>", "tmpID DESC", 1);

	echo "</table>";

	$result = $ab->select_line("SELECT el_val0 max_val, DATE_FORMAT(el_date, '%H:%i') el_date FROM tmp_".$el_p['ListID']." WHERE ContID=".$el_p['ContID']." AND DATE(el_date)=CURDATE() ORDER BY el_val0 DESC LIMIT 1");
	echo "Макс. потребление сегодня: <b>".$result['max_val']." Вт</b> (".$result['el_date'].")<br>";

	$result = $ab->select_line("SELECT el_val0 min_val, DATE_FORMAT(el_date, '%H:%i') el_date FROM tmp_".$el_p['ListID']." WHERE ContID=".$el_p['ContID']." AND DATE(el_date)=CURDATE() ORDER BY el_val0 LIMIT 1");
	echo "Мин. потребление сегодня: <b>".$result['min_val']." Вт</b> (".$result['el_date'].")<br>";

	$el_days_t1 = $ab->doc_read($ab->get_id("el_days_t1"));
	$el_days_t2 = $ab->doc_read($ab->get_id("el_days_t2"));
	$tarif = $ab->doc_read($ab->get_id("tarif"));
	
	$tar_cost_t1 = $ab->select_line("SELECT tar_cost FROM tmp_".$tarif['ListID']." WHERE ContID=".$tarif['ContID']." AND tar_type='Электроэнергия (Т1)'	ORDER BY tar_date DESC LIMIT 1");
	$tar_cost_t2 = $ab->select_line("SELECT tar_cost FROM tmp_".$tarif['ListID']." WHERE ContID=".$tarif['ContID']." AND tar_type='Электроэнергия (Т2)'	ORDER BY tar_date DESC LIMIT 1");
	
	echo "Потребление (Кв*Ч)<br>";
	echo "<table border=\"1\">";
	echo "<tr><th>Сегодня</th><th>Вчера</th><th>Позавчера</th></tr>";
	echo "<tr>";

	if ( file_exists("demo.txt" ) )
	echo "<td>30.90 / 10.16<br>41,06 (64 р)</td><td>29.17 / 10.43<br>39,6 (61 р)</td><td>39.38 / 13.69<br>53,07 (82 р)</td>";
	else
	{
		$result_t1 = $ab->select("SELECT dev_value, DATE_FORMAT(dev_date, '%d%m%Y') dev_date_f FROM tmp_".$el_days_t1['ListID']." WHERE ContID=".$el_days_t1['ContID']." ORDER BY dev_date DESC LIMIT 3");
		$result_t2 = $ab->select("SELECT dev_value, DATE_FORMAT(dev_date, '%d%m%Y') dev_date_f FROM tmp_".$el_days_t2['ListID']." WHERE ContID=".$el_days_t2['ContID']." ORDER BY dev_date DESC LIMIT 3");

		if ( $result_t1[0]['dev_date_f'] != $result_t2[0]['dev_date_f'] )
		{
			array_unshift($result_t1, "");
			$result_t1[0]['dev_value'] = 0;
		}
		
		for ( $i = 0; $i < 3; $i++ )
		{
				$costs_t1 = round($result_t1[$i]['dev_value'] * $tar_cost_t1['tar_cost'], 0);
				$costs_t2 = round($result_t2[$i]['dev_value'] * $tar_cost_t2['tar_cost'], 0);
				$costs = $costs_t1 + $costs_t2;

				$total = $result_t1[$i]['dev_value'] + $result_t2[$i]['dev_value'];
				
				echo "<td align=\"center\">";
					echo $result_t1[$i]['dev_value']." / ".$result_t2[$i]['dev_value']."<br>";
					echo "$total ($costs р)";
				echo "</td>";
		}
	}
	echo "</tr>";
	echo "</table>";

	$el_total_id = $ab->get_id("el_total");
	$result = $ab->select("SELECT a.ContFieldID, a.Title, b.Value FROM tContField a, tContFieldExt b WHERE a.Disabled=0 AND b.ContID=$el_total_id AND a.ContFieldID=b.ContFieldID AND b.Value<>''");
	for ( $i = 0; $i < count($result); $i++ )
	$my_Extra_Value[$result[$i]['Title']] = $result[$i]['Value'];
	$el_total = $ab->mod_read($el_total_id, "#dev_value#", "dev_date DESC", 1);
	$el_total2 = $el_total - $my_Extra_Value['dev_shift'];

	$el_total_id = $ab->get_id("el_total_t1");
	$result = $ab->select("SELECT a.ContFieldID, a.Title, b.Value FROM tContField a, tContFieldExt b WHERE a.Disabled=0 AND b.ContID=$el_total_id AND a.ContFieldID=b.ContFieldID AND b.Value<>''");
	for ( $i = 0; $i < count($result); $i++ )
	$my_Extra_Value[$result[$i]['Title']] = $result[$i]['Value'];
	$el_total_t1 = $ab->mod_read($el_total_id, "#dev_value#", "dev_date DESC", 1);
	$el_total_t1 = round($el_total_t1 - $my_Extra_Value['dev_shift'], 2);

	$el_total_id = $ab->get_id("el_total_t2", 1);
	$result = $ab->select("SELECT a.ContFieldID, a.Title, b.Value FROM tContField a, tContFieldExt b WHERE a.Disabled=0 AND b.ContID=$el_total_id AND a.ContFieldID=b.ContFieldID AND b.Value<>''");
	for ( $i = 0; $i < count($result); $i++ )
	$my_Extra_Value[$result[$i]['Title']] = $result[$i]['Value'];
	$el_total_t2 = $ab->mod_read($el_total_id, "#dev_value#", "dev_date DESC", 1);
	$el_total_t2 = round($el_total_t2 - $my_Extra_Value['dev_shift'], 2);

	if ( file_exists("demo.txt" ) )
	{
		echo "Текущие показания счетчика: <span style=\"font-size:16pt;font-weight:400\">16753</span><br>";
		echo "День / Ночь: <span style=\"font-size:16pt;font-weight:400\">9472,09 / 7280,91</span>";
	}
	else
	{
		echo "Текущие показания счетчика: <span style=\"font-size:16pt;font-weight:400\">$el_total2</span> ($el_total)<br>";
		echo "День / Ночь: <span style=\"font-size:16pt;font-weight:400\">$el_total_t1 / $el_total_t2</span>";
	}

}
elseif ( $_REQUEST['p'] == "graph_rain" )
{

	$rain = $ab->select("SELECT DATE_FORMAT(dev_date, '%H') my_date, dev_value FROM tmp_2 WHERE ContID=69 AND dev_date > NOW() - INTERVAL 1 DAY ORDER BY dev_date");
	$my_val = $rain[0]['dev_value'];
	$my_date = $rain[0]['my_date'];
	for ( $i = 0; $i < count($rain); $i++ )
	{
		if ( $my_date != $rain[$i]['my_date'] )
		{
			//echo $rain[$i]['dev_value']." - ".round($rain[$i]['dev_value'] - $my_val, 1)." - ".$my_date."<br>";
			$rain2[] = round($rain[$i]['dev_value'] - $my_val, 1);
			$rain2_date[] = $my_date;
			$my_val = $rain[$i]['dev_value'];
			$my_date = $rain[$i]['my_date'];
		}
	}
	$rain2[] = round($rain[$i-1]['dev_value'] - $my_val, 1);
	$rain2_date[] = $rain[$i-1]['my_date'];

	?>

	<script type="text/javascript">
	$(function () {
        $('#graph_rain').highcharts({
            credits: { enabled: false },
			chart: { type: 'column' },
            title: { text: 'График осадков за сутки' },
			legend: { enabled: false },
			
            yAxis: {
                min: 0,
                title: {
                    text: ''
                }
            },

            tooltip: {
                pointFormat: '<b>{point.y:.1f} mm</b>',
                useHTML: true
            },
			
            xAxis: {
                categories: [
				<?
				for ( $i = 0; $i < count($rain2_date); $i++ )
				echo  $rain2_date[$i].", ";
				?>
                ],
		labels: {
	                style: {
	                    fontSize:'8px'
                }}
            },
			
            series: [{
                data: [
				<?
				for ( $i = 0; $i < count($rain2); $i++ )
				echo  $rain2[$i].", ";
				?>
                ]
            }]
        });
    });
	</script>
	
	<div id="graph_rain" style="min-width: 420px; height: 240px; margin: 0 auto"></div>
	
	<?
}
elseif ( $_REQUEST['p'] == "water" )
{
	/*
	echo "<table border=\"1\">";
		echo "<tr>";
			echo "<td>Отмостка (1 м от дома)</td>";
			show_list(get_id_cl("temp_pipe_house", 1), "<td>#dev_value#&deg;</td>", "tmpID DESC", 1);
		echo "</tr>";
		echo "<tr>";
			echo "<td>Дренаж (5 м от дома)</td>";
			show_list(get_id_cl("temp_pipe_drain", 1), "<td>#dev_value#&deg;</td>", "tmpID DESC", 1);
		echo "<tr>";
		echo "</tr>";
			echo "<td>Забор (10 м от дома)</td>";
			show_list(get_id_cl("temp_pipe_fence", 1), "<td>#dev_value#&deg;</td>", "tmpID DESC", 1);
		echo "</tr>";
		echo "<tr>";
			echo "<td>Колодец (15 м от дома)</td>";
			show_list(get_id_cl("temp_pipe_well", 1), "<td>#dev_value#&deg;</td>", "tmpID DESC", 1);
		echo "</tr>";
	echo "</table>";
	
	*/

	//show_list(get_id_cl("wp_in", 1), "Давление в водопроводной сети: #dev_value# атм.<br>", "tmpID DESC", 1);

	/*
	$uv_uptime = round($ab->mod_read($ab->get_id("lamps"), "#lamp_uptime#", "", 1, "lamp_place='Водоочистка'") / 3600, 1);
	$uv_percent = round($uv_uptime / 8000 * 100, 2);
	echo "Отработка бакторицидных ламп: $uv_uptime часов / 8000 ч ($uv_percent%)";
	if ( $uv_percent > 95 )
	echo "<br><font color=\"red\"><strong>Требуется замена ламп!</strong></font>";
	*/

	echo "<br><br>Картриджи фильтров:<br>";

	$filter = $ab->doc_read($ab->get_id("filter"));
	echo "Фильтр <b>Аквафор Викинг Мини</b><br>";
	$result = $ab->select_line("SELECT DATE_FORMAT(dev_date + INTERVAL 60 DAY, '%d.%m.%Y') plan_date, dev_value, DATEDIFF(NOW(), dev_date) res
						FROM tmp_".$filter['ListID']."
						WHERE ContID=".$filter['ContID']."
						ORDER BY dev_date DESC
						LIMIT 1");
	echo "Плановая дата замены фильтра: ".$result['plan_date']."<br>";
	$filter_res = round($result['res'] / 60 * 100, 2);
	echo "Использован ресурс картриджа: $filter_res%";

	echo "<br>---<br>";

	$filter = $ab->doc_read($ab->get_id("filter_big_blue"));
	echo "Фильтр <b>Big Blue</b><br>";
	$result = $ab->select_line("SELECT DATE_FORMAT(dev_date + INTERVAL 120 DAY, '%d.%m.%Y') plan_date, dev_value, DATEDIFF(NOW(), dev_date) res
						FROM tmp_".$filter['ListID']."
						WHERE ContID=".$filter['ContID']."
						ORDER BY dev_date DESC
						LIMIT 1");
	echo "Плановая дата замены фильтра: ".$result['plan_date']."<br>";
	$filter_res = round($result['res'] / 120 * 100, 2);
	echo "Использован ресурс картриджа: $filter_res%";

}
/*
elseif ( $_REQUEST['p'] == "watering" )
{
	echo "<h3>Автополив</h3>";
	$watering = fopen("log_watering.log", "r");
	$contents = fread($watering, filesize("log_watering.log"));
	fclose($watering);
	echo $contents;

}
*/
elseif ( $_REQUEST['p'] == "gate_j" )
{
	echo "<h3>Они звонили в калитку</h3>";
	echo "<div class=\"alb\">";
		$gate_pic = glob("snap/gate/*.jpg");
		for ( $i = count($gate_pic); $i >= count($gate_pic) - 3; $i-- )
		{
			if ( !empty($gate_pic[$i]) )
			echo "<div><a title=\"\" class=\"preview\" target=\"_blank\" href=\"$gate_pic[$i]\" my_href=\"img.php?n=$gate_pic[$i]&w=300&h=240\"><img src=\"img.php?n=$gate_pic[$i]&w=125&h=100\" width=\"125\" height=\"100\" /></a></div>";
		}		
	echo "</div>";

	echo '<scripT type="text/javascript">$(document).ready(function(){ imagePreview(); });</script>';

	echo "<h3>Журнал работы замка калитки</h3>";
	echo "<table>";
		//echo $ab->mod_read($ab->get_id("gate_j"), "<tr><td>#ibj_date#</td><td>#ibj_keyid#-link</td></tr>", "ibj_date DESC", 5);
		$gate_j = $ab->doc_read($ab->get_id("gate_j"));
		$ib = $ab->doc_read($ab->get_id("ib"));
		$result = $ab->select("SELECT a.ibj_date, b.ib_label FROM tmp_".$gate_j['ListID']." a, tmp_".$ib['ListID']." b WHERE a.ContID=".$gate_j['ContID']." AND b.ContID=".$ib['ContID']." AND a.ibj_keyid=b.tmpID ORDER BY a.ibj_date DESC LIMIT 5");
		for ( $i = 0; $i < count($result); $i++ )
		{
			echo "<tr><td>".$result[$i]['ibj_date']."</td><td>".$result[$i]['ib_label']."</td></tr>";
		}
	echo "</table>";
}
elseif ( $_REQUEST['p'] == "alarm" )
{
	$keys_id = $ab->get_id("keys");
	if ( $ab->mod_read($keys_id, "#key_pio#", "", 1, "key_label='garage'") == 0 )
	echo '<strong><font color="#EE2211">Гараж открыт!!!</font></strong><br>';
	if ( $ab->mod_read($keys_id, "#key_pio#", "", 1, "key_label='tank_valve'") == 1 )
	echo '<strong><font color="#284d92">Идет наполнение емкости для воды!</font></strong><br>';
	echo $ab->mod_read($ab->get_id("alarm"), "<strong><font color=\"#EE2211\">#event_date# - #event_title#</font></strong><br>", "event_date DESC");
}
elseif ( $_REQUEST['p'] == "open-gate" )
{
	require "key.php";
	key_sw("door", 1);
	usleep(50000);
	key_sw("door", 0);

	echo "Замок калитки открыт";
}

elseif ( $_REQUEST['p'] == "unifi-users" )
{
	include("scripts/unifi-users.php");
}

/*
elseif ( $_REQUEST['p'] == "location" )
{

	$loc = $ab->doc_read($_REQUEST['person']);
	//print_r($loc);

	//$loc_list = "<h3><a href=\"#\">Страница 1</a></h3><div>";
	$my_cnt = 5;
	$my_page = 0;
	$my_flag = 1;
	$loc_list = "";

	$result = $ab->select("SELECT tmpID, pos_lat, pos_lng, pos_time, pos_place, pos_accuracy FROM tmp_".$loc['ListID']." WHERE ContID=".$loc['ContID']." ORDER BY pos_date DESC LIMIT 50");
	for ( $i = 0; $i < count($result); $i++ )
	{
		$pos_tmpID = $result[$i]['tmpID'];
		$pos_lat = $result[$i]['pos_lat'];
		$pos_lng = $result[$i]['pos_lng'];
		$pos_time = $result[$i]['pos_time'];
		$pos_place = $result[$i]['pos_place'];
		$pos_accuracy = $result[$i]['pos_accuracy'];

		if ( $my_cnt == 5 )
		{
			$my_cnt = 0;
			$my_page++;
			$loc_list .= "</div><h3><a href=\"#\">Страница $my_page (".date('d.m.Y', $pos_time).")</a></h3><div>";
		}

		if ( $my_flag == 1 )
		{
			$pos_lat1 = $pos_lat;
			$pos_lng1 = $pos_lng;
			$loc_list .= "<i>Текущее местоположение<br></i>";
			$my_flag = 0;
		}

		$loc_list .= "Место: $pos_place / Точность: $pos_accuracy м.) / Время: ".date('d.m.Y H:i:s', $pos_time)." (".getRelativeTime($pos_time).") / <u><a href=\"http://ab-log.ru/demo/map.php?lat=$pos_lat&lng=$pos_lng\" target=\"map\">Показать на карте</a></u><br><br>";

		$my_cnt++;
		//$loc_list .= "<a id=\"more\" href=\"#\">Еще</a><span id=\"more_cont\" style=\"display:none\">";

	}

	if ( !empty($my_cnt) )
	$loc_list .= "</div>";
	//$loc_list .= "</div>";

	echo "lat=$pos_lat1&lng=$pos_lng1;$loc_list";
}
*/

function getRelativeTime($date) {
    $diff = time() - $date;
    if ($diff<60)
        return $diff . " сек. назад";
    $diff = round($diff/60);
    if ($diff<60)
        return $diff . " мин. назад";
    $diff = round($diff/60);
    if ($diff<24)
        return $diff . " ч. назад";
    $diff = round($diff/24);
    if ($diff<7)
        return $diff . " дн. назад";
    $diff = round($diff/7);
    if ($diff<4)
        return $diff . " нед. назад";
    return "on " . date("F j, Y", strtotime($date));
}


?>