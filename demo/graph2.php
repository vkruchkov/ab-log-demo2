<?
/*
* Copyright (c) 2013, Andrey_B
* http://ab-log.ru
* Подробнее см. LICENSE.txt или http://www.gnu.org/licenses/
*/

if ( !isset($_GET['dev_cl']) )
exit;

DEFINE("TTF_DIR","fonts/");
require_once("ab-cms/class/main_class.php");
$ab = new cms_lib();

include ("libs/jpgraph/jpgraph.php");
include ("libs/jpgraph/jpgraph_line.php");

$my_page = $ab->doc_read($ab->get_id($_GET['dev_cl']));

if ( !isset($_GET['temp_date']) )
{
	$temp_date = "NOW() - INTERVAL 1 HOUR";
	$date_format = "DATE_FORMAT(el_date, '%H:%i')";
}
elseif ( $_GET['temp_date'] == "day" )
{
	$temp_date = "(NOW() - INTERVAL 1 DAY)";
	$date_format = "DATE_FORMAT(el_date, '%H.%i')";
}
elseif ( $_GET['temp_date'] == "month" )
{
	$temp_date = "(NOW() - INTERVAL 1 MONTH)";
	$date_format = "DATE_FORMAT(el_date, '%d.%m')";
}
else
$temp_date = $_GET['temp_date'];
	
if ( !isset($_GET['temp_date_to']) )
$temp_date_to = "NOW()";
else
$temp_date_to = $_GET['temp_date_to'];

$n = 0;
$y_min = 0;
//$sql="	SELECT dev_value, DATE_FORMAT(dev_date, '%d.%m.%Y')

if ( file_exists("demo.txt" ) )
$where = "el_date>='2013-05-10 20:00:00' AND el_date<='2013-05-10 21:00:00'";
else
$where = "el_date>=$temp_date AND el_date<=$temp_date_to";

$result = $ab->select("SELECT el_val0, $date_format el_date_f, el_val1, el_val2, el_val3 FROM tmp_".$my_page['ListID']." WHERE ContID=".$my_page['ContID']." AND $where ORDER BY el_date");
for ( $i = 0; $i < count($result); $i++ )
{
	if ( $result[$i]['el_val0'] > 0 )
	$sum_flag = 1;

	$ydata[$n] = $result[$i]['el_val0'];
	$ydata1[$n] = $result[$i]['el_val1'];
	$ydata2[$n] = $result[$i]['el_val2'];
	$ydata3[$n] = $result[$i]['el_val3'];

	if ( $y_min > $result[$i]['el_val0'] )
	$y_min = $result[$i]['el_val0'];;
	if ( $y_min > $result[$i]['el_val1'] )
	$y_min = $result[$i]['el_val1'];
	if ( $y_min > $result[$i]['el_val2'] )
	$y_min = $result[$i]['el_val2'];
	if ( $y_min > $result[$i]['el_val3'] )
	$y_min = $result[$i]['el_val3'];

	$xdata[$n] = $result[$i]['el_date_f'];
	$n++;

}

if ( $n > 1 )
{
	$my_interval = ceil($n / 30);

	// Create the graph. These two calls are always required
	$graph = new Graph(450,200,"auto");
	$graph->SetScale("textlin");

	$graph->SetMarginColor('white'); 
	$graph->SetFrame(true,'#B3BCCB', 1); 
	$graph->SetTickDensity(TICKD_DENSE);
	$graph->img->SetMargin(50,20,20,60);
	$graph->title->SetMargin(10);
	$graph->xaxis->SetTickLabels($xdata);
	$graph->xaxis->SetLabelAngle(90);
	$graph->xaxis->SetPos('min');
	$graph->xaxis->SetTextTickInterval($my_interval);

	// Create the linear plot
	$lineplot=new LinePlot($ydata);
	$lineplot1=new LinePlot($ydata1);
	$lineplot2=new LinePlot($ydata2);
	$lineplot3=new LinePlot($ydata3);

	// Add the plot to the graph
	if ( $sum_flag == 1 )
	$graph->Add($lineplot);
	$graph->Add($lineplot1);
	$graph->Add($lineplot2);
	$graph->Add($lineplot3);

	$lineplot->SetColor("#000000");
	$lineplot1->SetColor("#a3091f");
	$lineplot2->SetColor("#237c0b");
	$lineplot3->SetColor("#0a01b3");

	$lineplot->SetWeight(1);
	$lineplot1->SetWeight(1);
	$lineplot2->SetWeight(1);
	$lineplot3->SetWeight(1);

	// Display the graph
	$graph->Stroke();
}
else
{
	header("Content-type: image/png");
	function hex2dec2($hex)
	{
		$color = str_replace('#', '', $hex);
		$ret = array( 'r' => hexdec(substr($color, 0, 2)), 'g' => hexdec(substr($color, 2, 2)), 'b' => hexdec(substr($color, 4, 2)));
		return $ret;
	}


	$sizemax = 50;
	$sizemin = 10;
	$bg_color = "#FFFFFF";
	$text_color = "#000000"; 


//	$Title = "Text!";
	$Title = "За этот период нет данных!";
	//$text = iconv('CP1251', 'UTF-8', $Title);
	$size = imagettfbbox(26, 0, "cadmin/fonts/DEFAULT.TTF", $text);
	$dx = abs($size[2] - $size[0]);
	$dy = abs($size[5] - $size[3]);
	$xpad = 9;
	$ypad = 9;
	$im = imagecreate($dx + $xpad, $dy + $ypad);
	$bgc = hex2dec2($bg_color);
	$bg = ImageColorAllocate($im, $bgc['r'], $bgc['g'], $bgc['b']);
	imagecolortransparent($im,$bg);
	$mac = hex2dec2($text_color);
	$main = ImageColorAllocate($im, $mac['r'], $mac['g'], $mac['b']);
	ImageTTFText($im, 26, 0, (int)($xpad / 2), $dy + (int)($ypad / 2)-1, $main, "cadmin/fonts/DEFAULT.TTF", $text);
	ImagePng($im);

}

?>
