<?
/*
* Copyright (c) 2013-2017, Andrey_B
* http://ab-log.ru
* Подробнее см. LICENSE.txt или http://www.gnu.org/licenses/
*/

echo "<div class=\"container_12\">";

	// Главная страница

	if ( $_REQUEST['p'] == "home" )
	{
		?>
		<scripT type="text/javascript">

		$(document).ready(
		function()
		{
			$.get("ab-data.php?p=dev&id=wh_temp_out", function(data)
			{
				$("#temp_out2").html(data + '&deg;');
				$("#temp_out21").html(data + '&deg;');
			});

			$.get("ab-data.php?p=el_p", function(data){$("#el_p").html('<strong>' + data + '</strong> Вт');});
			$.get("ab-data.php?p=alarm", function(data){$("#alarm").html(data);});
			$.get("ab-data.php?p=wind_home", function(data){$("#wind_home").html(data);});
			$.get("ab-data.php?p=wind_max", function(data){$("#wind_max").html(data);});
			$.get("ab-data.php?p=forecast_home", function(data){$("#forecast").html(data);});
			$.get("ab-data.php?p=events", function(data){$("#events").html(data);});
		});

		var tab_index = $("#tabs").tabs("option", "selected") + 1;
		var cur_mon = 1;

		$("#ui-tabs-" + tab_index).everyTime(50000, function(i) {

			$.get("ab-data.php?p=dev&id=wh_temp_out", function(data)
			{
				$("#temp_out2").html(data + '&deg;');
				$("#temp_out21").html(data + '&deg;');
			});

			$.get("ab-data.php?p=el_p", function(data){$("#el_p").html('<strong>' + data + '</strong> Вт');});
			$.get("ab-data.php?p=alarm", function(data){$("#alarm").html(data);});
			$.get("ab-data.php?p=wind_home", function(data){$("#wind_home").html(data);});
			$.get("ab-data.php?p=wind_max", function(data){$("#wind_max").html(data);});
			$('#monitor1-home').attr('src', 'img.php?n=snap/Monitor' + cur_mon + '.jpg&w=300&q=95&nc'+Math.random());


		});


		$('#ui-tabs-' + tab_index).everyTime(300000, function(i) {
			$.get("ab-data.php?p=forecast_home", function(data){$("#forecast").html(data);});
			$.get("ab-data.php?p=events", function(data){$("#events").html(data);});
			$('#g_wh_temp_out').attr('src', 'graph.php?dev_cl=wh_temp_out&'+Math.random()+'&temp_date=' + $('#g_wh_temp_out').attr('date_type'));
		});
	
		$('.set_period').click(function () {
			$('#g_' + $(this).attr("dev_type")).attr('date_type', $(this).attr("dev_period"));
			$('#g_' + $(this).attr("dev_type")).attr('src', 'graph.php?dev_cl='+$(this).attr("dev_type")+'&'+Math.random()+'&temp_date='+$(this).attr("dev_period"));
		 });

		$('#monitor1-home').click(function () {
			if ( cur_mon == 1 )
			cur_mon = 3;
			else
			cur_mon = 1;
			$('#monitor1-home').attr('src', 'img.php?n=snap/Monitor' + cur_mon + '.jpg&w=300&q=95&nc'+Math.random());
		 });

		//function sleep(time) { return new Promise((resolve) => setTimeout(resolve, time)); }

		$('#open-gate').click(function () {
			$.get("ab-data.php?p=open-gate", function(data){$("#buttons-info").html(data);});
			setTimeout(function() { $("#buttons-info").html(''); }, 3000 );
			//sleep(3000).then(() => { $("#buttons-info").html(''); });
		 });


		</script>
		<?

		echo "<div class=\"grid_4\">";
			echo "<img id=\"monitor1-home\" src=\"img.php?n=snap/Monitor1.jpg&w=300&q=95&".rand()."\" width=\"300\" height=\"240\">";
		echo "</div>";

		echo "<div class=\"grid_8\">";
			echo "<div class=\"ui-corner-all ui-state-default\" style=\"overflow: hidden\">";
					
				echo "<div style=\"width:300px;height:240px;float:left;overflow: hidden\">";
					echo "<div style=\"margin:5px;height:100px\"\">Температура на улице";
						echo "<div style=\"position:relative;top:12px;left:10px\"><img src=\"img/30.png\"></div>";
						echo "<div id=\"temp_out2\" style=\"font-size:28pt;font-weight:600;color: #251dc6;float:right;position:relative;top:-38px;right:40px\"></div>";
					echo "</div>";

					echo "<div style=\"margin:5px\">Ветер";
						echo "<div style=\"position:relative;top:20px;left:20px\"><img src=\"img/wind.png\"></div>";
						echo "<div style=\"float:left;position:relative;top:-38px;left:130px\">";
							echo "<div id=\"wind_home\" style=\"font-size:10pt;font-weight:600;color: #251dc6;\"></div>";
							echo "<div id=\"wind_max\"></div>";
						echo "</div>";
					echo "</div>";

				echo "</div>";

				echo "<div style=\"width:300px;height:240px;float:left;overflow: hidden\">";
					echo "<div style=\"margin:5px\">Прогноз</div>";
					echo "<div id=\"forecast\"></div>";
				echo "</div>";

			echo "</div>";
		echo "</div>";

		echo "<div class=\"clear\"></div>";

		echo "<br>";

		echo "<div class=\"grid_12\">";
			echo "<div class=\"ui-widget-content ui-corner-all ab\">";
				echo "<span id=\"alarm\"></span>";
				echo "Текущее потребление электроэнергии: ";
				echo "<span id=\"el_p\"></span>";
			echo "</div>";
		echo "</div>";

		echo "<div class=\"clear\"></div>";

		echo "<br>";

		echo "<div class=\"grid_6\">";
			echo "<div class=\"ui-widget-content ui-corner-all ab-graph\">";
				echo "<div id=\"events\"></div>";
			echo "</div>";
		echo "</div>";

		echo "<div class=\"grid_6\">";
			echo "<div class=\"ui-corner-all ui-state-default ab-graph\">";
				echo "<div style=\"float:left\">Температура на улице</div>";
				echo "<div id=\"temp_out21\" class=\"ab-graph-val\"></div>";
				echo "<img id=\"g_wh_temp_out\" date_type=\"\" src=\"graph.php?dev_cl=wh_temp_out&".rand()."\" height=\"200\">";
				echo "<a href=\"javascript:void(0)\" class=\"set_period\" dev_type=\"wh_temp_out\" dev_period=\"\">за сутки</a> | ";
				echo "<a href=\"javascript:void(0)\" class=\"set_period\" dev_type=\"wh_temp_out\" dev_period=\"month\">за месяц</a>";
			echo "</div>";
		echo "</div>";

		?>
		<div class="clear"></div>

		<br>

		<div class="grid_12">
			<div class="ui-widget-content ui-corner-all ab">
				<input type="button" id="open-gate" value="Открыть калитку">
				<span id="buttons-info"></span>
			</div>
		</div>
		<?

	}

	// Камеры видеонаблюдения

	if ( $_REQUEST['p'] == "cams" )
	{
		echo '<scripT type="text/javascript" src="js2/preview2.js"></script>';

		?>
		<scripT type="text/javascript">

		$(document).ready(
		function()
		{
			var tab_index = $('#tabs').tabs("option", "selected") + 1;
			$.get("ab-data.php?p=gate_j", function(data){$("#gate_j").html(data);});
			$.get("ab-data.php?p=unifi-users", function(data){$("#unifi-users").html(data);});

			$('#ui-tabs-' + tab_index).everyTime(50000, function(i) {
				$('#monitor1').attr('src', 'img.php?n=snap/Monitor1.jpg&w=460&q=95&nc'+Math.random());
				$('#monitor2').attr('src', 'img.php?n=snap/Monitor2.jpg&w=460&q=368&Photo_Quality=95&nc'+Math.random());
				$('#monitor3').attr('src', 'img.php?n=snap/Monitor3.jpg&w=460&q=95&nc'+Math.random());
				//$('#monitor4').attr('src', 'img.php?n=snap/Monitor4.jpg&w=460&q=95&nc'+Math.random());
				$.get("ab-data.php?p=gate_j", function(data){$("#gate_j").html(data);});
				$.get("ab-data.php?p=unifi-users", function(data){$("#unifi-users").html(data);});
			});

		});

		</script>
		<?

		echo "<div class=\"grid_6\">";
			echo "<img id=\"monitor1\" src=\"img.php?n=snap/Monitor1.jpg&w=460&q=95&".rand()."\" width=\"460\" height=\"368\">";
		echo "</div>";

		echo "<div class=\"grid_6\">";
			echo "<img id=\"monitor3\" src=\"img.php?n=snap/Monitor3.jpg&w=460&q=95&".rand()."\" width=\"460\" height=\"368\">";
		echo "</div>";

		echo "<div class=\"clear\"></div>";

		echo "<br>";

		echo "<div class=\"grid_6\">";
			echo "<div class=\"ui-corner-all ui-state-default\">";
				echo "<span style=\"display:inline-block;\" class=\"ui-icon ui-icon-play\"></span> ";
				echo "<a href=\"/zm/index.php?view=watch&mid=1\" target=\"_blank\">Смотреть он-лайн</a>";
			echo "</div>";
		echo "</div>";

		echo "<div class=\"grid_6\">";
			echo "<div class=\"ui-corner-all ui-state-default\">";
				echo "<span style=\"display:inline-block;\" class=\"ui-icon ui-icon-play\"></span> ";
				echo "<a href=\"/zm/index.php?view=watch&mid=3\" target=\"_blank\">Смотреть он-лайн</a>";
			echo "</div>";
		echo "</div>";

		echo "<div class=\"clear\"></div>";

		echo "<br>";

		echo "<div class=\"grid_6\">";
			echo "<img id=\"monitor2\" src=\"img.php?n=snap/Monitor2.jpg&w=460&h=368&q=95&".rand()."\" width=\"460\" height=\"368\">";
		echo "</div>";

		echo "<div class=\"grid_6\">";
			echo "<div id=\"gate_j\" class=\"ui-corner-all ui-state-default ab\" style=\"height:358px;\">";
			echo "</div>";
		echo "</div>";

		echo "<div class=\"clear\"></div>";

		echo "<br>";

		echo "<div class=\"grid_6\">";
			echo "<div class=\"ui-corner-all ui-state-default\">";
				echo "<span style=\"display:inline-block;\" class=\"ui-icon ui-icon-play\"></span> ";
				echo "<a href=\"/zm/index.php?view=watch&mid=2\" target=\"_blank\">Смотреть он-лайн</a>";
			echo "</div>";
		echo "</div>";

		echo "<div class=\"clear\"></div>";

		echo "<br>";

		echo "<div class=\"grid_6\">";
			echo "<div id=\"unifi-users\" class=\"ui-corner-all ui-state-default ab\">";
			echo "</div>";
		echo "</div>";

		echo "<div class=\"clear\"></div>";

		echo "<br>";


		/*
		echo "<div class=\"grid_6\">";
			echo "<div class=\"ui-corner-all ui-state-default ab\">";
				echo "Микрофон в котельной:<br>";
				echo "<object width=\"180\" height=\"60\">";
					echo "<param name=\"movie\" value=\"libs/ffmp3/ffmp3-config.swf\" />";
					echo "<param name=\"flashvars\" value=\"url=http://".$_SERVER['SERVER_NAME'].":8000/mic1&codec=ogg;&title=Mic1&skin=ffmp3-mcclean.xml\" />";
					echo "<param name=\"wmode\" value=\"window\" />";
					echo "<param name=\"scale\" value=\"noscale\" />";
					echo "<embed src=\"libs/ffmp3/ffmp3-config.swf\" flashvars=\"url=http://".$_SERVER['SERVER_NAME'].":8000/mic1&codec=ogg&volume=100;&title=Mic1&skin=libs/ffmp3/ffmp3-mcclean.xml\" width=\"180\" height=\"60\" scale=\"noscale\" wmode=\"window\" type=\"application/x-shockwave-flash\" />";
				echo "</object>";
			echo "</div>";
		echo "</div>";
		*/

		/*
		echo "<div class=\"grid_6\">";
			echo "<img id=\"monitor4\" src=\"img.php?n=snap/Monitor4.jpg&w=460&q=95&".rand()."\" width=\"460\" height=\"368\">";
		echo "</div>";

		echo "<div class=\"clear\"></div>";

		echo "<br>";

		echo "<div class=\"grid_6\">";
			echo "<div>&nbsp;</div>";
		echo "</div>";

		echo "<div class=\"grid_6\">";
			echo "<div class=\"ui-corner-all ui-state-default\">";
				echo "<span style=\"display:inline-block;\" class=\"ui-icon ui-icon-play\"></span> ";
				if ( $_SERVER['SERVER_ADDR'] == "192.168.0.250" )
				echo "<a href=\"http://192.168.0.51/index1.htm\" target=\"_blank\">Смотреть он-лайн</a>";
				else
				echo "<a href=\"http://".$_SERVER['SERVER_ADDR']."/index1.htm:8554\" target=\"_blank\">Смотреть он-лайн</a>";
			echo "</div>";
		echo "</div>";
		*/

	}

	// Управление освещением

	if ( $_REQUEST['p'] == "light" )
	{
		?>
		<script>$(function() { $( "#accordion" ).accordion(); });</script>

		<scripT type="text/javascript">
			$(document).ready(
			function()
			{
				$.get("ab-data.php?p=keys2&floor=1", function(data){$(".cnv1").html(data);});
				$.get("ab-data.php?p=keys2&floor=2", function(data){$(".cnv2").html(data);});

                                //var flag = false;

				$(document).unbind("touchstart click").on("touchstart click", ".lamp", function () {

					//event.stopPropagation();
					//event.preventDefault();
					//if (!flag)
					{
						//flag = true;
						//setTimeout(function(){ flag = false; }, 100);

						var lamp = $(this);
						var label = lamp.attr("label");
						var action = 0;
						if ( lamp.hasClass("off") )
						{
							action = 1;
							lamp.removeClass("off").addClass("on");
						}
						else
						{
							action = 0;
							lamp.removeClass("on").addClass("off");
						}
	                                        //$( event.toElement ).one('touchstart', function(e){ e.stopImmediatePropagation(); } );
	                                        //$( event.toElement ).one('click', function(e){ e.stopImmediatePropagation(); } );

						$.get("key.php?key_label=" +label + "&key_pio=" + action, function(data){ 
							$.get("ab-data.php?p=keys", function(data)
							{
							//$("#keys").html(data);
							});
						});

					}
				 });
			});

		</script>                                                               

		<style>
			.lamp{position: absolute;height: 50px;width: 50px;border-radius: 50%;}
			.on{border: 2px solid #cb2424; background-color: #ffd900;box-shadow: 0px 0px 100px 100px rgba(255, 191, 0, 0.3)}
			.off{border: 2px solid #cb2424}
		</style>


		<div id="accordion">
			<h3>Этаж 1</h3>
			<div>
				<style>
				.cnv1{height: 800px;width: 900px; position: relative;background:url(/plans/plan-1.jpg) no-repeat 0 0;}
				</style>

				<div class="cnv1" floor="1"></div>
			</div>

			<h3>Этаж 2</h3>
			<div>
				<style>
				.cnv2{height: 800px;width: 900px; position: relative;background:url(/plans/plan-2.jpg) no-repeat 0 0;}
				</style>

				<div class="cnv2" floor="2"></div>
			</div>


			<h3>Списком</h3>
			<div>
				<scripT type="text/javascript">

				$(document).ready(
				function()
				{
					$.get("ab-data.php?p=keys", function(data){$("#keys").html(data);});
					$.get("ab-data.php?p=keys_hist", function(data){$("#keys_hist").html(data);});
	
				});

				var tab_index = $('#tabs').tabs("option", "selected") + 1;
		
				$('#ui-tabs-' + tab_index).everyTime(50000, function(i) {
					$.get("ab-data.php?p=keys", function(data){$("#keys").html(data);});
					$.get("ab-data.php?p=keys_hist", function(data){$("#keys_hist").html(data);});
				});

				</script>

				<div class="grid_7">
					<div id="keys" class="ui-corner-all ui-state-default"></div>
				</div>

				<div class="grid_4">
					<div id="keys_hist" class="ui-corner-all ui-state-default ab"></div>
				</div>

			</div>
		</div>
		<?
	}

	// Погода

	if ( $_REQUEST['p'] == "weather" )
	{
		?>
		<scripT type="text/javascript">

		$(document).ready(
		function()
		{
			$.get("ab-data.php?p=dev&id=wh_temp_out", function(data){$("#wh_temp_out2").html(data + '&deg;');});
			$.get("ab-data.php?p=dev&id=bmp180", function(data){$("#bmp180").html(data + ' мм. рт. ст.');});
			$.get("ab-data.php?p=weather", function(data){$("#weather").html(data);});
			$.get("ab-data.php?p=graph_rain", function(data){$("#wh_rain").html(data);});
		});


		var tab_index = $('#tabs').tabs("option", "selected") + 1;

		$('#ui-tabs-' + tab_index).everyTime(50000, function(i) {
			$.get("ab-data.php?p=dev&id=wh_temp_out", function(data){$("#wh_temp_out2").html(data + '&deg;');});
		});
		

		//$('#ui-tabs-' + tab_index).everyTime(180000, function(i) {
		$('#ui-tabs-' + tab_index).everyTime(3600000, function(i) {
			//$.get("ab-data.php?p=weather", function(data){$("#weather").html(data);});
			$('#weather_forecast').attr('src', 'img.php?n=weather.png&crop_w=15&crop_h=315&w=720&h=405&nc'+Math.random());
		});

		$('#ui-tabs-' + tab_index).everyTime(600000, function(i) {
			$('#g_wh_temp_out').attr('src', 'graph.php?dev_cl=wh_temp_out&'+Math.random()+'&temp_date=' + $('#g_wh_temp_out').attr('date_type'));
			$('#g_bmp180').attr('src', 'graph.php?dev_cl=bmp180&'+Math.random()+'&temp_date=' + $('#g_bmp180').attr('date_type'));

		});

		$('.set_period').click(function () {
			$('#g_' + $(this).attr("dev_type")).attr('date_type', $(this).attr("dev_period"));
			$('#g_' + $(this).attr("dev_type")).attr('src', 'graph.php?dev_cl='+$(this).attr("dev_type")+'&'+Math.random()+'&temp_date='+$(this).attr("dev_period"));
		 });

		$('#ui-tabs-' + tab_index).everyTime(3600000, function(i) {
			$.get("ab-data.php?p=graph_rain", function(data){$("#wh_rain").html(data);});
		});

		</script>
		<?

		echo "<div class=\"grid_6\">";
			echo "<div id=\"weather\" class=\"ui-corner-all ui-state-default ab-graph\"></div>";
		echo "</div>";

		echo "<div class=\"grid_6\">";
			echo "<div class=\"ui-corner-all ui-state-default ab-graph\">";
			echo "<div id=\"wh_rain\" class=\"ab-graph-val\"></div>";
			echo "</div>";
		echo "</div>";

		echo "<div class=\"clear\"></div>";
		echo "<br>";

		echo "<div class=\"grid_6\">";
			echo "<div class=\"ui-corner-all ui-state-default ab-graph\">";

				echo "<div style=\"float:left\">Температура на улице</div>";
				echo "<div id=\"wh_temp_out2\" class=\"ab-graph-val\"></div>";
				echo "<img id=\"g_wh_temp_out\" date_type=\"\" src=\"graph.php?dev_cl=wh_temp_out&".rand()."\" height=\"200\">";
				echo "<a href=\"javascript:void(0)\" class=\"set_period\" dev_type=\"wh_temp_out\" dev_period=\"\">за сутки</a> | ";
				echo "<a href=\"javascript:void(0)\" class=\"set_period\" dev_type=\"wh_temp_out\" dev_period=\"month\">за месяц</a>";
			echo "</div>";
		echo "</div>";

		echo "<div class=\"grid_6\">";
			echo "<div class=\"ui-corner-all ui-state-default ab-graph\">";

				echo "<div style=\"float:left\">Атмосферное давление</div>";
				echo "<div id=\"bmp180\" class=\"ab-graph-val\"></div>";
				echo "<img id=\"g_bmp180\" date_type=\"\" src=\"graph.php?dev_cl=bmp180&".rand()."\" height=\"200\">";
				echo "<a href=\"javascript:void(0)\" class=\"set_period\" dev_type=\"bmp180\" dev_period=\"\">за сутки</a> | ";
				echo "<a href=\"javascript:void(0)\" class=\"set_period\" dev_type=\"bmp180\" dev_period=\"month\">за месяц</a>";
			echo "</div>";
		echo "</div>";

		echo "<div class=\"clear\"></div>";
		echo "<br>";

		echo "<div class=\"grid_6\">";
			echo "<div class=\"ui-corner-all ui-state-default ab\">";
				echo "<h3>Прогноз погоды</h3>";
				//echo "<div id=\"weather\" style=\"font-size:0.9em\">";
				echo "<img id=\"weather_forecast\" src=\"img.php?n=weather.png&crop_w=15&crop_h=315&w=720&h=405&".rand()."\" width=\"445\" height=\"250\">";
				//echo file_get_contents("weather.dat");
				echo "</div>";
			echo "</div>";
		echo "</div>";
	}

	// Климат в доме

	if ( $_REQUEST['p'] == "climate" )
	{

		?>
		<scripT type="text/javascript">

		$(document).ready(
		function()
		{
			$.get("ab-data.php?p=heat_stat_main", function(data){$("#heat_stat_main").html(data);});
			$.get("ab-data.php?p=heat_stat_circ", function(data){$("#heat_stat_circ").html(data);});
			$.get("ab-data.php?p=heat_stat_log", function(data){$("#heat_stat_log").html(data);});

			$.get("ab-data.php?p=dev&id=wh_temp_out", function(data){$("#c_temp_out2").html(data + '&deg;');});
			$.get("ab-data.php?p=dev&id=temp_in", function(data){$("#temp_in").html(data + '&deg;');});
			$.get("ab-data.php?p=dev&id=temp_in_sf_md", function(data){$("#temp_in_sf_md").html(data + '&deg;');});
			$.get("ab-data.php?p=dev&id=temp_vas", function(data){$("#temp_vas").html(data + '&deg;');});
			$.get("ab-data.php?p=dev&id=hum_vas", function(data){$("#hum_vas").html(data + '&deg;');});
			$.get("ab-data.php?p=dev&id=temp_mar", function(data){$("#temp_mar").html(data + '&deg;');});
			$.get("ab-data.php?p=dev&id=hum_mar", function(data){$("#hum_mar").html(data + '&deg;');});
			$.get("ab-data.php?p=dev&id=temp_under", function(data){$("#temp_under").html(data + '&deg;');});
			$.get("ab-data.php?p=dev&id=temp_roof", function(data){$("#temp_roof").html(data + '&deg;');});
			$.get("ab-data.php?p=dev&id=temp_heat_tp_in", function(data){$("#temp_heat_tp_in").html(data + '&deg;');});
			$.get("ab-data.php?p=dev&id=temp_heat_main_in", function(data){$("#temp_heat_main_in").html(data + '&deg;');});
			$.get("ab-data.php?p=dev&id=temp_heat_under", function(data){$("#temp_heat_under").html(data + '&deg;');});
			$.get("ab-data.php?p=dev&id=temp_tank", function(data){$("#temp_tank").html(data + '&deg;');});

		});

		var tab_index = $('#tabs').tabs("option", "selected") + 1;

		$('#ui-tabs-' + tab_index).everyTime(600000, function(i) {
			$('#g_wh_temp_out').attr('src', 'graph.php?dev_cl=wh_temp_out&'+Math.random()+'&temp_date=' + $('#g_wh_temp_out').attr('date_type'));
			$('#g_temp_in').attr('src', 'graph.php?dev_cl=temp_in&'+Math.random()+'&temp_date=' + $('#g_temp_in').attr('date_type'));
			$('#g_temp_vas').attr('src', 'graph.php?dev_cl=temp_vas&'+Math.random()+'&temp_date=' + $('#g_temp_vas').attr('date_type'));
			$('#g_hum_vas').attr('src', 'graph.php?dev_cl=hum_vas&'+Math.random()+'&temp_date=' + $('#g_hum_vas').attr('date_type'));
			$('#g_temp_mar').attr('src', 'graph.php?dev_cl=temp_mar&'+Math.random()+'&temp_date=' + $('#g_temp_mar').attr('date_type'));
			$('#g_hum_mar').attr('src', 'graph.php?dev_cl=hum_mar&'+Math.random()+'&temp_date=' + $('#g_hum_mar').attr('date_type'));
			$('#g_temp_in_sf_md').attr('src', 'graph.php?dev_cl=temp_in_sf_md&'+Math.random()+'&temp_date=' + $('#g_temp_in_sf_md').attr('date_type'));
			$('#g_temp_under').attr('src', 'graph.php?dev_cl=temp_under&'+Math.random()+'&temp_date=' + $('#g_temp_under').attr('date_type'));
			$('#g_temp_roof').attr('src', 'graph.php?dev_cl=temp_roof&'+Math.random()+'&temp_date=' + $('#g_temp_roof').attr('date_type'));
			$('#g_temp_heat_tp_in').attr('src', 'graph.php?dev_cl=temp_heat_tp_in&'+Math.random()+'&temp_date=' + $('#g_temp_heat_tp_in').attr('date_type'));
			$('#g_temp_heat_main_in').attr('src', 'graph.php?dev_cl=temp_heat_main_in&'+Math.random()+'&temp_date=' + $('#g_temp_heat_main_in').attr('date_type'));
			$('#g_temp_heat_under').attr('src', 'graph.php?dev_cl=temp_heat_under&'+Math.random()+'&temp_date=' + $('#g_temp_heat_under').attr('date_type'));
			$('#g_temp_tank').attr('src', 'graph.php?dev_cl=temp_tank&'+Math.random()+'&temp_date=' + $('#g_temp_tank').attr('date_type'));
		});

		$('#ui-tabs-' + tab_index).everyTime(50000, function(i) {
			$.get("ab-data.php?p=heat_stat_main", function(data){$("#heat_stat_main").html(data);});
			$.get("ab-data.php?p=heat_stat_circ", function(data){$("#heat_stat_circ").html(data);});
			$.get("ab-data.php?p=heat_stat_log", function(data){$("#heat_stat_log").html(data);});

			$.get("ab-data.php?p=dev&id=wh_temp_out", function(data){$("#c_temp_out2").html(data + '&deg;');});
			$.get("ab-data.php?p=dev&id=temp_in", function(data){$("#temp_in").html(data + '&deg;');});
			$.get("ab-data.php?p=dev&id=temp_vas", function(data){$("#temp_vas").html(data + '&deg;');});
			$.get("ab-data.php?p=dev&id=hum_vas", function(data){$("#hum_vas").html(data + '&deg;');});
			$.get("ab-data.php?p=dev&id=temp_mar", function(data){$("#temp_mar").html(data + '&deg;');});
			$.get("ab-data.php?p=dev&id=hum_mar", function(data){$("#hum_mar").html(data + '&deg;');});
			$.get("ab-data.php?p=dev&id=temp_in_sf_md", function(data){$("#temp_in_sf_md").html(data + '&deg;');});
			$.get("ab-data.php?p=dev&id=temp_under", function(data){$("#temp_under").html(data + '&deg;');});
			$.get("ab-data.php?p=dev&id=temp_roof", function(data){$("#temp_roof").html(data + '&deg;');});
			$.get("ab-data.php?p=dev&id=temp_heat_tp_in", function(data){$("#temp_heat_tp_in").html(data + '&deg;');});
			$.get("ab-data.php?p=dev&id=temp_heat_main_in", function(data){$("#temp_heat_main_in").html(data + '&deg;');});
			$.get("ab-data.php?p=dev&id=temp_heat_under", function(data){$("#temp_heat_under").html(data + '&deg;');});
			$.get("ab-data.php?p=dev&id=temp_tank", function(data){$("#temp_tank").html(data + '&deg;');});


		});

		$('.set_period').click(function () {
			$('#g_' + $(this).attr("dev_type")).attr('date_type', $(this).attr("dev_period"));
			$('#g_' + $(this).attr("dev_type")).attr('src', 'graph.php?dev_cl='+$(this).attr("dev_type")+'&'+Math.random()+'&temp_date='+$(this).attr("dev_period"));
		 });

		</script>
		<?

		echo "<div class=\"grid_6\">";
			echo "<div id=\"heat_stat_main\" class=\"ui-corner-all ui-state-default ab-graph\">";
			echo "</div>";
		echo "</div>";

		echo "<div class=\"grid_6\">";
			echo "<div class=\"ui-corner-all ui-state-default ab-graph\">";

				echo "<div style=\"float:left\">Температура на улице</div>";
				echo "<div id=\"c_temp_out2\" class=\"ab-graph-val\"></div>";
				echo "<img id=\"g_wh_temp_out\" date_type=\"\" src=\"graph.php?dev_cl=wh_temp_out&".rand()."\" height=\"200\">";
				echo "<a href=\"javascript:void(0)\" class=\"set_period\" dev_type=\"wh_temp_out\" dev_period=\"\">за сутки</a> | ";
				echo "<a href=\"javascript:void(0)\" class=\"set_period\" dev_type=\"wh_temp_out\" dev_period=\"month\">за месяц</a>";

			echo "</div>";
		echo "</div>";

		echo "<div class=\"clear\"></div>";
		echo "<br>";

		echo "<div class=\"grid_6\">";
			echo "<div class=\"ui-corner-all ui-state-default ab-graph\">";

				echo "<div style=\"float:left\">Температура на 1 этаже</div>";
				echo "<div id=\"temp_in\" class=\"ab-graph-val\"></div>";
				echo "<img id=\"g_temp_in\" date_type=\"\" src=\"graph.php?dev_cl=temp_in&".rand()."\" height=\"200\">";
				echo "<a href=\"javascript:void(0)\" class=\"set_period\" dev_type=\"temp_in\" dev_period=\"\">за сутки</a> | ";
				echo "<a href=\"javascript:void(0)\" class=\"set_period\" dev_type=\"temp_in\" dev_period=\"month\">за месяц</a>";


			echo "</div>";
		echo "</div>";

		echo "<div class=\"grid_6\">";
			echo "<div class=\"ui-corner-all ui-state-default ab-graph\">";

				echo "<div style=\"float:left\">Температура на 2 этаже</div>";
				echo "<div id=\"temp_in_sf_md\" class=\"ab-graph-val\"></div>";
				echo "<img id=\"g_temp_in_sf_md\" date_type=\"\" src=\"graph.php?dev_cl=temp_in_sf_md&".rand()."\" height=\"200\">";
				echo "<a href=\"javascript:void(0)\" class=\"set_period\" dev_type=\"temp_in_sf_md\" dev_period=\"\">за сутки</a> | ";
				echo "<a href=\"javascript:void(0)\" class=\"set_period\" dev_type=\"temp_in_sf_md\" dev_period=\"month\">за месяц</a>";

			echo "</div>";
		echo "</div>";

		echo "<div class=\"clear\"></div>";
		echo "<br>";


		echo "<div class=\"grid_6\">";
			echo "<div class=\"ui-corner-all ui-state-default ab-graph\">";

				echo "<div style=\"float:left\">Температура у Василисы</div>";
				echo "<div id=\"temp_vas\" class=\"ab-graph-val\"></div>";
				echo "<img id=\"g_temp_vas\" date_type=\"\" src=\"graph.php?dev_cl=temp_vas&".rand()."\" height=\"200\">";
				echo "<a href=\"javascript:void(0)\" class=\"set_period\" dev_type=\"temp_vas\" dev_period=\"\">за сутки</a> | ";
				echo "<a href=\"javascript:void(0)\" class=\"set_period\" dev_type=\"temp_vas\" dev_period=\"month\">за месяц</a>";


			echo "</div>";
		echo "</div>";

		echo "<div class=\"grid_6\">";
			echo "<div class=\"ui-corner-all ui-state-default ab-graph\">";

				echo "<div style=\"float:left\">Влажность у Василисы</div>";
				echo "<div id=\"hum_vas\" class=\"ab-graph-val\"></div>";
				echo "<img id=\"g_hum_vas\" date_type=\"\" src=\"graph.php?dev_cl=hum_vas&".rand()."\" height=\"200\">";
				echo "<a href=\"javascript:void(0)\" class=\"set_period\" dev_type=\"hum_vas\" dev_period=\"\">за сутки</a> | ";
				echo "<a href=\"javascript:void(0)\" class=\"set_period\" dev_type=\"hum_vas\" dev_period=\"month\">за месяц</a>";

			echo "</div>";
		echo "</div>";

		echo "<div class=\"clear\"></div>";
		echo "<br>";

		echo "<div class=\"grid_6\">";
			echo "<div class=\"ui-corner-all ui-state-default ab-graph\">";

				echo "<div style=\"float:left\">Температура у Маши</div>";
				echo "<div id=\"temp_mar\" class=\"ab-graph-val\"></div>";
				echo "<img id=\"g_temp_mar\" date_type=\"\" src=\"graph.php?dev_cl=temp_mar&".rand()."\" height=\"200\">";
				echo "<a href=\"javascript:void(0)\" class=\"set_period\" dev_type=\"temp_mar\" dev_period=\"\">за сутки</a> | ";
				echo "<a href=\"javascript:void(0)\" class=\"set_period\" dev_type=\"temp_mar\" dev_period=\"month\">за месяц</a>";


			echo "</div>";
		echo "</div>";

		echo "<div class=\"grid_6\">";
			echo "<div class=\"ui-corner-all ui-state-default ab-graph\">";

				echo "<div style=\"float:left\">Влажность у Маши</div>";
				echo "<div id=\"hum_mar\" class=\"ab-graph-val\"></div>";
				echo "<img id=\"g_hum_mar\" date_type=\"\" src=\"graph.php?dev_cl=hum_mar&".rand()."\" height=\"200\">";
				echo "<a href=\"javascript:void(0)\" class=\"set_period\" dev_type=\"hum_mar\" dev_period=\"\">за сутки</a> | ";
				echo "<a href=\"javascript:void(0)\" class=\"set_period\" dev_type=\"hum_mar\" dev_period=\"month\">за месяц</a>";

			echo "</div>";
		echo "</div>";



		echo "<div class=\"clear\"></div>";
		echo "<br>";


		echo "<div class=\"grid_6\">";
			echo "<div class=\"ui-corner-all ui-state-default ab-graph\">";

				echo "<div style=\"float:left\">Температура в подвале</div>";
				echo "<div id=\"temp_under\" class=\"ab-graph-val\"></div>";
				echo "<img id=\"g_temp_under\" date_type=\"\" src=\"graph.php?dev_cl=temp_under&".rand()."\" height=\"200\">";
				echo "<a href=\"javascript:void(0)\" class=\"set_period\" dev_type=\"temp_under\" dev_period=\"\">за сутки</a> | ";
				echo "<a href=\"javascript:void(0)\" class=\"set_period\" dev_type=\"temp_under\" dev_period=\"month\">за месяц</a>";

			echo "</div>";
		echo "</div>";

		echo "<div class=\"grid_6\">";
			echo "<div class=\"ui-corner-all ui-state-default ab-graph\">";

				echo "<div style=\"float:left\">Температура на чердаке</div>";
				echo "<div id=\"temp_roof\" class=\"ab-graph-val\"></div>";
				echo "<img id=\"g_temp_roof\" date_type=\"\" src=\"graph.php?dev_cl=temp_roof&".rand()."\" height=\"200\">";
				echo "<a href=\"javascript:void(0)\" class=\"set_period\" dev_type=\"temp_roof\" dev_period=\"\">за сутки</a> | ";
				echo "<a href=\"javascript:void(0)\" class=\"set_period\" dev_type=\"temp_roof\" dev_period=\"month\">за месяц</a>";

			echo "</div>";
		echo "</div>";

		echo "<div class=\"clear\"></div>";
		echo "<br>";

		echo "<div class=\"grid_6\">";
			echo "<div class=\"ui-corner-all ui-state-default ab-graph\">";

				echo "<div style=\"float:left\">Температура ТП</div>";
				echo "<div id=\"temp_heat_tp_in\" class=\"ab-graph-val\"></div>";
				echo "<img id=\"g_temp_heat_tp_in\" date_type=\"\" src=\"graph.php?dev_cl=temp_heat_tp_in&".rand()."\" height=\"200\">";
				echo "<a href=\"javascript:void(0)\" class=\"set_period\" dev_type=\"temp_heat_tp_in\" dev_period=\"\">за сутки</a> | ";
				echo "<a href=\"javascript:void(0)\" class=\"set_period\" dev_type=\"temp_heat_tp_in\" dev_period=\"month\">за месяц</a>";

			echo "</div>";
		echo "</div>";

		echo "<div class=\"grid_6\">";
			echo "<div class=\"ui-corner-all ui-state-default ab-graph\">";

				echo "<div style=\"float:left\">Температура Основного контура</div>";
				echo "<div id=\"temp_heat_main_in\" class=\"ab-graph-val\"></div>";
				echo "<img id=\"g_temp_heat_main_in\" date_type=\"\" src=\"graph.php?dev_cl=temp_heat_main_in&".rand()."\" height=\"200\">";
				echo "<a href=\"javascript:void(0)\" class=\"set_period\" dev_type=\"temp_heat_main_in\" dev_period=\"\">за сутки</a> | ";
				echo "<a href=\"javascript:void(0)\" class=\"set_period\" dev_type=\"temp_heat_main_in\" dev_period=\"month\">за месяц</a>";

			echo "</div>";
		echo "</div>";

		echo "<div class=\"clear\"></div>";
		echo "<br>";

		echo "<div class=\"grid_6\">";
			echo "<div class=\"ui-corner-all ui-state-default ab-graph\">";

				echo "<div style=\"float:left\">Температура контура подвала</div>";
				echo "<div id=\"temp_heat_under\" class=\"ab-graph-val\"></div>";
				echo "<img id=\"g_temp_heat_under\" date_type=\"\" src=\"graph.php?dev_cl=temp_heat_under&".rand()."\" height=\"200\">";
				echo "<a href=\"javascript:void(0)\" class=\"set_period\" dev_type=\"temp_heat_under\" dev_period=\"\">за сутки</a> | ";
				echo "<a href=\"javascript:void(0)\" class=\"set_period\" dev_type=\"temp_heat_under\" dev_period=\"month\">за месяц</a>";

			echo "</div>";
		echo "</div>";

		echo "<div class=\"grid_6\">";

			echo "<div class=\"ui-corner-all ui-state-default ab-graph\">";

				echo "<div style=\"float:left\">Температура накопительной емкости</div>";
				echo "<div id=\"temp_tank\" class=\"ab-graph-val\"></div>";
				echo "<img id=\"g_temp_tank\" date_type=\"\" src=\"graph.php?dev_cl=temp_tank&".rand()."\" height=\"200\">";
				echo "<a href=\"javascript:void(0)\" class=\"set_period\" dev_type=\"temp_tank\" dev_period=\"\">за сутки</a> | ";
				echo "<a href=\"javascript:void(0)\" class=\"set_period\" dev_type=\"temp_tank\" dev_period=\"month\">за месяц</a>";

			echo '</div>';
		echo '</div>';

		echo "<div class=\"clear\"></div>";
		echo "<br>";

		echo "<div class=\"grid_6\">";
			echo "<div id=\"heat_stat_circ\" class=\"ui-corner-all ui-state-default ab\">";
			echo "</div>";
		echo "</div>";

		echo "<div class=\"grid_6\">";
			echo "<div id=\"heat_stat_log\" class=\"ui-corner-all ui-state-default ab\">";
			echo "</div>";
		echo "</div>";


	}

	// Системы

	if ( $_REQUEST['p'] == "systems" )
	{

		?>
		<scripT type="text/javascript">

		$(document).ready(
		function()
		{
			$.get("ab-data.php?p=el_cur", function(data){$("#el_cur").html(data);});
			//$.get("ab-data.php?p=water", function(data){$("#water").html(data);});
			//$.get("ab-data.php?p=watering", function(data){$("#watering").html(data);});

			$.get("ab-data.php?p=el_p", function(data){$("#el_p").html('<strong>' + data + '</strong> Вт');});
			//$.get("ab-data.php?p=dev&id=wp_in", function(data){$("#wp_in").html(data + ' атм.');});

		});

		var tab_index = $('#tabs').tabs("option", "selected") + 1;

		$('#ui-tabs-' + tab_index).everyTime(600000, function(i) {
			$('#g_el_p').attr('src', 'graph2.php?dev_cl=el_p&'+Math.random());
			$('#g_wp_in').attr('src', 'graph.php?dev_cl=wp_in&'+Math.random()+'&temp_date=' + $('#g_wp_in').attr('date_type'));

		});

		$('#ui-tabs-' + tab_index).everyTime(50000, function(i) {
			$.get("ab-data.php?p=el_p", function(data){$("#el_p").html('<strong>' + data + '</strong> Вт');});
		});

		$('#ui-tabs-' + tab_index).everyTime(300000, function(i) {
			$.get("ab-data.php?p=el_cur", function(data){$("#el_cur").html(data);});
			//$.get("ab-data.php?p=dev&id=wp_in", function(data){$("#wp_in").html(data + \' атм.\');});
			$.get("ab-data.php?p=water", function(data){$("#water").html(data);});
			$.get("ab-data.php?p=watering", function(data){$("#watering").html(data);});

		});

		$('.set_period').click(function () {
			$('#g_' + $(this).attr("dev_type")).attr('date_type', $(this).attr("dev_period"));
			$('#g_' + $(this).attr("dev_type")).attr('src', 'graph.php?dev_cl='+$(this).attr("dev_type")+'&'+Math.random()+'&temp_date='+$(this).attr("dev_period"));
		 });

		</script>
		
		<?
		
		echo "<div class=\"grid_6\">";
			echo "<div id=\"el_cur\" class=\"ui-corner-all ui-state-default ab\">";
			echo "</div>";
		echo "</div>";

		echo "<div class=\"grid_6\">";
			echo "<div class=\"ui-corner-all ui-state-default ab-graph\">";

				//echo "<h3>Общее потребление</h3>";
				//echo "<img id=\"g2_el_p\" src=\"/graph2.php?dev_cl=el_p&".rand()."\">";

				echo "<div style=\"float:left\">Общее потребление</div>";
				echo "<div id=\"el_p\" class=\"ab-graph-val\"></div>";
				echo "<img id=\"g_el_p\" date_type=\"\" src=\"graph2.php?dev_cl=el_p&".rand()."\" height=\"200\">";
				//echo "<a href=\"javascript:void(0)\" class=\"set_period\" dev_type=\"el_p\" dev_period=\"\">за сутки</a> | ";
				//echo "<a href=\"javascript:void(0)\" class=\"set_period\" dev_type=\"el_p\" dev_period=\"month\">за месяц</a>";

			echo "</div>";
		echo "</div>";

		/*
		echo "<div class=\"clear\"></div>";
		echo "<br>";

		echo "<div class=\"grid_6\">";
			echo "<div id=\"water\" class=\"ui-corner-all ui-state-default ab\">";
			echo "</div>";
		echo "</div>";

		echo "<div class=\"grid_6\">";
			echo "<div class=\"ui-corner-all ui-state-default ab-graph\">";
				//echo "<h3>Давление в водопроводной сети</h3>";
				//echo "<img id=\"g2_wp_in\" src=\"graph.php?dev_cl=wp_in&".rand()."\">";

				//echo "<div style=\"float:left\">Давление в водопроводной сети</div>";
				//echo "<div id=\"wp_in\" class=\"ab-graph-val\"></div>";
				//echo "<img id=\"g_wp_in\" date_type=\"\" src=\"graph.php?dev_cl=wp_in&".rand()."\" height=\"200\">";
				//echo "<a href=\"javascript:void(0)\" class=\"set_period\" dev_type=\"wp_in\" dev_period=\"\">за сутки</a> | ";
				//echo "<a href=\"javascript:void(0)\" class=\"set_period\" dev_type=\"wp_in\" dev_period=\"month\">за месяц</a>";

			echo "</div>";
		echo "</div>";
		*/

		/*
		echo "<div class=\"clear\"></div>";
		echo "<br>";

		echo "<div class=\"grid_6\">";
			echo "<div id=\"watering\" class=\"ui-corner-all ui-state-default ab\">";
			echo "</div>";
		echo "</div>";
		*/

		/*
		echo "<div class=\"grid_6\">";
			echo "<div class=\"ui-corner-all ui-state-default ab\">";
				echo "Тема: ";
				if ( file_exists("demo.txt" ) )
				echo "<form action=\"/demo/page.php\" method=\"POST\">";
				else
				echo "<form action=\"/\" method=\"POST\">";
					echo "<select name=\"ab-theme\">";
						$my_p = page(0, "", "themes");
						$theme_ContID = $my_p['ContID'];
						$theme_ListID = $my_p['ListID'];

						if ( !empty($_COOKIE['ab-theme']) )
						$cur_theme = $_COOKIE['ab-theme'];
						else
						$cur_theme = "redmond";

						$sql="SELECT tmpID, theme_title, theme_descr FROM tmp_$theme_ListID WHERE ContID=$theme_ContID ORDER BY tmpID";
						if (!($result = $conn2->query($sql)))
						echo "<!--#19-->";
						else
						{
							while ($data = $result->fetch_row())
							{
								if ( $cur_theme == $data[1] )
								$my_select = " selected";
								else
								unset($my_select);
								echo "<option value=\"$data[1]\"$my_select>$data[2]</option>";
							}
						}
					echo "</select>";
					echo "&nbsp;<input type=\"submit\" value=\"Применить\">";
				echo "</form>";
			echo "</div>";
		echo "</div>";
		*/


	}

	/*
	if ( $_REQUEST['p'] == "location" )
	{

		echo '
		<scripT type="text/javascript">

		$(document).ready(
		function()
		{
			function loc_info()
			{
				$.get("ab-data.php?p=location&person=" + $(\'#loc_person\').val(), function(data)
				{
					var data2 = data.split(";");
					$(\'#map\').attr(\'src\', \'http://ab-log.ru/demo/map.php?\' + data2[0] + \'&nc\'+Math.random());
					$("#locdet").html(data2[1]);
					$(\'#locdet\').accordion("destroy");
					$(\'#locdet\').accordion();

				});

			}

			$(\'#loc_form\').change(function () {loc_info();});

			$(\'#ui-tabs-\' + tab_index).everyTime(300000, function(i)
			{
				loc_info();
			});


			loc_info();

		});

		</script>
		';

		require_once("ab-cms/class/main_class.php");
		$ab = new cms_lib();
		
		$location_id = $ab->get_id("location");
		//$first_person = cat_list($location_id, "#ContID", 1, 0, 1);
		//$latlng = show_list($first_person, "lat=#pos_lat#&lng=#pos_lng#", "pos_date DESC", 1, "", 1);
		//http://ab-log.ru/demo/map.php?$latlng

		echo "<div class=\"grid_8\">";
			echo "<div class=\"ui-corner-all ui-state-default\" style=\"height:815px\">";
				echo "<iframE src=\"\" height=\"800\" width=\"615\" frameborder=\"0\" scrolling=\"auto\" id=\"map\" name=\"map\"></iframe>";
			echo "</div>";
		echo "</div>";

		echo "<div class=\"grid_4\">";
			echo "<div class=\"ui-corner-all ui-state-default ab\" style=\"height:805px\">";
				echo "Расположение членов семьи: <br>";
				echo "<form id=\"loc_form\">";
					echo "<select id=\"loc_person\">";
					echo $ab->cat_read($location_id, "<option value=\"#ContID#\">#Title#</option>");
					echo "</select>";
				echo "</form>";
				echo "<br>";
				echo "<div id=\"locdet\"></div>";
			echo "</div>";
		echo "</div>";
	}
	*/

	/*
	if ( $_REQUEST['p'] == "test" )
	{

		?>
		<scripT type="text/javascript">
		$(function()
		{
			$.ajaxSetup({ cache: false });

			$('#tabs2').tabs(
			{
			create: function(event, ui) { $(ui.panel).load(ui.tab.find('a').attr('url')); },
			beforeActivate: function(event, ui) {
				$(ui.oldPanel.selector).stopTime();
				$(ui.oldPanel).empty();
				$(ui.newPanel).load(ui.newTab.find('a').attr('url'));
				}
			}
			);
		
		});

		</script>
	
		<div id="tabs2">
			<ul>
				<li><a href="#ui-tabs2-1" url="ab-pages.php?p=test1"><span style="float:left;margin-right:3px;" class="ui-icon ui-icon-home"></span> Test1</a></li>
				<li><a href="#ui-tabs2-2" url="ab-pages.php?p=test2"><span style="float:left;margin-right:3px;" class="ui-icon ui-icon-video"></span> Test2</a></li>

			</ul>

			<div id="ui-tabs2-1"></div>
			<div id="ui-tabs2-2"></div>

		</div>
		<?

	}

	if ( $_REQUEST['p'] == "test1" )
	{
		echo "text1";
	}
	if ( $_REQUEST['p'] == "test2" )
	{
		echo "text2";
	}
	*/
	
echo "</div>";

?>