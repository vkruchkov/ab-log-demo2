<?
/*
* Copyright (c) 2013, Andrey_B
* http://ab-log.ru
* Подробнее см. LICENSE.txt или http://www.gnu.org/licenses/
*/

if ( !isset($ab) )
{
	require_once("ab-cms/class/main_class.php");
	$ab = new cms_lib();
}

require "/opt/owfs/share/php/OWNet/ownet.php";

function ippower_get($address, $port)
{
	$port--;

	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, "http://$address/Set.cmd?CMD=getpower");
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_USERPWD, "admin:12345678");
	curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
	$output = curl_exec($ch);
	$info = curl_getinfo($ch);
	curl_close($ch);

	$get_power_a = explode(",", $output);
	$get_power_val = explode("=", $get_power_a[$port]);
	return $get_power_val[1];
}

function ippower_set($address, $port, $val = 0)
{
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, "http://$address/Set.cmd?CMD=setpower+P6$port=$val");
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_USERPWD, "admin:12345678");
	curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
	$output = curl_exec($ch);
	$info = curl_getinfo($ch);
	curl_close($ch);
}

function key_sw($key_label = "", $key_pio = 0, $key_ch = "", $key_level = "")
{
	global $no_log;
	global $ab;

	$keys_id = $ab->get_id("keys");
	$key_addr = $ab->mod_read($keys_id, "#key_addr#", "", 1, "key_label='$key_label'");
	$key_tmpID = $ab->mod_read($keys_id, "#tmpID#", "", 1, "key_label='$key_label'");

	if ( empty($key_addr) )
	exit;

	// Обработчик для MegaD-328
	if ( preg_match("/^megad\./", $key_addr) )
	{
		list($null, $key_addr, $key_port) = explode(".", $key_addr);
		$key_addr = "192.168.0.$key_addr";
		$pass = "sec";

		file_get_contents("http://$key_addr/$pass/?cmd=$key_port:$key_pio");
		$key_time = microtime(get_as_float);

		// Записываем в журнал только информацию о вкл/выкл, так как при переключении (команда "2") мы не знаем в каком положении ключ
		if ( $key_pio < 2 )
		{
			$ab->mod_write($keys_id, "key_pio='$key_pio'", "key_label='$key_label'");
			$ab->mod_write($ab->get_id("keys_journal"), "SYSDATE(), $key_tmpID, '$key_pio'");
		}
	}
	// Обработчик для IP Power 9212 Deluxe
	elseif ( preg_match("/^ipp\./", $key_addr) )
	{
		list($null, $key_addr, $key_port) = explode(".", $key_addr);
		$key_addr = "192.168.0.$key_addr";

		ippower_set($key_addr, $key_port, $key_pio);

		$ab->mod_write($keys_id, "key_pio='$key_pio'", "key_label='$key_label'");
		$ab->mod_write($ab->get_id("keys_journal"), "SYSDATE(), $key_tmpID, '$key_pio'");

	}
	// Обработчик для X10
	elseif ( preg_match("/^x10\./", $key_addr) )
	{
		$key_addr = preg_replace("/^x10\./", "", $key_addr);

		if ( !empty($key_level) )
		{
			$key_level_old = $ab->mod_read($keys_id, "#key_level#", "", 1, "key_label='$key_label'");

			if ( $key_level > $key_level_old )
			{
				$bright_value = $key_level - $key_level_old;
				exec("heyu bright $key_addr $bright_value");
			}
			elseif ( $key_level < $key_level_old )
			{
				$dim_value = $key_level_old - $key_level;
				exec("heyu dim $key_addr $dim_value");
			}

			$ab->mod_write($keys_id, "key_level='$key_level'", "key_label='$key_label'");
		}
		else
		{
			if ( $key_pio == 1 )
			$heyu_cmd = "on";
			elseif ( $key_pio == 0 )
			$heyu_cmd = "off";

			exec("heyu $heyu_cmd $key_addr");

			$ab->mod_write($keys_id, "key_pio='$key_pio'", "key_label='$key_label'");
			$ab->mod_write($ab->get_id("keys_journal"), "SYSDATE(), $key_tmpID, '$key_pio'");
		}

	}
	// Обработчик для виртуальных ключей
	elseif ( $key_addr == "virt" )
	{
			$ab->mod_write($keys_id, "key_pio='$key_pio'", "key_label='$key_label'");
			$ab->mod_write($ab->get_id("keys_journal"), "SYSDATE(), $key_tmpID, '$key_pio'");

			if ( $key_label == "alarm" && $key_pio == 1 )
			file_get_contents("http://192.168.0.231/sound.php?sound=alarm-on");
			elseif ( $key_label == "alarm" && $key_pio == 0 )
			file_get_contents("http://192.168.0.231/sound.php?sound=alarm-off");


	}
	// Обработчик для 1-wire
	else
	{
		// ownet.php не удовлетворяет Strict Standarts
		error_reporting(0);
		$ow=new OWNet("tcp://localhost:3000");

		if ( empty($key_ch) )
		$key_ch = "PIO";
		else
		$key_ch = "PIO.".$key_ch;

		if ( !preg_match("/\//", $key_addr) )
		$key_addr = "$key_addr/$key_ch";

		// Временное
		$ow->set($key_addr, $key_pio);
		$key_time = microtime(get_as_float);

		$cpio = 10;
		$cpio = $ow->read("/uncached/".$key_addr);

		if ( $cpio != $key_pio )
		{
			$my_loop = 0;
			while ( $cpio != $key_pio && $my_loop < 3 )
			{ 
			        $my_loop++;
				$ow->set($key_addr, $key_pio);
				$key_time = microtime(get_as_float);
				$cpio = $ow->get("/uncached/".$key_addr);
				// $cpio = $ow->read("/uncached/".$key_addr);

				if ( $cpio != $key_pio )
				{
					sleep(5);
					$log = fopen("/var/www/log_error.log", "a");
					fwrite($log, date('d/m/Y H:i:s')." > [ $key_label | $key_pio | $key_ch ]\n");
					fclose($log);
				}
			}
		}

		if ( empty($no_log) )
		$ab->mod_write($ab->get_id("keys_journal"), "SYSDATE(), $key_tmpID, '$key_pio'");

		$ab->mod_write($keys_id, "key_pio='$key_pio'", "key_label='$key_label'");

		unset($ow);
		error_reporting($ab->err_rep);
	}

	return $key_time;
}

if ( !empty($_REQUEST['get_key']) )
{
	$keys_id = $ab->get_id("keys");
	echo $ab->mod_read($keys_id, "#key_pio#", "", 1, "key_label='".$_GET['get_key']."'");
}
else if ( !empty($_REQUEST['key_label']) )
{
	if ( !isset($_REQUEST['key_ch']) )
	$_REQUEST['key_ch'] = "";
	if ( !isset($_REQUEST['key_level']) )
	$_REQUEST['key_level'] = "";
	key_sw($_REQUEST['key_label'], $_REQUEST['key_pio'], $_REQUEST['key_ch'], $_REQUEST['key_level']);
}

?>
