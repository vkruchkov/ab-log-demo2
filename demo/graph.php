<?
/*
* Copyright (c) 2013, Andrey_B
* http://ab-log.ru
* Подробнее см. LICENSE.txt или http://www.gnu.org/licenses/
*/

if ( !isset($_GET['dev_cl']) )
exit;

DEFINE("TTF_DIR","libs/fonts/");
require_once("ab-cms/class/main_class.php");
$ab = new cms_lib();

include ("libs/jpgraph/jpgraph.php");
include ("libs/jpgraph/jpgraph_line.php");

if ( $_GET['dev_cl'] == "temp_wh_abs_pressure" )
$_GET['dev_cl'] = "wh_abs_pressure";

$my_page = $ab->doc_read($ab->get_id($_GET['dev_cl']));

if ( !isset($_GET['temp_date']) || empty($_GET['temp_date']) )
{
	$temp_date = "NOW() - INTERVAL 1 DAY";
	$date_format = "DATE_FORMAT(dev_date, '%H:%i')";
}
elseif ( $_GET['temp_date'] == "month" )
{
	$temp_date = "(NOW() - INTERVAL 1 MONTH)";
	$date_format = "DATE_FORMAT(dev_date, '%d.%m')";
}
else
{
	$temp_date = $_GET['temp_date'];
	$date_format = "DATE_FORMAT(dev_date, '%d.%m')";
}
	
if ( !isset($_GET['temp_date_to']) || empty($_GET['temp_date_to']) )
$temp_date_to = "NOW()";
else
$temp_date_to = $_GET['temp_date_to'];

$n = 0;
$y_min = 0;

$result = $ab->select("SELECT dev_value, $date_format dev_date_f FROM tmp_".$my_page['ListID']." WHERE ContID=".$my_page['ContID']." AND dev_date>=$temp_date AND dev_date<=$temp_date_to ORDER BY dev_date");
for ( $i = 0; $i < count($result); $i++ )
{
	$ydata[$n] = $result[$i]['dev_value'];
	if ( $y_min > $result[$i]['dev_value'] )
	$y_min = $result[$i]['dev_value'];
	$xdata[$n] = $result[$i]['dev_date_f'];
	$n++;
}

if ( $_GET['dev_cl'] == "temp_out2" )
{
	$my_page = $ab->doc_read($ab->get_id("wh_temp_out"));

	$result = $ab->select("SELECT dev_value FROM tmp_".$my_page['ListID']." WHERE ContID=".$my_page['ContID']." AND dev_date>=$temp_date AND dev_date<=$temp_date_to ORDER BY dev_date");
	for ( $i = 0; $i < count($result); $i++ )
	$ydata2[] = $result[$i]['dev_value'];
}

if ( $n > 1 )
{
	$my_interval = ceil($n / 30);

	// Create the graph. These two calls are always required
	$graph = new Graph(450,200,"auto");
	$graph->SetScale("textlin");
//	$graph->SetAlphaBlending(); 
	$graph->SetMarginColor('white'); 
	$graph->SetFrame(true,'#B3BCCB', 1); 
	$graph->SetTickDensity(TICKD_DENSE);
	$graph->img->SetMargin(50,20,20,60);
	$graph->title->SetMargin(10);
	$graph->xaxis->SetTickLabels($xdata);
	$graph->xaxis->SetLabelAngle(90);
	$graph->xaxis->SetPos('min');
	$graph->xaxis->SetTextTickInterval($my_interval);

	// Create the linear plot
	$lineplot=new LinePlot($ydata);
	// Add the plot to the graph
	$graph->Add($lineplot);

	$lineplot->SetWeight(1);

	if ( $_GET['dev_cl'] == "temp_in" )
	$my_color = "#b11111";
	elseif ( $_GET['dev_cl'] == "temp_out2" )
	//$my_color = "#1163B8";
	$my_color = "#0000FF";
	elseif ( $_GET['dev_cl'] == "temp_roof" )
	$my_color = "#0a91ae";
	else
	$my_color = "#9127ac";

	$lineplot->SetColor($my_color);

	if ( $_GET['dev_cl'] == "temp_out2" )
	{
		$lineplot2=new LinePlot($ydata2);
		$graph->Add($lineplot2);
		$lineplot2->SetWeight(1);
		$my_color = "#AA10AA";
		$lineplot2->SetColor($my_color);
	}
	
	// Display the graph
	$graph->Stroke();
}
else
{
	header("Content-type: image/png");
	function hex2dec2($hex)
	{
		$color = str_replace('#', '', $hex);
		$ret = array( 'r' => hexdec(substr($color, 0, 2)), 'g' => hexdec(substr($color, 2, 2)), 'b' => hexdec(substr($color, 4, 2)));
		return $ret;
	}


	$sizemax = 50;
	$sizemin = 10;
	$bg_color = "#FFFFFF";
	$text_color = "#000000"; 


//	$Title = "Text!";
	$Title = "За этот период нет данных!";
	//$text = iconv('CP1251', 'UTF-8', $Title);
	$size = imagettfbbox(26, 0, "cadmin/fonts/DEFAULT.TTF", $text);
	$dx = abs($size[2] - $size[0]);
	$dy = abs($size[5] - $size[3]);
	$xpad = 9;
	$ypad = 9;
	$im = imagecreate($dx + $xpad, $dy + $ypad);
	$bgc = hex2dec2($bg_color);
	$bg = ImageColorAllocate($im, $bgc['r'], $bgc['g'], $bgc['b']);
	imagecolortransparent($im,$bg);
	$mac = hex2dec2($text_color);
	$main = ImageColorAllocate($im, $mac['r'], $mac['g'], $mac['b']);
	ImageTTFText($im, 26, 0, (int)($xpad / 2), $dy + (int)($ypad / 2)-1, $main, "cadmin/fonts/DEFAULT.TTF", $text);
	ImagePng($im);

}

?>
