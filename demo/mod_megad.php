<?
/*
* Copyright (c) 2013, Andrey_B
* http://ab-log.ru
* Подробнее см. LICENSE.txt или http://www.gnu.org/licenses/
*/

require_once("ab-cms/class/main_class.php");
$ab = new cms_lib();
//error_reporting(0);

function key_update($key_label)
{
	global $ab;

	$keys_id = $ab->get_id("keys");
	$key_tmpID = $ab->mod_read($keys_id, "#tmpID#", "", 1, "key_label='$key_label'");
	$key_pio = $ab->mod_read($keys_id, "#key_pio#", "", 1, "tmpID=$key_tmpID");
	if ( $key_pio == 1 ) $key_pio = 0; else $key_pio = 1;
	$ab->mod_write($keys_id, "key_pio=$key_pio", "tmpID=$key_tmpID");
	$ab->mod_write($ab->get_id("keys_journal"), "SYSDATE(), $key_tmpID, $key_pio");
}

// Считываем состояние выхода и записываем значение в БД
if ( !empty($argv[1]) )
{
	sleep(2);
	$keys_id = $ab->get_id("keys");
	$key_tmpID = $ab->mod_read($keys_id, "#tmpID#", "", 1, "key_label='".$argv[1]."'");
	$key_addr = $ab->mod_read($keys_id, "#key_addr#", "", 1, "tmpID=$key_tmpID");
	list($null, $key_addr, $key_port) = explode(".", $key_addr);
	$key_addr = "192.168.0.$key_addr";
	$pass = "sec";
	$state = file_get_contents("http://$key_addr/$pass/?pt=$key_port&cmd=get");
	if ( preg_match("/^ON/", $state) )
	$key_pio = 1;
	else
	$key_pio = 0;

	//$key_pio = $ab->mod_read($keys_id, "#key_pio#", "", 1, "tmpID=$key_tmpID");
	//if ( $key_pio == 1 ) $key_pio = 0; else $key_pio = 1;
	$ab->mod_write($keys_id, "key_pio=$key_pio", "tmpID=$key_tmpID");
	$ab->mod_write($ab->get_id("keys_journal"), "SYSDATE(), $key_tmpID, $key_pio");
}

// Обработка сообщений MegaD-328
// Котельная
if ( $_SERVER['REMOTE_ADDR'] == "192.168.0.100" )
{
	// Датчик переполнения накопительной емкости для воды
	if ( $_GET['pt'] == "0" && $_GET['m'] != 2 )
	{
		// Проверяем текущее состояние крана и его доступность.
		// Если кран уже закрывается, пропускаем событие, чтобы не дергать его.
		$state = file_get_contents("http://192.168.0.101/sec/?pt=12&cmd=get");
		if ( preg_match("/^OFF/", $state) )
		{
			include("key.php");
			key_sw("water_valve_op", 0);
			key_sw("water_valve_cl", 1);
			// Разобраться! Не срабатывает аварийный дозвон!
			//file_get_contents("http://192.168.0.251/alarm.php");
			$ab->mod_write($ab->get_id("alarm", 1), "SYSDATE(), 'Зафиксировано переполнение накопительной емкости. Вводной кран закрыт!'");
			sleep(15);
			key_sw("water_valve_cl", 0);
		}
	}
	// Датчик напряжения на клапане наполнения емкости для воды
	elseif ( $_GET['pt'] == "1" && $_GET['m'] != 2 )
	{
		$keys_id = $ab->get_id("keys");
		$key_tmpID = $ab->mod_read($keys_id, "#tmpID#", "", 1, "key_label='tank_valve'");
		if ( $_GET['m'] == 1 ) $key_pio = 0; else $key_pio = 1;
		$ab->mod_write($keys_id, "key_pio=$key_pio", "tmpID=$key_tmpID");
		$ab->mod_write($ab->get_id("keys_journal"), "SYSDATE(), $key_tmpID, $key_pio");
	}

}
elseif ( $_SERVER['REMOTE_ADDR'] == "192.168.0.120" )
{
	// Выключатель L6 в Детской-2
	if ( $_GET['pt'] == "0" )
	{
		echo "9:2";
		#key_update("light_masha");
		exec("php mod_megad.php \"light_vasilisa\" > /dev/null &");
	}
	// Извещатель на лестнице
	elseif ( $_GET['pt'] == "4" )
	{
		$alarm_mode = $ab->mod_read($ab->get_id("keys"), "#key_pio#", "", 1, "key_label='alarm'");
		if ( $alarm_mode == 1 )
		{
			$sen_j = $ab->doc_read($ab->get_id("sen_j"));
			$sen_j_date = $ab->select_line("SELECT UNIX_TIMESTAMP(sen_j_date) sen_j_date FROM tmp_".$sen_j['ListID']." WHERE ContID=".$sen_j['ContID']." AND sen_j_label='alarm' ORDER BY sen_j_date DESC LIMIT 1");
			
			if ( $sen_j_date['sen_j_date'] < time() - 600 )
			{
				file_get_contents("http://192.168.0.251:8080/alarm2.php");
				$ab->email("Дом: Сработала сигнализация!", "Дом: Сработала сигнализация!");
				$ab->mod_write($sen_j['ContID'], "NOW(), 'alarm', 1");
				file_get_contents("http://192.168.0.231/sound.php?sound=alarm-alert2");

			}
		}
		else
		{
			// Записываем дату и время срабатывания в БД для последующей обработки
			$con_id = $ab->get_id("light-control");
			$con_tmpID = $ab->mod_read($con_id, "#tmpID#", "", 1, "con_sen='megad.120.4'");
			$ab->mod_write($con_id, "con_date=NOW()", "tmpID=$con_tmpID");
		}
	}
	// Выключатель L13 на лестнице или L7 в детской
	if ( $_GET['pt'] == "5" || $_GET['pt'] == "1")
	{
		echo "12:2";
		#key_update("light_mainstairs");
		exec("php mod_megad.php \"light_mainstairs\" > /dev/null &");
	}
	// Выключатель L3 в Детской-1
	elseif ( $_GET['pt'] == "6" )
	{
		echo "7:2";
		#key_update("light_masha");
		exec("php mod_megad.php \"light_masha\" > /dev/null &");
	}

	// Выключатель L2 в Коридоре
	elseif ( $_GET['pt'] == "3" )
	{
		echo "8:2";
		exec("php mod_megad.php \"light_2f_hall\" > /dev/null &");
	}

	// Выключатель L8 в Спальне
	elseif ( $_GET['pt'] == "2" )
	{
		echo "10:2";
		exec("php mod_megad.php \"light_bedroom\" > /dev/null &");
	}

}
// Щиток (1 этаж - комплект слева)
elseif ( $_SERVER['REMOTE_ADDR'] == "192.168.0.110" )
{
	// Выключатель L13 (кухня), правая клавиша
	if ( $_GET['pt'] == "0" )
	{
		echo "7:2";
		exec("php mod_megad.php \"light_kitchen\" > /dev/null &");
	}

	// Выключатель L13 (прихожая), левая клавиша
	if ( $_GET['pt'] == "2" )
	{
		echo "8:2";
		exec("php mod_megad.php \"light_hallway\" > /dev/null &");
	}

	// Выключатель L3 (входная группа)
	if ( $_GET['pt'] == "1" )
	{
		echo "8:2";
		exec("php mod_megad.php \"light_hallway\" > /dev/null &");
		/*
		require "key.php";
		key_sw("door", 1);
		usleep(50000);
		key_sw("door", 0);
		file_get_contents("http://192.168.0.231/sound.php?sound=gate-is-open");
		*/
	}

	// Выключатель L4 (рядом с лестницей), правый выключатель, правая клавиша
	if ( $_GET['pt'] == "6" )
	{
		echo "7:2";
		exec("php mod_megad.php \"light_kitchen\" > /dev/null &");
		//echo "12:2";
		//exec("php mod_megad.php \"light_table\" > /dev/null &");
	}

	// Выключатель L4 (рядом с лестницей), правый выключатель, левая клавиша
	if ( $_GET['pt'] == "5" )
	{
		//echo "13:2";
		//exec("php mod_megad.php \"light_holl\" > /dev/null &");
		echo "11:2";
		exec("php mod_megad.php \"light_table\" > /dev/null &");

	}

	// Выключатель L4 (рядом с лестницей), центральный выключатель, левая клавиша
	if ( $_GET['pt'] == "3" )
	{
		echo "9:2";
		exec("php mod_megad.php \"light_holl\" > /dev/null &");
	}

	// Выключатель L4 (рядом с лестницей), центральный выключатель, правая клавиша
	if ( $_GET['pt'] == "4" )
	{
		echo "12:2";
		exec("php mod_megad.php \"light_holl_center\" > /dev/null &");
	}



}
// Щиток (1 этаж - комплект справа)
elseif ( $_SERVER['REMOTE_ADDR'] == "192.168.0.111" )
{
	if ( $_GET['pt'] == "0" )
	{
		require "key.php";
		key_sw("light_mainstairs", 2);
		exec("php mod_megad.php \"light_mainstairs\" > /dev/null &");
	}

	if ( $_GET['pt'] == "1" )
	{
		//file_get_contents("http://192.168.0.231/sound.php?sound=Doorbell-chimes");
		file_get_contents("http://192.168.0.231/sound.php?sound=Doorbell-westminster&volume=100");
	}

	// Выключатель L1 (входная группа)
	if ( $_GET['pt'] == "2" )
	{
		require "key.php";
		key_sw("door", 1);
		usleep(50000);
		key_sw("door", 0);
		file_get_contents("http://192.168.0.231/sound.php?sound=gate-is-open&volume=88");
	}

	// Выключатель L15 (прихожая), правая клавиша
	if ( $_GET['pt'] == "3" )
	{
		file_get_contents("http://192.168.0.101/sec/?cmd=10:2");
		exec("php mod_megad.php \"light_garage_box\" > /dev/null &");
	}

	// Выключатель L14 (коридор)
	if ( $_GET['pt'] == "4" )
	{
		echo "7:2";
		exec("php mod_megad.php \"light_corr\" > /dev/null &");
	}



}
elseif ( $_SERVER['REMOTE_ADDR'] == "192.168.0.101" || $_GET['test'] == 1 )
{
	// Насос Grundfos MQ 3-45
	if ( $_GET['pt'] == "0" )
	{
		if ( $_GET['m'] == 1 )
		{
			unlink("/var/www/virt_alarm/water_pump.act");
			$key_pio = 0;
		}
		else
		{
			touch("/var/www/virt_alarm/water_pump.act");
			$key_pio = 1;
		}

		/*
		$keys_id = $ab->get_id("keys");
		$key_tmpID = $ab->mod_read($keys_id, "#tmpID#", "", 1, "key_label='water_pump'");
		$ab->mod_write($keys_id, "key_pio=$key_pio", "tmpID=$key_tmpID");
		$ab->mod_write($ab->get_id("keys_journal"), "SYSDATE(), $key_tmpID, $key_pio");
		*/
	}

	// Концевик гаражных ворот
	else if ( $_GET['pt'] == "1" )
	{
		$con_p = $ab->doc_read($ab->get_id("light-control"));
		$sensor = $ab->select_line("SELECT UNIX_TIMESTAMP(con_date) con_date FROM tmp_".$con_p['ListID']." WHERE ContID=".$con_p['ContID']." AND con_sen='megad.101.4' AND con_date < NOW() - INTERVAL 120 SECOND");

		if ( $_GET['m'] == 1 )
		{
			$key_pio = 0;

			if (!empty($sensor['con_date']) )
			{
				//echo "10:1";
				flush();
				file_get_contents("http://".$_SERVER['REMOTE_ADDR']."/sec/?cmd=10:1");
				exec("php mod_megad.php \"light_garage_box\" > /dev/null &");
				//exec("php mod_megad_exec.php \"garage\" > /dev/null &");
				$in_mode = 1;
			}

			exec("php mod_megad_exec.php \"garage\" > /dev/null &");
		}
		else
		$key_pio = 1;

		$keysj_p = $ab->doc_read($ab->get_id("keys_journal"));

		$keys_id = $ab->get_id("keys");
		$key_tmpID = $ab->mod_read($keys_id, "#tmpID#", "", 1, "key_label='garage'");
		$ab->mod_write($keys_id, "key_pio=$key_pio", "tmpID=$key_tmpID");
		//$ab->mod_write($ab->get_id("keys_journal"), "SYSDATE(), $key_tmpID, $key_pio");
		$ab->mod_write($keysj_p['ContID'], "SYSDATE(), $key_tmpID, $key_pio");

		// Автомобиль заезжает в гараж. Через 30 секунд включим свет в прихожей, если низкая освещенность.
		if ( $in_mode == 1 )
		{
			flush();
			$key_hall_addr = explode(".", $ab->mod_read($keys_id, "#key_addr#", "", 1, "key_label='light_hallway'"));
			$key_corr_addr = explode(".", $ab->mod_read($keys_id, "#key_addr#", "", 1, "key_label='light_corr'"));
			$ext_light_val = $ab->mod_read($keys_id, "#key_pio#", "", 1, "key_label='ext_light'");
			if ( $ext_light_val == 1 )
			{
				sleep(30);
				file_get_contents("http://192.168.0.".$key_hall_addr[1]."/sec/?cmd=".$key_hall_addr[2].":1");
				exec("php mod_megad.php \"light_hallway\" > /dev/null &");
				file_get_contents("http://192.168.0.".$key_corr_addr[1]."/sec/?cmd=".$key_corr_addr[2].":1");
				exec("php mod_megad.php \"light_corr\" > /dev/null &");
				sleep(300);
				// Нужен ИК-извещатель в прихожей!
				file_get_contents("http://192.168.0.".$key_hall_addr[1]."/sec/?cmd=".$key_hall_addr[2].":0");
				exec("php mod_megad.php \"light_hallway\" > /dev/null &");
				file_get_contents("http://192.168.0.".$key_corr_addr[1]."/sec/?cmd=".$key_corr_addr[2].":0");
				exec("php mod_megad.php \"light_corr\" > /dev/null &");
			}
		}

		if ( $key_pio == 1 )
		{
			// Если ворота открылись раньше, чем сработал датчик движения, значит автомобиль заезжает, выдерживаем паузу перед выключением
			$open_time = $ab->select_line("SELECT UNIX_TIMESTAMP(key_j_date) key_j_date FROM tmp_".$keysj_p['ListID']." WHERE ContID=".$keysj_p['ContID']." AND key_j_label=$key_tmpID AND key_i_pio='0' ORDER BY key_j_date DESC LIMIT 1");
			$sensor_time = $ab->select_line("SELECT UNIX_TIMESTAMP(con_date) con_date FROM tmp_".$con_p['ListID']." WHERE ContID=".$con_p['ContID']." AND con_sen='megad.101.4' AND UNIX_TIMESTAMP(con_date) < '".$open_time['key_j_date']."'");
			if ( !isset($sensor_time['con_date']) || ( isset($sensor_time['con_date']) && $sensor_time['con_date'] < $open_time['key_j_date'] - 120 ) )
			{
				//echo "pause";
				//exit;
				flush();
				sleep(60);
				file_get_contents("http://".$_SERVER['REMOTE_ADDR']."/sec/?cmd=10:0");
				exec("php mod_megad.php \"light_garage_box\" > /dev/null &");
			}
			else
			{
				echo "10:0";
				exec("php mod_megad.php \"light_garage_box\" > /dev/null &");
			}

			#$fh = fopen("megad.log", "w");
			#fwrite($fh, $sensor_time['con_date']." - ".$open_time['key_j_date']."\n");
		}
	}

	// Выключатель света в гараже
	else if ( $_GET['pt'] == "2" )
	{
		echo "10:2";
		exec("php mod_megad.php \"light_garage_box\" > /dev/null &");
	}

	// Извещатель в гараже
	else if ( $_GET['pt'] == "3" )
	{
		// Записываем дату и время срабатывания в БД для последующей обработки
		$con_id = $ab->get_id("light-control");
		$con_tmpID = $ab->mod_read($con_id, "#tmpID#", "", 1, "con_sen='megad.101.4'");
		$ab->mod_write($con_id, "con_date=NOW()", "tmpID=$con_tmpID");
	}

}

?>