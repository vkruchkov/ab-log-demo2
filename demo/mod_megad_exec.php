<?
/*
* Copyright (c) 2013, Andrey_B
* http://ab-log.ru
* Подробнее см. LICENSE.txt или http://www.gnu.org/licenses/
*/

/*
Этот скрипт необходим для выполнения отложенных действий в режиме CLI,
которые не всегда возможно выполнить из-под Web-сервера ввиду ограничений на время выполнения скрипта
*/

require_once("ab-cms/class/main_class.php");
$ab = new cms_lib();
error_reporting(0);
include("key.php");

$keys_id = $ab->get_id("keys");

if ( !empty($argv[1]) )
{
	// Гараж открыт. Контролируем, чтобы ворота автоматически закрылись через 3 минуты, если не было зафиксировано движения в подвале
	if ( $argv[1] == "garage" )
	{
		sleep(60);
		$con_p = $ab->doc_read($ab->get_id("light-control"));

		$close = 0;
		$my_cnt = 0;

		while ( $close == 0 )
		{
			sleep(240);
			$sensor = $ab->select_line("SELECT UNIX_TIMESTAMP(con_date) con_date FROM tmp_".$con_p['ListID']." WHERE ContID=".$con_p['ContID']." AND con_sen='megad.101.4' AND con_date > NOW() - INTERVAL 240 SECOND");
			$garage_pio = $ab->mod_read($keys_id, "#key_pio#", "", 1, "key_label='garage'");

			//$garage_pio = 0;

			if ( $garage_pio == 0 && !isset($sensor['con_date']) )
			{
				key_sw("garage_motor", 1);
				sleep(1);
				key_sw("garage_motor", 0);
				key_sw("light_garage_box", 0);
				//echo "Закрыть ворота\n";
				$close = 1;
			}

			$my_cnt++;

			// Если в течение 15 минут фиксируется движение в области гаража, значит отменяем процедуру автоматического закрытия гаража.
			if ( $my_cnt == 4 )
			$close = 1;
		}

	}
}

?>