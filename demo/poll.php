<?
/*
* Copyright (c) 2013, Andrey_B
* http://ab-log.ru
* Подробнее см. LICENSE.txt или http://www.gnu.org/licenses/
*/

/*
Этот скрипт опрашивает датчики DS18B20 и записывает данные в БД
Для демо-интерфейса необходимо:
1. Создать документ
2. В закладке "Свойства" для документа выбрать "Модуль" - Устройства
3. В закладке "Свойства" для документа прописать адрес устройства.

Адрес устройства для 1-wire шины в формате: 28.9C998D020000
Адрес устройства для MegD-328: megad.121.2 (где "121" - последнее число IP-адреса, а "2" - номер порта)
*/

require_once("ab-cms/class/main_class.php");
$ab = new cms_lib();

// ownet.php не удовлетворяет Strict Standarts
error_reporting(E_ALL ^ E_NOTICE);

require "/opt/owfs/share/php/OWNet/ownet.php";

$ow=new OWNet("tcp://localhost:3000");

$my_date = date("i");

sleep(7);

$result = $ab->select("SELECT b.ContID, c.ListID, b.Value, c.Cont_Label, c.Reserved_Text
			FROM tContField a, tContFieldExt b, tCont c
			WHERE a.Disabled=0 AND a.ContFieldID=b.ContFieldID AND
			a.Title='dev_address' AND b.ContID=c.ContID AND b.Value<>''");
for ( $i = 0; $i < count($result); $i++ )
{
	if ( $my_date == $result[$i]['Reserved_Text'] || empty($result[$i]['Reserved_Text']) )
	{
		if ( preg_match("/^10\./", $result[$i]['Value']) )
		$temp_var = "temperature";
		else
		$temp_var = "temperature12";

		$dev_shift = 0;
		$result2 = $ab->select_line("SELECT b.Value FROM tContField a, tContFieldExt b WHERE a.ContFieldID=b.ContFieldID AND b.ContID=".$result[$i]['ContID']." AND a.Title='dev_shift' AND b.Value<>'' LIMIT 1");
		$dev_shift = $result2['Value'];

		$my_value = NULL;
		if ( preg_match("/^10\./", $result[$i]['Value']) || preg_match("/^28\./", $result[$i]['Value']) )
		$my_value = $ow->get($result[$i]['Value']."/$temp_var");
		else
		{
			list($null, $key_addr, $key_port) = explode(".", $result[$i]['Value']);
			$key_addr = "192.168.0.$key_addr";
			$pass = "sec";
			$my_value = str_replace("temp:", "", file_get_contents("http://$key_addr/$pass/?pt=$key_port&cmd=get"));
		}

		if ( is_null($my_value) || $my_value == 85 || $my_value == "0" )
		{
			sleep(2);
			$my_value = $ow->get($result[$i]['Value']."/$temp_var");
		}
	
		if ( !is_null($my_value) && $my_value != 85 && $my_value != "0" && $my_value != "" )
		{
			$my_value = round($my_value, 2) + $dev_shift;
			$ab->mod_write($result[$i]['ContID'], "SYSDATE(), '$my_value'");
			// Дублирование уличной температуры
			//if ( $result[$i]['Cont_Label'] == "temp_out" )
			//$ab->mod_write($ab->get_id("temp_out2"), "SYSDATE(), '$my_value'");
		}
		else
		{
			//sleep(3);
			//$my_time1 = microtime();
			//$my_test = $ow->get($data[2]."/$temp_var");
			//$my_time2 = microtime() - $my_time1;
			$ab->mod_write($ab->get_id("net_errors"), "SYSDATE(), '".$result[$i]['Cont_Label']."'");
		}

		sleep(3);
	}
}

unset($ow);
