<?
/*
* Copyright (c) 2013, Andrey_B
* http://ab-log.ru
* Подробнее см. LICENSE.txt или http://www.gnu.org/licenses/
*/

/*
 Это пример того, как можно делать подключить к MegaD-328, энкодер и управлять работой ШИМ-порта
 
 Внимание! Код совместим только с прошивками 3.28 beta7 и выше!
 Необходимо, чтобы порты IN, к которым подключен энкодер были в режиме RAW
*/

// Номер входа, к которому подключен энкодер - 1
$enc_in1 = 3;
// Номер входа, к которому подключен энкодер - 2
$enc_in2 = 2;
// ШИМ-порт, с которым связан энкодер
$pwm = 12;
// Шаг изменения ШИМ
$step = 1;
// IP
$ip = "192.168.0.14";
// Пароль
$pwd = "sec";
$cmd = 0;

if ( $_SERVER['REMOTE_ADDR'] == $ip )
{
	if ( $_GET['pt'] == $enc_in1 || $_GET['pt'] == $enc_in2 )
	{
		$microtime2 = explode(";", file_get_contents("mod_megad3.php.act"));

		if ( !file_exists("mod_megad3.php.busy") )
		{
			touch("mod_megad3.php.busy");

			$pwm_val = file_get_contents("http://$ip/$pwd/?pt=$pwm&cmd=get");

			$microtime = microtime(true);

			if ( $microtime2[0] + 0.05 > $microtime && $microtime2[1] == 1 )
			$cmd = 1;
			elseif ( $microtime2[0] + 0.05 > $microtime && $microtime2[1] == 2 )
			$cmd = 2;
			elseif ( $_GET['pt'] == $enc_in1 )
			$cmd = 1;
			elseif ( $_GET['pt'] == $enc_in2 )
			$cmd = 2;

			if ( $cmd == 1 )
			$pwm_val = $pwm_val + $step;
			elseif ( $cmd == 2 )
			$pwm_val = $pwm_val - $step;

			if ( $pwm_val > 255 )
			$pwm_val = 255;
			if ( $pwm_val <= 0 )
			$pwm_val = 0;

			file_get_contents("http://$ip/$pwd/?pt=$pwm&pwm=$pwm_val");

			$fh1 = fopen("mod_megad3.php.act", "w");
			fwrite($fh1, $microtime.";".$cmd);
			fclose($fh1);

			/*
			$fh = fopen("log.log", "a");
			fwrite($fh, "$cmd http://$ip/$pwd/?pt=$pwm&pwm=$pwm_val  | $microtime2[0] | $microtime | $cmd\n");
			fclose($fh);
			*/

			usleep(10000);
			unlink("mod_megad3.php.busy");

		}

	}
}

?>