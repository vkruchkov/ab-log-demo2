<?                                                                        
/*
* Copyright (c) 2013, Andrey_B
* http://ab-log.ru
* Подробнее см. LICENSE.txt или http://www.gnu.org/licenses/
*/

require_once("ab-cms/class/main_class.php");
$ab = new cms_lib();

include("key.php");

$start = 1;
$gvs_mode = array();

function log_heat($msg)
{
	$log = fopen("/var/www/log_heat.log", "a");
	fwrite($log, "<span style=\"color: #666\">".date('d/m H:i:s')."</span> $msg\n");
	fclose($log);
}

while ( true )
{
	# Считываем из БД основные параметры отопления
	$heat_conf_id = $ab->get_id("heat_conf");
	$heat_mode = $ab->mod_read($heat_conf_id, "#heat_mode#", "", 1);
	$heat_boiler = $ab->mod_read($heat_conf_id, "#heat_boiler#", "", 1);
	$heat_boiler_status = $ab->mod_read($heat_conf_id, "#heat_boiler_status#", "", 1);
	$heat_boiler_temp = $ab->mod_read($heat_conf_id, "#heat_boiler_temp#", "", 1);
	$boiler_id = $ab->get_id("boiler", 1);

	$keys_id = $ab->get_id("keys");
	$heat_key = $ab->mod_read($keys_id, "#key_pio#", "", 1, "key_label='heat'");
	$eco_mode = $ab->mod_read($keys_id, "#key_pio#", "", 1, "key_label='gsm_arm'");
	$gvs_cur = $ab->mod_read($keys_id, "#key_pio#", "", 1, "key_label='gvs'");

	if ( $eco_mode == 1 )
	log_heat("<b><font color=\"#0b442e\">Включен экономичный режим</font></b>");

	# Восстановление состояния котла в случае некорректного выключения компьютера
	if ( $start == 1 )
	key_sw($heat_boiler, $heat_boiler_status);

	# Смотрим температуру на улице для расчета температуры подачи котла
	//$temp_out2 = $ab->doc_read($ab->get_id("temp_out2"));
	$temp_out2 = $ab->doc_read($ab->get_id("wh_temp_out"));
	$result = $ab->select_line("SELECT SUM(dev_value)/COUNT(tmpID) temp_out FROM tmp_".$temp_out2['ListID']." WHERE ContID=".$temp_out2['ContID']." AND dev_date>=(NOW() - INTERVAL 2 HOUR) AND dev_date<=NOW() ORDER BY dev_date");
	$temp_out = number_format(round($result['temp_out'], 2), 2);


	# Делаем рассчет максимальной температуры подающей линии котла
	if ( $heat_boiler_status == 1 )
	{
		//$heat_boiler_temp_new = $heat_boiler_temp;
		$heat_boiler_temp_calc = round(8 - $temp_out + 50);
		if ( $heat_boiler_temp_calc < 60 )
		$heat_boiler_temp_calc = 60;
		elseif ( $heat_boiler_temp_calc > 83 )
		$heat_boiler_temp_calc = 83;

		log_heat("Расч. t котла = ".$heat_boiler_temp_calc);

		# Изменяем расчетную температуру подачи котла не более чем на градус
		if ( $heat_boiler_temp_calc > $heat_boiler_temp )
		$heat_boiler_temp_new = $heat_boiler_temp + 1;
		elseif ( $heat_boiler_temp_calc < $heat_boiler_temp )
		$heat_boiler_temp_new = $heat_boiler_temp - 1;
		else
		$heat_boiler_temp_new = $heat_boiler_temp;

		## Устанавливаем новую расчетную температуру подачи котла 
		if ( $heat_boiler_temp != $heat_boiler_temp_new )
		{
			$ab->mod_write($heat_conf_id, "heat_boiler_temp_calc='$heat_boiler_temp_new'", "tmpID>0");
			$ab->mod_write($boiler_id, "boiler_write=1", "tmpID>0");
			log_heat("Новая t котла = $heat_boiler_temp_new");
		}
	}

	if ( $heat_key == 0 )
	log_heat("<b><font color=\"#0a3f7b\">Автоматическое управление отоплением отключено!</font></b>");


	# Обрабатываем контуры отопления
	unset($circ_pump_total);
	$heat_circ = $ab->doc_read($ab->get_id("heat_circ"));
	$circ_ContID = $heat_circ['ContID'];
	$result = $ab->select("SELECT tmpID AS circ_tmpID, circ_title, circ_type, circ_summer, circ_winter,
		circ_hyst, circ_temp_calc, circ_temp_water, circ_mix_up, circ_mix_down, circ_pump,
		circ_pump_status, circ_temp_in, circ_flap, circ_eco, circ_t_in, circ_mix_step
		FROM tmp_".$heat_circ['ListID']."
		WHERE ContID=".$heat_circ['ContID']);
	for ( $i = 0; $i < count($result); $i++ )
	{
		foreach ($result[$i] as $key => $val)
		$$key = $val;

		// Если котел выключен, для включения насоса контура гистерезис требуемой температуры устанавливается в 1 градус
		if ( $heat_boiler_status == 0 )
		$circ_hyst = 0.5;
			
		unset($circ_status);

		# Восстановление состояния насоса в случае некорректного выключения компьютера
		if ( $start == 1 )
		key_sw($circ_pump, $circ_pump_status);

		$circ_pump_total = $circ_pump_total + $circ_pump_status;

		# Расчитываем базовую температуру в помещении
		if ( $heat_mode == "Зима" )
		$temp_base = $circ_winter;
		elseif ( $heat_mode == "Лето" )
		$temp_base = $circ_summer;
		elseif ( $heat_mode == "Авто" )
		{
			if ( $temp_out >= 15 )
			$temp_base = $circ_summer;
			elseif ( $temp_out <= 0 )
			$temp_base = $circ_winter;
			else
			$temp_base = $circ_winter - round(($circ_winter - $circ_summer) * ($temp_out / 15), 2);

			# Если включен режим экономии, изменяем температуру на заданное значение
			if ( $eco_mode == 1 )
			$temp_base = $temp_base + $circ_eco;
		}

		$ab->mod_write($circ_ContID, "circ_temp_calc='$temp_base'", "tmpID=$circ_tmpID");

		unset($cur_temp);
		unset($past_temp);

		# Считываем информацию от датчика в помещении
		$circ_temp_in = $ab->doc_read($ab->get_id($circ_temp_in));
		
		# Текущая температура в помещении и температурный тренд
		$result1 = $ab->select("SELECT dev_value FROM tmp_".$circ_temp_in['ListID']." WHERE ContID=".$circ_temp_in['ContID']." ORDER BY dev_date DESC LIMIT 3");
		for ( $j = 0; $j < count($result1); $j++ )
		{
			if ( empty($cur_temp) )
			$cur_temp = $result1[$j]['dev_value'];
			$past_temp = $result1[$j]['dev_value'];
		}

		if ( $cur_temp > $past_temp + 0.1 )
		$temp_trend = "+";
		elseif ( $cur_temp < $past_temp - 0.1 )
		$temp_trend = "-";
		else
		unset($temp_trend);

		# Средняя температура в помещении за 30 минут
		$result1 = $ab->select_line("SELECT SUM(dev_value)/COUNT(tmpID) temp_in FROM tmp_".$circ_temp_in['ListID']." WHERE ContID=".$circ_temp_in['ContID']." AND dev_date>=(NOW() - INTERVAL 30 MINUTE) AND dev_date<=NOW() ORDER BY dev_date");
		$temp_in = number_format(round($result1['temp_in'], 2), 2);

		# Средняя температура подачи за 5 минут
		if ( !empty($circ_t_in) )
		{
			$circ_t_water = $ab->doc_read($ab->get_id($circ_t_in));
			$result1 = $ab->select_line("SELECT SUM(dev_value)/COUNT(tmpID) circ_temp_water_real FROM tmp_".$circ_t_water['ListID']." WHERE ContID=".$circ_t_water['ContID']." AND dev_date>=(NOW() - INTERVAL 5 MINUTE) AND dev_date<=NOW() ORDER BY dev_date");
			$circ_temp_water_real = number_format(round($result1['circ_temp_water_real'], 2), 2);

			log_heat("$circ_title: Фактическая температура подачи: $circ_temp_water_real</b>");

		}

		# Отладка. Выводим информацию по температурам
		log_heat("$circ_title: Расчет: $temp_base, Сред: $temp_in, Тек: $cur_temp");

		# РАССЧЕТ ТЕМПЕРАТУРЫ ПОДАЧИ ТЕПЛОНОСИТЕЛЯ

		// Если отопление отключено
		if ( $heat_key == 0 )
		{
			if ( $circ_pump_status == 1 )
			{
					if ( $circ_type == "Смесительный" )
					{
						key_sw($circ_mix_up, 0);
						key_sw($circ_mix_down, 1);
						sleep(130);
						key_sw($circ_mix_down, 0);
						$ab->mod_write($circ_ContID, "circ_flap='0'", "tmpID=$circ_tmpID");
						log_heat("$circ_title: <b><font color=\"#0a3f7b\">Полностью закрываем привод</font></b>");
					}
					
					$ab->mod_write($circ_ContID, "circ_pump_status='0'", "tmpID=$circ_tmpID");
					$circ_pump_total--;
					key_sw($circ_pump, 0);
					log_heat("$circ_title: <b><font color=\"#0a3f7b\">Выключаем насос</font></b>");
			}
		}
		elseif ( $circ_title == "ТП" )
		{
			// Определяем состояние основного контура
			$main_c_status = $ab->mod_read($circ_ContID, "#circ_pump_status#", "", 1, "circ_title='Дом'");
			$main_c_water = $ab->mod_read($circ_ContID, "#circ_temp_water#", "", 1, "circ_title='Дом'");
			
			// Температура на улице выше, чем в требуется в доме, отключаем контур
			if ( $temp_out > $temp_base && $circ_pump_status == 1)
			{
				log_heat("$circ_title: Температура на улице ($temp_out) выше, чем требуется в доме ($temp_base), отключаем контур");
				$circ_temp_water = 29;
			}
			else
			if ( $main_c_status == 0 && $circ_pump_status == 1 )
			{
				# Если температура внутри выше базовой, снижаем расчетную температуру подачи
				if ( $temp_in > $temp_base + $circ_hyst && $cur_temp > $temp_base + $circ_hyst  )
				{
					# Проверяем, не слишком ли температура подачи отличается от расчетной
					if ( $circ_temp_water_real > $circ_temp_water + 1 )
					log_heat("$circ_title: <font color=\"#0a3f7b\">Фактическая температура подачи значительно отличается от расчетной. Ждем, когда t достигнет нормы.</font>");
					elseif ( $temp_trend == "-" )
					log_heat("$circ_title: Температура в помещении снижается ($past_temp -> $cur_temp). Наблюдаем");
					else
					{
						$temp_corr = 0.05;
						$circ_temp_water = $circ_temp_water - $temp_corr;
						log_heat("$circ_title: <font color=\"#0a3f7b\">Понижаем расчетную температуру подачи на $temp_corr: <b>$circ_temp_water</b></font>");
						# Записываем значение расчетной температуры теплоносителя в БД
						$ab->mod_write($circ_ContID, "circ_temp_water='$circ_temp_water'", "tmpID=$circ_tmpID");
						$circ_status = "-";
					}
				}
				# Если температура внутри ниже базовой, повышаем расчетную температуру подачи
				elseif ( $temp_in < $temp_base - $circ_hyst && $cur_temp < $temp_base - $circ_hyst )
				{
					# Проверяем, не слишком ли температура подачи отличается от расчетной
					if ( $circ_temp_water_real < $circ_temp_water - 1 )
					log_heat("$circ_title: <font color=\"#891010\">Фактическая температура подачи значительно отличается от расчетной. Ждем, когда t достигнет нормы.</font>");
					elseif ( $temp_trend == "+" )
					log_heat("$circ_title: Температура в помещении повышается ($past_temp -> $cur_temp). Наблюдаем");
					else
					{
						$temp_corr = 0.1;
						$circ_temp_water = $circ_temp_water + $temp_corr;
						log_heat("$circ_title: <font color=\"#891010\">Повышаем расчетную температуру подачи на $temp_corr: <b>$circ_temp_water</b></font>");
						# Записываем значение расчетной температуры теплоносителя в БД
						$ab->mod_write($circ_ContID, "circ_temp_water='$circ_temp_water'", "tmpID=$circ_tmpID");
						$circ_status = "+";
					}
				}
			}
			elseif ( $main_c_status == 1 && $circ_pump_status == 1 )
			{
				if ( $main_c_water > $circ_temp_water )
				{
						if ( $circ_temp_water >= 55 )
						log_heat("$circ_title: <font color=\"#891010\">Достингута максимальная температура ТП: <b>$circ_temp_water</b></font>");
						//else
						// Повышаем температуру ТП, если температура основного контура выше, а температура внутри помещения ниже нормальной
						elseif ( $temp_in < $temp_base - $circ_hyst && $cur_temp < $temp_base - $circ_hyst )
						{
							$temp_corr = 0.05;
							$circ_temp_water = $circ_temp_water + $temp_corr;
							log_heat("$circ_title: <font color=\"#891010\">Температура основного контура выше ТП. Повышаем ТП на $temp_corr: <b>$circ_temp_water</b></font>");
							# Записываем значение расчетной температуры теплоносителя в БД
							$ab->mod_write($circ_ContID, "circ_temp_water='$circ_temp_water'", "tmpID=$circ_tmpID");
							$circ_status = "+";
						}
				
				}
				elseif ( $main_c_water < $circ_temp_water - 7 )
				{
						$temp_corr = 0.05;
						$circ_temp_water = $circ_temp_water - $temp_corr;
						log_heat("$circ_title: <font color=\"#0a3f7b\">Температура основного контура на 7 градусов ниже ТП. Понижаем ТП: <b>$circ_temp_water</b></font>");
						# Записываем значение расчетной температуры теплоносителя в БД
						$ab->mod_write($circ_ContID, "circ_temp_water='$circ_temp_water'", "tmpID=$circ_tmpID");
						$circ_status = "-";
				}
			}
			elseif ( $main_c_status == 1 && $circ_pump_status == 0 )
			{
					$ab->mod_write($circ_ContID, "circ_pump_status='1'", "tmpID=$circ_tmpID");
					$circ_pump_total++;
					key_sw($circ_pump, 1);

					# Если привод на момент включения насоса в закрытом положении, открываем его.
					if ( $circ_flap <= 0 )
					{
						$circ_flap = 65;
						$circ_temp_water = $main_c_water;

						key_sw($circ_mix_down, 0);
						$circ_time0 = key_sw($circ_mix_up, 1);
						sleep($circ_flap);
						$circ_time1 = key_sw($circ_mix_up, 0);
						$circ_time2 = round($circ_time1 - $circ_time0, 2);

						$circ_flap = $circ_time2;

						$ab->mod_write($circ_ContID, "circ_flap='$circ_flap', circ_temp_water='$circ_temp_water'", "tmpID=$circ_tmpID");

						log_heat("$circ_title: <b><font color=\"#891010\">Повышаем x $circ_flap с.</font></b> Положение привода $circ_flap с.");
					}
	
					log_heat("$circ_title: <b><font color=\"#891010\">Включаем насос</font></b>");
			}
		}
		# Если температура внутри выше базовой, снижаем расчетную температуру подачи
		elseif ( $temp_in > $temp_base + $circ_hyst && $cur_temp > $temp_base + $circ_hyst  )
		{
			if ( $circ_type == "Отключен" )
			log_heat("$circ_title: Контур отключен. Снижение температуры невозможно.");
			elseif ( $circ_type == "Прямой" )
			{
				if ( $circ_pump_status == 1 )
				{
					$ab->mod_write($circ_ContID, "circ_pump_status='0'", "tmpID=$circ_tmpID");
					$circ_pump_total--;
					key_sw($circ_pump, 0);

					log_heat("$circ_title: <b><font color=\"#0a3f7b\">Выключаем насос</font></b>");

				}
				else
				log_heat("$circ_title: Насос уже выключен");
			}
			else
			{
				// Изменять расчетную температуру подачи имеет смысл только если насос включен
				if ( $circ_pump_status == 1 )
				{
					// Температура на улице выше, чем в требуется в доме, отключаем контур
					if ( $temp_out > $temp_base && $circ_pump_status == 1)
					{
						log_heat("$circ_title: Температура на улице ($temp_out) выше, чем требуется в доме ($temp_base), отключаем контур");
						$circ_temp_water = 29;
					}
					else
					{
						# Проверяем, не слишком ли температура подачи отличается от расчетной
						if ( $circ_temp_water_real > $circ_temp_water + 1 )
						log_heat("$circ_title: <font color=\"#0a3f7b\">Фактическая температура подачи значительно отличается от расчетной. Ждем, когда t достигнет нормы.</font>");
						elseif ( $temp_trend == "-" )
						log_heat("$circ_title: Температура в помещении снижается ($past_temp -> $cur_temp). Наблюдаем");
						else
						{
							$temp_corr = 0.1;
							$circ_temp_water = $circ_temp_water - $temp_corr;
							log_heat("$circ_title: <font color=\"#0a3f7b\">Понижаем расчетную температуру подачи на $temp_corr: <b>$circ_temp_water</b></font>");
							# Записываем значение расчетной температуры теплоносителя в БД
							$ab->mod_write($circ_ContID, "circ_temp_water='$circ_temp_water'", "tmpID=$circ_tmpID");
							$circ_status = "-";
						}
					}
				}
				else
				log_heat("$circ_title: Насос отключен");

			}
		}

		# Если температура внутри ниже базовой, увеличиваем расчетную температуру подачи
		elseif ( $temp_in < $temp_base - $circ_hyst && $cur_temp < $temp_base - $circ_hyst )
		{
			if ( $circ_type == "Отключен" )
			log_heat("$circ_title: Контур отключен. Повышение температуры невозможно.");
			elseif ( $circ_type == "Прямой" )
			{
				if ( $circ_pump_status == 0 )
				{
					$ab->mod_write($circ_ContID, "circ_pump_status='1'", "tmpID=$circ_tmpID");
					$circ_pump_total++;
					key_sw($circ_pump, 1);

					log_heat("$circ_title: <b><font color=\"#891010\">Включаем насос</font></b>");
				}
				else
				log_heat("$circ_title: Насос уже включен");
			}
			else
			{
				# Если насос выключен, включаем его
				if ( $circ_pump_status == 0  )
				{
					$ab->mod_write($circ_ContID, "circ_pump_status='1'", "tmpID=$circ_tmpID");
					$circ_pump_total++;
					key_sw($circ_pump, 1);

					# Если привод на момент включения насоса в закрытом положении, открываем его.
					if ( $circ_flap <= 0 )
					{
						if ( $circ_title == "Дом" )
						$circ_flap = 25;
						else
						$circ_flap = 70;

						$circ_temp_water = 32;

						key_sw($circ_mix_down, 0);
						//$circ_time1 = microtime(get_as_float);
						$circ_time0 = key_sw($circ_mix_up, 1);
						sleep($circ_flap);
						$circ_time1 = key_sw($circ_mix_up, 0);
						$circ_time2 = round($circ_time1 - $circ_time0, 2);

						$circ_flap = $circ_time2;

						$ab->mod_write($circ_ContID, "circ_flap='$circ_flap', circ_temp_water='$circ_temp_water'", "tmpID=$circ_tmpID");

						log_heat("$circ_title: <b><font color=\"#891010\">Повышаем x $circ_flap с.</font></b> Положение привода $circ_flap с.");
					}
	
					log_heat("$circ_title: <b><font color=\"#891010\">Включаем насос</font></b>");
				}

				# Проверяем, не слишком ли температура подачи отличается от расчетной
				if ( $circ_temp_water_real < $circ_temp_water - 1 )
				log_heat("$circ_title: <font color=\"#891010\">Фактическая температура подачи значительно отличается от расчетной. Ждем, когда t достигнет нормы.</font>");
				elseif ( $temp_trend == "+" )
				log_heat("$circ_title: Температура в помещении повышается ($past_temp -> $cur_temp). Наблюдаем");
				else
				{
					$temp_corr = 0.1;
					$circ_temp_water = $circ_temp_water + $temp_corr;
					log_heat("$circ_title: <font color=\"#891010\">Повышаем расчетную температуру подачи на $temp_corr: <b>$circ_temp_water</b></font>");
					# Записываем значение расчетной температуры теплоносителя в БД
					$ab->mod_write($circ_ContID, "circ_temp_water='$circ_temp_water'", "tmpID=$circ_tmpID");
					$circ_status = "+";
				}
			}
		}
		else
		log_heat("$circ_title: Температура в помещении соответствует норме");

		# УПРАВЛЕНИЕ ПРИВОДАМИ
		# Если температура подачи в течение 5 минут больше расчетной температуры и нет повышения расч. температуры, убавляем

		if ( $circ_type == "Смесительный" && $circ_pump_status == 1 && $heat_key == 1 )
		{
			/*
			if ( $gvs_cur == 1 && $gvs_mode[$circ_tmpID] == 0 )
			{
				$circ_flap_duration = 20;

				key_sw($circ_mix_down, 0);
				$circ_time0 = key_sw($circ_mix_up, 1);
				usleep($circ_flap_duration);
				$circ_time1 = key_sw($circ_mix_up, 0);
				$circ_time2 = round($circ_time1 - $circ_time0, 2);

				$circ_flap = $circ_flap + $circ_time2;

				if ( $circ_flap > 120 )
				$circ_flap = 120;

				$ab->mod_write($circ_ContID, "circ_flap='$circ_flap'", "tmpID=$circ_tmpID");

				$gvs_mode[$circ_tmpID] = 1;

				log_heat("$circ_title: <b><font color=\"#891010\">Включен ГВС. Повышаем x $circ_flap_duration c.</font></b> Положение привода $circ_flap с");
				
			}
			else if ( $gvs_cur == 0 && $gvs_mode[$circ_tmpID] == 1 )
			{
				$circ_flap_duration = 20;

				key_sw($circ_mix_up, 0);
				$circ_time0 = key_sw($circ_mix_down, 1);
				usleep($circ_flap_duration);
				$circ_time1 = key_sw($circ_mix_down, 0);
				$circ_time2 = round($circ_time1 - $circ_time0, 2);

				$circ_flap = $circ_flap + $circ_time2;

				if ( $circ_flap > 120 )
				$circ_flap = 120;

				$ab->mod_write($circ_ContID, "circ_flap='$circ_flap'", "tmpID=$circ_tmpID");

				$gvs_mode[$circ_tmpID] = 0;

				log_heat("$circ_title: <b><font color=\"#0a3f7b\">Выключен ГВС. Снижаем x $circ_flap_duration c.</font></b> Положение привода $circ_flap с");

			}
			else
			*/
			{
				if ( $circ_temp_water < 30 )
				$turn_off = 1;

				if ( $circ_temp_water_real > $circ_temp_water + 0.5 || $turn_off == 1 )
				{
					if ( $circ_status != "+" || $circ_temp_water_real > $circ_temp_water + 1 || $turn_off == 1 )
					{
						if ( $turn_off != 1 )
						{
							if ( $circ_temp_water_real > $circ_temp_water + 3 )
							$circ_flap_duration = $circ_mix_step * 2;
							else
							$circ_flap_duration = $circ_mix_step;
	
							$circ_flap_duration2 = $circ_flap_duration * 1000000;
	
							key_sw($circ_mix_up, 0);
							$circ_time0 = key_sw($circ_mix_down, 1);
							usleep($circ_flap_duration2);
							$circ_time1 = key_sw($circ_mix_down, 0);
							$circ_time2 = round($circ_time1 - $circ_time0, 2);
			
							$circ_flap = $circ_flap - $circ_time2;
							$ab->mod_write($circ_ContID, "circ_flap='$circ_flap'", "tmpID=$circ_tmpID");
	
							//log_heat("$circ_title: circ_time2=$circ_time2; circ_time1=$circ_time1; circ_time0=$circ_time0; circ_flap_duration2=$circ_flap_duration2");
	
						}
	
						# Привод в полностью закрытом положении. Отключаем насос
						if ( $circ_flap <= 0 || ( $circ_title == "ТП" && $circ_flap <= 40 ) || ( $circ_title == "Дом" && $circ_flap <= 10 ) || $turn_off == 1  )
						{
							$ab->mod_write($circ_ContID, "circ_flap='0'", "tmpID=$circ_tmpID");
							$ab->mod_write($circ_ContID, "circ_pump_status='0'", "tmpID=$circ_tmpID");
							$circ_pump_total--;
							key_sw($circ_pump, 0);
							key_sw($circ_mix_up, 0);
							key_sw($circ_mix_down, 1);
							sleep(120);
							key_sw($circ_mix_down, 0);
	
							if ( $turn_off == 1 )
							{
								log_heat("$circ_title: <b><font color=\"#0a3f7b\">Расчетная температура теплоносителя менее 30 градусов</font></b>");
								$turn_off = 0;
							}
	
							log_heat("$circ_title: <b><font color=\"#0a3f7b\">Закрываем привод. Отключаем насос</font></b>");
							$ab->mod_write($circ_ContID, "circ_temp_water=32", "tmpID=$circ_tmpID");
	
						}
						else
						{
							$ab->mod_write($circ_ContID, "circ_flap='$circ_flap'", "tmpID=$circ_tmpID");
							log_heat("$circ_title: <b><font color=\"#0a3f7b\">Понижаем x $circ_flap_duration c.</font></b> Положение привода $circ_flap с");
						}
					}
					else
					log_heat("$circ_title: Пропускаем цикл уменьшения темп. подачи, так как требуется увеличить температуру в помещении.");

				}
				# Если температура подачи в течение 5 минут меньше расчетной температуры и нет снижения расч. температуры, прибавляем
				elseif ( $circ_temp_water_real < $circ_temp_water - 0.5  )
				{
					if ( $circ_status != "-" || $circ_temp_water_real < $circ_temp_water - 1 )
					{
						if ( $circ_temp_water_real < $circ_temp_water - 3 )
						$circ_flap_duration = $circ_mix_step * 2;
						else
						$circ_flap_duration = $circ_mix_step;
	
						$circ_flap_duration2 = $circ_flap_duration * 1000000;
	
						key_sw($circ_mix_down, 0);
						$circ_time0 = key_sw($circ_mix_up, 1);
						usleep($circ_flap_duration2);
						$circ_time1 = key_sw($circ_mix_up, 0);
						$circ_time2 = round($circ_time1 - $circ_time0, 2);
	
						$circ_flap = $circ_flap + $circ_time2;
	
						if ( $circ_flap > 120 )
						$circ_flap = 120;

						$ab->mod_write($circ_ContID, "circ_flap='$circ_flap'", "tmpID=$circ_tmpID");
						log_heat("$circ_title: <b><font color=\"#891010\">Повышаем x $circ_flap_duration c.</font></b> Положение привода $circ_flap с");
					}
					else
					log_heat("$circ_title: Пропускаем цикл увеличение темп. подачи, так как требуется снизить температуру в помещении.");

				}
				else
				log_heat("$circ_title: Изменение положения привода не требуется</font>");
			}
		}
	}


	# Если котел выключен, а есть запрос тепла - включаем котел.
	if ( $heat_boiler_status == 0 && $circ_pump_total > 0 )
	{
		// echo "Heating boiler on\n";
		$ab->mod_write($heat_conf_id, "heat_boiler_status='1'", "tmpID>0");
		$heat_boiler_status = 1;
		key_sw($heat_boiler, 1);

		log_heat("<b><font color=\"#ae0a35\">Включаем котел</font></b>");
		sleep(300);

	}
	# Если котел включен, а запроса тепла нет - выключаем котел.
	elseif ( $heat_boiler_status == 1 && $circ_pump_total == 0 )
	{
		// echo "Heating boiler off\n";
		$ab->mod_write($heat_conf_id, "heat_boiler_status='0'", "tmpID>0");
		$ab->mod_write($heat_conf_id, "heat_boiler_temp_calc='60'", "tmpID>0");
		$heat_boiler_status = 0;
		key_sw($heat_boiler, 0);

		log_heat("<b><font color=\"104fa5\">Выключаем котел</font></b>");
	}

	sleep(300);
	unset($start);
}

?>