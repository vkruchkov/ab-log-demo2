<?
/*
* Copyright (c) 2016, Andrey_B
* http://ab-log.ru
* Подробнее см. LICENSE.txt или http://www.gnu.org/licenses/
*/

/*
Скрипт для работы с датчиком атмосферного давления BMP180
Использует драйвер BMP180 и библиотеку I2C-PHP
*/

define("SCL", "7");
define("SDA", "8");
define("MD", "http://192.168.0.121/sec/?");

require_once("mod_i2c_bmp180.php");

// Вариант реализации I2C: 1 - полностью программный; 2 - частично аппаратный (прошивка 3.43beta1 и выше)
define("V", "2");

//$time1 = microtime(TRUE);

//echo get_temperature()."\n";

require_once("ab-cms/class/main_class.php");
$ab = new cms_lib();

$result = get_pressure();
$ab->mod_write(103, "SYSDATE(), '$result'");
//echo $result."\n";

//$time2 = microtime(TRUE);
//$time3 = $time2 - $time1;
//echo $time3."\n";

?>