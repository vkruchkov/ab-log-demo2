<?
/*
* Copyright (c) 2014, Andrey_B
* http://ab-log.ru
* Подробнее см. LICENSE.txt или http://www.gnu.org/licenses/
*/

# Проверка работоспособности крана на вводе и процедура для предотвращения "закисания"

require_once("ab-cms/class/main_class.php");
$ab = new cms_lib();
include("key.php");

# Закрываем кран
key_sw("water_valve_cl", 1);
sleep(15);

# Проверяем состояние крана
$state = file_get_contents("http://192.168.0.101/sec/?pt=6&cmd=get");
if ( preg_match("/^OFF/", $state) )
echo "Кран закрыт\n";
else
$ab->mod_write($ab->get_id("alarm", 1), "SYSDATE(), 'Ошибка крана на вводе воды!'");

# Открываем кран
key_sw("water_valve_op", 1);
sleep(15);
key_sw("water_valve_cl", 0);
key_sw("water_valve_op", 0);

$state = file_get_contents("http://192.168.0.101/sec/?pt=6&cmd=get");
if ( preg_match("/^ON/", $state) )
echo "Кран открыт\n";
else
$ab->mod_write($ab->get_id("alarm", 1), "SYSDATE(), 'Ошибка крана на вводе воды!'");

# Удаляем старые изображения из архива камеры наблюдения

$snap_archive = array("archive", "archive2");

for ( $l = 0; $l < count($snap_archive); $l++ )
{
	foreach (glob("/var/www/snap/$snap_archive[$l]/*") as $filename)
	{
		unset($my_dir);
		unset($i);
	
		foreach (glob("$filename/*") as $filename1)
		{
			$my_dir[$i++] = "$filename1";
		}

		$my_time = time() - 604800;
		if ( count($my_dir) > 50 && basename($filename) < date("Ymd", $my_time) )
		{
			for ( $j = 0; $j < count($my_dir); $j++ )
			{
				if ( $k == 1 )
				if ( !empty($my_dir[$j]) )
				unlink($my_dir[$j]);
				if ( $k == 0 ) $k = 1; else $k = 0;
			}
		}
	}
}


?>
