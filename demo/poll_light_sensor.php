<?
/*
* Copyright (c) 2013, Andrey_B
* http://ab-log.ru
* Подробнее см. LICENSE.txt или http://www.gnu.org/licenses/
*/

require_once("ab-cms/class/main_class.php");
$ab = new cms_lib();
include("key.php");

// Порог датчика (для DFRobot Ambient Light Sensor DFR0026 V2)
$act_level_on = 15;
$act_level_off = 15; // 20

$light_id = $ab->get_id("light1");
//$last_val = $ab->mod_read($light_id, "#dev_value#", "tmpID DESC", 1);
// Выбираем 2 предыдущих значения из БД
$my_p = $ab->doc_read($light_id);
$last_val = $ab->select("SELECT dev_value FROM tmp_".$my_p['ListID']." WHERE ContID=".$my_p['ContID']." ORDER BY tmpID DESC LIMIT 2");

// Опрашиваем датчик несколько раз, берем значение, близкое к минимальному
for ( $i = 0; $i < 5; $i++ )
{
	$cur_val[] = file_get_contents('http://192.168.0.120/sec/?pt=14&cmd=get');
	sleep(3);
}
sort($cur_val);

$ext_light = $ab->mod_read($ab->get_id("keys"), "#key_pio#", "", 1, "key_label='ext_light'");
// Записываем значение в БД
$ab->mod_write($light_id, "SYSDATE(), $cur_val[1]");
// Смотрим в журнале когда было последнее срабатывания для предотвращения вкл/выкл лампы, когда уровень освещенности находится в пограничном состоянии
$last_act = $ab->mod_read($ab->get_id("keys_journal"), "#key_i_pio#", "tmpID DESC", 1, "key_j_label=1  AND key_j_date>NOW() - INTERVAL 1 HOUR");

if ( $last_act != "" )
exit;

if ( $ext_light == 0 && $last_val[0]['dev_value'] < $act_level_on && $last_val[1]['dev_value'] < $act_level_on && $cur_val[1] < $act_level_on )
key_sw("ext_light", 1);
elseif ( $ext_light == 1 && $last_val[0]['dev_value'] > $act_level_off && $last_val[1]['dev_value'] > $act_level_off && $cur_val[1] > $act_level_off )
key_sw("ext_light", 0);
