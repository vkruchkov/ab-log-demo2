#!/usr/bin/env python
# coding: utf-8

import sys
import MySQLdb

from pywws import WeatherStation

ws = WeatherStation.weather_station()
ptr = ws.current_pos()
data = ws.get_data(ptr)
db = MySQLdb.connect(host='localhost', user='root', passwd='password', db='demo')
cursor= db.cursor()
cursor.execute("SELECT ListID FROM tList WHERE Title='Устройства' LIMIT 1")
ListID = cursor.fetchone()
cursor.execute("SELECT MAX(tmpID) FROM tmp_" + str(ListID[0]))
result = cursor.fetchone()
tmpID = result[0] + 1
#print tmpID
if ( data["status"] != 128 ):
	cursor.execute("SELECT MAX(tmpID) FROM tmp_6")
	result = cursor.fetchone()
	tmpID = result[0] + 1
	cursor.execute("INSERT INTO tmp_6 VALUES(" + str(tmpID) + ", 71, 'Нет связи с метеостанцией'")
	sys.exit(0)
for w_data in data:
	#print w_data, str(data[w_data])
	if w_data == 'abs_pressure':
		data[w_data] = round(data[w_data] * 0.750062, 2)
	if w_data != 'status' and w_data != 'delay':
		cursor.execute("SELECT ContID FROM tCont WHERE Cont_Label='wh_" + w_data + "' LIMIT 1")
		result = cursor.fetchone()
		if ( str(data[w_data]) == "None" ):
			data[w_data] = 0
		cursor.execute("INSERT INTO tmp_" + str(ListID[0]) + " VALUES(" + str(tmpID) + ", " + str(result[0]) + ", SYSDATE(), " + str(data[w_data]) + ")")
		#print "INSERT INTO tmp_" + str(ListID[0]) + " VALUES(" + str(tmpID) + ", " + str(result[0]) + ", SYSDATE(), " + str(data[w_data]) + ")"
		tmpID = tmpID + 1
db.commit()
#sys.exit(0)
