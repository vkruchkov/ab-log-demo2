<?
/*
* Copyright (c) 2013, Andrey_B
* http://ab-log.ru
* Подробнее см. LICENSE.txt или http://www.gnu.org/licenses/
*/

require_once("ab-cms/class/main_class.php");
$ab = new cms_lib();

// ownet.php не удовлетворяет Strict Standarts
error_reporting(E_ALL ^ E_NOTICE);

require "/opt/owfs/share/php/OWNet/ownet.php";

$ow=new OWNet("tcp://localhost:3000");

$my_value = NULL;

$my_value = $ow->get("28.D6C18D020000/temperature11");
if ( is_null($my_value) )
$my_value = $ow->get("28.D6C18D020000/temperature11");

if ( !is_null($my_value) && $my_value != 85 && $my_value != "0" )
{
	$my_value = round($my_value, 2) + $dev_shift;
	$ab->mod_write(55, "SYSDATE(), '$my_value'");
}
else
$ab->mod_write(get_id_cl("net_errors", 1), "SYSDATE(), '28.D6C18D020000'");

$my_value = $ow->get("28.0B1F27020000/temperature11");
if ( is_null($my_value) )
$my_value = $ow->get("28.0B1F27020000/temperature11");

if ( !is_null($my_value) && $my_value != 85 && $my_value != "0" )
{
	$my_value = round($my_value, 2) + $dev_shift;
	$ab->mod_write(40, "SYSDATE(), '$my_value'");
}
else
$ab->mod_write($ab->get_id("net_errors", 1), "SYSDATE(), '28.0B1F27020000'");

$my_value = $ow->get("28.683927020000/temperature11");
if ( is_null($my_value) )
$my_value = $ow->get("28.683927020000/temperature11");

if ( !is_null($my_value) && $my_value != 85 && $my_value != "0" )
{
	$my_value = round($my_value, 2) + $dev_shift;
	$ab->mod_write(41, "SYSDATE(), '$my_value'");
}
else
$ab->mod_write($ab->get_id("net_errors", 1), "SYSDATE(), '28.683927020000'");


unset($ow);
