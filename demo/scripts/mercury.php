<?
/*
* Copyright (c) 2013, Andrey_B
* http://ab-log.ru
* Подробнее см. LICENSE.txt или http://www.gnu.org/licenses/
*/

// BUS Type: RS485 or CAN
define('BUS', 'CAN');

require_once("/var/www/ab-cms/class/main_class.php");
$ab = new cms_lib();

###### WIN32 WORKAROUND

// Helper function
function getmicrotime(){
    list($usec, $sec) = explode(' ',microtime());
    return ((float)$usec + (float)$sec);
}

// usleep alias function for windows
function msleep($micro_seconds=0) {
    $stop  = getmicrotime() + ($micro_seconds / 1000);
    while (getmicrotime() <= $stop) {
        // loop
    }
    return true;
}

/*
function usleep_win($msec) {
   $usec = $msec * 1000;
   socket_select($read = NULL, $write = NULL, $sock = array(socket_create (AF_INET, SOCK_RAW, 0)), 0, $usec);
}
*/

function usleep_win($msec) {
   usleep($msec * 1000);
}

#exec('mode com3: baud=9600 data=8 stop=1 parity=n xon=off');

$fd = dio_open('/dev/ttyUSB0', O_RDWR);

###### WIN32 WORKAROUND


##### FOR LINUX BOX

//dio_fcntl($fd, F_SETFL, O_SYNC);


dio_tcsetattr($fd, array(
  'baud' => 9600,
  'bits' => 8,
  'stop'  => 1,
  'parity' => 0
)); 


##############

function merc_gd($cmd, $factor = 1, $total = 0)
{
	global $fd;
	global $sleep_time;

	usleep_win(50);
	flush();
	dio_write($fd, $cmd, 6);
	usleep_win($sleep_time);
	$result = dio_read($fd, 64);

	$ret = array();
	
	if ( BUS == "CAN" )
	$start_byte = 7;
	else
	$start_byte = 1;
	
	if ( $total != 1 )
	{
		for ( $i = 0; $i < 4; $i++ )
		{
			if ( dechex(ord($result[$start_byte + $i * 3])) >= 40 )
			$result[$start_byte + $i * 3] = chr(dechex(ord($result[$start_byte + $i * 3])) - 40);
			if ( strlen($result) > $start_byte + 2 + $i * 3 )
			$ret[$i] = hexdec(dd($result[$start_byte + $i * 3]).dd($result[$start_byte + $i * 3 + 2]).dd($result[$start_byte + $i * 3 + 1]))*$factor;
		}
	}
	else
	$ret[0] = hexdec(dd($result[$start_byte+1]).dd($result[$start_byte]).dd($result[$start_byte+3]).dd($result[$start_byte+2]))*$factor;


	return $ret;
}

$sleep_time = 200;

function dd($data = "")
{
	$result = "";
	$data2 = "";
	for ( $j = 0; $j < count($data); $j++ )
	{
		$data2 = dechex(ord($data[0]));
		if ( strlen($data2) == 1  )
		$result = "0".$data2;
		else
		$result .= $data2;

	}
	return $result;
}

# Инициализация соединения, передача пароля
dio_write($fd, "\x00\x01\x01\x01\x01\x01\x01\x01\x01\x77\x81", 11);
usleep_win($sleep_time);
$result = dio_read($fd, 15);

$n = 0;
$total_cnt = 90;
$total_cnt1 = 1080;
while ( $n == 0 )
{

	# Сила тока по фазам
	# =====================================================
	$Ia = merc_gd("\x00\x08\x16\x21\x4F\x9E", 0.001);
	echo "Ia: $Ia[0] - $Ia[1] - $Ia[2]\r\n";
	$It = $Ia[0] + $Ia[1] + $Ia[2];
	$ab->mod_write(30, "SYSDATE(), $It, $Ia[0], $Ia[1], $Ia[2]");
	# Мощность по фазам
	# =====================================================
	$Pv = merc_gd("\x00\x08\x16\x00\x8F\x86", 0.01);
	if ( round($Pv[0], 2) != round($Pv[1] + $Pv[2] + $Pv[3], 2) )
	$error = "error"; else $error = "";
	echo "Pv: $Pv[0] - $Pv[1] - $Pv[2] - $Pv[3] $error\r\n";

	$Pv[0] = $Pv[1] + $Pv[2] + $Pv[3];

	$ab->mod_write(31, "SYSDATE(), $Pv[0], $Pv[1], $Pv[2], $Pv[3]");
	# Cosf по фазам
	# =====================================================
	$Cos = merc_gd("\x00\x08\x16\x30\x8F\x92", 0.001);
	echo "Cos: $Cos[0] - $Cos[1] - $Cos[2] - $Cos[3]\r\n";
	$ab->mod_write(32, "SYSDATE(), $Cos[0], $Cos[1], $Cos[2], $Cos[3]");

	# Напряжение по фазам
	# =====================================================
	$Uv = merc_gd("\x00\x08\x16\x11\x4F\x8A", 0.01);
	echo "Uv: $Uv[0] - $Uv[1] - $Uv[2]\r\n";
	$ab->mod_write(33, "SYSDATE(), 0, $Uv[0], $Uv[1], $Uv[2]");

	/*
	if ( empty($Uv[0]) && empty($Uv[1]) && empty($Uv[2]) )
	{
		dio_close($fd);
		$fd = dio_open('/dev/ttyUSB0', O_RDWR);
		dio_tcsetattr($fd, array(
		  'baud' => 9600,
		  'bits' => 8,
		  'stop'  => 1,
		  'parity' => 0
		)); 

		sleep(2);

		# Инициализация соединения, передача пароля
		dio_write($fd, "\x00\x01\x01\x01\x01\x01\x01\x01\x01\x77\x81", 11);
		usleep_win($sleep_time);
		$result = dio_read($fd, 15);

	}
	*/

	$total_cnt++;
	if ( $total_cnt >= 90 )
	{
		// Общее потребление
		$Tot = merc_gd("\x00\x05\x00\x00\x10\x25", 0.001, 1);
		echo "Total: $Tot[0]\r\n";
		if ( round($Tot[0], 0) != 0 )
		$ab->mod_write(34, "dev_value=$Tot[0], dev_date=SYSDATE()", "ContID=34");

		$Tot = merc_gd("\x00\x05\x00\x01\xD1\xE5", 0.001, 1);
		echo "Total T1: $Tot[0]\r\n";
		if ( round($Tot[0], 0) != 0 )
		$ab->mod_write(74, "dev_value=$Tot[0], dev_date=SYSDATE()", "ContID=74");

		$Tot = merc_gd("\x00\x05\x00\x02\x91\xE4", 0.001, 1);
		echo "Total T2: $Tot[0]\r\n";
		if ( round($Tot[0], 0) != 0 )
		$ab->mod_write(75, "dev_value=$Tot[0], dev_date=SYSDATE()", "ContID=75");


		// За текущие сутки
		$Tot = merc_gd("\x00\x05\x40\x00\x21\xE5", 0.001, 1);
		echo "Total cur: $Tot[0]\r\n";
		if ( round($Tot[0], 0) != 0 )
		{
			$result = "";
			$result = $ab->select_line("SELECT tmpID FROM tmp_2 WHERE ContID=35 AND DATE_FORMAT(dev_date, '%Y%m%d')=DATE_FORMAT(NOW() - INTERVAL 1 HOUR, '%Y%m%d') LIMIT 1");
			if ( isset($result['tmpID']) )
			$ab->mod_write(35, "dev_value=$Tot[0]", "tmpID=".$result['tmpID']);
			else
			$ab->mod_write(35, "NOW() - INTERVAL 1 HOUR, $Tot[0]");
		}

		// За текущие сутки (Тариф 1)
		$Tot = merc_gd("\x00\x05\x40\x01\xE0\x25", 0.001, 1);
		echo "Total cur T1: $Tot[0]\r\n";
		if ( round($Tot[0], 0) != 0 )
		{
			$result = "";
			$result = $ab->select_line("SELECT tmpID FROM tmp_2 WHERE ContID=76 AND DATE_FORMAT(dev_date, '%Y%m%d')=DATE_FORMAT(NOW() - INTERVAL 1 HOUR, '%Y%m%d') LIMIT 1");
			if ( isset($result['tmpID']) )
			$ab->mod_write(76, "dev_value=$Tot[0]", "tmpID=".$result['tmpID']);
			else
			$ab->mod_write(76, "NOW() - INTERVAL 1 HOUR, $Tot[0]");
		}
		

		// За текущие сутки (Тариф 2)
		$Tot = merc_gd("\x00\x05\x40\x02\xA0\x24", 0.001, 1);
		echo "Total cur T2: $Tot[0]\r\n";
		if ( round($Tot[0], 0) != 0 )
		{
			$result = "";
			$result = $ab->select_line("SELECT tmpID FROM tmp_2 WHERE ContID=77 AND DATE_FORMAT(dev_date, '%Y%m%d')=DATE_FORMAT(NOW() - INTERVAL 1 HOUR, '%Y%m%d') LIMIT 1");
			if ( isset($result['tmpID']) )
			$ab->mod_write(77, "dev_value=$Tot[0]", "tmpID=".$result['tmpID']);
			else
			$ab->mod_write(77, "NOW() - INTERVAL 1 HOUR, $Tot[0]");
		}

		$total_cnt = 0;
	}

	$total_cnt1++;
	if ( $total_cnt1 >= 1080 )
	{
		// За предыдущие сутки
		$Tot = merc_gd("\x00\x05\x50\x00\x2C\x25", 0.001, 1);
		echo "Total prev: $Tot[0]\r\n";
		if ( round($Tot[0], 0) != 0 )
		$ab->mod_write(35, "dev_value=$Tot[0]", "DATE_FORMAT(dev_date, '%Y%m%d')=DATE_FORMAT(NOW() - INTERVAL 24 HOUR, '%Y%m%d')");


		// За предыдущие сутки (Тариф 1)
		$Tot = merc_gd("\x00\x05\x50\x01\xED\xE5", 0.001, 1);
		echo "Total prev T1: $Tot[0]\r\n";
		if ( round($Tot[0], 0) != 0 )
		$ab->mod_write(76, "dev_value=$Tot[0]", "DATE_FORMAT(dev_date, '%Y%m%d')=DATE_FORMAT(NOW() - INTERVAL 24 HOUR, '%Y%m%d')");
		//update_db(76, $Tot[0], 2);

		// За предыдущие сутки (Тариф 2)
		$Tot = merc_gd("\x00\x05\x50\x02\xAD\xE4", 0.001, 1);
		echo "Total prev T2: $Tot[0]\r\n";
		if ( round($Tot[0], 0) != 0 )
		$ab->mod_write(77, "dev_value=$Tot[0]", "DATE_FORMAT(dev_date, '%Y%m%d')=DATE_FORMAT(NOW() - INTERVAL 24 HOUR, '%Y%m%d')");
		//update_db(77, $Tot[0], 2);

		$total_cnt1 = 0;
	}

	sleep(10);
}

# Завершение соединения
dio_write($fd, "\x00\x02\x80\x71", 4);
usleep_win($sleep_time);
$result = dio_read($fd, 8);
dio_close($fd);

?>
