<?
/*
* Copyright (c) 2013, Andrey_B
* http://ab-log.ru
* Подробнее см. LICENSE.txt или http://www.gnu.org/licenses/
*/

/* Это тестовый скрипт для проверки связи со счетчиком Меркурий 230
* Выводит на экран значение Общей потребренной электроэнергии
*/

// BUS Type: RS485 or CAN
define('BUS', 'CAN');

$fd = dio_open('/dev/ttyUSB0', O_RDWR);

dio_tcsetattr($fd, array(
  'baud' => 9600,
  'bits' => 8,
  'stop'  => 1,
  'parity' => 0
)); 


##############

function merc_gd($cmd, $factor = 1, $total = 0)
{
	global $fd;
	global $sleep_time;

	usleep(50000);
	flush();
	dio_write($fd, $cmd, 6);
	usleep($sleep_time);
	$result = dio_read($fd, 64);

	$ret = array();
	
	if ( BUS == "CAN" )
	$start_byte = 7;
	else
	$start_byte = 1;
	
	if ( $total != 1 )
	{
		for ( $i = 0; $i < 4; $i++ )
		{
			if ( dechex(ord($result[$start_byte + $i * 3])) >= 40 )
			$result[$start_byte + $i * 3] = chr(dechex(ord($result[$start_byte + $i * 3])) - 40);
			if ( strlen($result) > $start_byte + 2 + $i * 3 )
			$ret[$i] = hexdec(dd($result[$start_byte + $i * 3]).dd($result[$start_byte + $i * 3 + 2]).dd($result[$start_byte + $i * 3 + 1]))*$factor;
		}
	}
	else
	$ret[0] = hexdec(dd($result[$start_byte+1]).dd($result[$start_byte]).dd($result[$start_byte+3]).dd($result[$start_byte+2]))*$factor;

	return $ret;
}

$sleep_time = 200000;

function dd($data = "")
{
	$result = "";
	$data2 = "";
	for ( $j = 0; $j < count($data); $j++ )
	{
		$data2 = dechex(ord($data[0]));
		if ( strlen($data2) == 1  )
		$result = "0".$data2;
		else
		$result .= $data2;

	}
	return $result;
}

# Инициализация соединения, передача пароля
dio_write($fd, "\x00\x01\x01\x01\x01\x01\x01\x01\x01\x77\x81", 11);
usleep($sleep_time);
$result = dio_read($fd, 15);

// Общее потребление
$Tot = merc_gd("\x00\x05\x00\x00\x10\x25", 0.001, 1);
echo "Total: $Tot[0]\r\n";

# Мощность по фазам
# =====================================================
$Pv = merc_gd("\x00\x08\x16\x00\x8F\x86", 0.01);
echo "Pv: $Pv[0] - $Pv[1] - $Pv[2] \r\n";

# Завершение соединения
dio_write($fd, "\x00\x02\x80\x71", 4);
usleep($sleep_time);
$result = dio_read($fd, 8);
dio_close($fd);

?>
