<?
/*
* Copyright (c) 2013, Andrey_B
* http://ab-log.ru
* Подробнее см. LICENSE.txt или http://www.gnu.org/licenses/
*/

/*
 Это пример того, как можно делать обработку длительного нажатия кнопок, подключенный к MegaD-328,
 не прибегая при этом к использованию приложения-сервера.
 При обычном нажатии переключается порт P7
 При длительном нажатии (1 секунда) переключается порт P8
 
 Внимание! Код совместим только с прошивками 3.04 и выше!
 Необходимо, чтобы режим (Mode) порта типа IN был настроен как P&R (Press & Release)
*/

if ( $_SERVER['REMOTE_ADDR'] == "192.168.0.14" )
{
	// Выключатель, подключенный к порту P3
	if ( $_GET['pt'] == "3" )
	{
		$act_file = "in-".$_SERVER['REMOTE_ADDR']."-".$_GET['pt'].".act";
		if ( $_GET['m'] == 1 )
		{
			if ( file_exists($act_file) )
			{
				echo "7:2";
				unlink($act_file);
			}
		}
		else
		{
			header('Connection: close');
			flush();

			touch($act_file);
       
			for ( $i = 0; $i < 100; $i++ )
			{
				if ( !file_exists($act_file) )
				exit;
				usleep(10000);
			}

			unlink($act_file);
			file_get_contents("http://192.168.0.14/sec/?pt=8&cmd=8:2");
		}
	}
}
?>