<?
/*
* Copyright (c) 2013, Andrey_B (Office-42)
* Подробнее см. LICENSE.txt или http://www.gnu.org/licenses/
*/
error_reporting(0);

function image2MIME($file)
{
	$fh=fopen($file,"r");
	if ($fh)
	{
		$start4=fread($fh,4);
		$start3=substr($start4,0,3);
			
		if ($start4=="\x89PNG")
		return "image/png";
		elseif ($start3=="GIF")
		return "image/gif";
		elseif ($start3=="\xFF\xD8\xFF")
		return "image/jpeg";
		elseif ($start4=="hsi1")
		return "image/jpeg";
		else
		return false;
			
		unset($start3);unset($start4);
		fclose($fh);
	}
	else 
	return false;
}

if ( isset($_REQUEST['n']) )
$Photo_Name = $_REQUEST['n'];

if ( isset($_REQUEST['w']) )
$Photo_Width = $_REQUEST['w'];
else
$Photo_Width = 0;

if ( isset($_REQUEST['h']) )
$Photo_Height = $_REQUEST['h'];
else
$Photo_Height = 0;

if ( isset($_REQUEST['q']) )
$Photo_Quality = $_REQUEST['q'];

if ( isset($_REQUEST['Photo_Size']) )
$Photo_Size = $_REQUEST['Photo_Size'];
else
$Photo_Size = 0;
if ( isset($_REQUEST['Photo_Orient']) )
$Photo_Orient = $_REQUEST['Photo_Orient'];
else
$Photo_Orient = "";

// Наложение временно отключено
$IFont = "";

// Если блок вызывается из системы управления, не накладывать изображение на фото
if ( isset($_SERVER['HTTP_REFERER']) )
{
	if (preg_match("/^http:\/\/.*\/cadmin\/index\.php/", $_SERVER['HTTP_REFERER']))
	unset($IFont);
}

if ( isset($_GET['sec']) && $_GET['sec'] == 1 )
$Photo_Name = "sfiles/lists/".photo_decode($Photo_Name);

	if ( preg_match("/^\//", $Photo_Name) )
	$Photo_Name = preg_replace("/^\//", "", $Photo_Name);
	$Photo_Name = preg_replace("/^(\/)+/", "/", $Photo_Name);

	if ( !file_exists($Photo_Name) )
	exit;

	list($width, $height, $type, $attr) = getimagesize("$Photo_Name");

	if ( isset($_REQUEST['NoUpscale']) && $_REQUEST['NoUpscale'] == 1 )
	{
		if ( $Photo_Width > 0 && $Photo_Width > $width )
		{
			$Photo_Width = $width;
			$Photo_Height = $height;
		}

		if ( $Photo_Height > 0 && $Photo_Height > $height )
		{
			$Photo_Width = $width;
			$Photo_Height = $height;
		}
	}

	if ( $width == 0 || $height == 0 )
	exit;

	if ( function_exists("apache_request_headers") )
	{
		$headers=apache_request_headers();
		$rtime=strtotime(@$headers['If-Modified-Since']);
		$fp=fopen($Photo_Name,'rb');
		$fs=fstat($fp);
		$mtime=$fs['mtime'];

		if($rtime>=$mtime && $headers['Cache-Control'] != "no-cache" && !empty($headers['If-Modified-Since']))
		{
			header("HTTP/1.0 304 Not Modified");
			exit();
		}
	}

	$mime=image2MIME($Photo_Name);

	header("Content-type: $mime");

	if ( isset($_SERVER['HTTP_REFERER']) && preg_match("/^http:\/\/.*\/cadmin\/index\.php/", $_SERVER['HTTP_REFERER']))
	header('Cache-control: no-cache');
	else
	{
		header('Last-Modified: '.gmdate('D, d M Y H:i:s',$mtime).' GMT');
		header('Cache-control: public');
	}

	if ( empty($Photo_Quality) )
	$Photo_Quality = 85;

	// Кеширование временно отключено.
	$ImgCache = 0;

	if ( isset($_REQUEST['NoCache']) && $_REQUEST['NoCache'] == 1 )
	$ImgCache = 0;
	
	// Кеширование изображений
	if ( $ImgCache == 1 )
	{
		if ( $mime == "image/jpeg" || $mime == "image/jpg" || $mime == "image/pjpeg" )
		$cache_ext = "jpg";
		elseif ( $mime == "image/gif" )
		$cache_ext = "gif";
		elseif ( $mime == "image/png" )
		$cache_ext = "png";

		if ( !empty($IFont) )
		$cache_add = $IFont_Text.$IFont_Pic.$IFont_Size.$IFont_X.$IFont_Y.$IFont_Angle.$IFont_Pos.$IFont_Color.$IFont_BG.$IFont_BG_Transp.$IFont_Name.$IFont_Transparency;

		$cache_file = "sfiles/cache/".md5($mtime.$_REQUEST['Photo_Name'].$_REQUEST['Photo_Size'].$_REQUEST['Photo_Width'].$_REQUEST['Photo_Height'].$_REQUEST['Photo_Quality'].$_REQUEST['Photo_Orient'].$_REQUEST['NoUpscale'].$cache_add).".$cache_ext";
	}

	if ( $Photo_Width == $width && $Photo_Height == $height && empty($IFont) ) 
	{
		$foreground = file($Photo_Name);
		echo implode($foreground);
	}
	elseif ( $ImgCache == 1 && file_exists($cache_file) )
	{
		$foreground = file($cache_file);
		echo implode($foreground);
	}
	elseif ( ($Photo_Size > 0 || $Photo_Width > 0 || $Photo_Height > 0) || $IFont > 0 )
	{


		if ( $mime == "image/jpeg" || $mime == "image/jpg" || $mime == "image/pjpeg" )
		$foreground = imagecreatefromjpeg($Photo_Name);
		elseif ( $mime == "image/gif" )
		$foreground = imagecreatefromgif($Photo_Name);
		elseif ( $mime == "image/png" )
		$foreground = imagecreatefrompng($Photo_Name);

		if ( empty($Photo_Size) && empty($Photo_Width) && empty($Photo_Height) )
		{
			$Photo_Width = $width;
			$Photo_Height = $height;
		}

		if ( !empty($Photo_Width) && !empty($Photo_Height) )
		{
			unset($resize_type);
			$tmp_res1 = $width / $Photo_Width;
			$tmp_res2 = $height / $Photo_Height;
			if ( $tmp_res1 <= $tmp_res2 )
			$resize_type = "width";
			else
			$resize_type = "height";

		}

		if ( $Photo_Size > 0 )
		{
			if ( $width > $height )
			{ 
				$w = $Photo_Size;
				$h = round($height / ($width / $w));
				$orient = "h";
			}
			else
			{ 
				$h = $Photo_Size;
				$w = round($width / ($height / $h));
				$orient = "v";
			}
		}
		elseif ( ( $Photo_Width > 0 && empty($Photo_Height) ) || $resize_type == "width" )
		{
			$w = $Photo_Width;
			$h = round($height / ($width / $w));
			$orient = "h";
		}
		elseif ( ( $Photo_Height > 0 && empty($Photo_Width) ) || $resize_type == "height" ) 
		{
			$h = $Photo_Height;
			$w = round($width / ($height / $h));
			$orient = "v";
		}

		//echo $orient;

		$background = imagecreatetruecolor(intval($w), intval($h));
		if ( isset($_GET['crop_w']) && isset($_GET['crop_h']) )
		$background = $foreground;
		else
		imagecopyresampled ($background,$foreground, 0, 0, 0, 0, intval($w), intval($h), $width, $height);
		if ( $Photo_Orient == "v" && $orient == "h" )
		$background = imagerotate($background, 90, 0);
		elseif ( $Photo_Orient == "h" && $orient == "v" )
		$background = imagerotate($background, 90, 0);

		if ( $Photo_Width > 0 && $Photo_Height > 0 )
		{
			$crop = imagecreatetruecolor( $Photo_Width, $Photo_Height );

			if ( isset($_GET['crop_w']) )
			$crop_w = intval($_GET['crop_w']);
			else
			$crop_w = round(($w - $Photo_Width) / 2);

			if ( isset($_GET['crop_h']) )
			$crop_h = intval($_GET['crop_h']);
			else
			$crop_h = round(($h - $Photo_Height) / 2);

			imagecopy($crop, $background, 0, 0, $crop_w, $crop_h, $Photo_Width, $Photo_Height);
			//imagecopy($crop, $background, 0, 0, 0, 300, 700, 400);
			$background = $crop;
//			$w = $w1;
//			$h = $h1;
		}

		$fx = imagesx($background);
		$fy = imagesy($background);


//		echo "width=$width<br>height=$height<br>";
//		echo "w=$w<br>h=$h<br>";
//		echo "w1=$w1<br>h1=$h1<br>";

		if ( $w == $width && $h == $height && empty($IFont) ) 
		{
			$foreground = file($Photo_Name);
			echo implode($foreground);
		}
		else
		{
//			if ( $IFont == 1 && ($w >= 200 || ( $width >= 200 && empty($w) ) ) )
			if ( $IFont > 0 && ($fx >= $IFont || $fy >= $IFont ) )
			{

				if ( empty($IFont_Pic) )
				{
					if ( empty($IFont_Text) )
					$IFont_Text = $URL_Site;

					if ( preg_match("/#\w+#/", $IFont_Text) )
					{
						if ( preg_match("/#datetime#/", $IFont_Text) )
						{
							$IFont_Text = preg_replace("/#datetime#/", date("d.m.Y H:i:s", filemtime($Photo_Name)), $IFont_Text);
						}
					}

					if ( $IFont_Size == 0 )
					$IFont_Size = 12;
					if ( empty($IFont_Color) )
					{
						if ( !empty($IFont_Transparency) )
						$IFont_Color2 = imagecolorallocatealpha($background, 0, 0, 0, $IFont_Transparency);
						else
						$IFont_Color2 = imagecolorallocate($background, 0, 0, 0);
					}
					else
					{
						$IFont_Color_Red = hexdec($IFont_Color[0].$IFont_Color[1]);
						$IFont_Color_Green = hexdec($IFont_Color[2].$IFont_Color[3]);
						$IFont_Color_Blue = hexdec($IFont_Color[4].$IFont_Color[5]);
						if ( !empty($IFont_Transparency) )
						$IFont_Color2 = imagecolorallocatealpha($background, $IFont_Color_Red, $IFont_Color_Green, $IFont_Color_Blue, $IFont_Transparency);
						else
						$IFont_Color2 = imagecolorallocate($background, $IFont_Color_Red, $IFont_Color_Green, $IFont_Color_Blue);
					}


					$IFont_Text = iconv('CP1251', 'UTF-8', $IFont_Text);
					$IFont_Name = "cadmin/fonts/DEFAULT.TTF";

					$boundingbox = imagettfbbox($IFont_Size, $IFont_Angle, $IFont_Name, $IFont_Text);


					if ( $IFont_Pos > 0 )
					{
						if ( $IFont_Pos == 1 )
						{
							$IFont_X = 2;
							$IFont_Y = $IFont_Size+1;
						}
						elseif ( $IFont_Pos == 2 )
						{
							$IFont_X = round($fx / 2) - round($boundingbox[4] / 2);
							$IFont_Y = round($fy / 2) - round($boundingbox[3] / 2);
						}
						elseif ( $IFont_Pos == 3 )
						{
							$IFont_X = 1;
							$IFont_Y = $fy - 1;
						}

						elseif ( $IFont_Pos == 4 )
						{
							$IFont_X = $fx - $boundingbox[4];
							$IFont_Y = $IFont_Size+1;
						}

						elseif ( $IFont_Pos == 5 )
						{
							$IFont_X = $fx - $boundingbox[4];
							$IFont_Y = $fy - $boundingbox[3];
						}
						
					}
					else
					{
						if ( $IFont_Y == 0 )
						$IFont_Y = $IFont_Y + $IFont_Size;
					}


					if ( !empty($IFont_BG) )
					{
						$IFont_BG_Red = hexdec($IFont_BG[0].$IFont_BG[1]);
						$IFont_BG_Green = hexdec($IFont_BG[2].$IFont_BG[3]);
						$IFont_BG_Blue = hexdec($IFont_BG[4].$IFont_BG[5]);
						if ( !empty($IFont_BG_Transp) )
						$bg_color = imagecolorallocatealpha($background, $IFont_BG_Red, $IFont_BG_Green, $IFont_BG_Blue, $IFont_BG_Transp);
						else
						$bg_color = imagecolorallocate($background, $IFont_BG_Red, $IFont_BG_Green, $IFont_BG_Blue);
						imagefilledrectangle($background, $boundingbox[6]+$IFont_X-1, $boundingbox[7]+$IFont_Y-1, $boundingbox[2]+$IFont_X+1, $boundingbox[3]+$IFont_Y+1, $bg_color);
					}

					imagettftext($background, $IFont_Size, $IFont_Angle, $IFont_X, $IFont_Y, $IFont_Color2, $IFont_Name, $IFont_Text);


				}
				else
				{
					$mime2=image2MIME($IFont_Pic);

					if ( $mime2 == "image/jpeg" || $mime2 == "image/jpg" || $mime2 == "image/pjpeg" )
					$img2 = imagecreatefromjpeg($IFont_Pic);
					elseif ( $mime2 == "image/gif" )
					$img2 = imagecreatefromgif($IFont_Pic);
					elseif ( $mime2 == "image/png" )
					$img2 = imagecreatefrompng($IFont_Pic);


					list($w2, $h2, $type2, $attr2) = getimagesize($IFont_Pic);

					if ( $IFont_Pos > 0 )
					{
						if ( $IFont_Pos == 1 )
						{
							$IFont_X = 0;
							$IFont_Y = 0;
						}
						elseif ( $IFont_Pos == 2 )
						{
							$IFont_X = round($fx / 2) - round($w2 / 2);
							$IFont_Y = round($fy / 2) - round($h2 / 2);

						}
						elseif ( $IFont_Pos == 3 )
						{
							$IFont_X = 0;
							$IFont_Y = $fy-$h2;
						}

						elseif ( $IFont_Pos == 4 )
						{
							$IFont_X = $fx - $w2;
							$IFont_Y = 0;
						}

						elseif ( $IFont_Pos == 5 )
						{
							$IFont_X = $fx - $w2;
							$IFont_Y = $fy - $h2;;
						}

						
					}


					if ( $IFont_Transparency > 0 )
					{

						$img3 = imagecreatetruecolor($w2, $h2);

						for($y=0; $y<$h2; $y++)
						{
							for($x=0; $x < $w2; $x++)
							{
								$color_img2 = imagecolorat($img2, $x, $y);
								$color_img2_alpha = ($color_img2 >> 24) << 1;
								if($color_img2_alpha < 253)
								{
									$color_img2_r = ($color_img2 >> 16) & 0xFF;
									$color_img2_g = ($color_img2 >> 8) & 0xFF;
									$color_img2_b = $color_img2 & 0xFF;
									$color_img1 = imagecolorat($background, $x, $y);
									$color_img1_r = ($color_img1 >> 16) & 0xFF;
									$color_img1_g = ($color_img1 >> 8) & 0xFF;
									$color_img1_b = $color_img1 & 0xFF;
									$color_img3_r = (($color_img2_r * (0xFF-$color_img2_alpha)) >> 8) + (($color_img1_r * $color_img2_alpha) >> 8);
									$color_img3_g = (($color_img2_g * (0xFF-$color_img2_alpha)) >> 8) + (($color_img1_g * $color_img2_alpha) >> 8);
									$color_img3_b = (($color_img2_b * (0xFF-$color_img2_alpha)) >> 8) + (($color_img1_b * $color_img2_alpha) >> 8);
									$color_img3 = imagecolorallocatealpha($img3,  $color_img3_r,  $color_img3_g,  $color_img3_b, 80);
									// imagesetpixel($background, $x, $y, $color_img3);
									imagesetpixel($background, $x+$IFont_X, $y+$IFont_Y, $color_img3);
								}
							}
						}

					}

					imagecopy ($background, $img2, $IFont_X, $IFont_Y, 0, 0, $w2, $h2);
//					imagecopy ($background, $img2, 0, 100, 0, 0, 327, 37);
				}
			}

			if ( $mime == "image/jpeg" || $mime == "image/jpg" || $mime == "image/pjpeg" )
			{
				imagejpeg($background, null, $Photo_Quality);
				if ( $ImgCache == 1 )
				imagejpeg($background, $cache_file, $Photo_Quality);
			}
			elseif ( $mime == "image/gif" )
			{
				imagegif($background);
				if ( $ImgCache == 1 )
				imagegif($background, $cache_file);
			}
			elseif ( $mime == "image/png" )
			{
				imagepng($background);
				if ( $ImgCache == 1 )
				imagepng($background, $cache_file);
			}

		}
	}
	else
	{
		$foreground = file($Photo_Name);
		echo implode($foreground);

	}


?>
