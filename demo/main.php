<?
if ( !empty($_POST['ab-theme']) )
{
	setcookie("ab-theme", $_POST['ab-theme'], 0, "/", $_SERVER['SERVER_NAME']);
	$cur_theme = $_POST['ab-theme'];
}
else
{
	if ( empty($_REQUEST['ab-theme']) )
	$cur_theme = "redmond";
	else
	$cur_theme = $_REQUEST['ab-theme'];
}
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Система Управления Домом</title>
<link rel="stylesheet" href="css2/reset.css" />
<link rel="stylesheet" href="css2/text.css" />
<link rel="stylesheet" href="css2/960.css" />

<!-- Мой CSS -->
<link rel="stylesheet" href="css2/demo.css" />

<!--link type="text/css" href="css2/themes/redmond/jquery-ui-1.8.16.custom.css" rel="stylesheet" /-->
<link type="text/css" href="css2/themes/<? echo $cur_theme; ?>/jquery-ui.min.css" rel="stylesheet" />
<scripT type="text/javascript" src="js2/jquery-1.12.0.min.js"></script>
<scripT type="text/javascript" src="js2/jquery-ui.min.js"></script>
<scripT type="text/javascript" src="js2/jquery.timers.js"></script>
<scripT type="text/javascript" src="js2/highcharts.js"></script>

<scripT type="text/javascript">

	$(function()
	{
		$.ajaxSetup({ cache: false });

		$('#tabs').tabs(
		{
		create: function(event, ui) { $(ui.panel).load(ui.tab.find('a').attr('url')); },
		beforeActivate: function(event, ui) {
			$(ui.oldPanel.selector).stopTime();
			$(ui.oldPanel).empty();
			$(ui.newPanel).load(ui.newTab.find('a').attr('url'));

			if( $(ui.newTab).find('a').attr('href').indexOf('#') != 0 )
			window.open($(ui.newTab).find('a').attr('href'), '_blank');
			}
		});
		
		$.get("ab-data.php?p=rtop", function(data){$("#rtop").html(data);});

		$(document).everyTime(60000, function(i) {
			$.get("ab-data.php?p=rtop", function(data){$("#rtop").html(data);});
		});
	

	});

</script>
</head>

<body>
<?

if ( file_exists("demo.txt" ) )
{
	echo "<h1><a href=\"#\">Система Управления Домом. Демонстрационный режим</a></h1>";
	echo "<div style=\"text-align:center;position:relative;top:-15px;color:#ffffff\">Данный режим предназначен для демонстрации и обсуждения Web-интерфейса. Обновляются данные только по погоде. <a href=\"http://www.ab-log.ru/smart-house/linux/demo-src\"><u>Подробнее ab-log.ru</u></a></div>";
}

?>
<div id="tabs" style="width:980px;margin-left: auto; margin-right: auto;">
	<ul>
		<li><a href="#ui-tabs-1" url="ab-pages.php?p=home"><span style="float:left;margin-right:3px;" class="ui-icon ui-icon-home"></span> Главная</a></li>
		<li><a href="#ui-tabs-2" url="ab-pages.php?p=cams"><span style="float:left;margin-right:3px;" class="ui-icon ui-icon-video"></span> Камеры</a></li>
		<li><a href="#ui-tabs-3" url="ab-pages.php?p=light"><span style="float:left;margin-right:3px;" class="ui-icon ui-icon-lightbulb"></span> Свет&nbsp;&nbsp;&nbsp;</a></li>
		<li><a href="#ui-tabs-4" url="ab-pages.php?p=weather"><span style="float:left;margin-right:3px;" class="ui-icon ui-icon-image"></span> Погода</a></li>
		<li><a href="#ui-tabs-5" url="ab-pages.php?p=climate"><span style="float:left;margin-right:3px;" class="ui-icon ui-icon-gear"></span> Климат</a></li>
		<li><a href="#ui-tabs-6" url="ab-pages.php?p=systems"><span style="float:left;margin-right:3px;" class="ui-icon ui-icon-wrench"></span> Системы</a></li>
		<li><a href="http://192.168.0.251:32400/web" target="_blank"><span style="float:left;margin-right:3px;" class="ui-icon ui-icon-volume-on"></span> Media</a></li>
		
		<li style="float:right" id="rtop"></li>

	</ul>

	<div id="ui-tabs-1"></div>
	<div id="ui-tabs-2"></div>
	<div id="ui-tabs-3"></div>
	<div id="ui-tabs-4"></div>
	<div id="ui-tabs-5"></div>
	<div id="ui-tabs-6"></div>

</div>

<?
if ( file_exists("demo.txt" ) )
include("counter.php");
?>

</body>
</html>
