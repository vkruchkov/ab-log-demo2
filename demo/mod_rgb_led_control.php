<?
/*
* Copyright (c) 2014, Andrey_B
* http://ab-log.ru
* Подробнее см. LICENSE.txt или http://www.gnu.org/licenses/
*/

# Управление светодиодной RGB-лентой с помощью MegaD-328-IN Kit
# Подробнее: http://ab-log.ru/smart-house/ethernet/megad-led-rgb
?>

<scripT type="text/javascript" src="js2/jquery-1.12.0.min.js"></script>
<script src="/js2/jscolor.min.js"></script>

Цвет: <input class="jscolor {onFineChange:'update(this)'}" value="e1e1e1">

<script>
function update(jscolor) {
    // 'jscolor' instance can be used as a string
    //document.getElementById('rect').style.backgroundColor = '#' + jscolor
	$.get("mod_rgb_led_control_ajax.php?color=" + jscolor, function(data){});

}
</script>
