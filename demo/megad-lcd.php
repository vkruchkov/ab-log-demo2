<?
/*
* Copyright (c) 2014, Andrey_B
* http://ab-log.ru
* Подробнее см. LICENSE.txt или http://www.gnu.org/licenses/
*/

define("LCD_RS", 8);
define("LCD_E", 9);
// D4, D5, D6, D7
$ports = array("6", "13", "12", "11", "6", "13", "12", "11");

function send_data($cmd)
{
	file_get_contents("http://192.168.0.14/sec/?cmd=$cmd");
}

function megad_lcd($byte, $mode)
{
	global $ports;

	$bin = base_convert($byte, 16, 2);
	$test = 8 - strlen($bin);
	for ( $i = 0; $i < $test; $i++ )
	$bin = "0".$bin;

	$cmd = LCD_RS.":$mode;";

	for ( $i = 0; $i < 4; $i++ )
	{
		$cmd .= $ports[$i].":";
		if ( $bin[$i] == "1" )
		$cmd .= "1;";
		else
		$cmd .= "0;";
	}

	send_data($cmd.LCD_E.":1");
	send_data(LCD_E.":0");

	$cmd = "";
	for ( $i = 4; $i < 8; $i++ )
	{
		$cmd .= $ports[$i].":";
		if ( $bin[$i] == "1" )
		$cmd .= "1;";
		else
		$cmd .= "0;";
	}

	send_data($cmd.LCD_E.":1");
	send_data(LCD_E.":0");
}

function str2lcd($string)
{
	for ( $i = 0; $i < strlen($string); $i++ )
	megad_lcd(dechex(ord($string[$i])), 1);
}

$cmd = "7:0;8:0;9:0;10:0;11:0;12:0;13:0";
send_data($cmd);

megad_lcd("33", 0);
megad_lcd("32", 0);
megad_lcd("2A", 0);
megad_lcd("0C", 0);
megad_lcd("01", 0);

str2lcd(iconv("utf-8", "cp1251", "Привет!"));
megad_lcd("C0", 0);
str2lcd("http://ab-log.ru");

?>