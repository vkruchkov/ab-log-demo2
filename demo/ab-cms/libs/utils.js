function ow()	{	
	w1=window.open('','new_window','resizable=yes, menubar=no, status=yes, scrollbars=yes, width=750, height=500');
}

function FieldValidator( sName, sMode, pAfterValidation, pBeforeValidation, sRegExp	)
{
	var oSelf = this
	var aNodes = document.getElementsByName(sName)
	if( aNodes.length == 0 ) throw 1
	this.oFieldNode = aNodes.item(0)
	if( !this.oFieldNode.form ) throw 2
	this.sMode = sMode ? sMode : 'RequiredField'
	if( sRegExp ) this.oRegExp = new RegExp( sRegExp )
	this.SetupEvent( this.oFieldNode.form, 'submit', function(e){ return oSelf.Validator(e) } )
	this.bValidated = false
	this.BeforeValidation = pBeforeValidation ? pBeforeValidation : function() {}
	this.AfterValidation = pAfterValidation ? pAfterValidation : this.AfterValidationDefault
	return this
}

FieldValidator.prototype.Validator = function(e)
{
	this.BeforeValidation()
	switch( this.sMode )
	{
		case 'RequiredField' : this.bValidated = this.RequiredFieldValidator(); break;
		case 'RegExp'        : this.bValidated = this.RegExpValidator();        break;
		case 'Email'         : this.bValidated = this.EmailValidator();         break;
		case 'Int'           : this.bValidated = this.IntValidator();           break;
		case 'Float'         : this.bValidated = this.FloatValidator();         break;
		default              : this.bValidated = false
	}
	if( !this.bValidated ) this.KillEvent(e)
	this.AfterValidation()
}

FieldValidator.prototype.RequiredFieldValidator = function()
{
	return (
		( this.oFieldNode.type != 'checkbox' && this.oFieldNode.value != '' ) ||
		( this.oFieldNode.type == 'checkbox' && this.oFieldNode.checked )
	)
}
FieldValidator.prototype.RegExpValidator = function()
{
	return ( this.oFieldNode.type != 'checkbox' && this.oRegExp.test( this.oFieldNode.value ) )
}

FieldValidator.prototype.EmailValidator = function()
{
	var sEmail = this.oFieldNode.value.replace( new RegExp('/\(.*?\)/'), '' )
	var oRegExp = /^[A-Za-z0-9][-\w]*(\.[A-Za-z0-9][-\w]*)*@[A-Za-z0-9][-\w]*(\.[A-Za-z0-9][-\w]*)*\.[a-zA-Z]{2,4}$/
	return oRegExp.test(sEmail)
}

FieldValidator.prototype.IntValidator = function()
{
	return ( parseInt(this.oFieldNode.value) == this.oFieldNode.value )
}

FieldValidator.prototype.FloatValidator = function()
{
	return ( parseFloat(this.oFieldNode.value) == this.oFieldNode.value )
}

FieldValidator.prototype.AfterValidationDefault = function()
{
	if( !this.bValidated )
	{
		alert(
			'��������!\n' +
			'���� �� ����� ����� ��������� �������!\n\n' +
			'---\n' +
			'Field ' + this.oFieldNode.name + ' has validated as ' + this.sMode
		)
	}
}

FieldValidator.prototype.SetupEvent = function( oElement, sEventType, pHandler )
{
	if( oElement.attachEvent ) oElement.attachEvent('on' + sEventType, pHandler)
	if( oElement.addEventListener ) oElement.addEventListener(sEventType, pHandler, false)
}

FieldValidator.prototype.KillEvent = function(e)
{
	var oEvent = e ? e : window.event
	if( oEvent.preventDefault )
	{
		oEvent.preventDefault()
	}
	else
	{
		oEvent.returnValue = false
	}
}

function countletter(field, divid, maxletter){
	var tv=document.getElementById(divid);
	var fv=document.getElementById(field);
	var fvl=fv.value.length;
	var ost=maxletter-fvl;
	if(ost<=0){
		fv.value=fv.value.substring(0, maxletter);
	}
	tv.innerHTML='�������� '+ ost + ' ��������';
}