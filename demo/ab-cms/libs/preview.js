this.imagePreview = function()
{	
	xOffset = 10;
	yOffset = 30;

	$("a.preview").hover(function(e)
	{
		var my_href = this.getAttribute('my_href');
		this.t = this.title;
		this.title = "";	
		var c = (this.t != "") ? "<br/>" + this.t : "";
		$("body").append("<p id='preview'><img src='"+ my_href +"' alt='Image preview' />"+ c +"</p>");								 
		$("#preview")
			.css("top",(e.pageY - xOffset) + "px")
			.css("left",(e.pageX + yOffset) + "px")
			.fadeIn("fast");						
	},

	function()
	{
		this.title = this.t;	
		$("#preview").remove();
	});	

	$("a.preview").mousemove(function(e)
	{

		var innerHeight;

	        if (typeof(window.innerHeight) == 'number')
		{ innerHeight = window.innerHeight; }
		else if (document.documentElement && document.documentElement.clientHeight)
		{ innerHeight = document.documentElement.clientHeight; }
		else if(document.body && document.body.clientHeight)
		{ innerHeight = document.body.clientHeight; }

		var innerWidth;
	        if (typeof(window.innerWidth) == 'number')
		{ innerWidth = window.innerWidth; }
		else if(document.documentElement && document.documentElement.clientWidth)
		{ innerWidth = document.documentElement.clientWidth; }
		else if(document.body && document.body.clientWidth)
		{ innerWidth = document.body.clientWidth; }

		var block_width = $("#preview").width();
		var block_height = $("#preview").height();

		var my_offset_x = 0;
		var my_offset_y = 0;
		if ( e.clientX > innerWidth - block_width - 60 )
		{ my_offset_x = block_width + 60; }
		if ( e.clientY > innerHeight - block_height )
		{ my_offset_y = block_height; }
		
		$("#preview")
			.css("top",(e.pageY - xOffset - my_offset_y) + "px")
			.css("left",(e.pageX + yOffset - my_offset_x) + "px");

	});			
};

$(document).ready(function(){ imagePreview(); });
