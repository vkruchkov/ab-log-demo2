
<script type="text/javascript">
	$(function ()
	{ 
		$("#cadmin_tree").tree
		({
			/*
			types :
			{
				"doc" : { icon : { image : "cms-f/file.png" } }
			},
			*/

			callback :
			{
				onchange : function (NODE)
				{ document.location.href = "index.php?p=doc&id=" + $(NODE).attr("id"); }
//				{ document.location.href = $(NODE).children("a:eq(0)").attr("href"); }
			},

			plugins :
			{

				cookie :
				{
					prefix : "jstree_",
					types : { selected : false }
				},

				contextmenu :
				{
					items :
					{
						create : false,
						rename : false,
						remove : false,

						add_doc :
						{
							label	: "Добавить документ", 
							icon	: "",
							visible	: function (NODE, TREE_OBJ) { 
								if(NODE.length != 1) return 0; 
								if(TREE_OBJ.get_type(NODE) == "adoc" || TREE_OBJ.get_type(NODE) == "doc" || TREE_OBJ.get_type(NODE) == "ban") return -1; 
								return 1; 
							}, 
							action	: function (NODE, TREE_OBJ) { 
								{ document.location.href = "index.php?p=doc&parent=" + $(NODE).attr("id"); }
							},
							//separator_before : true
						},

						add_cat :
						{
							label	: "Добавить подраздел", 
							icon	: "",
							visible	: function (NODE, TREE_OBJ) { 
								if(NODE.length != 1) return 0; 
								if(TREE_OBJ.get_type(NODE) == "adoc" || TREE_OBJ.get_type(NODE) == "doc" || TREE_OBJ.get_type(NODE) == "ban") return -1; 
								return 1; 
							}, 
							action	: function (NODE, TREE_OBJ) { 
								{ document.location.href = "index.php?p=cat&parent=" + $(NODE).attr("id"); }
							},
							//separator_before : true
						},

						<? //if ( $U_Type == 3 || $U_Type == 2 ) { ?>

						permission :
						{
							label	: "Управление доступом", 
							icon	: "",
							visible	: function (NODE, TREE_OBJ) { 
								if(NODE.length != 1) return 0; 
							}, 
							action	: function (NODE, TREE_OBJ) { 
								{ document.location.href = "index.php?p=access&id=" + $(NODE).attr("id"); }
							},
							separator_before : true
						}

						<? //} ?>
					}
				}

			}
		});
	});
</script>
