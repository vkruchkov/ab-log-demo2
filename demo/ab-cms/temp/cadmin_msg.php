<?
/*
* Copyright (c) 2013, Andrey_B (Office-42)
* Подробнее см. LICENSE.txt или http://www.gnu.org/licenses/
*/

# index
# ---

$cadmin_msg['system_error'] = "Код ошибки";

# System messages
$cadmin_msg['index_exit'] = "<h1>Выход из системы осуществлен!</h1><p><a href=\"index.php?auth\">Войти в систему управления</a></p>";
$cadmin_msg['index_deny'] = "<h1>Доступ в систему закрыт!</h1><p><a href=\"index.php?auth\">Попробовать еще раз</a></p>";

# Service menu
$cadmin_msg['index_smenu_help'] = "помощь";
$cadmin_msg['index_smenu_conf'] = "настройки";
$cadmin_msg['index_smenu_admin'] = "администрирование";
$cadmin_msg['index_smenu_exit'] = "выход";

$cadmin_msg['index_support'] = "поддержка";

$cadmin_msg['index_utype'] =  array(3 => "Разработчик", 2 => "Администратор", 1 => "Пользователь");

# Main menu
$cadmin_msg['index_mmenu_bookmarks'] = "избранное";
$cadmin_msg['index_mmenu_create'] = "создать";
	$cadmin_msg['index_mmenu_c_cat'] = "раздел";
	$cadmin_msg['index_mmenu_c_doc'] = "документ";
	$cadmin_msg['index_mmenu_c_news'] = "новость";
	$cadmin_msg['index_mmenu_c_banner'] = "баннер";
$cadmin_msg['index_mmenu_structure'] = "структура";
$cadmin_msg['index_mmenu_banners'] = "баннеры";
$cadmin_msg['index_mmenu_modules'] = "модули";
$cadmin_msg['index_mmenu_files'] = "файлы";
$cadmin_msg['index_mmenu_misc'] = "дополнительно";
$cadmin_msg['index_mmenu_m_msg'] = "Почтовые сообщения";
$cadmin_msg['index_mmenu_m_ml'] = "Список рассылки";
$cadmin_msg['index_mmenu_m_journal'] = "Журнал";
$cadmin_msg['index_mmenu_m_maillist'] = "рассылка";
$cadmin_msg['index_mmenu_m_vacancies'] = "вакансии";
$cadmin_msg['index_mmenu_m_sstat'] = "статистика поиска";

# form_cont
# ---

# Messages
$cadmin_msg['form_cont_title'] = "Создать раздел";
$cadmin_msg['form_cont_write_error_title'] = "Системная ошибка. Документ не сохранен!";
$cadmin_msg['form_cont_write_error_text'] = "Ошибка сохранения документа в базе данных. Пожалуйста, обратитесь к администратору сайта или разработчикам.";

$cadmin_msg['form_cont_write_ok_title'] = "Информация успешно записана";
$cadmin_msg['form_cont_write_del_title'] = "Информация успешно удалена";
$cadmin_msg['form_cont_write_ok_return'] = "вернуться к редактированию документа/раздела";
$cadmin_msg['form_cont_write_ok_return_ban'] = "вернуться к редактированию баннера";
$cadmin_msg['form_cont_write_ok_new1'] = "создать новый";
$cadmin_msg['form_cont_write_ok_new2'] = "документ";
$cadmin_msg['form_cont_write_ok_new3'] = "раздел";
$cadmin_msg['form_cont_write_ok_new4'] = " в текущем разделе";
$cadmin_msg['form_cont_write_ok_new5'] = "баннер";
$cadmin_msg['form_cont_write_ok_tree'] = "перейти к карте сайта";

$cadmin_msg['form_cont_deny_title'] = "Ошибка доступа!";
$cadmin_msg['form_cont_deny_text'] = "Вы не имеете прав на редактирование этого объекта. Обратитесь к администратору.";
$cadmin_msg['form_cont_denyparent_title'] = "Ошибка доступа!";
$cadmin_msg['form_cont_denyparent_text'] = "У вас нет прав на запись в этот раздел. Обратитесь к администратору.";
$cadmin_msg['form_cont_rewrite_title'] = "URL-заголовок не уникален";
$cadmin_msg['form_cont_rewrite_text'] = "Документ сохранен, но значение поля URL-заголовок должно быть уникально в рамках текущего раздела.<br>Аналогичные значения найдены в документах:";
$cadmin_msg['form_cont_rewrite_text2'] = "Система управления автоматически изменила значения поля URL-заголовок на:";
$cadmin_msg['form_cont_label_title'] = "Метка не уникальна";
$cadmin_msg['form_cont_label_text'] = "Документ сохранен, но значение поля Метка должно быть уникально.<br>Аналогичные значения найдены в документах:";
$cadmin_msg['form_cont_label_text2'] = "Система управления автоматически изменила значения поля Метка на:";
$cadmin_msg['form_cont_parentcheck_title'] = "Ошибка починения";
$cadmin_msg['form_cont_parentcheck_text'] = "Родительский объект не может быть подчинен дочернему! Запись не сохранена.";
$cadmin_msg['form_cont_label_text2'] = "Система управления автоматически изменила значения поля Метка на:";

$cadmin_msg['form_cont_block_title'] = "Объект заблокирован разработчиком!";
$cadmin_msg['form_cont_block_text'] = "Доступ к данному объекту заблокирован разработчиком.";
$cadmin_msg['form_cont_block_text2'] = "Возможность редактирования данного объекта заблокирована разработчиком";


$cadmin_msg['form_cont_trans_error_title'] = "Ошибка перевода!";
$cadmin_msg['form_cont_trans_error_text'] = "Не удалось перевести документ с помощью Google Translate API. URL-заголовок сформирован транслитом.";

# Global tabs
$cadmin_msg['form_cont_main'] = "основные свойства";
$cadmin_msg['form_cont_misc'] = "дополнительно";
$cadmin_msg['form_cont_ban'] = "баннер";
$cadmin_msg['form_cont_sub'] = "свойства подчиненных документов";

# Global labels
$cadmin_msg['form_cont_choose'] = "выбрать";
$cadmin_msg['form_cont_save'] = "сохранить";
$cadmin_msg['form_cont_copy'] = "копировать в раздел";
$cadmin_msg['form_cont_delete'] = "удалить";


# Titles

$cadmin_msg['form_cont_cat_title'] = "Создать раздел";
$cadmin_msg['form_cont_doc_title'] = "Создать документ";
$cadmin_msg['form_cont_ban_title'] = "Создать баннер";


# Main tab
$cadmin_msg['form_cont_main_cat'] = "Родительский раздел";
$cadmin_msg['form_cont_main_cat_start'] = "Основной";
$cadmin_msg['form_cont_main_title'] = "Заголовок";
$cadmin_msg['form_cont_main_title_menu'] = "Заголовок для меню";
	$cadmin_msg['form_cont_main_url'] = "URL-заголовок";
	$cadmin_msg['form_cont_main_url_translate'] = "перевод";
	$cadmin_msg['form_cont_main_url_translit'] = "транслит";
	$cadmin_msg['form_cont_main_url_code'] = "раздел+id";
	$cadmin_msg['form_cont_main_url_native'] = "русский";
	$cadmin_msg['form_cont_main_url_wo'] = "не использовать";
$cadmin_msg['form_cont_main_priority'] = "Приоритет";
$cadmin_msg['form_cont_main_brief'] = "Анонс";

# Misc tab
$cadmin_msg['form_cont_misc_view'] = "Отображение";
	$cadmin_msg['form_cont_misc_view_normal'] = "выводить в меню";
	$cadmin_msg['form_cont_misc_view_hidden'] = "не выводить в меню, доступ по ссылке";
	$cadmin_msg['form_cont_misc_view_disabled'] = "не выводить в меню, доступ запрещен";
$cadmin_msg['form_cont_misc_label'] = "Метка";
$cadmin_msg['form_cont_misc_style'] = "Стиль";
$cadmin_msg['form_cont_misc_url'] = "Переадресация";
$cadmin_msg['form_cont_misc_title_url'] = "Заголовок окна";
$cadmin_msg['form_cont_misc_keywords'] = "Ключевые слова";
$cadmin_msg['form_cont_misc_description'] = "Описание";
$cadmin_msg['form_cont_misc_date'] = "Дата";
$cadmin_msg['form_cont_misc_template'] = "шаблон";
$cadmin_msg['form_cont_misc_photo'] = "Фото №";
$cadmin_msg['form_cont_misc_reserved'] = "дополнительное поле";

$cadmin_msg['form_cont_misc_dev'] = "Блокировка";
	$cadmin_msg['form_cont_misc_dev_normal'] = "виден в карте сайта, возможно редактировать";
	$cadmin_msg['form_cont_misc_dev_view'] = "виден в карте сайта, редактировать невозможно";
	$cadmin_msg['form_cont_misc_dev_block'] = "не виден в карте сайта, редактирование невозможно";

$cadmin_msg['form_cont_misc_list'] = "Модуль";
$cadmin_msg['form_cont_misc_list_on'] = "Подключить";
$cadmin_msg['form_cont_misc_list_off'] = "Отключить";

# Sub tab
$cadmin_msg['form_cont_sub_sort'] = "сортировать <br /><small>подчиненные документы</small>";
	$cadmin_msg['form_cont_sub_sort_prio_up'] = "по приоритету (по возрастанию)";
	$cadmin_msg['form_cont_sub_sort_prio_down'] = "по приоритету (по убыванию)";
	$cadmin_msg['form_cont_sub_sort_title_up'] = "по наименованию (по возрастанию)";
	$cadmin_msg['form_cont_sub_sort_title_down'] = "по наименованию (по убыванию)";
	$cadmin_msg['form_cont_sub_sort_date_up'] = "по дате (по возрастанию)";
	$cadmin_msg['form_cont_sub_sort_date_down'] = "по дате (по убыванию)";
$cadmin_msg['form_cont_sub_show'] = "отображать";
$cadmin_msg['form_cont_sub_show_docs'] = "подчиненных документов в карте сайта (в порядке сортировки)";
$cadmin_msg['form_cont_sub_no_rewrite'] = "не использовать <span class=\"title\" title=\"Человекопонятный URL (например: site.ru/about вместо site.ru/page.php&amp;ID=12)\">ЧПУ</span> для данного раздела и подчиненных документов";

# Banner tab
$cadmin_msg['form_cont_banner_period_from'] = "Период действия с";
$cadmin_msg['form_cont_banner_period_to'] = "Период действия по";
$cadmin_msg['form_cont_banner_shows'] = "Количество показов";
$cadmin_msg['form_cont_banner_weight'] = "Вес (для баннеров в ротации)";
$cadmin_msg['form_cont_banner_pos'] = "Блок баннеров";
$cadmin_msg['form_cont_banner_pos_default'] = "По умолчанию";
$cadmin_msg['form_cont_banner_pos_1'] = "Блок №1";
$cadmin_msg['form_cont_banner_pos_2'] = "Блок №2";
$cadmin_msg['form_cont_banner_pos_3'] = "Блок №3";
$cadmin_msg['form_cont_banner_scope'] = "Область видимости";
$cadmin_msg['form_cont_banner_scope_pass'] = "Сквозной";
$cadmin_msg['form_cont_banner_scope_local'] = "Локальный";
$cadmin_msg['form_cont_banner_type'] = "Тип";
$cadmin_msg['form_cont_banner_type_static'] = "Статика";
$cadmin_msg['form_cont_banner_type_rotate'] = "Ротация";
$cadmin_msg['form_cont_banner_group'] = "Группировка";


# Form check
$cadmin_msg['form_cont_form_check_title'] = "Не все обязательные поля заполнены!";
$cadmin_msg['form_cont_form_check_text'] = "Пожалуйста, заполните обязательные поля в закладке \"Основные свойства\", помеченные красным цветом.";

# Delete errors
$cadmin_msg['form_cont_delete_title'] = "Ошибка удаления!";
$cadmin_msg['form_cont_delete_1'] = "У этого объекта есть подчиненные элементы. Удаление невозможно.";
$cadmin_msg['form_cont_delete_2'] = "Найдены документы, ссылающиеся на данный объект:";
$cadmin_msg['form_cont_delete_3'] = "Удаление документа с указанной Меткой невозможно, так как это может привести к неработоспособности некоторых функций сайта";
$cadmin_msg['form_cont_delete_4'] = "Этот объект является центральной страницей. Удаление невозможно.";

# ContExt errors
$cadmin_msg['form_cont_listext_insert_title'] = "Ошибка подключения модуля!";
$cadmin_msg['form_cont_listext_insert_text'] = "Данный модуль уже подключен к документу.";
$cadmin_msg['form_cont_listext_delete_title'] = "Ошибка отключения модуля!";
$cadmin_msg['form_cont_listext_delete_text'] = "Модуль содержит записи. Невозможно отключить модуль, в котором содержатся данные.";


# form_access
# ---

# Form access
$cadmin_msg['form_access_access'] = "Доступ разрешен";
$cadmin_msg['form_access_deny'] = "Доступ запрещен";
$cadmin_msg['form_access_rec'] = "включая дочерние объекты";
$cadmin_msg['form_access_submit_access'] = "разрешить доступ";
$cadmin_msg['form_access_submit_deny'] = "запретить доступ";

# form_admin
# ---

$cadmin_msg['form_admin_title'] = "Администрирование";

# Common tab
$cadmin_msg['form_admin_common'] = "Общие настройки";
$cadmin_msg['form_admin_templ'] = "Шаблоны";
$cadmin_msg['form_admin_list'] = "Списки";
$cadmin_msg['form_admin_user'] = "Пользователи";
$cadmin_msg['form_admin_contfield'] = "Дополнительные поля";
$cadmin_msg['form_admin_backup'] = "Backup";

# Admin labels
$cadmin_msg['form_admin_common_title_site'] = "Название компании";
$cadmin_msg['form_admin_common_url_site'] = "URL (адрес) сайта в Интернете";
$cadmin_msg['form_admin_common_email_admin'] = "E-mail администратора";
$cadmin_msg['form_admin_common_email_user'] = "E-mail для получения почтовых сообщений";
$cadmin_msg['form_admin_common_history'] = "Журнал";
$cadmin_msg['form_admin_common_history_off'] = "Отключено";
$cadmin_msg['form_admin_common_history_on'] = "Включено";
$cadmin_msg['form_admin_common_user_security'] = "Авторизация пользователей на сайте";
$cadmin_msg['form_admin_common_user_security_off'] = "Отключено";
$cadmin_msg['form_admin_common_user_security_on'] = "Включено";
$cadmin_msg['form_admin_common_photo'] = "Дополнительных фото";
$cadmin_msg['form_admin_common_photo_descr'] = "Описание фото";
$cadmin_msg['form_admin_common_mod_rewrite'] = "Mod_Rewrite";
$cadmin_msg['form_admin_common_mod_rewrite_off'] = "Отключено";
$cadmin_msg['form_admin_common_mod_rewrite_on'] = "Включено";
$cadmin_msg['form_admin_common_mod_rewrite_link'] = "Ссылки в формате Mod_Rewrite";
$cadmin_msg['form_admin_common_mod_rewrite_link_off'] = "Отключено";
$cadmin_msg['form_admin_common_mod_rewrite_link_on'] = "Включено";
$cadmin_msg['form_admin_common_editor_type'] = "Редактор контента";
$cadmin_msg['form_admin_common_search_stat'] = "Статистика поисковых запросов";
$cadmin_msg['form_admin_common_search_stat_off'] = "Отключено";
$cadmin_msg['form_admin_common_search_stat_on'] = "Включено";
$cadmin_msg['form_admin_common_securemail'] = "Защита почтовых форм от спама";
$cadmin_msg['form_admin_common_securemail_off'] = "Отключено";
$cadmin_msg['form_admin_common_securemail_on'] = "Включено";
$cadmin_msg['form_admin_common_ifont'] = "Наложение изображения на фото";
$cadmin_msg['form_admin_common_ifont_min'] = "Мин. размер фото";
$cadmin_msg['form_admin_common_ifont_off'] = "Отключено";
$cadmin_msg['form_admin_common_ifont_on'] = "Включено";
$cadmin_msg['form_admin_common_ifont_text'] = "Текст";
$cadmin_msg['form_admin_common_ifont_size'] = "Размер шрифта";
$cadmin_msg['form_admin_common_ifont_x'] = "Координата X";
$cadmin_msg['form_admin_common_ifont_y'] = "Координата Y";
$cadmin_msg['form_admin_common_ifont_angle'] = "Угол";
$cadmin_msg['form_admin_common_ifont_pos'] = "Позиция вывода";
$cadmin_msg['form_admin_common_ifont_pos_default'] = "---";

$cadmin_msg['form_admin_common_ifont_pos_top'] = "Вверху (лев)";
$cadmin_msg['form_admin_common_ifont_pos_center'] = "По центру";
$cadmin_msg['form_admin_common_ifont_pos_bottom'] = "Внизу (лев)";

$cadmin_msg['form_admin_common_ifont_pos_top_r'] = "Вверху (прав)";
$cadmin_msg['form_admin_common_ifont_pos_bottom_r'] = "Внизу (прав)";

$cadmin_msg['form_admin_common_ifont_color'] = "Цвет шрифта";
$cadmin_msg['form_admin_common_ifont_bg'] = "Цвет фона";
$cadmin_msg['form_admin_common_ifont_bg_transp'] = "Прозрачность фона";
$cadmin_msg['form_admin_common_ifont_name'] = "Шрифт";
$cadmin_msg['form_admin_common_ifont_transparency'] = "Прозрачность";
$cadmin_msg['form_admin_common_ifont_pic'] = "Изображение";
$cadmin_msg['form_admin_common_imgcache'] = "Кеширование изображений";
$cadmin_msg['form_admin_common_imgcache_off'] = "Отключено";
$cadmin_msg['form_admin_common_imgcache_on'] = "Включено";
$cadmin_msg['form_admin_common_index'] = "Главная страница (index)";
$cadmin_msg['form_admin_common_index_descr'] = "Описание главной страницы";

# Template labels
$cadmin_msg['form_admin_templ_head_title'] = "Название шаблона";
$cadmin_msg['form_admin_templ_head_filename'] = "Имя файла";
$cadmin_msg['form_admin_templ_head_save'] = "Сохранить";
$cadmin_msg['form_admin_templ_head_del'] = "Удалить";
$cadmin_msg['form_admin_templ_form_save'] = "Сохранить";
$cadmin_msg['form_admin_templ_form_del'] = "Удалить";
$cadmin_msg['form_admin_templ_new_record'] = "Новый шаблон";

$cadmin_msg['form_admin_templ_delerr_title'] = "Ошибка удаления";
$cadmin_msg['form_admin_templ_delerr_text'] = "Этот шаблон нельзя удалить, так как он является системным.";
$cadmin_msg['form_admin_templ_delerr2_title'] = "Ошибка удаления";
$cadmin_msg['form_admin_templ_delerr2_text'] = "Невозможно удалить шаблон, так как он используется в следующих документах:<br>";

# List labels
$cadmin_msg['form_admin_list_head_title'] = "Наименование";
$cadmin_msg['form_admin_list_head_fields'] = "Поля";
$cadmin_msg['form_admin_list_head_type'] = "Тип";
$cadmin_msg['form_admin_list_head_sort'] = "Сортировка";
$cadmin_msg['form_admin_list_head_status'] = "Состояние";
$cadmin_msg['form_admin_list_head_dev'] = "Служ";
$cadmin_msg['form_admin_list_head_save'] = "Сохранить";
$cadmin_msg['form_admin_list_head_del'] = "Удалить";
$cadmin_msg['form_admin_list_head_act'] = "Активировать";
$cadmin_msg['form_admin_list_head_dump'] = "Выгрузить";

$cadmin_msg['form_admin_list_edit'] = "Править";
$cadmin_msg['form_admin_list_type_list'] = "Список";
$cadmin_msg['form_admin_list_type_record'] = "Запись";
$cadmin_msg['form_admin_list_sort_default'] = "по умолчанию";
$cadmin_msg['form_admin_list_status_not_defined'] = "Поля не определены";
$cadmin_msg['form_admin_list_status_not_activated'] = "Не активирован";
$cadmin_msg['form_admin_list_status_act_required'] = "Требуется реактивация!";
$cadmin_msg['form_admin_list_status_activated'] = "Активирован";

$cadmin_msg['form_admin_list_save'] = "сохранить";
$cadmin_msg['form_admin_list_del'] = "удалить";
$cadmin_msg['form_admin_list_react'] = "реактивировать";
$cadmin_msg['form_admin_list_act'] = "активировать";

$cadmin_msg['form_admin_list_new'] = "Новая запись";

# Listfield labels
$cadmin_msg['form_admin_listfield_head_title'] = "Наименование поля";
$cadmin_msg['form_admin_listfield_head_descr'] = "Описание";
$cadmin_msg['form_admin_listfield_head_type'] = "Тип";
$cadmin_msg['form_admin_listfield_head_list'] = "Список";
$cadmin_msg['form_admin_listfield_head_field'] = "Поле";
$cadmin_msg['form_admin_listfield_head_foption'] = "Опции";
$cadmin_msg['form_admin_listfield_head_protect'] = "Защита";
$cadmin_msg['form_admin_listfield_head_create'] = "Создавать<br>всегда";
$cadmin_msg['form_admin_listfield_head_size'] = "Размер";
$cadmin_msg['form_admin_listfield_head_status'] = "Состояние";
$cadmin_msg['form_admin_listfield_head_index'] = "Инд";
$cadmin_msg['form_admin_listfield_head_main'] = "Глав";
$cadmin_msg['form_admin_listfield_head_save'] = "Сохранить";
$cadmin_msg['form_admin_listfield_head_del'] = "Удалить";
$cadmin_msg['form_admin_listfield_head_pos'] = "Позиция";

$cadmin_msg['form_admin_listfield_fields_string'] = "Строка";
$cadmin_msg['form_admin_listfield_fields_text'] = "Текст";
$cadmin_msg['form_admin_listfield_fields_num'] = "Число";
$cadmin_msg['form_admin_listfield_fields_date'] = "Дата";
$cadmin_msg['form_admin_listfield_fields_file'] = "Файл";
$cadmin_msg['form_admin_listfield_fields_photo'] = "Фото";
$cadmin_msg['form_admin_listfield_fields_checkbox'] = "Ключ";
$cadmin_msg['form_admin_listfield_fields_list'] = "Список";
$cadmin_msg['form_admin_listfield_fields_object'] = "Объект";
$cadmin_msg['form_admin_listfield_fields_lobject'] = "Справочник";
$cadmin_msg['form_admin_listfield_fields_link'] = "Ссылка";
$cadmin_msg['form_admin_listfield_fields_user'] = "Пользователь";
$cadmin_msg['form_admin_listfield_fields_dhtml'] = "DHTML";
$cadmin_msg['form_admin_listfield_fields_folder'] = "Папка";
$cadmin_msg['form_admin_listfield_fields_sysdate'] = "Системная дата";

$cadmin_msg['form_admin_listfield_fields_string_std'] = "Стандартное";
$cadmin_msg['form_admin_listfield_fields_string_auto'] = "Автозаполнение";


$cadmin_msg['form_admin_listfield_fields_sysdate_add'] = "Добавление";
$cadmin_msg['form_admin_listfield_fields_sysdate_change'] = "Добавление, правка";

$cadmin_msg['form_admin_listfield_fields_photo_wo_preview'] = "Без превью";
$cadmin_msg['form_admin_listfield_fields_photo_with_preview'] = "С превью";


$cadmin_msg['form_admin_listfield_select'] = "Выбрать";
$cadmin_msg['form_admin_listfield_not_defined'] = "Не определен";

$cadmin_msg['form_admin_listfield_enabled'] = "Выводить";
$cadmin_msg['form_admin_listfield_disabled'] = "Не выводить";

$cadmin_msg['form_admin_listfield_save'] = "Сохранить";
$cadmin_msg['form_admin_listfield_del'] = "Удалить";
$cadmin_msg['form_admin_listfield_new'] = "Новая запись";

$cadmin_msg['form_admin_listfield_unique_title'] = "Поле не уникально";
$cadmin_msg['form_admin_listfield_unique_text'] = "Наименование поля должно быть уникальным в рамках одного списка.";

# User labels
$cadmin_msg['form_admin_user_head_name'] = "ФИО";
$cadmin_msg['form_admin_user_head_descr'] = "Описание";
$cadmin_msg['form_admin_user_head_email'] = "Email";
$cadmin_msg['form_admin_user_head_login'] = "Логин";
$cadmin_msg['form_admin_user_head_pass'] = "Пароль";
$cadmin_msg['form_admin_user_head_type'] = "Тип";
$cadmin_msg['form_admin_user_head_save'] = "Сохранить";
$cadmin_msg['form_admin_user_head_del'] = "Удалить";
$cadmin_msg['form_admin_user_type_disabled'] = "Отключен";
$cadmin_msg['form_admin_user_type_user'] = "Пользователь";
$cadmin_msg['form_admin_user_type_editor'] = "Администратор";
$cadmin_msg['form_admin_user_type_admin'] = "Разработчик";
$cadmin_msg['form_admin_user_type_web'] = "Посетитель сайта";
$cadmin_msg['form_admin_user_save'] = "сохранить";
$cadmin_msg['form_admin_user_del'] = "удалить";
$cadmin_msg['form_admin_user_new'] = "Новая запись";

$cadmin_msg['form_admin_user_login_exists_title'] = "Login не уникален";
$cadmin_msg['form_admin_user_login_exists_text'] = "Пользователь с таким Login'ом существует. Пожалуйста, укажите другой Login.";

# Contfield labels
$cadmin_msg['form_admin_contfield_head_title'] = "Наименование";
$cadmin_msg['form_admin_contfield_head_descr'] = "Описание";
$cadmin_msg['form_admin_contfield_head_type'] = "Тип";
$cadmin_msg['form_admin_contfield_head_list'] = "Список";
$cadmin_msg['form_admin_contfield_head_sort'] = "Сортировка";
$cadmin_msg['form_admin_contfield_head_place'] = "Раздел";
$cadmin_msg['form_admin_contfield_head_size'] = "Размер";
$cadmin_msg['form_admin_contfield_head_checkbox'] = "Ключ";
$cadmin_msg['form_admin_contfield_head_access'] = "Доступ";
$cadmin_msg['form_admin_contfield_head_doctype'] = "Доступ";
$cadmin_msg['form_admin_contfield_head_save'] = "Сохранить";
$cadmin_msg['form_admin_contfield_head_del'] = "Удалить";

$cadmin_msg['form_admin_contfield_default'] = "Стандартное";
$cadmin_msg['form_admin_contfield_list'] = "Список";

$cadmin_msg['form_admin_contfield_sort_asc'] = "В прямом порядке";
$cadmin_msg['form_admin_contfield_sort_desc'] = "В обратном порядке";

$cadmin_msg['form_admin_contfield_type_main'] = "Основные";
$cadmin_msg['form_admin_contfield_type_misc'] = "Дополнительные";
$cadmin_msg['form_admin_contfield_type_cat'] = "Раздел";

$cadmin_msg['form_admin_contfield_enabled'] = "Выводить";
$cadmin_msg['form_admin_contfield_disabled'] = "Не выводить";

$cadmin_msg['form_admin_contfield_doctype_any'] = "Любой";
$cadmin_msg['form_admin_contfield_doctype_doc'] = "Документ";
$cadmin_msg['form_admin_contfield_doctype_banner'] = "Баннер";

$cadmin_msg['form_admin_contfield_save'] = "сохранить";
$cadmin_msg['form_admin_contfield_del'] = "удалить";
$cadmin_msg['form_admin_contfield_newrecord'] = "Новая запись";

$cadmin_msg['form_admin_contfield_exists_title'] = "Поле не уникально";
$cadmin_msg['form_admin_contfield_exists_text'] = "Поле с таким именем уже существует. Необходимо указать уникальное имя поля.";
$cadmin_msg['form_admin_contfield_empty_title'] = "Поле не определено";
$cadmin_msg['form_admin_contfield_empty_text'] = "Наименование поля не должно быть пустым.";

$cadmin_msg['form_admin_backup_note'] = "Примечание";
$cadmin_msg['form_admin_backup_onlycmstables'] = "Только таблицы CMS";
$cadmin_msg['form_admin_backup_dbencoding'] = "Кодировка таблиц";
$cadmin_msg['form_admin_backup_dumpencoding'] = "Кодировка дампа";
$cadmin_msg['form_admin_backup_save'] = "Сохранить";
$cadmin_msg['form_admin_backup_download'] = "Скачать";


# Mail labels
$cadmin_msg['form_mail_search'] = "поиск";
$cadmin_msg['form_mail_find'] = "найти";
$cadmin_msg['form_mail_n'] = "№";
$cadmin_msg['form_mail_clear'] = "очистить";
$cadmin_msg['form_mail_not_fount'] = "По вашему запросу ничего не найдено!";
$cadmin_msg['form_mail_del'] = "удалить";
$cadmin_msg['form_mail_empty'] = "База данных почтовых сообщений пуста.";

$cadmin_msg['form_mail_head_date'] = "Дата";
$cadmin_msg['form_mail_head_n'] = "№";
$cadmin_msg['form_mail_head_msg'] = "Сообщение";
$cadmin_msg['form_mail_head_ip_host'] = "IP-адрес / Узел";
//$cadmin_msg['form_mail_head_host'] = "Хост";
$cadmin_msg['form_mail_head_del'] = "Удалить";

# Maillist labels
$cadmin_msg['form_maillist_subject'] = "Сообщение с сайта";
$cadmin_msg['form_maillist_success'] = "Сообщение отправлено!";
$cadmin_msg['form_maillist_send'] = "отправить";


# Journal labels
$cadmin_msg['form_journal_clear'] = "Очистить журнал";
$cadmin_msg['form_journal_date'] = "Дата";
$cadmin_msg['form_journal_author'] = "Пользователь";
$cadmin_msg['form_journal_oper'] = "Операция";
$cadmin_msg['form_journal_title'] = "Заголовок";
$cadmin_msg['form_journal_create'] = "Создание";
$cadmin_msg['form_journal_edit'] = "Редактирование";
$cadmin_msg['form_journal_del'] = "Удаление";


# Tree labels
$cadmin_msg['form_tree_title'] = "Структура сайта";
$cadmin_msg['form_tree_site_map'] = "Карта сайта";
$cadmin_msg['form_tree_open'] = "раскрыть";
$cadmin_msg['form_tree_close'] = "свернуть";
$cadmin_msg['form_tree_bookmarks'] = "Избранное";
$cadmin_msg['form_tree_last'] = "Последние документы";
$cadmin_msg['form_tree_root'] = "Начало сайта";
$cadmin_msg['form_tree_limit1'] = "отображается";
$cadmin_msg['form_tree_limit2'] = "подчиненных элементов";

# Banners labels
$cadmin_msg['form_ban_title'] = "Список баннеров";
$cadmin_msg['form_ban_create'] = "Создать баннер";

?>