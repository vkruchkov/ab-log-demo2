/**
 * @license Copyright (c) 2003-2014, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
	// config.uiColor = '#AADC6E';

	config.ignoreEmptyParagraph = false;

 	config.filebrowserBrowseUrl = '/cadmin/ckeditor/ckfinder/ckfinder.html';
 	config.filebrowserImageBrowseUrl = '/cadmin/ckeditor/ckfinder/ckfinder.html?type=Images';
 	config.filebrowserFlashBrowseUrl = '/cadmin/ckeditor/ckfinder/ckfinder.html?type=Flash';
 	config.filebrowserUploadUrl = '/cadmin/ckeditor/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files';
 	config.filebrowserImageUploadUrl = '/cadmin/ckeditor/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images';
 	config.filebrowserFlashUploadUrl = '/cadmin/ckeditor/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash';
 	config.filebrowserWindowWidth = '1000';
 	config.filebrowserWindowHeight = '700';


};
