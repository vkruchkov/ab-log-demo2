﻿<script type="text/javascript">
$(document).ready(
function()
{
	var date1 = $('#date1').datepicker().on('changeDate', function(ev) {date1.hide();}).data('datepicker');
	var date2 = $('#date2').datepicker().on('changeDate', function(ev) {date2.hide();}).data('datepicker');
})
</script>

<?	
if ( isset($user_info['UType']) && $user_info['UType'] > 0 && $user_info['UType'] < 4 )
{

	if ( empty($_GET['date1']) )
	$_GET['date1'] = date("Y-m-01");
	if ( empty($_GET['date2']) )
	$_GET['date2'] = date("Y-m-d");

	$acc_type="Расход";
	
	$total_sum = $o42->select_line("SELECT SUM(a.acc_oper_sum) tsum
	FROM tmp_17 a, tmp_16 b
	WHERE DATE_FORMAT(a.acc_oper_date, '%Y-%m-%d')>='".$_GET['date1']."'
	AND DATE_FORMAT(a.acc_oper_date, '%Y-%m-%d')<='".$_GET['date2']."'
	AND a.acc_oper_type=b.tmpID AND b.acc_type_type='$acc_type'");
		
	$result = $o42->select("SELECT b.acc_type_title, SUM(a.acc_oper_sum) my_sum
	FROM tmp_17 a, tmp_16 b
	WHERE DATE_FORMAT(a.acc_oper_date, '%Y-%m-%d')>='".$_GET['date1']."'
	AND DATE_FORMAT(a.acc_oper_date, '%Y-%m-%d')<='".$_GET['date2']."'
	AND a.acc_oper_type=b.tmpID AND b.acc_type_type='$acc_type'
	GROUP BY b.acc_type_title
	ORDER BY my_sum DESC");
	
	$acc_type="Доход";
	
	$total_sum2 = $o42->select_line("SELECT SUM(a.acc_oper_sum) tsum
	FROM tmp_17 a, tmp_16 b
	WHERE DATE_FORMAT(a.acc_oper_date, '%Y-%m-%d')>='".$_GET['date1']."'
	AND DATE_FORMAT(a.acc_oper_date, '%Y-%m-%d')<='".$_GET['date2']."'
	AND a.acc_oper_type=b.tmpID AND b.acc_type_type='$acc_type'");
		
	$result2 = $o42->select("SELECT b.acc_type_title, SUM(a.acc_oper_sum) my_sum
	FROM tmp_17 a, tmp_16 b
	WHERE DATE_FORMAT(a.acc_oper_date, '%Y-%m-%d')>='".$_GET['date1']."'
	AND DATE_FORMAT(a.acc_oper_date, '%Y-%m-%d')<='".$_GET['date2']."'
	AND a.acc_oper_type=b.tmpID AND b.acc_type_type='$acc_type'
	GROUP BY b.acc_type_title
	ORDER BY my_sum DESC");
	

	?>

	<script src="libs/jquery-1.7.min.js"></script>
	<script src="../js2/highcharts.js"></script>

	<script type="text/javascript">
	$(function () {
        $('#container').highcharts({
            credits: { enabled: false },
			chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false
            },
            title: {
                text: 'Структура расходов'
            },
            tooltip: {
			formatter: function() {
				return this.point.name +'<b>: '+ Math.round(this.percentage*100)/100 +' %</b>';
			}
       	    //pointFormat: '{series.name}: <b>{point.percentage}%</b>',
            },
            plotOptions: {
                pie: {
					allowPointSelect: true,
                    //cursor: 'pointer',
                    dataLabels: {
                        enabled: true,
                        color: '#000000',
                        connectorColor: '#000000',
                        formatter: function() {
                            return '<b>'+ this.point.name +'</b>: '+ Math.round(this.percentage*100)/100 +' %';
                        }
                    }
                }
            },
            series: [{
                type: 'pie',
                name: 'Browser share',
				startAngle: 100,
                data: [
		<?
		for ( $i = 0; $i < count($result); $i++ )
		echo  "['".$result[$i]['acc_type_title']."', ".round($result[$i]['my_sum'] / $total_sum['tsum'] * 100, 2)."],";
		?>

                ]
            }]
        });
		
        $('#container2').highcharts({
            credits: { enabled: false },
			chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false
            },
            title: {
                text: 'Структура доходов'
            },
            tooltip: {
			formatter: function() {
				return this.point.name +'<b>: '+ Math.round(this.percentage*100)/100 +' %</b>';
			}
       	    //pointFormat: '{series.name}: <b>{point.percentage}%</b>',
            },
            plotOptions: {
                pie: {
					allowPointSelect: true,
                    //cursor: 'pointer',
                    dataLabels: {
                        enabled: true,
                        color: '#000000',
                        connectorColor: '#000000',
                        formatter: function() {
                            return '<b>'+ this.point.name +'</b>: '+ Math.round(this.percentage*100)/100 +' %';
                        }
                    }
                }
            },
            series: [{
                type: 'pie',
                name: 'Browser share',
				startAngle: 100,
                data: [
		<?
		for ( $i = 0; $i < count($result2); $i++ )
		echo  "['".$result2[$i]['acc_type_title']."', ".round($result2[$i]['my_sum'] / $total_sum2['tsum'] * 100, 2)."],";
		?>

                ]
            }]
        });
		

		
    });
	</script>

	<?

	echo "<h1>Финансовый отчет</h1><br>";

	echo "<div style=\"width:30%;float:left; \">";
		echo "<form action=\"index.php\">";
			echo "<input type=\"hidden\" name=\"p\" value=\"acc_graph.php\">";
			echo "<div class=\"controls\">";
				echo "с <input type=\"datetime-local\" class=\"input-medium\" data-date-format=\"yyyy-mm-dd\" name=\"date1\" id=\"date1\" value=\"".$_GET['date1']."\"> ";
				echo "по <input type=\"datetime-local\" class=\"input-medium\" data-date-format=\"yyyy-mm-dd\" name=\"date2\" id=\"date2\" value=\"".$_GET['date2']."\"> ";
				echo "<input type=\"submit\" value=\"Применить\">";
			echo "</div>";
		echo "</form>";
		
		echo "<h3>Расходы</h3>";
		echo "<ul>";
			for ( $i = 0; $i < count($result); $i++ )
			echo  "<li>".$result[$i]['acc_type_title'].": ".$result[$i]['my_sum']." (".round($result[$i]['my_sum'] / $total_sum['tsum'] * 100, 2)."%)</li>";
		echo "</ul>";
		echo "<h3>ИТОГО: ".$total_sum['tsum']."</h3>";

		echo "<hr size=\"1\">";
		echo "<h3>Доходы</h3>";
		echo "<ul>";
			for ( $i = 0; $i < count($result2); $i++ )
			echo  "<li>".$result2[$i]['acc_type_title'].": ".$result2[$i]['my_sum']." (".round($result2[$i]['my_sum'] / $total_sum2['tsum'] * 100, 2)."%)</li>";
		echo "</ul>";
		echo "<h3>ИТОГО: ".$total_sum2['tsum']."</h3>";

		
	echo "</div>";
	
	echo "<div id=\"container\" style=\"float:right;width:70%; height:450px;\"></div>";
	echo "<div id=\"container2\" style=\"float:right;width:70%; height:300px;\"></div>";
}
?>