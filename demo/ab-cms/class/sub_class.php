<?
/*
* Copyright (c) 2013, Andrey_B (Office-42)
* ��������� ��. LICENSE.txt ��� http://www.gnu.org/licenses/
*/

class cms_full extends cms_lib
{
	public function __construct()
	{
		parent::__construct();
	}


	// ���������� � �������������� ������� � ������������ �������
	public function table_update($table_name, $table_key, $table_struct, $table_data)
	{
		$u_query = "";
		$u_query_fields = "";

		if ( empty($table_data[$table_key]) )
		{
			$max_key = $this->select_line("SELECT MAX($table_key) $table_key FROM $table_name");
			$max_key[$table_key]++;
		}
		
		foreach($table_struct as $key => $value)
		$table_struct_fields[] = $key;

		foreach($table_data as $key => $value)
		{
			if ( in_array($key, $table_struct_fields) )
			{
				if ( !empty($table_data[$table_key]))
				{
					if ( $key != $table_key )
					$u_query .= "$key = '".mysqli_real_escape_string($this->db_conn, $value)."',";
				}
				else
				{
					$u_query_fields .= "$key,";
					$u_query .= "'".mysqli_real_escape_string($this->db_conn, $value)."',";
				}
			}
		}

		if ( !empty($table_data[$table_key]) )
		$u_query = "UPDATE $table_name SET ".rtrim($u_query, ",")." WHERE $table_key=".$table_data[$table_key];
		else
		$u_query = "INSERT INTO $table_name ($table_key, ".rtrim($u_query_fields, ",").") VALUES (".$max_key[$table_key].",".rtrim($u_query, ",").")";

		//echo $u_query;
		$this->execute($u_query);
	}

	// �������� ������ � ������������ �������
	public function table_delete($table_name, $table_key, $table_key_value)
	{
		$table_key_value = intval($table_key_value);
		$this->execute("DELETE FROM $table_name WHERE $table_key=$table_key_value");
	}
	
	public function auth()
	{
		if ( isset($_GET['cadmin_out']) )
		{
			setcookie("cadmin_out", 1, 0, "/", $_SERVER['SERVER_NAME']);
			//setcookie("cadmin_out", 1);
			return -1;
		}

		if (!isset($_SERVER['PHP_AUTH_USER']) || $_COOKIE['cadmin_out'] == 1 )
		{
			header('WWW-Authenticate: Basic realm="Input password"');
			header('HTTP/1.0 401 Unauthorized');
			setcookie("cadmin_out", 0, 0, "/", $_SERVER['SERVER_NAME']);
			return 0;
		}
		else
		{
			$user_pwd = $this->select_line("SELECT Password, MD5_Hash, UserID FROM tUser WHERE Login='".mysqli_real_escape_string($this->db_conn, $_SERVER['PHP_AUTH_USER'])."' LIMIT 1");
			
			$user_info = array();
			if ( isset($user_pwd['Password']) && $user_pwd['Password'] == md5(md5($_SERVER['PHP_AUTH_PW']).$user_pwd['MD5_Hash']) )
			$user_info = $this->select_line("SELECT UserID, UType, Name FROM tUser WHERE UserID=".$user_pwd['UserID']);

			if ( empty($user_info) )
			setcookie("cadmin_out", 1, 0, "/", $_SERVER['SERVER_NAME']);
			else
			setcookie("cadmin_out", 0, 0, "/", $_SERVER['SERVER_NAME']);
		}

		return $user_info;
	}
	
	// ��������� mod_rewrite ������ � ������ �� � .htaccess
	public function write_rewrite()
	{
		$htaccess_clr = "";
		
		if (file_exists("../.htaccess"))
		{
			$htaccess = file("../.htaccess");
			$ht_flag = 0;
			for ( $i = 0; $i < count($htaccess); $i++ )
			{ 
				if ( preg_match("/###\+\+\+###/", $htaccess[$i]) )
				$ht_flag = 1;
				if ( $ht_flag != 1 )
				$htaccess_clr .= $htaccess[$i];
				if ( preg_match("/###---###/", $htaccess[$i]) )
				$ht_flag = 0;
			}
		}

		$new = fopen("../.htaccess", "w");
		fwrite($new, $htaccess_clr, strlen($htaccess_clr));
		fclose($new);
		
		if ( !empty($this->conf['Mod_Rewrite']) )
		{
			$my_rb = pathinfo($_SERVER['REQUEST_URI']);
			$RewriteBase = preg_replace("/cadmin/", "", $my_rb['dirname']);

			$Rewrite_Rules = "###+++### Autogeneration. Do not change this section!\nOptions -MultiViews\nRewriteEngine On\nRewriteBase	$RewriteBase\n";
			$Rewrite_Rules .= "RewriteRule	^sitemap.xml$	page.php?sitemap=google\n";
			
			$result = $this->select("SELECT ContID, Title FROM tCont WHERE Rewrite='' AND Disabled=0 AND Status=0 AND No_Rewrite<>1");
			for ( $i = 0; $i < count($result); $i++ )
			{
				$doc_rewrite = preg_replace("/[^\-a-z_0-9]+/", "", $this->translit(strtolower(preg_replace("/\s+/", "-", $result[$i]['Title']))));
				$this->execute("UPDATE tCont SET Rewrite='$doc_rewrite' WHERE ContID=".$result[$i]['ContID']);
			}

			$result = $this->select("SELECT a.ContID, a.Rewrite, b.ParentID, a.Mod_Rewrite
									FROM tCont a, tContExt b
									WHERE (a.CType=0 OR a.CType=1) AND a.Rewrite<>'' AND a.Disabled=0 AND a.Status=0 AND a.No_Rewrite<>1 AND a.ContID=b.ContID");
			for ( $i = 0; $i < count($result); $i++ )
			{
				$Rewrite_p = "/".$result[$i]['Rewrite'];
				$parent = $result[$i]['ParentID'];

				$r_Disabled = 0;
				while ( $parent != 0 )
				{
					//$old_parent = $parent;
					$result2 = $this->select("SELECT a.ContID, a.Rewrite, b.ParentID, a.Disabled
											FROM tCont a, tContExt b
											WHERE a.ContID=b.ContID AND a.ContID=$parent AND a.No_Rewrite<>1 AND a.Rewrite<>''");
					for ( $j = 0; $j < count($result2); $j++ )
					{
						$parent = $result2[$j]['ParentID'];
						$Rewrite_p = "/".$result2[$j]['Rewrite'].$Rewrite_p;
						if ( $result2[$j]['Disabled'] == 1 && $result2[$j]['ParentID'] > 0 && $result2[$j]['ContID'] > 0 )
						$r_Disabled = 1;
					}
					
					/*
					if ( $old_parent == $parent )
					{
						$parent = 0;
						$r_Disabled = 1;
					}
					*/
					
				}

				if ( $r_Disabled == 0 )
				{
					$Rewrite_p = preg_replace("/^\//", "", $Rewrite_p);
					if ( file_exists("../".$Rewrite_p) )
					$Rewrite_p .= "/";

					$Rewrite_Rules .= "RewriteRule	^$Rewrite_p\$	page.php?ID=".$result[$i]['ContID']."\n";
				
					if ( $result[$i]['Mod_Rewrite'] != "/$Rewrite_p" )
					$this->execute("UPDATE tCont SET Mod_Rewrite='/$Rewrite_p' WHERE ContID=".$result[$i]['ContID']);
				}
				
			}

			$Rewrite_Rules .= "###---###\n";
			$new = fopen("../.htaccess", "a");
			fwrite($new, $Rewrite_Rules, strlen($Rewrite_Rules));
			fclose($new);
		}
	}
	
	// ����������� �������������� ����� � ���������
	public function show_contfields($field_type = 0, $ContID = 0)
	{
		$ContID = intval($ContID);
		$field_type = intval($field_type);
		
		//$this->select("SELECT ContFieldID, Title, Descr, LType, Size, ListID, SortList FROM tContField WHERE FieldType=$field_type AND UserType<=$U_Type AND Disabled=0 ORDER BY Priority, ContFieldID");
		$result = $this->select("SELECT ContFieldID, Title, Descr, LType, Size, ListID, SortList FROM tContField WHERE Disabled=0 ORDER BY Priority, ContFieldID");
		for ( $i = 0; $i < count($result); $i++ )
		{
			//$Extra_Value = array();
			if ( !empty($ContID) )
			$Extra_Value = $this->select_line("SELECT Value FROM tContFieldExt WHERE ContID=$ContID AND ContFieldID=".$result[$i]['ContFieldID']." LIMIT 1");
			if (!isset($Extra_Value['Value']) )
			$Extra_Value['Value'] = "";

			echo "<div class=\"control-group\">";
				echo "<label class=\"control-label\">".$result[$i]['Descr']."</label>";
				echo "<div class=\"controls\"><input type=\"text\" class=\"\" id=\"".$result[$i]['Title']."\" name=\"".$result[$i]['Title']."\" value=\"".$Extra_Value['Value']."\"></div>";
			echo "</div>";

		}
		
	}

}


?>