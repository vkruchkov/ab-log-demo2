<?
/*
* Copyright (c) 2013, Andrey_B (Office-42)
* Подробнее см. LICENSE.txt или http://www.gnu.org/licenses/
*/

require_once(dirname(__FILE__)."/../config.php");
//require_once(dirname(__FILE__)."/lang/msg_ru.php");

// Работа с БД
class cms_db extends cms_conf
{
	public $db_conn;
	protected $fetch_method;

	// Конструктор для класса cms_db -> соединение с БД
	public function __construct()
	{
		error_reporting($this->err_rep);

		$this->db_conn=new mysqli($this->db_host, $this->db_user, $this->db_pwd, $this->db_name);

		if($this->db_conn->connect_error)
		throw new Exception($this->db_conn->connect_error);

	}

	// Запрос к БД
	public function query($db_query)
	{
		$fetch_Method = $this->fetch_method;
		$db_res = array();

		if (!($result = $this->db_conn->query($db_query)))
		throw new Exception(mysqli_error($this->db_conn));
		else
		{
			while ($row = $result->$fetch_Method())
			$db_res[] = $row;
		}

		return($db_res);
	}

	// Запросы типа INSERT и UPDATE
	public function execute($db_query)
	{
		$db_res = array();

		if (!($result = $this->db_conn->query($db_query)))
		throw new Exception(mysqli_error($this->db_conn));
	
		return($db_res);
	}

	
	// Запрос к БД, возвращающий одну (в случае, если в запросе LIMIT 1) или последнюю строку
	public function query_l($db_query)
	{
		$fetch_Method = $this->fetch_method;
		$db_res = array();

		if (!($result = $this->db_conn->query($db_query)))
		throw new Exception(mysqli_error($this->db_conn));
		elseif ( !is_null($fetch_Method) )
		{
			while ($row = $result->$fetch_Method())
			$db_res = $row;
		}

		return $db_res;
	}

	// Запрос к БД, где в качестве результата выдается двумерный ассоциативный массив (таблица)
	public function select($db_query)
	{
		$this->fetch_method = "fetch_assoc";
		$arguments = func_get_args();
		return call_user_func_array(array($this, 'query'), $arguments);
	}

	// Запрос к БД, где в качестве результата выдается двумерный нумерованный массив (таблица)
	public function select_row($db_query)
	{
		$this->fetch_method = "fetch_row";
		$arguments = func_get_args();
		return call_user_func_array(array($this, 'query'), $arguments);
	}

	// Запрос к БД, где в качестве результата ассоциативный массив (одна строка)
	public function select_line($db_query)
	{
		$this->fetch_method = "fetch_assoc";
		$arguments = func_get_args();
		return call_user_func_array(array($this, 'query_l'), $arguments);
	}

	// Запрос типа SET
	public function set($db_query)
	{
		$this->fetch_method = null;
		$arguments = func_get_args();
		return call_user_func_array(array($this, 'query_l'), $arguments);
	}


	// Деструктор. Завершение соединения с БД
	public function __destruct()
	{
		$this->db_conn->close();
	}
}

// Библиотека методов для работы со структурой данных CMS
// ======================================================
class cms_lib extends cms_db
{
	public $conf; // Настройки сайта

	public function __construct()
	{
		parent::__construct();
		// !!!! ВПОСЛЕДСТВИЕ УБРАТЬ !!!!
		$result = $this->set("SET NAMES 'UTF8'");
		
		$this->conf = $this->select_line("SELECT * FROM tConf LIMIT 1");
	}

	// Метод для получения всех атрибутов (полей) документа
	public function doc_read($id)
	{
		$id = intval($id);
		
		$result = $this->select_line("SELECT a.*, b.Title List_Title FROM tCont a, tList b WHERE a.ContID=$id AND a.ListID=b.ListID LIMIT 1");
		$result_photo = $this->select("SELECT * FROM tPhoto WHERE ContID=$id");
		$photo_cnt = 1;
		for ( $i = 0; $i < count($result_photo); $i++ )
		{
			$result['Photo'.$photo_cnt] = $result_photo[$i]['Photo'];
			$result['Alt'.$photo_cnt] = $result_photo[$i]['Alt'];
			$photo_cnt++;
		}
		return $result;
	}

	// Метод для поиска первого документа/раздела в структуре (дереве) сайта
	public function doc_find()
	{
		$result = $this->select_line("SELECT a.ContID FROM tCont a, tContExt b WHERE a.ContID=b.ContID AND b.ParentID=0 ORDER BY a.Priority LIMIT 1");
		return $result['ContID'];
	}
	
	// Метод для создания (считывания) дерева родителей документа
	public function ptree_find($id)
	{
		$safe_cnt = 0;
		$id = intval($id);
		
		$result = $this->select_line("SELECT ContID, Cont_Label, Title, Mod_Rewrite FROM tCont WHERE ContID=$id LIMIT 1");
		$func_res[] = $result;
		
		while ( !empty($id) )
		{
				$result = $this->select_line("SELECT b.ContID, b.Cont_Label, b.Title, b.Mod_Rewrite FROM tContExt a, tCont b WHERE a.ParentID=b.ContID AND a.ContID=$id LIMIT 1");
				if ( isset($result['ContID']) )
				$id = $result['ContID'];
				else
				$id = 0;
				$func_res[] = $result;
				
				// Защита от зацикливания, в случае проблем с БД
				$safe_cnt++;
				if ( $safe_cnt > 1000 )
				{
					$id = 0;
					throw new Exception("CMS: find_ptree() loop problem!");
				}
		}

		//$func_res[] = array();
		
		return array_reverse($func_res);
	}

	// Вывод информации из "Модулей" с помощью шаблона
	// ВНИМАНИЕ! НЕОБХОДИМА ПРОВЕРКа ПАРАМЕТРОВ $sort и $limit и ФОРМАТИРОВАНИЕ $where на стороне шаблона!!!
	// Потенциально небезопасная функция!
	public function mod_read($id, $tpl = "", $sort = "", $limit = "", $where = "")
	{
		$tpl_res = "";
		$id = intval($id);
		$sort = mysqli_real_escape_string($this->db_conn, $sort);
		$limit = mysqli_real_escape_string($this->db_conn, $limit);
		// $where - ???
	
		$attr = $this->doc_read($id);
		$query = "SELECT * FROM tmp_".$attr['ListID']." WHERE ContID=".$attr['ContID'];
		if ( !empty($where) )
		$query .= " AND $where";
		if ( !empty($sort) )
		$query .= " ORDER BY $sort";
		if ( !empty($limit) )
		$query .= " LIMIT $limit";

		$result = $this->select($query);
		
		for ( $i = 0; $i < count($result); $i++ )
		{
			$tpl_rep = $tpl;

			foreach ( $result[$i] as $key => $val )
			$tpl_rep = str_replace('#'.$key.'#', $val, $tpl_rep);

			// Счетчик
			$tpl_rep = str_replace('#counter#', $i, $tpl_rep);

			$tpl_res .= $tpl_rep;
		}
		
		return $tpl_res;
	}

	// ВНИМАНИЕ! НЕОБХОДИМО ФОРМАТИРОВАНИЕ ПАРАМЕТРОВ $sql и $where на стороне шаблона!!!
	// Потенциально небезопасная функция!
	public function mod_write($id, $sql, $where = "")
	{
		$id = intval($id);
		// $sql, where - ???
		
		$attr = $this->doc_read($id);
		if ( !empty($where) )
		$this->execute("UPDATE tmp_".$attr['ListID']." SET $sql WHERE ContID=$id AND $where");
		else
		{
			$max_tmpID = $this->select_line("SELECT MAX(tmpID) tmpID FROM tmp_".$attr['ListID']);
			$max_tmpID['tmpID']++;
			$this->execute("INSERT INTO tmp_".$attr['ListID']." VALUES (".$max_tmpID['tmpID'].", $id, $sql)");
		}

		if ( !empty($max_tmpID['tmpID']) )
		return $max_tmpID['tmpID'];
	}
	
	// Добавление и редактирование записей в модуле
	public function mod_update($id, $mod_data)
	{
		$id = intval($id);

		$mod_struct_fields = array();
		$u_query = "";
		$u_query_fields = "";
		$file_cnt = 0;

		$mod_struct = $this->select("SELECT Title FROM tListField WHERE ListID=".intval($_REQUEST['ListID']));
		$mod_id = $this->select_line("SELECT ListID FROM tCont WHERE ContID=$id");
		if ( empty($mod_data['tmpID']) )
		{
			$max_tmpID = $this->select_line("SELECT MAX(tmpID) tmpID FROM tmp_".$mod_id['ListID']);
			$max_tmpID['tmpID']++;
		}
		
		for ( $i = 0; $i < count($mod_struct); $i++ )
		$mod_struct_fields[] = $mod_struct[$i]['Title'];

		//print_r($mod_data);
	
		foreach($mod_data as $key => $value)
		{
			// Обработка загрузки файлов
			if ( is_array($value) )
			{
				//print_r($value);
				if ( empty($mod_data['tmpID']) )
				$file_tmpID = $max_tmpID['tmpID'];
				else
				$file_tmpID = $mod_data['tmpID'];
				$file_ext = pathinfo($value['name'], PATHINFO_EXTENSION);
				$upload_filename = $id."_".$mod_id['ListID']."_".$file_tmpID."_".$file_cnt.".".$file_ext;
				$real_upload_filename = "../sfiles/lists/".$upload_filename;

				if (move_uploaded_file($value['tmp_name'], $real_upload_filename))
				{
					$value = $upload_filename;
					chmod($real_upload_filename, 0644);
				}
				else
				$value = "";

				$file_cnt++;
			}
			
			if ( in_array($key, $mod_struct_fields) )
			{
				$key = mysqli_real_escape_string($this->db_conn, $key);
				$value = mysqli_real_escape_string($this->db_conn, $value);
				
				if ( !empty($mod_data['tmpID']) )
				$u_query .= "$key = '$value',";
				else
				{
					$u_query_fields .= "$key,";
					$u_query .= "'$value',";
				}
			}
		}

		if ( !empty($mod_data['tmpID']) )
		$u_query = "UPDATE tmp_".$mod_id['ListID']." SET ".rtrim($u_query, ",")." WHERE tmpID=".$mod_data['tmpID'];
		else
		$u_query = "INSERT INTO tmp_".$mod_id['ListID']." (tmpID, ContID, ".rtrim($u_query_fields, ",").") VALUES (".$max_tmpID['tmpID'].",$id,".rtrim($u_query, ",").")";

		$this->execute($u_query);
	}
	
	// Удаление записи в модуле
	public function mod_delete($id, $tmpID)
	{
		$id = intval($id);
		$tmpID = intval($tmpID);
		
		$mod_id = $this->select_line("SELECT ListID FROM tCont WHERE ContID=$id");
		$this->execute("DELETE FROM tmp_".$mod_id['ListID']." WHERE tmpID=$tmpID");
		
		// Удаление связанных с записью файлов
		$del_wildcard = "../sfiles/lists/".$id."_".$mod_id['ListID']."_".$tmpID."_*";
		foreach (glob("$del_wildcard") as $del_filename)
		unlink($del_filename);

	}
		
	// Получение идентификатора (ContID) по символьной "Метке" документа, описанной пользователем в системе управления
	public function get_id($label)
	{
		$label = mysqli_real_escape_string($this->db_conn, $label);
		$result = $this->select_line("SELECT ContID FROM tCont WHERE Cont_Label='$label' LIMIT 1");
		return $result['ContID'];
	}

	// Вывод карты сайта
	public function site_map($level, $current = 0, $inc = 0)
	{
		$level = intval($level);
		
		$result = $this->select("SELECT a.ContID, b.ParentID AS parent, a.Title FROM tCont a, tContExt b WHERE a.ContID=b.ContID AND b.ParentID=$level ORDER BY a.Priority");
		
		for ( $i = 0; $i < count($result); $i++ )
		{
			$inc++;

			if ( !empty($current) && $current == $result[$i]['ContID'])
			$selected = " selected=\"selected\""; else $selected = "";
			echo "<option value=\"".$result[$i]['ContID']."\"$selected>";
				for ( $j = 0; $j < $inc; $j++ )
				echo "&nbsp;&nbsp;&nbsp;";
				echo $result[$i]['Title'];
			echo "</option>";
				
			$this->site_map($result[$i]['ContID'], $current, $inc);
			$inc--;
		}
	}

	// Метод для создания меню типа ul-li с раскрытой текущей веткой
	public function menu($id, $params = "")
	{
		$id = intval($id);
		
		$safe_cnt = 0;
		$old_id = $id;
		$func_res = "";
		$loc_res = "";

		$params_list = array();
		if ( !empty($params) )
		$params_list = $this->pars_params($params);
		else
		$params_list['stop'] = 0;
		
		while ( $id != $params_list['stop'] )
		{
				$result = $this->select("SELECT b.ContID, b.Title, b.Mod_Rewrite, a.ParentID, b.Title_Menu FROM tContExt a, tCont b, tContExt c WHERE b.ContID=a.ContID AND a.ParentID=c.ParentID AND c.ContID=$id AND b.Disabled=0 AND b.Hidden=0 ORDER BY b.Priority");
				$id = $result[0]['ParentID'];
				if ( count($result) > 0 )
				{
					$func_res = "<ul>";
					for ( $i = 0; $i < count($result); $i++ )
					{
						if ( !empty($result[$i]['Title_Menu']) )
						$result[$i]['Title'] = $result[$i]['Title_Menu'];

						if ( $result[$i]['ContID'] == $old_id )
						$func_res .= "<li><a href=\"".$result[$i]['Mod_Rewrite']."\" class=\"current\">".$result[$i]['Title']."</a>".$loc_res."</li>";
						else
						$func_res .= "<li><a href=\"".$result[$i]['Mod_Rewrite']."\">".$result[$i]['Title']."</a></li>";
					}
					$func_res .= "</ul>";
			
					$loc_res = $func_res;
					$old_id = $id;
				}
				
				// Защита от зацикливания, в случае проблем с БД
				$safe_cnt++;
				if ( $safe_cnt > 1000 )
				{
					$id = 0;
					throw new Exception("CMS: find_ptree() loop problem!");
				}
		}

		$func_res = preg_replace("/^<ul>|<\/ul>$/", "", $func_res);
		
		return $func_res;
	}
	

	public $tCont_fields = array("ContID", "TemplateID", "ListID", "CType", "DType", "Title", "Title_Menu",
						"Title_URL", "Title_Pic", "Title_Pic_Alt", "Man_Date", "Author", "Cont_Date",
						"Status", "Keywords", "Descr", "Rewrite", "Mod_Rewrite", "URL", "Head", "Priority",
						"Disabled", "Hidden", "Notes", "Cont_Text", "Reserved_Text", "Reserved_Int",
						"Target", "GTitle_Type", "GTitle_Size", "GTitle_Font", "GTitle_Color",
						"GTitle_BG", "Stat_Type", "Stat_Hits", "Cont_Label", "Cat_Sort", "Sort_Order",
						"Style_Type", "Style_Val", "No_Rewrite", "Dev" );
	
	// Создание документа
	public function create_doc($doc_fields, $parent = 0)
	{
		$result = $this->select_line("SELECT MAX(ContID) AS ContID FROM tCont");
		$id = $result['ContID'] + 1;
		$fields = "";
		$values = "";		
		for ( $i = 1; $i < count($this->tCont_fields); $i++ )
		{
			$fields .= ", ".$this->tCont_fields[$i];
			$values .= ", ";
			if (isset($doc_fields[$this->tCont_fields[$i]]) && !empty($doc_fields[$this->tCont_fields[$i]]) )
			$values .= "'".mysqli_real_escape_string($this->db_conn, $doc_fields[$this->tCont_fields[$i]])."'";
			elseif ( $this->tCont_fields[$i] == "Man_Date" || $this->tCont_fields[$i] == "Cont_Date" )
			$values .= "SYSDATE()"; 
			else
			$values .= "''";
		}
		$this->execute("INSERT INTO tCont (ContID $fields) VALUES($id $values)");
		$this->execute("INSERT INTO tContExt (ContID, ParentID) VALUES($id, ".intval($parent).")");
		
		// Сохранение дополнительных фото
		if ( !empty($this->conf['Photo']) )
		{
			for ( $i = 1; $i <= $this->conf['Photo']; $i++ )
			$this->execute("INSERT INTO tPhoto (PhotoID, ContID, Photo, Alt) VALUES($i, $id, '".mysqli_real_escape_string($this->db_conn,$_REQUEST['Photo'.$i])."', '".mysqli_real_escape_string($this->db_conn, $_REQUEST['Alt'.$i])."')");
		}

		// Сохранение дополнительных полей
		$result = $this->select("SELECT ContFieldID, Title FROM tContField WHERE Disabled=0");
		for ( $i = 0; $i < count($result); $i++ )
		{
			if ( isset($_REQUEST[$result[$i]['Title']]) && !empty($_REQUEST[$result[$i]['Title']]) )
			$this->execute("INSERT INTO tContFieldExt (ContID, ContFieldID, Value) VALUES ($id, ".$result[$i]['ContFieldID'].", '".mysqli_real_escape_string($this->db_conn, $_REQUEST[$result[$i]['Title']])."')");
		}
		
		return $id;
	}

	// Редактирование документа
	public function update_doc($id, $doc_fields, $parent = 0)
	{
		$id = intval($id);
		
		$query = "";
		for ( $i = 1; $i < count($this->tCont_fields); $i++ )
		{
			if (isset($doc_fields[$this->tCont_fields[$i]]) )
			$query .= $this->tCont_fields[$i]."='".mysqli_real_escape_string($this->db_conn, $doc_fields[$this->tCont_fields[$i]])."', ";
		}
		$query = rtrim($query, " ,");
		//echo $query;
		$this->execute("UPDATE tCont SET $query WHERE ContID=$id");
		$this->execute("UPDATE tContExt SET ParentID=$parent WHERE ContID=$id");
		//$this->execute("DELETE FROM tContExt WHERE ContID=$id");
		//$this->execute("INSERT INTO tContExt (ContID, ParentID) VALUES($id, $parent)");
		
		// Сохранение дополнительных фото
		if ( !empty($this->conf['Photo']) )
		{
			$this->execute("DELETE FROM tPhoto WHERE ContID=$id");
			for ( $i = 1; $i <= $this->conf['Photo']; $i++ )
			$this->execute("INSERT INTO tPhoto (PhotoID, ContID, Photo, Alt) VALUES($i, $id, '".mysqli_real_escape_string($this->db_conn, $_REQUEST['Photo'.$i])."', '".mysqli_real_escape_string($this->db_conn, $_REQUEST['Alt'.$i])."')");
		}
		// Сохранение дополнительных полей
		$result = $this->select("SELECT ContFieldID, Title FROM tContField WHERE Disabled=0");
		for ( $i = 0; $i < count($result); $i++ )
		{
			if ( isset($_REQUEST[$result[$i]['Title']]) )
			{
				$this->execute("DELETE FROM tContFieldExt WHERE ContID=$id AND ContFieldID=".$result[$i]['ContFieldID']);
				if ( !empty($_REQUEST[$result[$i]['Title']]) )
				$this->execute("INSERT INTO tContFieldExt (ContID, ContFieldID, Value) VALUES ($id, ".$result[$i]['ContFieldID'].", '".mysqli_real_escape_string($this->db_conn, $_REQUEST[$result[$i]['Title']])."')");
			}
		}


	}
	
	// Удаление документа
	public function delete_doc($id)
	{
		$id = intval($id);
		
		$doc_attr = $this->doc_read($id);
		$result = $this->select_row("SHOW TABLES LIKE 'tmp_".$doc_attr['ListID']."'");
		if ( isset($result[0][0]) )
		$this->execute("DELETE FROM tmp_".$doc_attr['ListID']." WHERE ContID=$id");
		$this->execute("DELETE FROM tContExt WHERE ContID=$id");
		$this->execute("DELETE FROM tPhoto WHERE ContID=$id");
		$this->execute("DELETE FROM tContFieldExt WHERE ContID=$id");
		$this->execute("DELETE FROM tCont WHERE ContID=$id");

		$del_wildcard = "../sfiles/lists/".$id."_*";
		foreach (glob("$del_wildcard") as $del_filename)
		unlink($del_filename);
		
	}
	
	public function replace_quotes(&$array, $dhtml_fields = array())
	{
		if ( is_array($array) )
		{
			foreach($array as $key => $elem)
			{
				$elem = stripslashes($elem);
				if ( !in_array($key, $dhtml_fields) )
				$elem = str_replace("\"", "&quot;", $elem);
				$elem = str_replace("'", "&#039;", $elem);
				$array[$key] = $elem;
			}
		}
	}
	
	// Парсинг параметров функций
	public function pars_params($params)
	{
		$params = str_replace(" ", "", $params);
		$params_pairs = array();
		if ( preg_match("/\,/", $params) )
		$params_pairs = explode(",", $params);
		else
		$params_pairs[] = $params;
		
		for ( $i = 0; $i < count($params_pairs); $i++ )
		{
			list($params_var, $params_val) = explode(":", $params_pairs[$i]);
			$params_list[$params_var] = $params_val;
		}
		
		return $params_list;

	}
	
	// Вывод списка подчиненных разделу объектов
	public function cat_read($id, $tpl = "", $params = "")
	{
		$id = intval($id);
		$tpl_res = "";
		
		$params_list = array();
		if ( !empty($params) )
		$params_list = $this->pars_params($params);
		
		$result = $this->select("SELECT a.* FROM tCont a, tContExt b
								WHERE a.ContID=b.ContID AND b.ParentID=$id AND a.Hidden=0
								ORDER BY a.Priority, a.ContID");
		
		for ( $i = 0; $i < count($result); $i++ )
		{
			$tpl_rep = $tpl;

			foreach ( $result[$i] as $key => $val )
			{
				if ( $key == "Mod_Rewrite" )
				{
					$search_key = "#url#";
					if ( preg_match("/$search_key/", $tpl_rep) )
					{
						if ( $this->conf['Mod_Rewrite'] == 1 )
						$rep_val = $val;
						else
						$rep_val = "/page.php?ID=".$result[$i]['ContID'];
					}
				}
				else if ( $key == "Title" )
				{
					$search_key = "#Title_Auto#";
					if ( preg_match("/$search_key/", $tpl_rep) )
					{
						if ( !empty($result[$i]['Title_Menu']) )
						$rep_val = $result[$i]['Title_Menu'];
						else
						$rep_val = $val;
					}
					$tpl_rep = str_replace($search_key, $rep_val, $tpl_rep);

					$search_key = "#$key#";
					$rep_val = $val;

				}
				else
				{
					$search_key = "#$key#";
					$rep_val = $val;
				}
				
				$tpl_rep = str_replace($search_key, $rep_val, $tpl_rep);
				
			}
			
			// Выделение текущего элемента
			if ( isset($params_list['current']) )
			{
				$search_key = "\s*#current#";
				if ( $result[$i]['ContID'] == $params_list['current'] )
				$tpl_rep = preg_replace("/$search_key/", " class=\"current\"", $tpl_rep);
				else
				$tpl_rep = preg_replace("/$search_key/", "", $tpl_rep);
			}
			
			$tpl_res .= $tpl_rep;
		}
		
		return $tpl_res;
	}
	
	// Выдача актуальной для текущей страницы картинки (из Доп. фото)
	public function show_pic($id)
	{
		$id = intval($id);
		$pic = array();
		while ( $id )
		{
			$result = $this->select_line("SELECT a.ContID, b.ParentID FROM tCont a, tContExt b WHERE a.ContID=b.ContID AND a.ContID=$id LIMIT 1");
			for ( $i = 0; $i < count($result); $i++ )
			{
				$cnt = 0;
				$id = $result['ParentID'];
				$result2 = $this->select("SELECT PhotoID, Photo, Alt FROM tPhoto WHERE ContID=".$result['ContID']." ORDER BY PhotoID");
				for ( $j = 0; $j < count($result2); $j++ )
				{
					if ( empty($pic[$cnt][0]) && !empty($result2[$j]['Photo']) )
					{
						$pic[$cnt][0] = $result2[$j]['Photo'];
						$pic[$cnt][1] = $result2[$j]['Alt'];
					}
					$cnt++;
				}
			}
		}
		return $pic;
	}
	
	// Транслит
	public function translit($string)
	{
		$replace=array(
			"'"=>"", "`"=>"","а"=>"a","А"=>"a","б"=>"b","Б"=>"b","в"=>"v","В"=>"v","г"=>"g","Г"=>"g","д"=>"d","Д"=>"d","е"=>"e","Е"=>"e",
			"ж"=>"zh","Ж"=>"zh","з"=>"z","З"=>"z","и"=>"i","И"=>"i","й"=>"y","Й"=>"y","к"=>"k","К"=>"k","л"=>"l","Л"=>"l","м"=>"m","М"=>"m",
			"н"=>"n","Н"=>"n","о"=>"o","О"=>"o","п"=>"p","П"=>"p","р"=>"r","Р"=>"r","с"=>"s","С"=>"s","т"=>"t","Т"=>"t","у"=>"u","У"=>"u",
			"ф"=>"f","Ф"=>"f","х"=>"h","Х"=>"h","ц"=>"c","Ц"=>"c","ч"=>"ch","Ч"=>"ch","ш"=>"sh","Ш"=>"sh","щ"=>"sch","Щ"=>"sch",
			"ъ"=>"","Ъ"=>"","ы"=>"y","Ы"=>"y","ь"=>"","Ь"=>"","э"=>"e","Э"=>"e","ю"=>"yu","Ю"=>"yu","я"=>"ya","Я"=>"ya","і"=>"i","І"=>"i",
			"ї"=>"yi","Ї"=>"yi","є"=>"e","Є"=>"e"
		);
		return $str=iconv("UTF-8","UTF-8//IGNORE",strtr($string,$replace));
	}
	
	public function search($q)
	{
		$where = "";
		$s_result = "";
		
		$q = explode(" ", preg_replace("/\s+/", " ", str_replace("*", "", $q)));
		for ( $i = 0; $i < count($q); $i++ )
		{
			$q[$i] = mysqli_real_escape_string($this->db_conn, $q[$i]);
			$where .= " (UPPER(Title) LIKE UPPER('%$q[$i]%') OR UPPER(Cont_Text) LIKE UPPER('%$q[$i]%') OR UPPER(Title_Menu) LIKE UPPER('%$q[$i]%')) AND ";
		}
		$where = substr($where, 0, strlen($where)-4);
		
		$result = $this->select("SELECT ContID, Title, Mod_Rewrite FROM tCont WHERE Disabled=0 AND Status=0 AND $where ORDER BY Priority, Title");
		for ( $i = 0; $i < count($result); $i++ )
		{
			$counter = $i + 1;
			$s_result .= "$counter. <a href=\"".$result[$i]['Mod_Rewrite']."\">".$result[$i]['Title']."</a><br><br>";
		}
		
		return $s_result;
	}
	
	public function email($subject, $message, $params = "")
	{
		$subject = "=?UTF-8?b?".base64_encode($subject)."?=";
		$domain = $this->conf['URL_Site'];
		$from = "mail@".$domain;
		
		$headers = "Return-Path: <admin@".$domain.">\r\nFrom: <$from>\r\n";
		$headers .= "MIME-Version: 1.0\r\n";
		$headers .= "Content-Type: text/plain; charset=UTF-8; format=flowed\r\n";
		$headers .= "Content-Transfer-Encoding: 8 bit\r\n";
		$headers .= "X-Mailer: PHP script\r\n";

		mail($this->conf['Email_user'], $subject, $message, $headers, "-f $from");
	}
	
}

class cms extends cms_lib
{
	public $attr; // Атрибуты текущего документа
	public $ptree; // Дерево родителей документа
	public $level; // Уровень, на котором документ находится в структуре
	public $id; // Текущий документ
	public $imode; // Режим центральной страницы
	
	public function __construct($id = 0)
	{
		parent::__construct();

		if ( empty($id) )
		{
			$this->id = $this->doc_find();
			$this->imode = 1;
		}
		else
		$this->id = intval($id);
		
		$this->attr = $this->doc_read($this->id);

		// Рекурсивный поиск документа в дереве.
		// Предполагается [временно!], что раздел является только элементом структуры
		//if ( $this->attr['CType'] == 0 && empty($this->attr['Cont_Text']) )
		if ( $this->attr['CType'] == 0 )
		{
			$id_parent = $this->attr['ContID'];
			while ( true )
			{
				$result = $this->select_line("	SELECT a.ContID, a.CType, a.Cont_Text FROM tCont a, tContExt b
												WHERE a.ContID=b.ContID AND b.ParentID=$id_parent
												AND a.Disabled=0 AND a.Status=0 AND a.CType<>2
												ORDER BY a.Priority, a.ContID
												LIMIT 1");
				if ( $result['ContID'] == $id_parent )
				break;
				// Если добрались до документа или в разделе есть информация в DHTML-поле, то считаем элемент текущим 
				elseif ( $result['CType'] == 1 || ($result['CType'] == 0 && !empty($result['Cont_Text'])) )
				{
					$this->id = $result['ContID'];
					$this->attr = $this->doc_read($this->id);
					break;
				}
				else
				$id_parent = $result['ContID'];
			}
		}
		
		$this->ptree = $this->ptree_find($this->id);
		$this->level = count($this->ptree);

	}
	
	// Получение шаблона страницы
	public function get_template($template_id = 0)
	{
		if ( empty($template_id) )
		$template_id = $this->attr['TemplateID'];
		$result = $this->select_line("SELECT Filename FROM tTemplate WHERE TemplateID=$template_id LIMIT 1");
		return $result['Filename'];
	}
	
	public function get_keywords()
	{
		$result = "";
		if ( !empty($this->attr['Keywords']) )
		$result .= "<meta name=keywords content=\"".$this->attr['Keywords']."\">";
		if ( !empty($this->attr['Descr']) )
		$result .= "<meta name=description content=\"".$this->attr['Descr']."\">";

		return $result;
	}
	
}

?>
