<?
/*
* Copyright (c) 2013, Andrey_B (Office-42)
* Подробнее см. LICENSE.txt или http://www.gnu.org/licenses/
*/
?>
<script type="text/javascript" src="ckeditor/ckeditor.js"></script>

<script type="text/javascript">
$(document).ready(
function()
{ $('.datepicker').datepicker() })
</script>

<?
if ( !empty($_REQUEST['action']) )
{
	if ( $_REQUEST['action'] == 1 )
	{
		// Преобразуем Status_Main в Hidden, Disabled и Status
		// ==============================
		if ( $_REQUEST['Status_Main'] == "Hidden" )
		$_REQUEST['Hidden'] = 1;
		else
		$_REQUEST['Hidden'] = 0;

		if ( $_REQUEST['Status_Main'] == "Disabled" )
		$_REQUEST['Disabled'] = 1;
		else
		$_REQUEST['Disabled'] = 0;

		if ( $_REQUEST['Status_Main'] == "Status" )
		$_REQUEST['Status'] = 1;
		else
		$_REQUEST['Status'] = 0;
		// ==============================
		
		if ( empty($_REQUEST['Rewrite']) )
		$_REQUEST['Rewrite'] = preg_replace("/[^\-a-z_0-9]+/", "", $o42->translit(strtolower(preg_replace("/\s+/", "-", $_REQUEST['Title']))));
		else
		$_REQUEST['Rewrite'] = preg_replace("/[^\-a-z_0-9]+/", "", $o42->translit(strtolower(preg_replace("/\s+/", "-", $_REQUEST['Rewrite']))));
		
		// Проверка уникальности Mod_Rewrite
		if ( !empty($_REQUEST['id']) )
		$old_Rewrite = $o42->select_line("SELECT Rewrite FROM tCont WHERE ContID=".$_REQUEST['id']);
		if ( empty($_REQUEST['id']) || $old_Rewrite['Rewrite'] != $_REQUEST['Rewrite'] )
		{
			$Rewrite_unique = $_REQUEST['Rewrite'];
			$Rewrite_cnt = 0;
			while ( true )
			{
				$chek_rewrite = array();
				$check_rewrite = $o42->select_line("SELECT a.ContID FROM tCont a, tContExt b WHERE a.ContID=b.ContID AND b.ParentID=".$_REQUEST['parent']." AND a.Rewrite='$Rewrite_unique'");
				if ( !empty($check_rewrite) )
				{
					$Rewrite_cnt++;
					$Rewrite_unique = $_REQUEST['Rewrite'].$Rewrite_cnt;
				}
				else
				break;
			}

			if ( $Rewrite_cnt > 0 )
			$_REQUEST['Rewrite'] = $Rewrite_unique;
			
		}
		
		// <p>&nbsp;</p> Killer
		$_REQUEST['Cont_Text'] = preg_replace("/^<p>(\r\n)*\t*&nbsp;<\/p>(\r\n)*/", "", $_REQUEST['Cont_Text']);
		$_REQUEST['Notes'] = preg_replace("/^<p>(\r\n)*\t*&nbsp;<\/p>(\r\n)*/", "", $_REQUEST['Notes']);

		for ( $i = 0; $i < count($o42->tCont_fields); $i++ )
		{
			if (isset($_REQUEST[$o42->tCont_fields[$i]]) )
			$doc_fields[$o42->tCont_fields[$i]] = $_REQUEST[$o42->tCont_fields[$i]];
		}

		if ( !empty($_REQUEST['id']) )
		$o42->update_doc($_REQUEST['id'], $doc_fields, $_REQUEST['parent']);
		else
		{
			//$doc_fields['CType'] = 1;
			$_REQUEST['id'] = $o42->create_doc($doc_fields, $_REQUEST['parent']);
		}

	}
	elseif ( $_REQUEST['action'] == 2 && !empty($_REQUEST['id']) )
	{
		$o42->delete_doc($_REQUEST['id']);
		echo "Документ удален!";
		return 0;
	}
	
	if ( !empty($o42->conf['Mod_Rewrite']) )
	$o42->write_rewrite();
}
if ( !empty($_REQUEST['id']) )
$doc_fields = $o42->doc_read($_REQUEST['id']);
?>

<? if ( !empty($o42->conf['Photo']) ) { ?>

	<script type="text/javascript" src="ckeditor/ckfinder/ckfinder.js"></script>
	<script>
		function browserServer1(inputId)
		{
			var finder = new CKFinder();
			finder.BasePath = 'ckeditor/ckfinder/';
			finder.selectActionFunction = SetFileField;
			finder.selectActionData = inputId;
			finder.popup() ;
		}

		function SetFileField( fileUrl, data )
		{
			document.getElementById( data["selectActionData"] ).value = fileUrl;
		}
	</script>

<? } ?>

<?
if ( isset($doc_fields) && $doc_fields['CType'] == 0 )
echo "<h3>Редактирование раздела</h3>";
elseif ( isset($doc_fields) && $doc_fields['CType'] == 1 )
echo "<h3>Редактирование документа</h3>";
elseif ( $_REQUEST['p'] == "doc" )
echo "<h3>Создание документа</h3>";
elseif ( $_REQUEST['p'] == "cat" )
echo "<h3>Создание раздела</h3>";
?>

<ul class="nav nav-tabs">
	<li class="active"><a href="#home" data-toggle="tab">Содержание</a></li>
  	<li><a href="#properties" data-toggle="tab">Свойства</a></li>
	<?
	if ( !empty($_REQUEST['id']) && isset($doc_fields) && $doc_fields['CType'] != 0 )
	{
		if ( !empty($doc_fields['ListID']) )
		{
			$list_prop = $o42->select_line("SELECT Title FROM tList WHERE ListID=".$doc_fields['ListID']." LIMIT 1");
			echo "<li><a href=\"index.php?p=mods&id=".$_REQUEST['id']."&ListID=".$doc_fields['ListID']."\">".$list_prop['Title']."</a></li>";
		}
	}
	?>
</ul>

<form class="form-horizontal" action="index.php" name="doc" method="POST">
	<input type="hidden" name="p" value="doc">
	<input type="hidden" name="action" value="1">
	
	<?
	if ( isset($doc_fields) )
	$doc_ctype = $doc_fields['CType'];
	elseif ( $_REQUEST['p'] == 'doc' )
	$doc_ctype = 1;
	else
	$doc_ctype = 0;
	echo "<input type=\"hidden\" name=\"CType\" value=\"$doc_ctype\">";
	
	if ( !empty($_REQUEST['id']) )
	echo "<input type=\"hidden\" name=\"id\" value=\"".$_REQUEST['id']."\">";
	?>

	<div id="myTabContent" class="tab-content">
		<div class="tab-pane fade active in" id="home">
			<div class="row-fluid">
				<div class="span6">
					<div class="control-group">
						<label class="control-label" for="ParentID">Раздел</label>
						<div class="controls">
							<select class="input-xlarge" id="ParentID" name="parent">
								<option value="0">Основной</option>
								<?
								if ( !empty($_REQUEST['id']) && isset($doc_fields) )
								{
									$doc_parent = $o42->select_line("SELECT ParentID FROM tContExt WHERE ContID=".$_REQUEST['id']." LIMIT 1");
									$o42->site_map(0, $doc_parent['ParentID']);
								}
								elseif ( !empty($_REQUEST['parent']) )
								$o42->site_map(0, $_REQUEST['parent']);
								else
								$o42->site_map(0);
								?>
							</select>
	             </div>
						</div>
						<div class="control-group error">
							<label class="control-label" for="Title">Заголовок</label>
							<div class="controls"><input required type="text" class="input-xlarge" name="Title" id="Title" autofocus value="<? if (isset($doc_fields)) echo $doc_fields['Title']; ?>"></div>
						</div>
					</div>
					<div class="span6">
						<div class="control-group">
							<label class="control-label" for="sort">Приоритет</label>
							<?
							$Priority_value = "";
							if ( !empty($_GET['parent']) )
							{
								$result = $o42->select_line("SELECT MAX(a.Priority) + 10 AS Priority FROM tCont a, tContExt b WHERE a.ContID=b.ContID AND b.ParentID=".$_GET['parent']);
								$Priority_value = ceil($result['Priority'] / 10) * 10;
							}
							elseif (isset($doc_fields)) 
							$Priority_value = round($doc_fields['Priority']);
							?>
							<div class="controls"><input type="number" class="span1" id="sort" name="Priority" value="<? echo $Priority_value; ?>"></div>
						</div>

						<div class="control-group">
							<label class="control-label" for="url-name">Заголовок для url</label>
							<div class="controls">
								<input type="text" class="input-xlarge" id="url-name" name="Rewrite" value="<? if (isset($doc_fields)) echo $doc_fields['Rewrite']; ?>">
								<span class="help-inline"><sup>site.ru/home/<strong>about</strong></sup></span>
							</div>
						</div>
					</div>
				</div>
				<h6>Содержание</h6>
				<textarea style="height:300px; width:99%" name="Cont_Text"><? if (isset($doc_fields)) echo htmlspecialchars($doc_fields['Cont_Text']); ?></textarea>
				<script type="text/javascript">CKEDITOR.replace( 'Cont_Text', { height : 400 } );</script>

				<div class="accordion" id="accordion">
					<div class="accordion-group">
						<div class="accordion-heading">
							<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseOne"><h6><i class="icon-tag"></i> Добавить анонс</h6></a>
						</div>
						<div id="collapseOne" class="accordion-body collapse" style="height: 0px; ">
							<div class="accordion-inner">
								<textarea style="height:100px; width:99%" name="Notes"><? if (isset($doc_fields)) echo htmlspecialchars($doc_fields['Notes']); ?></textarea>
								<script type="text/javascript">
									CKEDITOR.replace( 'Notes', { height : 100, toolbarStartupExpanded : false,
									toolbar_Full : 
									[
										['Source'],
										['Cut','Copy','Paste','PasteText','PasteFromWord','-','SpellChecker', 'Scayt'],
										['Undo','Redo','-','SelectAll','RemoveFormat'],
										['Link','Unlink','Anchor'],
										['Image','Flash','Table'],
										['Bold','Italic','Underline','Strike','-','Subscript','Superscript'],
										['NumberedList','BulletedList','-','Outdent','Indent','Blockquote'],
										['JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock'],
										'/',
										['Styles','Format','Font','FontSize'],
										['TextColor','BGColor'],
									]
									} );
								</script>
							</div>
						</div>
					</div>
				</div>
			</div>




			<div class="tab-pane fade" id="properties">
				<div class="row-fluid">
					<div class="span6">

						<div class="control-group">
							<label class="control-label" for="Man_Date">Дата</label>
							<div class="controls"><input type="datetime-local" class="input-xlarge datepicker" data-date-format="yyyy-mm-dd" name="Man_Date" id="Man_Date" value="<? if (isset($doc_fields)) echo $doc_fields['Man_Date']; ?>"></div>
						</div>

						<? // if ( ( isset($doc_fields) && $doc_fields['CType'] != 0 ) || $_REQUEST['p'] == "doc" ) { ?>
							<div class="control-group">
								<label class="control-label" for="ListID">Модуль</label>
								<div class="controls">
									<select name="ListID" class="input-xlarge" id="ListID">
										<?
										$result = $o42->select("SELECT ListID, Title FROM tList ORDER BY ListID");
										for ( $i = 0; $i < count($result); $i++ )
										{
											if ( isset($doc_fields) )
											if ( $doc_fields['ListID'] == $result[$i]['ListID'] )
											$selected = " selected=\"selected\""; else $selected = "";
											echo "<option value=\"".$result[$i]['ListID']."\"$selected>".$result[$i]['Title']."</option>";
										}
										?>
									</select>
								</div>
							</div>
						<? //} ?>
						
						<div class="control-group">
							<label class="control-label" for="view">Отображение</label>
							<div class="controls">
							<select name="Status_Main" class="input-xlarge" id="view">
								<?
								if ( isset($doc_fields) ) if ( empty($doc_fields['Status']) && empty($doc_fields['Hidden']) ) $selected = " selected=\"selected\""; else $selected = "";
								echo "<option value=\"\"$selected>выводить в меню</option>";
								if ( isset($doc_fields) ) if ( $doc_fields['Hidden'] == 1 ) $selected = " selected=\"selected\""; else $selected = "";
								echo "<option value=\"Hidden\"$selected>не выводить в меню, доступ по ссылке</option>";
								if ( isset($doc_fields) ) if ( $doc_fields['Status'] == 1 ) $selected = " selected=\"selected\""; else $selected = "";
								echo "<option value=\"Status\"$selected>не выводить в меню, доступ запрещен</option>";
								?>
						    </select>
						  </div>
						</div>

						<div class="control-group warning">
							<label class="control-label" for="srv-name">Метка</label>
							<div class="controls"><input type="text" class="input-xlarge" id="srv-name" name="Cont_Label" value="<? if (isset($doc_fields)) echo $doc_fields['Cont_Label']; ?>"></div>
						</div>
						
						<div class="control-group">
							<label class="control-label" for="Style_Val">Стиль</label>
							<div class="controls">
							<select name="Style_Type" class="span1" id="Style_Val">
								<?
								if ( isset($doc_fields) ) if ( $doc_fields['Style_Type'] == "class" ) $selected = " selected=\"selected\""; else $selected = "";
								echo "<option value=\"class\"$selected>class</option>";
								if ( isset($doc_fields) ) if ( $doc_fields['Style_Type'] == "id" ) $selected = " selected=\"selected\""; else $selected = "";
								echo "<option value=\"id\"$selected>id</option>";
								?>
						    </select>
							<input name="Style_Val" class="span2" value="<? if (isset($doc_fields)) echo $doc_fields['Style_Val']; ?>" type="text">
						  </div>
						</div>
						
						<div class="control-group">
							<label class="control-label" for="URL">Переадресация</label>
							<div class="controls">
								<input type="text" class="span1" id="URL" value="<? if (isset($doc_fields)) echo $doc_fields['URL']; ?>">
								<span class="help-inline"><a href="link2.php">выбрать</a> (target: <input name="Target" class="span1" type="text" value="" /> )</span>
							</div>
						</div>

						<div class="control-group">
							<label class="control-label" for="TemplateID">Шаблон</label>
							<div class="controls">
							<select name="TemplateID" class="input-xlarge" id="TemplateID">
								<?
								$result = $o42->select("SELECT TemplateID, Title FROM tTemplate ORDER BY TemplateID");
								for ( $i = 0; $i < count($result); $i++ )
								{
									if ( $result[$i]['TemplateID'] == $doc_fields['TemplateID'] ) $selected = " selected=\"selected\""; else $selected = "";
									echo "<option value=\"".$result[$i]['TemplateID']."\"$selected>".$result[$i]['Title']."</option>";
								}
								?>
						    </select>
						  </div>
						</div>
						
						<?
						for ( $i = 1; $i <= $o42->conf['Photo']; $i++ )
						{
							echo "<div class=\"control-group\">";
								echo "<label class=\"control-label\" for=\"Photo$i\">Фото $i</label>";
								echo "<div class=\"controls\">";
									echo "<input type=\"text\" class=\"span1\" name=\"Photo$i\" id=\"txtURL$i\" value=\"";
										if ( isset($doc_fields['Photo'.$i]) )
										echo $doc_fields['Photo'.$i];
									echo "\">";
									echo "<span class=\"help-inline\"><a href=\"#\" onclick=\"browserServer1('txtURL$i');\">выбрать</a> (alt: <input name=\"Alt$i\" class=\"span1\" type=\"text\" value=\"";
										if ( isset($doc_fields['Alt'.$i]) )
										echo $doc_fields['Alt'.$i];
									echo "\" /> )</span>";
								echo "</div>";
							echo "</div>";
						}
						?>

					</div>
					<div class="span6">

						<div class="control-group">
							<label class="control-label" for="name-menu">Заголовок для меню</label>
							<div class="controls"><input type="text" class="input-xlarge" id="name-menu" name="Title_Menu" value="<? if (isset($doc_fields)) echo $doc_fields['Title_Menu']; ?>"></div>
						</div>
						
						<div class="control-group">
							<label class="control-label" for="Title_URL">Заголовок окна</label>
							<div class="controls"><input type="text" name="Title_URL" class="input-xlarge" id="Title_URL" value="<? if (isset($doc_fields)) echo $doc_fields['Title_URL']; ?>"></div>
						</div>
						
						<div class="control-group">
							<label class="control-label" for="Keywords">Ключевые слова</label>
							<div class="controls"><input type="text" name="Keywords" class="input-xlarge" id="Keywords" value="<? if (isset($doc_fields)) echo $doc_fields['Keywords']; ?>"></div>
						</div>
						
						<div class="control-group">
							<label class="control-label" for="Description">Описание</label>
							<div class="controls"><input type="text" name="Descr" class="input-xlarge" id="Description" value="<? if (isset($doc_fields)) echo $doc_fields['Descr']; ?>"></div>
						</div>
						
						<div class="control-group">
							<label class="control-label" for="Head">HEAD</label>
							<div class="controls"><textarea class="input-xlarge" id="Head" rows="3"><? if (isset($doc_fields)) echo $doc_fields['Head']; ?></textarea></div>
						</div>
		
						<div class="control-group">
							<label class="control-label" for="Reserved_Text">дополнительное поле</label>
							<div class="controls"><input type="text" class="input-xlarge" id="Reserved_Text" value="<? if (isset($doc_fields)) echo $doc_fields['Reserved_Text']; ?>"></div>
						</div>
						
						<?
						if ( isset($_REQUEST['id']) )
						$o42->show_contfields(0, $_REQUEST['id']);
						else
						$o42->show_contfields(0);
						?>

					</div>
				</div>
			</div>

				<div class="form-actions">
					<div class="btn-group" style="float:left;">
						<a class="btn btn-primary" href="#" onclick="document.forms['doc'].submit()"><i class="icon-ok icon-white"></i> Сохранить</a>
						<a class="btn btn-primary dropdown-toggle" data-toggle="dropdown" href="#"><span class="caret"></span></a>
						<ul class="dropdown-menu">
							<li><a href="#"><i class="icon-file"></i> Сохранить и новый документ</a></li>
							<li><a href="#"><i class="icon-folder-open"></i> Сохранить и новый раздел</a></li>
						</ul>
					</div>

					<div class="btn-group" style="float:left;margin-left:50px;">
						<a class="btn btn-danger" href="#" onclick="document.forms['doc'].action.value='2';document.forms['doc'].submit()"><i class="icon-trash icon-white"></i> В корзину</a>
						<a class="btn btn-danger dropdown-toggle" data-toggle="dropdown" href="#"><span class="caret"></span></a>
						<ul class="dropdown-menu">
							<li><a data-toggle="modal" href="#deleteDoc"><i class="icon-trash"></i> Удалить навсегда</a></li>
						</ul>
					</div>
				</div>
			</form>


<div id="deleteDoc" class="modal hide fade in">
	<div class="modal-header">
		<h3>Удаление документа</h3>
	</div>
	<div class="modal-body">
		<p>Вы уверены, что хотите удалить документ навсегда, без возможности восстановления?</p>
	</div>
	<div class="modal-footer">
		<a href="#" class="btn" data-dismiss="modal">Отменить</a>
		<a href="#" class="btn btn-success"><i class="icon-trash icon-white"></i> Перенести в корзину</a>
		<a href="#" class="btn btn-danger"><i class="icon-trash icon-white"></i> Удалить документ</a>
	</div>
</div>
