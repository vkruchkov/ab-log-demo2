<?
/*
* Copyright (c) 2013, Andrey_B (Office-42)
* Подробнее см. LICENSE.txt или http://www.gnu.org/licenses/
*/

	if ( !isset($_REQUEST['p']) )
	$_REQUEST['p'] = "tree";
	
	if ( $_REQUEST['p'] != "add_link" ) { ?>
	<div class="navbar navbar-fixed-top">
		<div class="navbar-inner">
			<div class="container-fluid">
				<a class="brand" href="#"><? echo $o42->cms_title; ?></a>
				<div class="nav-collapse">
					<ul class="nav">
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">Создать <b class="caret"></b></a>
							<ul class="dropdown-menu">
								<li><a href="index.php?p=cat">Раздел</a></li>
								<li><a href="index.php?p=doc">Документ</a></li>
								<li class="divider"></li>
								<li><a href="#">Банер</a></li>
							</ul>
						</li>
            			<li><a href="index.php?p=tree">Структура</a></li>
						<li><a href="index.php?p=mods_list">Модули</a></li>
						<li><a href="ckfinder.php" target="_new">Файлы</a></li>
						<!--li><a href="#">Банеры</a></li-->
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">Дополнительно <b class="caret"></b></a>
							<ul class="dropdown-menu">
								<?
								if ( file_exists("plugins") )
								{
									$d = dir("plugins");
									while($entry = $d->read()) 
									$dir_list[] = $entry;
									sort($dir_list);

									for ( $i = 0; $i < count($dir_list); $i++ )
									{
										$entry = $dir_list[$i];	
										if ( $entry != "." && $entry != ".." && preg_match("/\.php/", $entry) )
										{
											echo "<li><a href=\"index.php?p=$entry\">$entry</a></li>";
										}
									}
								}

								?>
								<!--li><a href="#">Почтовые сообщения</a></li>
								<li><a href="#">Журнал</a></li>
								<li><a href="#">Корзина</a></li-->
							</ul>
						</li>
					</ul>
	
					<ul class="nav pull-right">
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">Настройки <b class="caret"></b></a>
							<ul class="dropdown-menu">
								<li><a href="index.php?p=conf">Общие</a></li>
								<li><a href="index.php?p=users">Пользователи</a></li>
								<li class="divider"></li>
								<!--li><a href="#">Шаблоны</a></li-->
								<li><a href="index.php?p=conf_mods">Модули</a></li>
								<li><a href="index.php?p=conf_cont_fields">Дополнительные поля</a></li>
							</ul>
						</li>
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown"><? echo $user_info['Name']; ?> <b class="caret"></b></a>
							<ul class="dropdown-menu">
							<li><a href="index.php?cadmin_out=1">Выход</a></li>
							</ul>
						</li>
					</ul>	
				</div><!--/.nav-collapse -->
			</div>
		</div>
	</div>
<? } ?>
	<div class="container-fluid">
		<?
		if ( !empty($_REQUEST['p']) )
		{
			if ( $_REQUEST['p'] == "doc" || $_REQUEST['p'] == "cat" )
			include("mods/mod_doc.php");
			elseif ( $_REQUEST['p'] == "tree" )
			include("mods/mod_tree.php");
			elseif ( $_REQUEST['p'] == "add_link" )
			include("mods/mod_link.php");
			elseif ( $_REQUEST['p'] == "mods" )
			include("mods/mod_mods.php");
			elseif ( $_REQUEST['p'] == "mods_rec" )
			include("mods/mod_mods_rec.php");
			elseif ( $_REQUEST['p'] == "mods_list" )
			include("mods/mod_mods_list.php");
			elseif ( $_REQUEST['p'] == "users" )
			include("mods/mod_users.php");
			elseif ( $_REQUEST['p'] == "conf" )
			include("mods/mod_conf.php");
			elseif ( $_REQUEST['p'] == "conf_mods" )
			include("mods/mod_conf_mods.php");
			elseif ( $_REQUEST['p'] == "conf_cont_fields" )
			include("mods/mod_conf_cont_fields.php");
			elseif ( $_REQUEST['p'] == "conf_mods_fields" )
			include("mods/mod_conf_mods_fields.php");
			elseif ( preg_match("/\.php/", $_REQUEST['p']) )
			require("plugins/".$_REQUEST['p']);

		}
		?>
	</div>

