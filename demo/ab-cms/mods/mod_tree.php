<?
/*
* Copyright (c) 2013, Andrey_B (Office-42)
* Подробнее см. LICENSE.txt или http://www.gnu.org/licenses/
*/
?>
<script type="text/javascript" src="libs/jquery.tree.min.js"></script>
<script type="text/javascript" src="libs/jquery.cookie.js"></script>
<script type="text/javascript" src="libs/jquery.tree.contextmenu.js"></script>
<script type="text/javascript" src="libs/jquery.tree.cookie.js"></script>

<?
include("libs/cadmin_tree.js");
include("temp/cadmin_msg.php");

$tree_list = array();

$result = $o42->select_row("SHOW TABLES");
for ( $i = 0; $i < count($result); $i++ )
{
	if ( preg_match("/tmp_/", $result[$i][0]) )
	{
		$tree_list_id = str_replace("tmp_", "", $result[$i][0]);

		$result2 = $o42->select("SELECT ContID FROM tmp_$tree_list_id GROUP BY ContID");
		for ( $j = 0; $j < count($result2); $j++ )
		array_push($tree_list, $result2[$j]['ContID']);
	}
}

function cadmin_tree($level = 0, $cat_sort = 0, $sort_order = 0, $limit = 0)
{
	global $o42;
	//global $U_ID;
	//global $U_Type;
	global $tree_list;
	global $cadmin_msg;

	$my_limit = "";
	$tree_class = "";
	$tree_rel = "";
	
	if ( $cat_sort == 1 )
	$order_by = "a.Title";
	elseif ( $cat_sort == 2 )
	$order_by = "a.Man_Date";
	else
	$order_by = "a.Priority";

	if ( $sort_order == 1 )
	$order_by .= " DESC ";

	if ( $limit > 0 )
	$my_limit = " LIMIT $limit ";

	$tree_res = "<ul>";

		unset($sql_add);
		//if ( $U_Type != 3 )
		$sql_add = " AND a.Dev<>2";

		$result = $o42->select("SELECT a.Title, a.ContID, b.ParentID, a.Descr, a.Priority, a.Rewrite, a.CType, a.No_Rewrite, a.Title_Menu, a.Cat_Sort, a.Sort_Order, a.Notes, a.Dev
								FROM tCont a, tContExt b
								WHERE a.ContID=b.ContID AND b.ParentID=$level AND a.CType<>2 $sql_add
								ORDER BY $order_by
								$my_limit");
		for ( $i = 0; $i < count($result); $i++ )
		{
			if ( $result[$i]['CType'] == 0 )
			$tree_rel = "";
			else
			{
				$tree_class = "";
				if ( in_array($result[$i]['ContID'], $tree_list) )
				{
					if ( $result[$i]['Dev'] == 1 )
					$tree_rel = " rel=\"adoch\"";
					elseif ( $result[$i]['Dev'] == 2 )
					$tree_rel = " rel=\"adocb\"";
					else
					$tree_rel = " rel=\"adoc\"";
				}
				else
				{
					if ( $result[$i]['Dev'] == 1 )
					$tree_rel = " rel=\"doch\"";
					elseif ( $result[$i]['Dev'] == 2 )
					$tree_rel = " rel=\"docb\"";
					else
					$tree_rel = " rel=\"doc\"";
				}
			}

			$tree_res .= "<li$tree_class$tree_rel id=\"".$result[$i]['ContID']."\">";

			$result[$i]['Title'] = strip_tags($result[$i]['Title']);
			if ( !empty($result[$i]['Notes']) && $result[$i]['CType'] == 0 )
			$result[$i]['Title'] .= " (<strong>".$cadmin_msg['form_tree_limit1']." ".$result[$i]['Notes']." ".$cadmin_msg['form_tree_limit2']."</strong>) ";

			if ( mb_strlen($result[$i]['Title'], "UTF-8") > 120 )
			$result[$i]['Title'] = mb_substr($result[$i]['Title'], 0, 120, "UTF-8")."...";

			$tree_res .= "<a href=\"#\"><ins>&nbsp;</ins>".$result[$i]['Title']."</a>";
			//if ( $result[6] == 0 )
			$tree_res .= cadmin_tree($result[$i]['ContID'], $result[$i]['Cat_Sort'], $result[$i]['Sort_Order'], $result[$i]['Notes']);
			$tree_res .= "</li>";

		}

	$tree_res .= "</ul>";
	$tree_res = str_replace("<ul></ul>", "", $tree_res);
	return $tree_res;
}


if ( $_REQUEST['p'] == "tree" && empty($cadmin_break) )
{

	// Заголовок
	echo "<h3>Структура сайта</h3>";

	echo "<div id=\"mid\">";

		echo "<ul id=\"qlinks\">";
	
			echo "<li>".$cadmin_msg['form_tree_site_map']."<ul>";
			echo "<li><a href=\"#\" onclick='$.tree.focused().open_all(\"#tree_root\");'>".$cadmin_msg['form_tree_open']."</a> | <a href=\"#\" onclick='$.tree.focused().close_all(\"#tree_root\");'>".$cadmin_msg['form_tree_close']."</a></li>";
			echo "</ul></li>";

			/*
			$result = $o42->select("SELECT ListID FROM tList WHERE Title='".$cadmin_msg['form_tree_bookmarks']."'";
			if(!($result=mysql_query($sql,$conn)))
			echo "<!-- #FA1X40n4Lu -->";
			else
			{
				while(($data=mysql_fetch_row($result)))
				$BookmarksID = $data[0];
			}

			if ( !empty($BookmarksID) )
			{
				echo "<li>".$cadmin_msg['form_tree_bookmarks']."<ul>";

				$sql="SELECT tmpID, bmark_title, bmark_link, bmark_target FROM tmp_$BookmarksID";
				if(!($result=mysql_query($sql,$conn)))
				echo "<!-- #8z3NV2IDbb -->";
				else
				{
					while(($data=mysql_fetch_row($result)))
					{
						if ( $data[3] == 1 ) $bmark_target = " target=\"_blank\""; else unset($bmark_target);
						echo "<li><a href=\"$data[2]\"".$bmark_target.">$data[1]</a></li>";
					}
				}
				echo "</ul></li>";
			}

			echo "<li>".$cadmin_msg['form_tree_last']."<ul>";
	
				$sql="SELECT ContID, Title FROM tCont WHERE CType=1 ORDER BY Cont_Date DESC LIMIT 10"; 
				if(!($result=mysql_query($sql,$conn)))
				echo "<!-- #E1ie2pGh8o -->";
				else
				{
					while(($data=mysql_fetch_row($result)))
					echo "<li><a href=\"index.php?p=doc&id=$data[0]\">$data[1]</a></li>";
				}
			
			echo "</ul></li>";
			*/

		echo "</ul>";

		echo "<div id=\"cadmin_tree\">";
			echo "<ul>";
				echo "<li id=\"tree_root\" rel=\"home\" class=\"open\"><ins>&nbsp;</ins>".$cadmin_msg['form_tree_root']."";
				echo cadmin_tree();
				echo "</li>";
			echo "</ul>";
		echo "</div>";
	echo "</div>";
}

?>