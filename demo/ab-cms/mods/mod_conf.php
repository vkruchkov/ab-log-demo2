<?
/*
* Copyright (c) 2013, Andrey_B (Office-42)
* Подробнее см. LICENSE.txt или http://www.gnu.org/licenses/
*/
?>
<h3>Общие настройки</h3>

	<?
	
	$table_struct = array(
		'ConfID' => array('Descr' => 'ConfID', 'Edit' => 0),
		'Title_Site' => array('Descr' => 'Название компании', 'Edit' => 1),
		'URL_Site' => array('Descr' => 'Адрес сайта', 'Edit' => 1),
		'Email_user' => array('Descr' => 'Email', 'Edit' => 1),
		'Mod_Rewrite' => array('Descr' => 'Mod_Rewrite', 'Edit' => 1, 'Select' => array(0 => 'Нет', 1 => 'Да') ),
		'Photo' => array('Descr' => 'Дополнительных фото', 'Edit' => 1),
	);

	$table_name = "tConf";
	$table_key = "ConfID";
	
	if ( !empty($_REQUEST['action']) )
	{
		if ( $_REQUEST['action'] == 1 )
		{
			$o42->table_update($table_name, $table_key, $table_struct, $_POST);
			$o42->conf['Mod_Rewrite'] = $_REQUEST['Mod_Rewrite'];
			$o42->write_rewrite();
		}
	}
	
	$table_data = $o42->select("SELECT * FROM $table_name ORDER BY $table_key");
	
	?>

<form class="form-horizontal" action="index.php" name="doc" method="POST">
	<?
	echo "<input type=\"hidden\" name=\"p\" value=\"conf\">";
	echo "<input type=\"hidden\" name=\"$table_key\" value=\"".$table_data[0][$table_key]."\">";
	echo "<input type=\"hidden\" name=\"action\" value=\"1\">";

	?>

	<div class="subnav listsubmenu">
		<ul class="nav nav-pills">
			<li><a href="#table-list-add" data-toggle="tab"><i class="icon-plus-sign"></i> Добавить запись</a></li>
			<li><a href="#table-list-filter" data-toggle="tab"><i class="icon-search"></i> Искать записи</a></li>
			<li class="dropdown">
				<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-list-alt"></i> Данные <b class="caret"></b></a>
				<ul class="dropdown-menu">
					<li><a href="#"><i class="icon-download"></i> Импорт</a></li>
					<li><a href="#"><i class="icon-upload"></i> Экспорт</a></li>
					<li class="divider"></li>
					<li><a href="#"><i class="icon-refresh"></i> Перенос файлов</a></li>
					<li><a href="#"><i class="icon-camera"></i> Загрузить фото</a></li>
				</ul>
			</li>
		</ul>	
	</div>

	<div class="control-group">
		<?
		foreach($table_struct as $key => $value)
		{
			if ( $table_struct[$key]['Edit'] == 1 )
			{
				echo "<label class=\"control-label\" for=\"$key\">".$table_struct[$key]['Descr']."</label>";

				echo '<div class="controls">';

					if ( isset($table_struct[$key]['Select']) )
					{
						echo "<select name=\"$key\">";
							foreach($table_struct[$key]['Select'] as $select_key => $select_value)
							{
								if ( $select_key == $table_data[0][$key] )
								$selected = " selected"; else $selected = "";
								echo "<option value=\"$select_key\"$selected>$select_value</option>";
							}
						echo "</select>";
					}
					else
					echo "<input type=\"input\" class=\"input-xlarge\" name=\"$key\" value=\"".$table_data[0][$key]."\">";

				echo '</div>';
			}
		}
		?>
	</div>
	
	<div class="form-actions">
		<div class="btn-group" style="float:left;">
			<a class="btn btn-primary" href="#" onclick="document.forms['doc'].submit()"><i class="icon-ok icon-white"></i> Сохранить</a>
			<!--a class="btn btn-primary dropdown-toggle" data-toggle="dropdown" href="#"><span class="caret"></span></a>
			<ul class="dropdown-menu">
				<li><a href="#"><i class="icon-file"></i> Сохранить и новый документ</a></li>
				<li><a href="#"><i class="icon-folder-open"></i> Сохранить и новый раздел</a></li>
			</ul-->
		</div>

		<!--div class="btn-group" style="float:left;margin-left:50px;">
			<a class="btn btn-danger" href="#" onclick="document.forms['doc'].action.value='2';document.forms['doc'].submit()"><i class="icon-trash icon-white"></i> В корзину</a>
			<a class="btn btn-danger dropdown-toggle" data-toggle="dropdown" href="#"><span class="caret"></span></a>
			<ul class="dropdown-menu">
				<li><a data-toggle="modal" href="#deleteDoc"><i class="icon-trash"></i> Удалить навсегда</a></li>
			</ul>
		</div-->
		
	</div>

</form>
