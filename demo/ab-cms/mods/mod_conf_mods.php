<?
/*
* Copyright (c) 2013, Andrey_B (Office-42)
* Подробнее см. LICENSE.txt или http://www.gnu.org/licenses/
*/
?>
<h3>Модули</h3>

	<?
	$table_struct = array(
		'ListID' => array('Descr' => 'ListID', 'Edit' => 0),
		'Title' => array('Descr' => 'Наименование', 'Edit' => 1),
		'ListType' => array('Descr' => 'Тип', 'Edit' => 1, 'Select' => array (0 => 'Список', 1 => 'Запись')),
		'SortID' => array('Descr' => 'Сортировка', 'Edit' => 1),
		'SortDesc' => array('Descr' => 'Desc', 'Edit' => 1, 'Type' => 'checkbox'),
		'Activate' => array('Descr' => 'Активация', 'Edit' => 2),
	);
	
	$table_name = "tList";
	$table_key = "ListID";
	
	if ( !empty($_REQUEST['action']) )
	{
		if ( $_REQUEST['action'] == 1 )
		$o42->table_update($table_name, $table_key, $table_struct, $_POST);
		elseif ( $_REQUEST['action'] == 2 )
		{
			$o42->execute("DELETE FROM tListField WHERE ListID=".$_POST[$table_key]);
			$o42->table_delete($table_name, $table_key, $_POST[$table_key]);
		}

	}

	$table_data = $o42->select("SELECT * FROM $table_name ORDER BY $table_key");
	$result = $o42->select_row("SHOW TABLES LIKE 'tmp_%'");
	for ( $i = 0; $i < count($result); $i++ )
	$mod_tables[] = $result[$i][0];
	$result = $o42->select("SELECT DISTINCT ListID FROM tListField");
	for ( $i = 0; $i < count($result); $i++ )
	$check_fields[] = $result[$i]['ListID'];
	
	if ( !empty($_REQUEST['activate']) )
	{
		if ( in_array("tmp_".$_REQUEST['ListID'], $mod_tables) )
		$o42->execute("DROP TABLE tmp_".$_REQUEST['ListID']);

		$c_query = "CREATE TABLE tmp_".$_REQUEST['ListID']." ( tmpID INT UNSIGNED NOT NULL PRIMARY KEY, ContID INT UNSIGNED NOT NULL, ";
		$table_index = "";
			
		$result = $o42->select("SELECT ListFieldID, Descr, Title, LType, UseIndex, Size, FOption FROM tListField WHERE ListID=".$_REQUEST['ListID']." ORDER BY ListFieldID");
		for ( $i = 0; $i < count($result); $i++ )
		{
			$c_query .= $result[$i]['Title'];
			if ( $result[$i]['LType'] == 1 || $result[$i]['LType'] == 12 )
			$c_query .= " DATETIME,";
			elseif ( $result[$i]['LType'] == 3 )
			$c_query .= " TINYINT UNSIGNED NOT NULL DEFAULT '0',";
			elseif ( $result[$i]['LType'] == 6 || $result[$i]['LType'] == 7 || $result[$i]['LType'] == 9 )
			$c_query .= " INT UNSIGNED,";
			elseif ( $result[$i]['LType'] == 8 )
			{
				// Внимание! Реализовать в редакторе модуля поддержку FOption!
				if ( empty($result[$i]['FOption']) )
				$result[$i]['FOption'] = 0;
				$c_query .= " DECIMAL(30,".$result[$i]['FOption']."),";
			}
			elseif ( $result[$i]['LType'] == 14 )
			{}
			else
			$c_query .= " TEXT,";

			if ( $result[$i]['UseIndex'] == 1 )
			{
				if ( $result[$i]['LType'] == 1 || $result[$i]['LType'] == 12 || $result[$i]['LType'] == 3 || $result[$i]['LType'] == 6 || $result[$i]['LType'] == 7 || $result[$i]['LType'] == 8 || $result[$i]['LType'] == 9 )
				$table_index .= "INDEX(".$result[$i]['Title']."),";
				elseif ( $result[$i]['Size'] > 0 )
				$table_index .= "INDEX(".$result[$i]['Title']."(".$result[$i]['Size'].")),";
			}
		}

		$c_query .= $table_index."FOREIGN KEY (ContID) REFERENCES tCont(ContID) ) ENGINE=InnoDB";
		$o42->execute($c_query);
		$mod_tables[] = "tmp_".$_REQUEST['ListID'];
	}
	
	?>

	<div class="subnav listsubmenu">
		<ul class="nav nav-pills">
			<li><a href="#table-list-add" data-toggle="tab"><i class="icon-plus-sign"></i> Добавить запись</a></li>
			<li><a href="#table-list-filter" data-toggle="tab"><i class="icon-search"></i> Искать записи</a></li>
			<li class="dropdown">
				<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-list-alt"></i> Данные <b class="caret"></b></a>
				<ul class="dropdown-menu">
					<li><a href="#"><i class="icon-download"></i> Импорт</a></li>
					<li><a href="#"><i class="icon-upload"></i> Экспорт</a></li>
					<li class="divider"></li>
					<li><a href="#"><i class="icon-refresh"></i> Перенос файлов</a></li>
					<li><a href="#"><i class="icon-camera"></i> Загрузить фото</a></li>
				</ul>
			</li>
		</ul>	
	</div>

	
	<table class="table table-striped table-list">
  		<thead>
			<tr>
				<th></th>
				<?
				foreach($table_struct as $key => $value)
				{
					if ( $table_struct[$key]['Edit'] == 1 )
					echo "<th><a href=\"\" title=\"".$table_struct[$key]['Descr']."\">".$table_struct[$key]['Descr']."</a></th>";
				}
				?>
				<th></th>
				<th></th>
			</tr>
		</thead>

		<thead  class="tab-content">
			<tr class="table-list-add tab-pane" id="table-list-add">
				<th></th>
				<?
				echo "<form action=\"index.php\" method=\"POST\">";
					echo "<input type=\"hidden\" name=\"p\" value=\"conf_mods\">";
					echo "<input type=\"hidden\" name=\"action\" value=\"1\">";

					foreach($table_struct as $key => $value)
					{
						if ( $table_struct[$key]['Edit'] > 0 )
						{
							echo "<th>";
								if ( $table_struct[$key]['Edit'] == 1 )
								{
									if ( isset($table_struct[$key]['Type']) && $table_struct[$key]['Type'] == "checkbox" )
									echo "<input type=\"checkbox\" name=\"$key\" value=\"\">";
									elseif ( isset($table_struct[$key]['Select']) )
									{
										echo "<select name=\"$key\">";
											foreach($table_struct[$key]['Select'] as $select_key => $select_value)
											echo "<option value=\"$select_key\">$select_value</option>";
										echo "</select>";
									}
									elseif ( $key == "SortID" )
									echo "&nbsp;";
									else
									echo "<input class=\"text\" type=\"text\" name=\"$key\" />";
								}
								elseif ( $table_struct[$key]['Edit'] == 2  )
								echo "&nbsp;";
							echo "</th>";
						}
					}

					echo "<th colspan=\"2\"><button class=\"btn btn-small btn-success\" href=\"#\"><i class=\"icon-ok icon-white\"></i> добавить</button></th>";
				echo "</form>";
				?>
			</tr>

			<tr class="table-list-filter tab-pane" id="table-list-filter">
				<th></th>
				<th></th>
				<?
				// Фильтр
				//for ( $i = 0; $i < count($mod_struct); $i++ )
				echo "<th><input class=\"text\" type=\"text\" /></th>";
				?>
				<th><button class="btn btn-small btn-primary" href="#"><i class="icon-search icon-white"></i> найти</button></th>
				<th><button class="btn btn-small btn btn-warning" href="#"><i class="icon-remove icon-white"></i></button></th>
			</tr>
		</thead>

		<tbody>
			<?
			for ( $i = 0; $i < count($table_data); $i++ )
			{
				echo "<form action=\"index.php\" method=\"POST\" name=\"mod-".$table_data[$i][$table_key]."\" enctype=\"multipart/form-data\">";
					echo "<input type=\"hidden\" name=\"p\" value=\"conf_mods\">";
					echo "<input type=\"hidden\" name=\"$table_key\" value=\"".$table_data[$i][$table_key]."\">";
					echo "<input type=\"hidden\" name=\"action\" value=\"1\">";
					
					echo "<tr>";
						echo "<td><a href=\"index.php?p=conf_mods_fields&$table_key=".$table_data[$i][$table_key]."\" class=\"btn btn-mini btn-primary\"><i class=\"icon-pencil icon-white\"></i></a></td>";

						foreach($table_struct as $key => $value)
						{
							if ( $table_struct[$key]['Edit'] > 0 )
							{
								echo "<td>";
									// Поле сортировка модуля. Выводим перечень полей для данного модуля.
									if ( $key == "SortID" )
									{
										echo "<select name=\"$key\">";
											echo "<option>по умолчанию</option>";
											$result = $o42->select("SELECT ListFieldID, Title FROM tListField WHERE ListID=".$table_data[$i]['ListID']);
											for ( $j = 0; $j < count($result); $j++ )
											{
												if ( $result[$j]['ListFieldID'] == $table_data[$i][$key] )
												$selected = " selected"; else $selected = "";
												echo "<option value=\"".$result[$j]['ListFieldID']."\"$selected>".$result[$j]['Title']."</option>";
											}
										echo "</select>";
									}
									elseif ( $key == "Activate" )
									{
										if ( in_array("tmp_".$table_data[$i]['ListID'], $mod_tables) )
										echo "<a href=\"index.php?p=conf_mods&ListID=".$table_data[$i]['ListID']."&activate=1\">Реактивировать</a>";
										elseif ( !in_array($table_data[$i]['ListID'], $check_fields) )
										echo "Поля не определены";
										else
										echo "<a href=\"index.php?p=conf_mods&ListID=".$table_data[$i]['ListID']."&activate=1\">Активировать</a>";
									}
									elseif ( isset($table_struct[$key]['Type']) && $table_struct[$key]['Type'] == "checkbox" )
									{
										
										if ( $table_data[$i][$key] == 1 )
										$selected = " checked"; else $selected = "";
										echo "<input type=\"checkbox\" name=\"$key\" value=\"1\"$selected>";
									}
									elseif ( isset($table_struct[$key]['Select']) )
									{
										echo "<select name=\"$key\">";
											foreach($table_struct[$key]['Select'] as $select_key => $select_value)
											{
												if ( $select_key == $table_data[$i][$key] )
												$selected = " selected"; else $selected = "";
												echo "<option value=\"$select_key\"$selected>$select_value</option>";
											}
										echo "</select>";
									}
									else
									echo "<input class=\"text\" name=\"$key\" value=\"".$table_data[$i][$key]."\">";
							echo "</td>";
							}
						}

						echo "<td>";
							echo "<button class=\"btn btn-mini btn-success\" href=\"#\" onclick=\"document.forms['mod-".$table_data[$i][$table_key]."'].submit()\"><i class=\"icon-ok icon-white\"></i></button>";
							//echo '<button class="btn btn-mini btn-info" href="#"><i class="icon-arrow-up icon-white"></i></button>';
							//echo '<button class="btn btn-mini btn-info" href="#"><i class="icon-arrow-down icon-white"></i></button>';
						echo "</td>";
						echo "<td><button class=\"btn btn-mini btn-danger\" href=\"#\" onclick=\"document.forms['mod-".$table_data[$i][$table_key]."'].action.value='2';document.forms['mod-".$table_data[$i][$table_key]."'].submit()\"><i class=\"icon-remove icon-white\"></i></button></td>";

						echo "</tr>";
				echo "</form>";
			}
			?>

		</tbody>
	</table>

	<?
	/*
	if ( $mod_total_rec['max'] > $rec_per_page )
	{
		if ( !isset($_REQUEST['page']) )
		$_REQUEST['page'] = 0;
		
		echo "<div class=\"pagination\">";
			echo "<ul>";
				if ( $_REQUEST['page'] > 0 )
				{
					$mod_prev = $_REQUEST['page'] - 1;
					echo "<a href=\"index.php?p=mods&id=".$_REQUEST['id']."&ListID=".$_REQUEST['ListID']."&page=$mod_prev\">«</a>";
				}
		
				for ( $i = 0; $i < ceil($mod_total_rec['max'] / $rec_per_page); $i++ )
				{
					$mod_page = $i + 1;
					if ( $_REQUEST['page'] == $i ) $current = " class=\"active\""; else $current = "";
					echo "<a href=\"index.php?p=mods&id=".$_REQUEST['id']."&ListID=".$_REQUEST['ListID']."&page=$i\"$current>$mod_page</a>";
				}

				if ( $_REQUEST['page'] < ceil($mod_total_rec['max'] / $rec_per_page) - 1 )
				{
					$mod_next = $_REQUEST['page'] + 1;
					echo "<a href=\"index.php?p=mods&id=".$_REQUEST['id']."&ListID=".$_REQUEST['ListID']."&page=$mod_next\">»</a>";
				}
			echo "</ul>";
		echo "</div>";
    }
	*/
