<?
/*
* Copyright (c) 2013, Andrey_B (Office-42)
* Подробнее см. LICENSE.txt или http://www.gnu.org/licenses/
*/
?>
<h3>Пользователи</h3>

	<?
	$table_struct = array(
		'UserID' => array('Descr' => 'UserID', 'Edit' => 0),
		'Name' => array('Descr' => 'Имя', 'Edit' => 1),
		'Descr' => array('Descr' => 'Описание', 'Edit' => 1),
		'Email' => array('Descr' => 'Email', 'Edit' => 1),
		'Login' => array('Descr' => 'Логин', 'Edit' => 1),
		'Password' => array('Descr' => 'Пароль', 'Edit' => 1),
		'UType' => array('Descr' => 'Тип', 'Edit' => 1, 'Select' => array(3 => 'Разработчик', 2 => 'Администратор', 1 => 'Пользователь')),
		'MD5_Hash' => array('Descr' => 'Salt', 'Edit' => 0)
	);
	
	$table_name = "tUser";
	$table_key = "UserID";
	
	if ( !empty($_REQUEST['action']) )
	{
		if ( $_REQUEST['action'] == 1 )
		{
			if ( $_POST['Password'] == "*****" )
			unset($_POST['Password']);
			else
			{
				$_POST['MD5_Hash'] = md5(mt_rand());
				$_POST['Password'] = md5(md5($_POST['Password']).$_POST['MD5_Hash']);
			}
			$o42->table_update($table_name, $table_key, $table_struct, $_POST);
	
		}
		elseif ( $_REQUEST['action'] == 2 )
		$o42->table_delete($table_name, $table_key, $_POST[$table_key]);

	}
	
	$table_data = $o42->select("SELECT * FROM $table_name ORDER BY $table_key");
		
	?>

	<div class="subnav listsubmenu">
		<ul class="nav nav-pills">
			<li><a href="#table-list-add" data-toggle="tab"><i class="icon-plus-sign"></i> Добавить запись</a></li>
			<li><a href="#table-list-filter" data-toggle="tab"><i class="icon-search"></i> Искать записи</a></li>
			<li class="dropdown">
				<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-list-alt"></i> Данные <b class="caret"></b></a>
				<ul class="dropdown-menu">
					<li><a href="#"><i class="icon-download"></i> Импорт</a></li>
					<li><a href="#"><i class="icon-upload"></i> Экспорт</a></li>
					<li class="divider"></li>
					<li><a href="#"><i class="icon-refresh"></i> Перенос файлов</a></li>
					<li><a href="#"><i class="icon-camera"></i> Загрузить фото</a></li>
				</ul>
			</li>
		</ul>	
	</div>

	
	<table class="table table-striped table-list">
  		<thead>
			<tr>
				<th class="but1"></th>
				<?
				foreach($table_struct as $key => $value)
				{
					if ( $table_struct[$key]['Edit'] == 1 )
					echo "<th><a href=\"\" title=\"".$table_struct[$key]['Descr']."\">".$table_struct[$key]['Descr']."</a></th>";
				}
				?>
				<th></th>
				<th></th>
			</tr>
		</thead>

		<thead  class="tab-content">
			<tr class="table-list-add tab-pane" id="table-list-add">
				<th></th>
				<?
				echo "<form action=\"index.php\" method=\"POST\">";
					echo "<input type=\"hidden\" name=\"p\" value=\"users\">";
					echo "<input type=\"hidden\" name=\"action\" value=\"1\">";

					foreach($table_struct as $key => $value)
					{
						if ( $table_struct[$key]['Edit'] == 1 )
						{
							echo "<th>";
								if ( isset($table_struct[$key]['Select']) )
								{
									echo "<select name=\"$key\">";
										foreach($table_struct[$key]['Select'] as $select_key => $select_value)
										{
											if ( $select_key == $table_data[$i][$key] )
											$selected = " selected"; else $selected = "";
											echo "<option value=\"$select_key\"$selected>$select_value</option>";
										}
									echo "</select>";
								}
								else
								echo "<input class=\"text\" type=\"text\" name=\"$key\" />";
							echo "</th>";
						}
					}

					echo "<th colspan=\"2\"><button class=\"btn btn-small btn-success\" href=\"#\"><i class=\"icon-ok icon-white\"></i> добавить</button></th>";
				echo "</form>";
				?>
			</tr>

			<tr class="table-list-filter tab-pane" id="table-list-filter">
				<th></th>
				<th></th>
				<?
				// Фильтр
				//for ( $i = 0; $i < count($mod_struct); $i++ )
				echo "<th><input class=\"text\" type=\"text\" /></th>";
				?>
				<th colspan="2"><button class="btn btn-small btn-primary" href="#"><i class="icon-search icon-white"></i> найти</button></th>
				<!--th><button class="btn btn-small btn btn-warning" href="#"><i class="icon-remove icon-white"></i></button></th-->
			</tr>
		</thead>

		<tbody>
			<?
			for ( $i = 0; $i < count($table_data); $i++ )
			{
				echo "<form action=\"index.php\" method=\"POST\" name=\"mod-".$table_data[$i][$table_key]."\" enctype=\"multipart/form-data\">";
					echo "<input type=\"hidden\" name=\"p\" value=\"users\">";
					echo "<input type=\"hidden\" name=\"$table_key\" value=\"".$table_data[$i][$table_key]."\">";
					echo "<input type=\"hidden\" name=\"action\" value=\"1\">";
					
					echo "<tr>";
						echo "<td><button class=\"btn btn-mini btn-primary\"><i class=\"icon-pencil icon-white\"></i></button></td>";

						foreach($table_struct as $key => $value)
						{
							if ( $table_struct[$key]['Edit'] == 1 )
							{
								echo "<td>";
									// Тип поля "пароль"
									if ( $key == "Password" )
									echo "<input type=\"password\" class=\"text\" name=\"$key\" value=\"*****\">";
									elseif ( isset($table_struct[$key]['Select']) )
									{
										echo "<select name=\"$key\">";
											foreach($table_struct[$key]['Select'] as $select_key => $select_value)
											{
												if ( $select_key == $table_data[$i][$key] )
												$selected = " selected"; else $selected = "";
												echo "<option value=\"$select_key\"$selected>$select_value</option>";
											}
										echo "</select>";
									}
									else
									echo "<input type=\"input\" class=\"text\" name=\"$key\" value=\"".$table_data[$i][$key]."\">";
								echo "</td>";
							}
						}

						echo "<td>";
							//echo "<div class=\"btn-group\">";
								echo "<button class=\"btn btn-mini btn-success\" href=\"#\" onclick=\"document.forms['mod-".$table_data[$i][$table_key]."'].submit()\"><i class=\"icon-ok icon-white\"></i></button>";
								//echo '<button class="btn btn-mini btn-info" href="#"><i class="icon-arrow-up icon-white"></i></button>';
								//echo '<button class="btn btn-mini btn-info" href="#"><i class="icon-arrow-down icon-white"></i></button>';
							//echo "</div>";
						echo "</td>";
						echo "<td><button class=\"btn btn-mini btn-danger\" href=\"#\" onclick=\"document.forms['mod-".$table_data[$i][$table_key]."'].action.value='2';document.forms['mod-".$table_data[$i][$table_key]."'].submit()\"><i class=\"icon-remove icon-white\"></i></button></td>";

						echo "</tr>";
				echo "</form>";
			}
			?>

		</tbody>
	</table>

	<?
	/*
	if ( $mod_total_rec['max'] > $rec_per_page )
	{
		if ( !isset($_REQUEST['page']) )
		$_REQUEST['page'] = 0;
		
		echo "<div class=\"pagination\">";
			echo "<ul>";
				if ( $_REQUEST['page'] > 0 )
				{
					$mod_prev = $_REQUEST['page'] - 1;
					echo "<a href=\"index.php?p=mods&id=".$_REQUEST['id']."&ListID=".$_REQUEST['ListID']."&page=$mod_prev\">«</a>";
				}
		
				for ( $i = 0; $i < ceil($mod_total_rec['max'] / $rec_per_page); $i++ )
				{
					$mod_page = $i + 1;
					if ( $_REQUEST['page'] == $i ) $current = " class=\"active\""; else $current = "";
					echo "<a href=\"index.php?p=mods&id=".$_REQUEST['id']."&ListID=".$_REQUEST['ListID']."&page=$i\"$current>$mod_page</a>";
				}

				if ( $_REQUEST['page'] < ceil($mod_total_rec['max'] / $rec_per_page) - 1 )
				{
					$mod_next = $_REQUEST['page'] + 1;
					echo "<a href=\"index.php?p=mods&id=".$_REQUEST['id']."&ListID=".$_REQUEST['ListID']."&page=$mod_next\">»</a>";
				}
			echo "</ul>";
		echo "</div>";
    }
	*/
