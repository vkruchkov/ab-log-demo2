<?
/*
* Copyright (c) 2013, Andrey_B (Office-42)
* Подробнее см. LICENSE.txt или http://www.gnu.org/licenses/
*/
?>
<h3>Модули</h3>
<?		
$list_array = array();

function mod_tree($level = 0)
{
	global $o42;
	global $list_array;

	echo "<ul>";

		$result = $o42->select("SELECT a.Title, a.ContID, a.CType, a.ListID
								FROM tCont a, tContExt b
								WHERE a.ContID=b.ContID AND b.ParentID=$level AND a.CType<2
								ORDER BY a.Priority");
		for ( $i = 0; $i < count($result); $i++ )
		{
			if ( in_array($result[$i]['ContID'], $list_array) )
			{
				echo "<li>";
					if ( $result[$i]['CType'] == 1 )
					echo "<a href=index.php?p=mods&id=".$result[$i]['ContID']."&ListID=".$result[$i]['ListID'].">".$result[$i]['Title']."</a>";
					else
					echo $result[$i]['Title'];
					if ( $result[$i]['CType'] == 0 )
					mod_tree($result[$i]['ContID']);
				echo "</li>";

			}
		}

	echo "</ul>";
				
}

$result = $o42->select_row("SHOW TABLES");
for ($i = 0; $i < count($result); $i++ )
{
	if ( preg_match("/tmp_(\d)+/", $result[$i][0]) )
	{
		$result2 = $o42->select("SELECT ContID FROM ".$result[$i][0]." GROUP BY ContID");
		for ( $j = 0; $j < count($result2); $j++ )
		{
			$result3 = $o42->select("SELECT DISTINCT a.ParentID, b.ListID
									FROM tContExt a, tCont b
									WHERE a.ContID=".$result2[$j]['ContID']." AND a.ContID=b.ContID
									ORDER BY b.Priority");
			for ( $k = 0; $k < count($result3); $k++ )
			{
				if ( !in_array($result2[$j]['ContID'], $list_array) )
				{
					$parent = $result3[$k]['ParentID'];
					while ( $parent != 0 )
					{
						$result4 = $o42->select("SELECT a.ContID, a.Title, b.ParentID, a.CType
												FROM tCont a, tContExt b
												WHERE a.ContID=b.ContID AND a.ContID=$parent
												ORDER BY a.Priority");
						for ( $n = 0; $n < count($result4); $n++ )
						{
							$parent = $result4[$n]['ParentID'];
							if ( !in_array($result4[$n]['ContID'], $list_array) )
							$list_array[] = $result4[$n]['ContID'];
							else
							break;
						}
					}
				}

				if ( !in_array($result2[$j]['ContID'], $list_array) )
				{
					$list_array[] = $result2[$j]['ContID'];
					//$list_array2[$result3[$k]['ListID']] = $result2[$j]['ContID'];
				}
			}
		}
	}
}

mod_tree();
?>