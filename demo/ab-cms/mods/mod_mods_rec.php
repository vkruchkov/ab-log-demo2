<?
/*
* Copyright (c) 2013, Andrey_B (Office-42)
* Подробнее см. LICENSE.txt или http://www.gnu.org/licenses/
*/
?>
	
	<ul class="nav nav-tabs">
  		<?
		echo "<li><a href=\"index.php?p=doc&id=".$_REQUEST['id']."\">Содержание</a></li>";
		echo "<li><a href=\"index.php?p=doc&id=".$_REQUEST['id']."\">Свойства</a></li>";
		
		$list_prop = $o42->select_line("SELECT Title, SortID, SortDesc FROM tList WHERE ListID=".$_REQUEST['ListID']." LIMIT 1");
		echo "<li class=\"active\"><a href=\"index.php?p=mods&id=".$_REQUEST['id']."&ListID=".$_REQUEST['ListID']."\">".$list_prop['Title']."</a></li>";
		?>

	</ul>


	<div class="subnav listsubmenu">
		<ul class="nav nav-pills">
			<?
			echo "<li><a href=\"index.php?p=mods_rec&id=".$_REQUEST['id']."&ListID=".$_REQUEST['ListID']."\"><i class=\"icon-plus-sign\"></i> Добавить запись</a></li>";
			echo "<li><a href=\"index.php?p=mods&id=".$_REQUEST['id']."&ListID=".$_REQUEST['ListID']."\"><i class=\"icon-backward\"></i> Вернуться в список</a></li>";
			if ( !empty($_REQUEST['rec']) )
			{
				$prev_rec = $o42->select_line("SELECT tmpID FROM tmp_".$_REQUEST['ListID']." WHERE ContID=".$_REQUEST['id']." AND tmpID<".$_REQUEST['rec']." ORDER BY tmpID DESC LIMIT 1");
				if ( isset($prev_rec['tmpID']) )
				echo "<li><a href=\"index.php?p=mods_rec&id=".$_REQUEST['id']."&ListID=".$_REQUEST['ListID']."&rec=".$prev_rec['tmpID']."\"><i class=\"icon-arrow-left\"></i> Предыдущая запись</a></li>";
				$next_rec = $o42->select_line("SELECT tmpID FROM tmp_".$_REQUEST['ListID']." WHERE ContID=".$_REQUEST['id']." AND tmpID>".$_REQUEST['rec']." ORDER BY tmpID LIMIT 1");
				if ( isset($next_rec['tmpID']) )
				echo "<li><a href=\"index.php?p=mods_rec&id=".$_REQUEST['id']."&ListID=".$_REQUEST['ListID']."&rec=".$next_rec['tmpID']."\"><i class=\"icon-arrow-right\"></i> Следующая запись</a></li>";
			}
			?>

			<li class="dropdown">
				<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-list-alt"></i> Данные <b class="caret"></b></a>
					<ul class="dropdown-menu">
					<li><a href="#"><i class="icon-download"></i> Импорт</a></li>
					<li><a href="#"><i class="icon-upload"></i> Экспорт</a></li>
					<li class="divider"></li>
					<li><a href="#"><i class="icon-refresh"></i> Перенос файлов</a></li>
					<li><a href="#"><i class="icon-camera"></i> Загрузить фото</a></li>
				</ul>
			</li>
		</ul>	
	</div>

	<?
	$mod_sizes = array(0 => 'mini', 1 => 'small', 2 => 'medium', 3 => 'large', 4 => 'xlarge', 5 => 'xxlarge');
	$mod_struct = $o42->select("SELECT * FROM tListField WHERE ListID=".$_REQUEST['ListID']." ORDER BY ListFieldID");
	
	for ( $i = 0; $i < count($mod_struct); $i++ )
	{
		if ( $mod_struct[$i]['LType'] == 10 )
		{
			echo "<script type=\"text/javascript\" src=\"ckeditor/ckeditor.js\"></script>";
			break;
		}
	}

	if ( empty($_REQUEST['rec']) )
	{
		for ( $i = 0; $i < count($mod_struct) + 2; $i++ )
		$mod_data[0][$i] = "";
	}

	if ( !empty($_REQUEST['action']) )
	{
		if ( $_REQUEST['action'] == 1 )
		{

			// Если поле фото не заполнено, исключаем его из передаваемых данных
			$file_cnt = 0;
			for ( $i = 0; $i < count($mod_struct); $i++ )
			{
				if ( ($mod_struct[$i]['LType'] == 13 || $mod_struct[$i]['LType'] == 2) )
				{
					if ( empty($_FILES[$mod_struct[$i]['Title']]['name']) && empty($_REQUEST['fileremove']) )
					unset($_FILES[$mod_struct[$i]['Title']]);
					elseif ( empty($_FILES[$mod_struct[$i]['Title']]['name']) && $mod_struct[$i]['ListFieldID'] == $_REQUEST['fileremove'] )
					{
						$filemask = ("../sfiles/lists/".$_REQUEST['id']."_".$_REQUEST['ListID']."_".$_REQUEST['tmpID']."_".$file_cnt.".*");
						foreach (glob($filemask) as $filename)
						unlink($filename);
					}
					
					$file_cnt++;
				}
			}
			
			
			$o42->mod_update($_REQUEST['id'], array_merge($_POST, $_FILES));
		}
		elseif ( $_REQUEST['action'] == 2 )
		$o42->mod_delete($_REQUEST['id'], $_POST['rec']);
	}
	
	$rec_per_page = 25;
	$total_pages = 20;
	if ( isset($_REQUEST['page']) && $_REQUEST['page'] > 0 )
	$pos = $_REQUEST['page'] * $rec_per_page;
	else
	$pos = 0;
	
	$mod_order = "";
	if ( !empty($list_prop['SortID']) )
	{
		for ( $i = 0; $i < count($mod_struct); $i++ )
		{
			if ( $mod_struct[$i]['ListFieldID'] == $list_prop['SortID'] )
			{
				$mod_order = $mod_struct[$i]['Title'];
				if ( !empty($list_prop['SortDesc']) )
				$mod_order .= " DESC";
			}
		}
		$mod_order .= ",";
	}
	$mod_order .= "tmpID";
	// Обязательное условие использование индексов - направление сортировки должно совпадать
	if ( !empty($list_prop['SortDesc']) )
	$mod_order .= " DESC";

	if ( !empty($_REQUEST['rec']) )
	$mod_data = $o42->select_row("SELECT * FROM tmp_".$_REQUEST['ListID']." WHERE ContID=".$_REQUEST['id']." AND tmpID=".$_REQUEST['rec']." LIMIT 1");
	//$mod_total_rec = $o42->select_line("SELECT COUNT(tmpID) max FROM tmp_".$_REQUEST['ListID']." WHERE ContID=".$_REQUEST['id']);
	$i = 0;
		
	echo "<form class=\"form-horizontal\" action=\"index.php\" method=\"POST\" name=\"mod-".$mod_data[$i][0]."\" enctype=\"multipart/form-data\">";
		echo "<input type=\"hidden\" name=\"p\" value=\"mods_rec\">";
		echo "<input type=\"hidden\" name=\"id\" value=\"".$_REQUEST['id']."\">";
		echo "<input type=\"hidden\" name=\"ListID\" value=\"".$_REQUEST['ListID']."\">";
		echo "<input type=\"hidden\" name=\"tmpID\" value=\"".$mod_data[$i][0]."\">";
		if ( !empty($_REQUEST['rec']) )
		echo "<input type=\"hidden\" name=\"rec\" value=\"".$mod_data[$i][0]."\">";
		echo "<input type=\"hidden\" name=\"action\" value=\"1\">";
		echo "<input type=\"hidden\" name=\"fileremove\" value=\"\">";
					
		echo "<div id=\"myTabContent\" class=\"tab-content\">";
			
			for ( $j = 2; $j < count($mod_data[$i]); $j++ )
			{

				if ( $mod_struct[$j-2]['Size'] > 5 )
				$mod_struct[$j-2]['Size'] = 2;

				
				// Тип поля DHTML
				if ( $mod_struct[$j-2]['LType'] == 10 )
				{
					echo "<h6>".$mod_struct[$j-2]['Descr']."</h6>";
					echo "<textarea style=\"height:300px; width:99%; margin-bottom:20px;\" name=\"".$mod_struct[$j-2]['Title']."\">".$mod_data[$i][$j]."</textarea>";
					echo "<script type=\"text/javascript\">CKEDITOR.replace( '".$mod_struct[$j-2]['Title']."', { height : 300 } );</script>";
				}
				else
				{
					echo "<div class=\"control-group ".$mod_struct[$j-2]['Title']."\">";

						echo "<label class=\"control-label\" for=\"".$mod_struct[$j-2]['Title']."\">".$mod_struct[$j-2]['Descr']."</label>";
						// Тип поля "Фото"
						if ( $mod_struct[$j-2]['LType'] == "13" || $mod_struct[$j-2]['LType'] == "2" )
						{
							echo "<input class=\"\" type=\"file\" name=\"".$mod_struct[$j-2]['Title']."\" />";
							if ( !empty($mod_data[$i][$j]) )
							echo "<button class=\"btn btn-mini\" onclick=\"document.forms['mod-".$mod_data[$i][0]."'].fileremove.value='".$mod_struct[$j-2]['ListFieldID']."';document.forms['mod-".$mod_data[$i][0]."'].submit()\"><i class=\"icon-remove\"></i></button>";
						}
						// Тип поля "объект" (связанная таблица)
						elseif ( $mod_struct[$j-2]['LType'] == 6 )
						{
							if ( !isset($obj_doc[$mod_struct[$i]['ListFieldID']]) )
							{
								$obj_doc[$mod_struct[$j-2]['ListFieldID']] = $o42->doc_read($mod_struct[$j-2]['LSelect']);
								$obj_field[$mod_struct[$j-2]['ListFieldID']] = $o42->select_line("SELECT Title FROM tListField WHERE ListFieldID=".$mod_struct[$j-2]['LField']." LIMIT 1");
							}

							echo "<select name=\"".$mod_struct[$j-2]['Title']."\">";
								$obj_data = $o42->select("SELECT tmpID, ".$obj_field[$mod_struct[$j-2]['ListFieldID']]['Title']." field_data FROM tmp_".$obj_doc[$mod_struct[$j-2]['ListFieldID']]['ListID']." WHERE ContID=".$obj_doc[$mod_struct[$j-2]['ListFieldID']]['ContID']);
								for ( $k = 0; $k < count($obj_data); $k++ )
								{
									if ( $mod_data[$i][$j] == $obj_data[$k]['tmpID'] )
									$selected = " selected"; else $selected = "";
									echo "<option value=\"".$obj_data[$k]['tmpID']."\"$selected>".$obj_data[$k]['field_data']."</option>";
								}
							echo "</select>";
						}
						elseif ( $mod_struct[$j-2]['LType'] == 4 )
						echo "<div class=\"controls\"><textarea class=\"input-".$mod_sizes[$mod_struct[$j-2]['Size']]."\" name=\"".$mod_struct[$j-2]['Title']."\" />".$mod_data[$i][$j]."</textarea></div>";
						else
						echo "<div class=\"controls\"><input class=\"input-".$mod_sizes[$mod_struct[$j-2]['Size']]."\" name=\"".$mod_struct[$j-2]['Title']."\" value=\"".$mod_data[$i][$j]."\"></div>";
					echo "</div>";
				}
			}

		echo "</div>";
						
		echo "<div class=\"row form-actions\">";
			echo "<div class=\"btn-group span2\">";
				echo "<button class=\"btn btn-primary\" href=\"#\" onclick=\"document.forms['mod-".$mod_data[$i][0]."'].submit()\"><i class=\"icon-ok icon-white\"></i> Сохранить</button>";
				echo "<a class=\"btn btn-primary dropdown-toggle\" data-toggle=\"dropdown\" href=\"#\"><span class=\"caret\"></span></a>";
				echo "<ul class=\"dropdown-menu\">";
					echo "<li><a href=\"#\"><i class=\"icon-file\"></i> Сохранить и новая запись</a></li>";
				echo "</ul>";
			echo "</div>";

			echo '
			<div class="span4">
				<select class="span2">
					<option>Перенести в ...</option>
					<option>2</option>
					<option>3</option>
					<option>4</option>
					<option>5</option>
				</select>
				<a href="" class="btn"><i class="icon-share-alt"></i></a>
			</div>';

			echo "<button class=\"btn btn-danger\" href=\"#\" onclick=\"document.forms['mod-".$mod_data[$i][0]."'].action.value='2';document.forms['mod-".$mod_data[$i][0]."'].p.value='mods';document.forms['mod-".$mod_data[$i][0]."'].submit()\" ><i class=\"icon-trash icon-white\"></i> Удалить</button>";

		echo "</div>";
			
	echo "</form>";
				
