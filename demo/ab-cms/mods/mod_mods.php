<?
/*
* Copyright (c) 2013, Andrey_B (Office-42)
* Подробнее см. LICENSE.txt или http://www.gnu.org/licenses/
*/
?>

<?
$_REQUEST['id'] = intval($_REQUEST['id']);
$_REQUEST['ListID'] = intval($_REQUEST['ListID']);

$mod_doc = $o42->doc_read($_REQUEST['id']);
echo "<h3>Редактирования модуля / документ: <a href=\"index.php?p=doc&id=".$_REQUEST['id']."\">".$mod_doc['Title']."</a></h3>";
?>
	<script type="text/javascript" src="/cadmin/libs/preview.js"></script>		

	<ul class="nav nav-tabs">
  		<?
		echo "<li><a href=\"index.php?p=doc&id=".$_REQUEST['id']."\">Содержание</a></li>";
		echo "<li><a href=\"index.php?p=doc&id=".$_REQUEST['id']."\">Свойства</a></li>";
		
		$list_prop = $o42->select_line("SELECT Title, SortID, SortDesc, ListType FROM tList WHERE ListID=".$_REQUEST['ListID']." LIMIT 1");
		echo "<li class=\"active\"><a href=\"index.php?p=mods&id=".$_REQUEST['id']."&ListID=".$_REQUEST['ListID']."\">".$list_prop['Title']."</a></li>";
		?>

	</ul>

		
	<div class="subnav listsubmenu">
		<ul class="nav nav-pills">

			<? //if ( $list_prop['ListType'] == 1 ) ?>
			
			<li><a href="#table-list-add" data-toggle="tab"><i class="icon-plus-sign"></i> Добавить запись</a></li>
			<li><a href="#table-list-filter" data-toggle="tab"><i class="icon-search"></i> Искать записи</a></li>
			<li class="dropdown">
				<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-list-alt"></i> Данные <b class="caret"></b></a>
				<ul class="dropdown-menu">
					<li><a href="#"><i class="icon-download"></i> Импорт</a></li>
					<li><a href="#"><i class="icon-upload"></i> Экспорт</a></li>
					<li class="divider"></li>
					<li><a href="#"><i class="icon-refresh"></i> Перенос файлов</a></li>
					<li><a href="#"><i class="icon-camera"></i> Загрузить фото</a></li>
				</ul>
			</li>
		</ul>	
	</div>
	
	<?
	$mod_sizes = array(0 => 'mini', 1 => 'small', 2 => 'medium', 3 => 'large', 4 => 'xlarge', 5 => 'xxlarge');
	$mod_struct = $o42->select("SELECT * FROM tListField WHERE ListID=".$_REQUEST['ListID']." ORDER BY ListFieldID");

	if ( !empty($_REQUEST['action']) )
	{
		if ( $_REQUEST['action'] == 1 )
		{
			// Если поле фото не заполнено, исключаем его из передаваемых данных
			$file_cnt = 0;
			for ( $i = 0; $i < count($mod_struct); $i++ )
			{
				if ( ($mod_struct[$i]['LType'] == 13 || $mod_struct[$i]['LType'] == 2) )
				{
					if ( empty($_FILES[$mod_struct[$i]['Title']]['name']) && empty($_REQUEST['fileremove']) )
					unset($_FILES[$mod_struct[$i]['Title']]);
					elseif ( empty($_FILES[$mod_struct[$i]['Title']]['name']) && $mod_struct[$i]['ListFieldID'] == $_REQUEST['fileremove'] )
					{
						$filemask = ("../sfiles/lists/".$_REQUEST['id']."_".$_REQUEST['ListID']."_".$_REQUEST['tmpID']."_".$file_cnt.".*");
						foreach (glob($filemask) as $filename)
						unlink($filename);
					}
					
					$file_cnt++;
				}
			}

			$o42->mod_update($_REQUEST['id'], array_merge($_POST, $_FILES));
		}
		elseif ( $_REQUEST['action'] == 2 )
		$o42->mod_delete($_REQUEST['id'], $_POST['tmpID']);
	}
	
	$rec_per_page = 25;
	$total_pages = 20;
	if ( isset($_REQUEST['page']) && $_REQUEST['page'] > 0 )
	$pos = $_REQUEST['page'] * $rec_per_page;
	else
	$pos = 0;
	
	$mod_order = "";
	if ( !empty($list_prop['SortID']) )
	{
		for ( $i = 0; $i < count($mod_struct); $i++ )
		{
			if ( $mod_struct[$i]['ListFieldID'] == $list_prop['SortID'] )
			{
				$mod_order = $mod_struct[$i]['Title'];
				if ( !empty($list_prop['SortDesc']) )
				$mod_order .= " DESC";
			}
		}
		$mod_order .= ",";
	}
	$mod_order .= "tmpID";
	// Обязательное условие использование индексов - направление сортировки должно совпадать
	if ( !empty($list_prop['SortDesc']) )
	$mod_order .= " DESC";
	
	$where = "";
	
	if ( !empty($_REQUEST['filter']) )
	{
		for ( $i = 0; $i < count($mod_struct); $i++ )
		{
			if ( !empty($_REQUEST[$mod_struct[$i]['Title']]) )
			{
				// Если тип поля число (или объект)
				if ( $mod_struct[$i]['LType'] == 8 || $mod_struct[$i]['LType'] == 6 )
				$where .= " AND ".$mod_struct[$i]['Title']."=".intval($_REQUEST[$mod_struct[$i]['Title']])." ";
				else
				{
					if ( preg_match("/\s/", $_REQUEST[$mod_struct[$i]['Title']]) )
					{
						$where .= " AND ( ";
						$s_query = preg_split("/\s+/", $_REQUEST[$mod_struct[$i]['Title']]);
						for ( $k = 0; $k < count($s_query); $k++ )
						$where .= " ".$mod_struct[$i]['Title']." LIKE '%".mysqli_real_escape_string($o42->db_conn, $s_query[$k])."%' AND ";
						$where = preg_replace("/AND $/", ")", $where);
					}
					else
					$where .= " AND ".$mod_struct[$i]['Title']." LIKE '%".mysqli_real_escape_string($o42->db_conn, $_REQUEST[$mod_struct[$i]['Title']])."%' ";
				}
			}
		}
	}

	$mod_data = $o42->select_row("SELECT * FROM tmp_".$_REQUEST['ListID']." WHERE ContID=".$_REQUEST['id']." $where ORDER BY $mod_order LIMIT $pos, $rec_per_page");
	$mod_total_rec = $o42->select_line("SELECT COUNT(tmpID) max FROM tmp_".$_REQUEST['ListID']." WHERE ContID=".$_REQUEST['id'].$where);

	?>
	
	<table class="table table-striped table-list">
  		<thead>
			<tr>
				<th></th>
				<th><a href="" title="Номер">№</a></th>
				<?
				for ( $i = 0; $i < count($mod_struct); $i++ )
				{
					if ( $mod_struct[$i]['Size'] > 5 )
					$mod_struct[$i]['Size'] = 2;

					if ( $list_prop['ListType'] == 0 || ( $list_prop['ListType'] == 1 && $mod_struct[$i]['Main'] == 1 ) )
					echo '<th>';
						echo "<a href=\"\" title=\"".$mod_struct[$i]['Descr']."\">";
							echo $mod_struct[$i]['Descr'];
							if ( $user_info['UType'] == 3 )
							echo "<br />".$mod_struct[$i]['Title'];
						echo '</a>';
					echo '</th>';
				}
				?>
				<th></th>
				<th></th>
			</tr>
		</thead>

		<thead  class="tab-content">
			<tr class="table-list-add tab-pane" id="table-list-add">
				<th></th>
				<th></th>
				<?
				echo "<form action=\"index.php\" method=\"POST\" enctype=\"multipart/form-data\">";
					echo "<input type=\"hidden\" name=\"p\" value=\"mods\">";
					echo "<input type=\"hidden\" name=\"id\" value=\"".$_REQUEST['id']."\">";
					echo "<input type=\"hidden\" name=\"ListID\" value=\"".$_REQUEST['ListID']."\">";
					echo "<input type=\"hidden\" name=\"action\" value=\"1\">";

					for ( $i = 0; $i < count($mod_struct); $i++ )
					{
						if ( $list_prop['ListType'] == 0 || ( $list_prop['ListType'] == 1 && $mod_struct[$i]['Main'] == 1 ) )
						{

							// Тип Фото
							if ( $mod_struct[$i]['LType'] == "13" || $mod_struct[$i]['LType'] == "2" )
							echo "<th class=\"type-file\"><input class=\"\" type=\"file\" name=\"".$mod_struct[$i]['Title']."\" /></th>";
							// Тип "Дата". Ставим текущую дату
							elseif ( $mod_struct[$i]['LType'] == "1" )
							echo "<th><input class=\"text\" type=\"text\" name=\"".$mod_struct[$i]['Title']."\" value=\"".date('Y-m-d H:i:s')."\"/></th>";
							// Тип "Ключ"
							elseif ( $mod_struct[$i]['LType'] == 3 )
							{
								echo "<th>";
									echo "<select name=\"".$mod_struct[$i]['Title']."\" class=\"input-".$mod_sizes[$mod_struct[$i]['Size']]."\">";
										echo "<option value=\"0\">Нет</option>";
										echo "<option value=\"1\">Да</option>";
									echo "</select>";
								echo "</th>";
							}
							// Тип поля "список"
							elseif ( $mod_struct[$i]['LType'] == 5 )
							{
								echo "<th>";
									echo "<select name=\"".$mod_struct[$i]['Title']."\" class=\"input-".$mod_sizes[$mod_struct[$i]['Size']]."\">";
										$mod_lselect = explode(";", $mod_struct[$i]['LSelect']);
										for ( $k = 0; $k < count($mod_lselect); $k++ )
										echo "<option value=\"".$mod_lselect[$k]."\">".$mod_lselect[$k]."</option>";
									echo "</select>";
								echo "</th>";
							}
							// Тип "Объект" (связанная таблица)
							elseif ( $mod_struct[$i]['LType'] == 6 )
							{
								$obj_doc[$mod_struct[$i]['ListFieldID']] = $o42->doc_read($mod_struct[$i]['LSelect']);
								$obj_field[$mod_struct[$i]['ListFieldID']] = $o42->select_line("SELECT Title FROM tListField WHERE ListFieldID=".$mod_struct[$i]['LField']." LIMIT 1");
									
								echo "<th>";
									echo "<select name=\"".$mod_struct[$i]['Title']."\">";
										$obj_data = $o42->select("SELECT tmpID, ".$obj_field[$mod_struct[$i]['ListFieldID']]['Title']." field_data FROM tmp_".$obj_doc[$mod_struct[$i]['ListFieldID']]['ListID']." WHERE ContID=".$obj_doc[$mod_struct[$i]['ListFieldID']]['ContID']);
										for ( $k = 0; $k < count($obj_data); $k++ )
										echo "<option value=\"".$obj_data[$k]['tmpID']."\">".$obj_data[$k]['field_data']."</option>";
									echo "</select>";
								echo "</th>";
							}
							elseif ( $mod_struct[$i]['LType'] == 4 )
							echo "<th><textarea class=\"input-".$mod_sizes[$mod_struct[$i]['Size']]."\" name=\"".$mod_struct[$i]['Title']."\" /></textarea></th>";
							else
							echo "<th><input class=\"input-".$mod_sizes[$mod_struct[$i]['Size']]."\" type=\"text\" name=\"".$mod_struct[$i]['Title']."\" /></th>";
						}
					}

					echo "<th colspan=\"2\"><button class=\"btn btn-small btn-success\" href=\"#\"><i class=\"icon-ok icon-white\"></i> добавить</button></th>";
				echo "</form>";
			echo "</tr>";

			$active = "";
			if ( !empty($where) )
			$active = " active";

			echo "<tr class=\"table-list-filter tab-pane$active\" id=\"table-list-filter\">";
				echo "<th></th>";
				echo "<th></th>";
				// ФИЛЬТР
				echo "<form action=\"index.php\" method=\"POST\" enctype=\"multipart/form-data\">";
					echo "<input type=\"hidden\" name=\"p\" value=\"mods\">";
					echo "<input type=\"hidden\" name=\"id\" value=\"".$_REQUEST['id']."\">";
					echo "<input type=\"hidden\" name=\"ListID\" value=\"".$_REQUEST['ListID']."\">";
					echo "<input type=\"hidden\" name=\"filter\" value=\"1\">";

					for ( $i = 0; $i < count($mod_struct); $i++ )
					{
						if ( $list_prop['ListType'] == 0 || ( $list_prop['ListType'] == 1 && $mod_struct[$i]['Main'] == 1 ) )
						{
							if ( !empty($_REQUEST[$mod_struct[$i]['Title']]) )
							$value = $_REQUEST[$mod_struct[$i]['Title']];
							else
							$value = "";

							// Тип "Ключ"
							if ( $mod_struct[$i]['LType'] == 3 )
							{
								echo "<th>";
									echo "<select name=\"".$mod_struct[$i]['Title']."\" class=\"input-".$mod_sizes[$mod_struct[$i]['Size']]."\">";
										if ( $value == 0 ) $selected = " selected"; else $selected = "";
										echo "<option value=\"0\"$selected>Нет</option>";
										if ( $value == 1 ) $selected = " selected"; else $selected = "";
										echo "<option value=\"1\"$selected>Да</option>";
									echo "</select>";
								echo "</th>";
							}
							// Тип "Объект" (связанная таблица)
							elseif ( $mod_struct[$i]['LType'] == 6 )
							{
								$obj_doc[$mod_struct[$i]['ListFieldID']] = $o42->doc_read($mod_struct[$i]['LSelect']);
								$obj_field[$mod_struct[$i]['ListFieldID']] = $o42->select_line("SELECT Title FROM tListField WHERE ListFieldID=".$mod_struct[$i]['LField']." LIMIT 1");
									
								echo "<th>";
									echo "<select name=\"".$mod_struct[$i]['Title']."\" class=\"input-".$mod_sizes[$mod_struct[$i]['Size']]."\">";
										$obj_data = $o42->select("SELECT tmpID, ".$obj_field[$mod_struct[$i]['ListFieldID']]['Title']." field_data FROM tmp_".$obj_doc[$mod_struct[$i]['ListFieldID']]['ListID']." WHERE ContID=".$obj_doc[$mod_struct[$i]['ListFieldID']]['ContID']);
										echo "<option value=\"\">---</option>";
										for ( $k = 0; $k < count($obj_data); $k++ )
										{
											if ( $value == $obj_data[$k]['tmpID'] ) $selected = " selected"; else $selected = "";
											echo "<option value=\"".$obj_data[$k]['tmpID']."\"$selected>".$obj_data[$k]['field_data']."</option>";
										}
									echo "</select>";
								echo "</th>";
							}
							else
							echo "<th><input class=\"input-".$mod_sizes[$mod_struct[$i]['Size']]."\" type=\"text\" name=\"".$mod_struct[$i]['Title']."\" value=\"$value\" /></th>";
						}
					}
				
					echo "<th colspan=\"2\"><button class=\"btn btn-small btn-primary\" href=\"#\"><i class=\"icon-search icon-white\"></i> найти</button></th>";
					//<th><button class="btn btn-small btn btn-warning" href="#"><i class="icon-remove icon-white"></i></button></th>
				echo "</form>";
			echo "</tr>";
			?>
		</thead>

		<tbody>
			<?
			for ( $i = 0; $i < count($mod_data); $i++ )
			{
				echo "<form action=\"index.php\" method=\"POST\" name=\"mod-".$mod_data[$i][0]."\" enctype=\"multipart/form-data\">";
					echo "<input type=\"hidden\" name=\"p\" value=\"mods\">";
					echo "<input type=\"hidden\" name=\"id\" value=\"".$_REQUEST['id']."\">";
					echo "<input type=\"hidden\" name=\"ListID\" value=\"".$_REQUEST['ListID']."\">";
					echo "<input type=\"hidden\" name=\"tmpID\" value=\"".$mod_data[$i][0]."\">";
					echo "<input type=\"hidden\" name=\"action\" value=\"1\">";
					echo "<input type=\"hidden\" name=\"fileremove\" value=\"\">";
					
					echo "<tr>";
						echo "<td><a href=\"index.php?p=mods_rec&id=".$_REQUEST['id']."&ListID=".$_REQUEST['ListID']."&rec=".$mod_data[$i][0]."\" class=\"btn btn-mini btn-primary\"><i class=\"icon-pencil icon-white\"></i></a></td>";

						echo "<td>".$mod_data[$i][0]."</td>";

						for ( $j = 2; $j < count($mod_data[$i]); $j++ )
						{
							if ( $list_prop['ListType'] == 0 || ( $list_prop['ListType'] == 1 && $mod_struct[$j-2]['Main'] == 1 ) )
							{

								echo "<td>";
									// Тип поля "Фото"
									if ( $mod_struct[$j-2]['LType'] == "13" || $mod_struct[$j-2]['LType'] == "2" )
									{
										echo "<input class=\"\" type=\"file\" name=\"".$mod_struct[$j-2]['Title']."\" />";
										if ( !empty($mod_data[$i][$j]) )
										{
											if ( $mod_struct[$j-2]['LType'] == "13" )
											echo "<a class=\"preview\" href=\"/sfiles/lists/".$mod_data[$i][$j]."\" my_href=\"/img.php?w=300&NoUpscale=1&n=/sfiles/lists/".$mod_data[$i][$j]."\" target=\"_blank\"><img src=\"/img.php?n=sfiles/lists/".$mod_data[$i][$j]."&w=65\"></a> ";
											echo "<button class=\"btn btn-mini\" onclick=\"document.forms['mod-".$mod_data[$i][0]."'].fileremove.value='".$mod_struct[$j-2]['ListFieldID']."';document.forms['mod-".$mod_data[$i][0]."'].submit()\" ><i class=\"icon-remove\"></i></button>";
										}
									}
									// Тип "Ключ"
									elseif ( $mod_struct[$j-2]['LType'] == 3 )
									{
										echo "<select name=\"".$mod_struct[$j-2]['Title']."\" class=\"input-".$mod_sizes[$mod_struct[$j-2]['Size']]."\">";
											if ( $mod_data[$i][$j] == 0 ) $selected = " selected"; else $selected = "";
											echo "<option value=\"0\"$selected>Нет</option>";
											if ( $mod_data[$i][$j] == 1 ) $selected = " selected"; else $selected = "";
											echo "<option value=\"1\"$selected>Да</option>";
										echo "</select>";
									}
									// Тип поля "список"
									elseif ( $mod_struct[$j-2]['LType'] == 5 )
									{
										echo "<select name=\"".$mod_struct[$j-2]['Title']."\" class=\"input-".$mod_sizes[$mod_struct[$j-2]['Size']]."\">";
											$mod_lselect = explode(";", $mod_struct[$j-2]['LSelect']);
											for ( $k = 0; $k < count($mod_lselect); $k++ )
											{
												if ( $mod_data[$i][$j] == $mod_lselect[$k] )
												$selected = " selected"; else $selected = "";
												echo "<option value=\"".$mod_lselect[$k]."\"$selected>".$mod_lselect[$k]."</option>";
											}
										echo "</select>";
									}
									// Тип поля "объект" (связанная таблица)
									elseif ( $mod_struct[$j-2]['LType'] == 6 )
									{
										echo "<select name=\"".$mod_struct[$j-2]['Title']."\">";
											$obj_data = $o42->select("SELECT tmpID, ".$obj_field[$mod_struct[$j-2]['ListFieldID']]['Title']." field_data FROM tmp_".$obj_doc[$mod_struct[$j-2]['ListFieldID']]['ListID']." WHERE ContID=".$obj_doc[$mod_struct[$j-2]['ListFieldID']]['ContID']);
											for ( $k = 0; $k < count($obj_data); $k++ )
											{
												if ( $mod_data[$i][$j] == $obj_data[$k]['tmpID'] )
												$selected = " selected"; else $selected = "";
												echo "<option value=\"".$obj_data[$k]['tmpID']."\"$selected>".$obj_data[$k]['field_data']."</option>";
											}
										echo "</select>";
									}
									elseif ( $mod_struct[$j-2]['LType'] == 4 )
									{
										echo "<textarea class=\"input-".$mod_sizes[$mod_struct[$j-2]['Size']]."\" name=\"".$mod_struct[$j-2]['Title']."\">".$mod_data[$i][$j]."</textarea>";
									}
									else
									echo "<input class=\"input-".$mod_sizes[$mod_struct[$j-2]['Size']]."\" name=\"".$mod_struct[$j-2]['Title']."\" value=\"".$mod_data[$i][$j]."\">";
								echo "</td>";
							}
						}
						
						if ( $list_prop['ListType'] == 0 )
						{
							echo "<td>";
								echo "<button class=\"btn btn-mini btn-success\" href=\"#\" onclick=\"document.forms['mod-".$mod_data[$i][0]."'].submit()\"><i class=\"icon-ok icon-white\"></i></button>";
								//echo '<button class="btn btn-mini btn-info" href="#"><i class="icon-arrow-up icon-white"></i></button>';
								//echo '<button class="btn btn-mini btn-info" href="#"><i class="icon-arrow-down icon-white"></i></button>';
							echo "</td>";
						}
						echo "<td><button class=\"btn btn-mini btn-danger\" href=\"#\" onclick=\"document.forms['mod-".$mod_data[$i][0]."'].action.value='2';document.forms['mod-".$mod_data[$i][0]."'].submit()\"><i class=\"icon-remove icon-white\"></i></button></td>";

					echo "</tr>";
				echo "</form>";
			}
			?>

		</tbody>
	</table>

	<?
	
	//$show_pages = 20;
	
	if ( $mod_total_rec['max'] > $rec_per_page )
	{
		if ( !isset($_REQUEST['page']) )
		$_REQUEST['page'] = 0;
		
		// Необходимо использовать вместо этого кода сессии!
		$url_par = "";
		if ( !empty($where) )
		{
			$url_par = "&filter=1";
			for ( $i = 0; $i < count($mod_struct); $i++ )
			{
				if ( !empty($_REQUEST[$mod_struct[$i]['Title']]) )
				$url_par .= "&".$mod_struct[$i]['Title']."=".urlencode($_REQUEST[$mod_struct[$i]['Title']]);
			}
		}
		
		echo "<div class=\"pagination\">";
			echo "<ul>";

				if ( $_REQUEST['page'] < ceil($total_pages / 2) )
				$start = 0;
				else
				$start = $_REQUEST['page'] - ceil($total_pages / 2) + 1;
				
				if ( $_REQUEST['page'] < ceil($mod_total_rec['max'] / $rec_per_page) - floor($total_pages / 2) && $start + $total_pages < ceil($mod_total_rec['max'] / $rec_per_page) )
				$end = $start + $total_pages;
				else
				{
					$end = ceil($mod_total_rec['max'] / $rec_per_page);
					if ( $end > $total_pages)
					$start = $end - $total_pages;
					else
					$start = 0;
				}

				if ( $_REQUEST['page'] >= ceil($total_pages / 2) && ceil($mod_total_rec['max'] / $rec_per_page) > $total_pages )
				{
					echo "<li><a href=\"index.php?p=mods&id=".$_REQUEST['id']."&ListID=".$_REQUEST['ListID']."&page=0$url_par\">««</a></li>";
					$mod_prev = $start - 1;
					echo "<li><a href=\"index.php?p=mods&id=".$_REQUEST['id']."&ListID=".$_REQUEST['ListID']."&page=$mod_prev$url_par\">«</a></li>";
				}
		
				for ( $i = $start; $i < $end; $i++ )
				{
					$mod_page = $i + 1;
					if ( $_REQUEST['page'] == $i ) $current = " class=\"active\""; else $current = "";
					echo "<li $current><a href=\"index.php?p=mods&id=".$_REQUEST['id']."&ListID=".$_REQUEST['ListID']."&page=$i$url_par\">$mod_page</a></li>";
				}

				if ( $_REQUEST['page'] < (ceil($mod_total_rec['max'] / $rec_per_page) - floor($total_pages / 2) ) - 1 && ceil($mod_total_rec['max'] / $rec_per_page) > $total_pages )
				{
					$mod_next = $end;
					echo "<li><a href=\"index.php?p=mods&id=".$_REQUEST['id']."&ListID=".$_REQUEST['ListID']."&page=$mod_next$url_par\">»</a></li>";
					$mod_next = ceil($mod_total_rec['max'] / $rec_per_page) - 1;
					echo "<li><a href=\"index.php?p=mods&id=".$_REQUEST['id']."&ListID=".$_REQUEST['ListID']."&page=$mod_next$url_par\">»»</a></li>";
				}
			echo "</ul>";
		echo "</div>";
    }
