<?
/*
* Copyright (c) 2013, Andrey_B (Office-42)
* Подробнее см. LICENSE.txt или http://www.gnu.org/licenses/
*/
?>
<script  language="JavaScript">
<!-- 
function AddLink(txtLink, idLink) 
{
	<?
	if ( !empty($_GET['cnt']) )
	{
		echo "window.top.opener.document.getElementById('link".$_GET['cnt']."').value = idLink ;";
		echo "window.top.opener.document.getElementById('LSelect".$_GET['cnt']."').value = txtLink ;";
	}
	else
	{
		echo "var dialog = window.opener.CKEDITOR.dialog.getCurrent();";
		echo "dialog.setValueOf('info','url',txtLink);";
		echo "dialog.setValueOf('info','protocol','');";
	}
        echo "window.close();";
	?>
} 
-->
</script>

<script type="text/javascript" src="libs/jquery.tree.min.js"></script>
<script type="text/javascript" src="libs/jquery.cookie.js"></script>
<script type="text/javascript" src="libs/jquery.tree.contextmenu.js"></script>
<script type="text/javascript" src="libs/jquery.tree.cookie.js"></script>

<?
include("libs/cadmin_tree_link.js");
include("temp/cadmin_msg.php");

function cadmin_tree($level = 0, $cat_sort = 0, $sort_order = 0)
{
	global $o42;

	//global $U_ID;
	//global $U_Type;
	//global $tConf;

	if ( $cat_sort == 1 )
	$order_by = "a.Title";
	elseif ( $cat_sort == 2 )
	$order_by = "a.Man_Date";
	else
	$order_by = "a.Priority";

	if ( $sort_order == 1 )
	$order_by .= " DESC ";

	$tree_res = "<ul>";
	$tree_rel = "";

		if ( empty($_GET['admin']) )
		$sql_where = " AND a.Disabled=0 AND a.Status=0 ";


		$result = $o42->select("SELECT a.Title, a.ContID, b.ParentID, a.Descr, a.Priority, a.Rewrite, a.CType, a.No_Rewrite, a.Title_Menu, a.Cat_Sort, a.Sort_Order, a.Mod_Rewrite
								FROM tCont a, tContExt b
								WHERE a.ContID=b.ContID AND b.ParentID=$level AND CType<>2 $sql_where
								ORDER BY $order_by");
		for ( $i = 0; $i < count($result); $i++ )
		{
			if ( $o42->conf['Mod_Rewrite_Link'] == 1 && $o42->conf['Mod_Rewrite'] == 1 )
			$my_link = $result[$i]['Mod_Rewrite'];
			else
			$my_link = "/page.php?ID=".$result[$i]['ContID'];
			
			if ( $result[$i]['CType'] == 1 )
			$tree_rel = " rel=\"doc\"";
			else
			$tree_rel = "";

			$tree_res .= "<li$tree_rel id=\"".$result[$i]['ContID']."\" my_link=\"$my_link\" my_id=\"".$result[$i]['ContID']."\">";
				$tree_res .= "<a href=\"#\"><ins>&nbsp;</ins>".$result[$i]['Title']."</a>";
				if ( $result[$i]['CType'] == 0 )
				$tree_res .= cadmin_tree($result[$i]['ContID'], $result[$i]['Cat_Sort'], $result[$i]['Sort_Order']);
			$tree_res .= "</li>";

		}

	$tree_res .= "</ul>";
	$tree_res = str_replace("<ul></ul>", "", $tree_res);
	return $tree_res;
}


// Заголовок
echo "<h1>".$cadmin_msg['form_tree_title']."</h1>";

echo "<div id=\"mid\">";
	echo "<div id=\"cadmin_tree\">";
		echo "<ul>";
			echo "<li id=\"tree_root\" rel=\"home\" class=\"open\"><ins>&nbsp;</ins>".$cadmin_msg['form_tree_root']."";
			echo cadmin_tree();
			echo "</li>";
		echo "</ul>";
	echo "</div>";
echo "</div>";

?>