<?
/*
* Copyright (c) 2013, Andrey_B (Office-42)
* ��������� ��. LICENSE.txt ��� http://www.gnu.org/licenses/
*/

require_once("class/main_class.php");
require_once("class/sub_class.php");
//try
{ $o42 = new cms_full; }
//catch (Exception $e)
//{ echo ": ".$e->getMessage(); }
$user_info = $o42->auth();

// �������������� ������� � HTML-��������
$dhtml_fields = array("Cont_Text", "Notes");
$o42->replace_quotes($_POST, $dhtml_fields);
$o42->replace_quotes($_GET, $dhtml_fields);
$o42->replace_quotes($_REQUEST, $dhtml_fields);

?>
<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="utf-8">
	<title><? echo $o42->cms_title; ?></title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link href="bootstrap/css/bootstrap.css" rel="stylesheet">
	<link href="f/backoffice42.css" rel="stylesheet">
	<link href="f/datepicker.css" rel="stylesheet">

	<!--[if lt IE 9]>
	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
</head>

<script src="libs/jquery-1.7.min.js"></script>

<body>

<?
if ( $user_info == -1 )
include("mods/mod_exit.php");
elseif ( isset($user_info['UType']) && $user_info['UType'] > 0 && $user_info['UType'] < 4 )
include("mods/mod_index.php");
else
include("mods/mod_auth_fault.php");
?>

<script src="bootstrap/js/bootstrap.js"></script>
<script src="libs/apps.js"></script>
<script src="libs/bootstrap-datepicker.js"></script>
<!--script src="libs/bootstrap-filestyle.min.js"></script>
<scripT>
   $(document).ready(function() {
   $(":file").filestyle({ 
   icon: true,
   textField:false,
   classIcon: "icon-file",
   buttonText:"",
   classButton:"btn-mini"
      })
   });
</script-->

</body>
</html>
