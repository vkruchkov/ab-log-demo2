<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>404</title>
<style type="text/css">
<!--
body {font-family: Tahoma, Verdana, Arial, Helvetica, sans-serif; color: #333; padding: 20px; font-size: 80%;}
a {color: #009;}
h1 {font-weight: lighter; border-bottom: 1px solid #000; font-size: 200%; letter-spacing: -1px; display: block; width: 650px;}
h2 {font-size: 150%; font-weight: lighter; letter-spacing: -1px;}
ul {list-style-type: none; font-size: 100%; font-weight: bold; text-transform: uppercase;}
ul ul {font-size: 95%; font-weight: normal; list-style-type: circle; text-transform: none;}
-->
</style>
</head>

<body>
<h1><strong>404</strong> - страница не найдена</h1>
<p><strong>[!]</strong> Данной страницы на сайте скорее всего уже не существует. Возможно она была  перемещена или удалена.</p>
<p>Вы можете просмотреть сайт с <a href="/">начальной страницы</a>.</p>
</body>
</html>