<?
/*
* Copyright (c) 2016, Andrey_B
* http://ab-log.ru
* Подробнее см. LICENSE.txt или http://www.gnu.org/licenses/
*/

/*
Скрипт для работы с датчиком освещенности BH1750
Использует драйвер BH1750 и библиотеку I2C-PHP
*/

define("SCL", "30");
define("SDA", "31");
define("MD", "http://192.168.0.14/sec/?");

// Вариант реализации I2C: 1 - полностью программный; 2 - частично аппаратный (прошивка 3.43beta1 и выше)
//define("V", "1");

require_once("mod_i2c_bh1750.php");

$lux = get_lux();

echo "Lux: $lux\n";

//require_once("ab-cms/class/main_class.php");
//$ab = new cms_lib();

//$result = get_pressure();
//$ab->mod_write(105, "SYSDATE(), '$hum_compensated'");
//$ab->mod_write(106, "SYSDATE(), '$temperature'");
//$ds18b20_temp = file_get_contents("http://192.168.0.14/sec/?pt=10&cmd=get");
//$ab->mod_write(107, "SYSDATE(), '$ds18b20_temp'");

?>
