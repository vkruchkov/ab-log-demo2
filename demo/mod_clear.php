<?
/*
* Copyright (c) 2013, Andrey_B
* http://ab-log.ru
* Подробнее см. LICENSE.txt или http://www.gnu.org/licenses/
*/

# Очистка базы данных
require_once("ab-cms/class/main_class.php");
$ab = new cms_lib();

# Очистка таблицы с данными по потреблению электроэнергии
$ab->execute("DELETE FROM tmp_10 WHERE el_date < SYSDATE() - INTERVAL 10 day");
# Очистка таблицы с данными температура в доме
$ab->execute("DELETE FROM tmp_2 WHERE ContID=6 AND dev_date < SYSDATE() - INTERVAL 15 day");
# Очистка таблицы с данными температура в доме (2 этаж)
$ab->execute("DELETE FROM tmp_2 WHERE ContID=54 AND dev_date < SYSDATE() - INTERVAL 15 day");
# Очистка таблицы с данными температура в котельной
$ab->execute("DELETE FROM tmp_2 WHERE ContID=61 AND dev_date < SYSDATE() - INTERVAL 15 day");
# Очистка таблицы с данными температура на улице-2
$ab->execute("DELETE FROM tmp_2 WHERE ContID=60 AND dev_date < SYSDATE() - INTERVAL 15 day");
# Очистка таблицы с данными температура в подвале
$ab->execute("DELETE FROM tmp_2 WHERE ContID=21 AND dev_date < SYSDATE() - INTERVAL 15 day");
# Очистка таблицы с данными температура на чердаке
$ab->execute("DELETE FROM tmp_2 WHERE ContID=22 AND dev_date < SYSDATE() - INTERVAL 15 day");
# Очистка таблицы с данными температура подачи отопление
$ab->execute("DELETE FROM tmp_2 WHERE ContID=40 AND dev_date < SYSDATE() - INTERVAL 3 day");
# Очистка таблицы с данными температура подачи основного контура
$ab->execute("DELETE FROM tmp_2 WHERE ContID=41 AND dev_date < SYSDATE() - INTERVAL 15 day");
# Очистка таблицы с данными температура подачи контура ТП
$ab->execute("DELETE FROM tmp_2 WHERE ContID=55 AND dev_date < SYSDATE() - INTERVAL 3 day");
# Очистка таблицы с данными об освещенности
$ab->execute("DELETE FROM tmp_2 WHERE ContID=84 AND dev_date < SYSDATE() - INTERVAL 1 YEAR");
# Очистка таблицы с данными влажности в котельной
$ab->execute("DELETE FROM tmp_2 WHERE ContID=64 AND dev_date < SYSDATE() - INTERVAL 15 day");
# Очистка таблицы с журналом работы ключей
$ab->execute("DELETE FROM tmp_8 WHERE ContID=24 AND key_j_date < SYSDATE() - INTERVAL 3 MONTH");

?>
